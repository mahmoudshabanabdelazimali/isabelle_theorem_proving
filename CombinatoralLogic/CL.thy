theory CL
imports Main "~~/src/HOL/IMP/Star" 
begin

(*Combinatory Logic - Systems of combinators which designed to do the same work as systems of \<lambda>-calculus,
 but without using bound variables.In fact, the annoying technical complications involved in
 substitution and \<alpha>-conversion will be avoided completely.
*)
type_synonym vname = string

(*Combinatory logic terms*)
datatype   CLexp = I | K | S | (*Basic Combinators*)
                     V vname |
                     App CLexp CLexp ("(_ \<leftarrow> _)")

(*Since I, K and S are the only atomic constants, the system will be called pure*)          
inductive atomic_cons :: " CLexp \<Rightarrow> bool" where
"atomic_cons I "|
"atomic_cons K"|
"atomic_cons S"

inductive_cases atomic_consE[elim!]: "atomic_cons  m "

(*An atom is a variable or atomic constant*)
inductive atom :: " CLexp \<Rightarrow> bool" where
"atomic_cons m \<Longrightarrow> atom m "|
"atom (V x)"

inductive_cases atomE[elim!]: "atom m "

fun vars :: "CLexp \<Rightarrow> vname set" where
"vars I = {}" |
"vars K = {}" |
"vars S = {}" |
"vars (V x) = {x}"|
"vars (U \<leftarrow> W) = (vars U) \<union> (vars W)"

(*A closed term (in our pure system it is the same as a combinator) is a term containing no variables.*)
inductive closed_term :: "CLexp \<Rightarrow> bool" where
"closed_term I" |
"closed_term K" |
"closed_term S" |
"(vars m) = {} \<Longrightarrow>  closed_term m "

inductive_cases closed_termE[elim!]: "closed_term m "

(*The length of X (or lgh(X)) is the number of occurrences of atoms in X*)
fun lgh :: "CLexp \<Rightarrow> nat " where
"lgh I = 1"|
"lgh K = 1"|
"lgh S = 1"|
"lgh (V v) = 1"|
"lgh (U \<leftarrow> W) = (lgh U) + (lgh W)"

value "lgh (( ((V x) \<leftarrow> S) \<leftarrow> K) \<leftarrow> (V x))" 

(*X is a subterm of Y*)
inductive subterm :: "CLexp \<Rightarrow> CLexp \<Rightarrow> bool" where
subterm0:"subterm X X " |
subterm1:"subterm X U \<Longrightarrow> subterm X (U \<leftarrow> W)"|
subterm2:"subterm X W \<Longrightarrow> subterm X (U \<leftarrow> W)" 

inductive_cases subtermE[elim!]: "subterm X Y"

lemma "subterm (K \<leftarrow> (V ''x'')) (K  \<leftarrow> I) = False"
by auto

lemma "subterm (K \<leftarrow> K) (K  \<leftarrow> I) = False"
by auto

lemma "subterm K  (((V ''x '') \<leftarrow> K)  \<leftarrow> I)"
by(auto intro: subterm.intros)


(*Computing the set of free variables in a CL-term*)
fun FV :: "CLexp \<Rightarrow> vname set" where
"FV I = {}"| 
"FV K = {}"|
"FV S = {}"| 
"FV (V y) = {y}"|
"FV (App M N) =  (FV M) Un (FV N)" 


(*substituting N for the free occurences of x in M,
 notation M[x :== N], is defined as follows.*)
fun subst :: " CLexp \<Rightarrow> vname \<Rightarrow> CLexp \<Rightarrow> CLexp" ("(_ [_ :== _])" [0,0] 50) where 
"(V y)[x:== N]  = (if x = y then N else (V y))" |
"(App M\<^sub>1 M\<^sub>2)[x:== N] = App (M\<^sub>1[x:== N])(M\<^sub>2[x:==N])"|
"(I [_ :== _]) = I"|
"(K [_ :== _]) = K"|
"(S [_ :== _]) = S"

lemma V_subst1: "x = y \<Longrightarrow> (V y)[x:== N] = N" by simp

lemma V_subst2: "x \<noteq> y \<Longrightarrow> (V y)[x:== N] = V y" by simp

lemma UI_subst: " x \<notin> FV(U) \<Longrightarrow> ((U \<leftarrow> I)[x:== N]) = (U \<leftarrow> I) " 
proof(induction U) 
 case (App V W) 
 hence 1:"((V \<leftarrow> I) [x :== N]) = (V \<leftarrow> I)"  by simp
 from App have 2:"(W \<leftarrow> I) [x :== N] = (W \<leftarrow> I)" by simp
 thus ?case using "1" CLexp.distinct(13) CLexp.distinct(17) CLexp.distinct(19) CLexp.distinct(7) CLexp.inject(2) subst.elims by auto
qed auto

lemma UK_subst :" x \<notin> FV(U) \<Longrightarrow> ((U \<leftarrow> K)[x:== N]) = (U \<leftarrow> K) "
proof(induction U) 
 case (App V W) 
 hence 1:"((V \<leftarrow> K) [x :== N]) = (V \<leftarrow> K)"  by simp
 from App have 2:"(W \<leftarrow> K) [x :== N] = (W \<leftarrow> K)" by simp
 thus ?case using "1" CLexp.distinct(13) CLexp.distinct(17) CLexp.distinct(19) CLexp.distinct(7) CLexp.inject(2) subst.elims by auto
qed auto


lemma US_subst :" x \<notin> FV(U) \<Longrightarrow> ((U \<leftarrow> S)[x:== N]) = (U \<leftarrow> S) "
proof(induction U) 
 case (App V W) 
 hence 1:"((V \<leftarrow> S) [x :== N]) = (V \<leftarrow> S)"  by simp
 from App have 2:"(W \<leftarrow> S) [x :== N] = (W \<leftarrow> S)" by simp
 thus ?case using "1" CLexp.distinct(13) CLexp.distinct(17) CLexp.distinct(19) CLexp.distinct(7) CLexp.inject(2) subst.elims by auto
qed auto


lemma UZ_subst1 :" x \<notin> FV(U) \<Longrightarrow> x \<notin> FV(Z) \<Longrightarrow> ((U \<leftarrow> Z)[x:== N]) = (U \<leftarrow> Z) "
proof(induction U) 
 case (App V W) 
 hence 1:"((V \<leftarrow> Z) [x :== N]) = (V \<leftarrow> Z)"  by simp
 from App have 2:"(W \<leftarrow> Z) [x :== N] = (W \<leftarrow> Z)" by simp
 thus ?case using "1" CLexp.distinct(13) CLexp.distinct(17) CLexp.distinct(19) CLexp.distinct(7) CLexp.inject(2) subst.elims by auto
next
  case I thus ?case using CLexp.inject(2) UK_subst subst.simps(2) subst.simps(4) by auto
next
  case K thus ?case using CLexp.inject(2) UK_subst subst.simps(2) by auto 
next
  case S thus ?case using CLexp.inject(2) UK_subst subst.simps(2) subst.simps(4) by auto
next
  case V thus ?case using CLexp.inject(2) UK_subst subst.simps(2) by auto 
qed 



lemma UZ_subst2 :" x \<notin> FV(U) \<Longrightarrow> x \<in> FV(Z) \<Longrightarrow> ((U \<leftarrow> Z)[x:== N]) = (U \<leftarrow> (Z[x:== N]))"
using UZ_subst1 by auto
 
lemma UZ_subst3 :" x \<in> FV(U) \<Longrightarrow> x \<notin> FV(Z) \<Longrightarrow> ((U \<leftarrow> Z)[x:== N]) = ((U[x:== N]) \<leftarrow> Z)"
using UZ_subst1 by auto

value " (((V ''y'') \<leftarrow> (V ''x'')) \<leftarrow> (V ''x''))[''x'' :== (S \<leftarrow> K)]"

(*Any term IX, KXY or SXYZ is called a (weak) redex*)
inductive weak_redex::"CLexp \<Rightarrow> bool" where
"weak_redex (I \<leftarrow> X)" |
"weak_redex ((K \<leftarrow> X) \<leftarrow> Y)" |
"weak_redex (((S \<leftarrow> X) \<leftarrow> Y) \<leftarrow> Z)" 

inductive_cases wrE[elim!]:"weak_redex M"

(*A weak normal form is a term that contains no weak redexes.*)
inductive weak_nf :: "CLexp => bool" where
"weak_nf I" |
"weak_nf K" |
"weak_nf S" |
"weak_nf (V x)"|
"weak_nf U \<Longrightarrow> weak_nf W \<Longrightarrow> \<not> (weak_redex(U \<leftarrow> W)) \<Longrightarrow> weak_nf (U \<leftarrow> W)"
 
inductive_cases weak_nfE[elim!]:"weak_nf m "

(*Abstraction in Combinatory Logic*)
fun abs :: " vname \<Rightarrow> CLexp \<Rightarrow> CLexp" ("\<lambda>*_._")where
"\<lambda>* x . (V a) = (if x = a then I else (K \<leftarrow> (V a)))"|
"\<lambda>* x . K = (K\<leftarrow>K)" |
"\<lambda>* x . I = (K\<leftarrow>I)"|
"\<lambda>* x . S = (K\<leftarrow>S)"|
"\<lambda>* x .  (U \<leftarrow> (V a)) =
(if x = a & x \<notin> FV(U) then U 
else
  (if x = a & x \<in> FV(U) then
    ((S \<leftarrow> (\<lambda>* x. U)) \<leftarrow> (\<lambda>* x.(V a)))  
  else 
    (if x \<noteq> a & x \<notin> FV(U) then K \<leftarrow> (U \<leftarrow> (V a)) 
    else   ((S \<leftarrow> (\<lambda>* x. U)) \<leftarrow> (\<lambda>* x.(V a))) 
  ) 
   )
) 
" |
"\<lambda>* x . (N \<leftarrow> M) =  (if x \<notin> FV(N \<leftarrow> M) then (K \<leftarrow> (N \<leftarrow> M)) else ((S \<leftarrow> (\<lambda>* x. N)) \<leftarrow> (\<lambda>* x. M)) )"

lemma abs_Ua_eq_x1: "x = a & x \<notin> FV(U) \<Longrightarrow> (\<lambda>* x .  (U \<leftarrow> (V a))) = U "
by auto

lemma abs_Ua_eq_x2: "x = a & x \<in> FV(U) \<Longrightarrow> (\<lambda>* x .  (U \<leftarrow> (V a))) =  ((S \<leftarrow> (\<lambda>* x. U)) \<leftarrow> (\<lambda>* x.(V a)))  "
by auto

lemma abs_Ua_diff_x1:"x \<noteq> a & x \<notin> FV(U) \<Longrightarrow> (\<lambda>* x .  (U \<leftarrow> (V a))) =  (K \<leftarrow> (U \<leftarrow> (V a)))"
by auto

lemma abs_Ua_diff_x2:"x \<noteq> a & x \<in> FV(U) \<Longrightarrow> (\<lambda>* x .  (U \<leftarrow> (V a))) =   ((S \<leftarrow> (\<lambda>* x. U)) \<leftarrow> (\<lambda>* x.(V a)))  "
by auto

lemma abs_UI:" x \<in> FV(N \<leftarrow> I)  \<Longrightarrow> (\<lambda>* x . (N \<leftarrow> I)) =  ((S \<leftarrow> (\<lambda>* x. N)) \<leftarrow> (\<lambda>* x. I))   "
by simp

lemma abs_UK:" x \<in> FV(N \<leftarrow> K)  \<Longrightarrow> (\<lambda>* x . (N \<leftarrow> K)) =  ((S \<leftarrow> (\<lambda>* x. N)) \<leftarrow> (\<lambda>* x. K))   "
by simp

lemma abs_US:" x \<in> FV(N \<leftarrow> S)  \<Longrightarrow> (\<lambda>* x . (N \<leftarrow> S)) =  ((S \<leftarrow> (\<lambda>* x. N)) \<leftarrow> (\<lambda>* x. S))   "
by simp

lemma abs_UV1:" x \<notin> FV(N \<leftarrow> M) & (\<forall> b. M \<noteq> (V b))  \<Longrightarrow> (\<lambda>* x . (N \<leftarrow> M)) = (K \<leftarrow> (N \<leftarrow> M))   "
by (metis FV.elims abs.simps(6) abs.simps(7) abs.simps(8) abs.simps(9))

lemma abs_UV2:" x \<in> FV(N \<leftarrow> M) & (\<forall> b. M \<noteq> (V b)) & M \<noteq> I & M = K & M \<noteq> S \<Longrightarrow> (\<lambda>* x . (N \<leftarrow> M)) =  ((S \<leftarrow> (\<lambda>* x. N)) \<leftarrow> (\<lambda>* x. M))   "
by auto

(*If x in not free in \<lambda>* x . U and \<lambda>* x . W then x is not free in (\<lambda>* x . (U \<leftarrow> W) ) *)
lemma x_notin_abs_app:
assumes "x \<notin> FV (\<lambda>* x . U)" and "x \<notin> FV (\<lambda>* x . W)"
shows "x \<notin> FV (\<lambda>* x . (U \<leftarrow> W))"
proof(cases W)
  case (App Z Y) thus ?thesis
  proof(cases "x \<in> FV(U)")
      case t1:True thus ?thesis
      proof(cases "x : FV(Z \<leftarrow>Y)")
        case t2:True 
        from t2 t1 App have 0:"(\<lambda>* x . (U \<leftarrow> W)) = ((S \<leftarrow> (\<lambda>* x . U)) \<leftarrow> (\<lambda>* x . W))" by auto
        have 1:"x \<notin> FV(S)" by auto 
        from assms 1 have "x \<notin>  FV ((S \<leftarrow> (\<lambda>* x . U)) \<leftarrow> (\<lambda>* x . W))" by auto
        thus ?thesis using 0 App by auto
      next
        case f2:False
        from f2 t1 App have 0:"(\<lambda>* x . (U \<leftarrow> W)) = ((S \<leftarrow> (\<lambda>* x . U)) \<leftarrow> (\<lambda>* x . W))" by auto
        have 1:"x \<notin> FV(S)" by auto 
        from assms 1 have "x \<notin>  FV ((S \<leftarrow> (\<lambda>* x . U)) \<leftarrow> (\<lambda>* x . W))" by auto
        thus ?thesis using 0 App by auto
      qed
      next
      case f:False thus ?thesis
       proof(cases "x : FV(Z \<leftarrow>Y)")
        case t2:True 
        from t2 f App have 0:"(\<lambda>* x . (U \<leftarrow> W)) = ((S \<leftarrow> (\<lambda>* x . U)) \<leftarrow> (\<lambda>* x . W))" by auto
        have 1:"x \<notin> FV(S)" by auto 
        from assms 1 have "x \<notin>  FV ((S \<leftarrow> (\<lambda>* x . U)) \<leftarrow> (\<lambda>* x . W))" by auto
        thus ?thesis using 0 App by auto
      next
        case f2:False
        from f2 f App have 0:"(\<lambda>* x . (U \<leftarrow> W)) = (K \<leftarrow>  (U \<leftarrow> W))" by auto
        have 1:"x \<notin> FV(S)" by auto 
        from assms 1 have "x \<notin>  FV ((S \<leftarrow> (\<lambda>* x . U)) \<leftarrow> (\<lambda>* x . W))" by auto
        thus ?thesis using 0 App by auto
      qed
  qed
next   
  case (V y) thus ?thesis using assms by (cases "y=x") auto
qed (auto simp add:assms)

(*x is not free in \<lambda>* x .M*)
lemma x_notin_abs: "x \<notin> FV (\<lambda>* x .M)"
proof(induction M)
case (App U Z) thus ?case
proof(cases Z)
  case (App W Y) thus ?thesis
  proof(cases "x : FV(W \<leftarrow> Y) ")
    case t:True thus ?thesis using `Z = (W \<leftarrow> Y)` using App.IH(1) App.IH(2) x_notin_abs_app by blast
  next
    case f:False thus ?thesis using App.IH(1) App.IH(2) x_notin_abs_app by blast
  qed
  next
  case I thus ?thesis using App.IH(1) App.IH(2) x_notin_abs_app by blast 
  next
  case K thus ?thesis  using App.IH(1) App.IH(2) x_notin_abs_app by blast
  next
   case S  thus ?thesis using App.IH(1) App.IH(2) x_notin_abs_app by blast 
  next
   case V thus ?thesis   using App.IH(1) App.IH(2) x_notin_abs_app by blast 
qed
qed auto

lemma Abs_FV:"x \<in> FV (M) \<Longrightarrow> FV(\<lambda>* x . M) = FV (M) - {x}"
proof(induction M)
case (App U W) 
hence "x \<in> FV U \<or> x \<in> FV W" by auto
thus ?case 
  proof
    assume u:"x \<in> FV U"
    show ?thesis 
    proof(cases "x \<in> FV(W)")
      case w:True
      from u App have 1:"FV (\<lambda>* x . U) = FV(U) - {x}" by blast
      from w App have 2:"FV (\<lambda>* x . W) = FV(W) - {x}" by blast
      from w u have 3:"(\<lambda>* x . (U \<leftarrow> W)) = ((S \<leftarrow> (\<lambda>* x. U)) \<leftarrow> (\<lambda>* x. W))" by(cases "W")  simp_all
      from 1 2 3 w u App show ?thesis by auto
   next
      case nw:False
      from u App have 1:"FV (\<lambda>* x . U) = FV(U) - {x}" by blast
      from nw abs_UV1  have 2:"FV (\<lambda>* x . W) = FV(W) - {x}" by(cases "W";simp_all ; fastforce)
      from u have 3:"(\<lambda>* x . (U \<leftarrow> W)) = ((S \<leftarrow> (\<lambda>* x. U)) \<leftarrow> (\<lambda>* x. W))" by(cases "W")  simp_all
      from 1 2 3 show ?thesis by auto
    qed 
  next
    assume w:"x \<in> FV W"
    show ?thesis 
    proof(cases "x \<in> FV(U)")
      case u:True
      from u App have 1:"FV (\<lambda>* x . U) = FV(U) - {x}" by blast
      from w App have 2:"FV (\<lambda>* x . W) = FV(W) - {x}" by blast
      from w u have 3:"(\<lambda>* x . (U \<leftarrow> W)) = ((S \<leftarrow> (\<lambda>* x. U)) \<leftarrow> (\<lambda>* x. W))" by(cases "W")  simp_all
      from 1 2 3 w u App show ?thesis by auto
   next
      case nu:False
      from w App have 1:"FV (\<lambda>* x . W) = FV(W) - {x}" by blast
      from nu abs_UV1  have 2:"FV (\<lambda>* x . U) = FV(U) - {x}" by(cases "U";simp_all ; fastforce)
      show ?thesis 
      proof(cases "W")
         case (V a)
         show ?thesis 
         proof(cases "a=x")
           case ta: True
           from nu w ta V have 3:"(\<lambda>* x . (U \<leftarrow> W)) = U " by auto
           from 3  nu ta V  show ?thesis by simp
        next
          case nta:False 
          from nu w nta V have 3:"(\<lambda>* x . (U \<leftarrow> W)) = (K \<leftarrow> (U \<leftarrow> W)) " by auto
          from  3  nu nta V  show ?thesis by simp
        qed
        next
          case I
          from I nu w have 3:"(\<lambda>* x . (U \<leftarrow> W)) = ((S \<leftarrow> (\<lambda>* x. U)) \<leftarrow> (\<lambda>* x. W))" by  simp_all
          from 1 2 3 show ?thesis by auto
        next
          case K 
          from K nu w have 3:"(\<lambda>* x . (U \<leftarrow> W)) = ((S \<leftarrow> (\<lambda>* x. U)) \<leftarrow> (\<lambda>* x. W))" by  simp_all
          from K 2 3 show ?thesis by auto
         next
          case S
          from S nu w have 3:"(\<lambda>* x . (U \<leftarrow> W)) = ((S \<leftarrow> (\<lambda>* x. U)) \<leftarrow> (\<lambda>* x. W))" by  simp_all
          from 1 2 3 show ?thesis by auto
         next
          case App
          from App nu w have 3:"(\<lambda>* x . (U \<leftarrow> W)) = ((S \<leftarrow> (\<lambda>* x. U)) \<leftarrow> (\<lambda>* x. W))" by  simp_all
          from 1 2 3 show ?thesis by auto
      qed 
    qed 
  qed
qed auto

value "\<lambda>* ''x'' . (V ''x'')" 
value "\<lambda>* ''x'' . (V ''s'')" 
value "\<lambda>* ''x'' . K" 
value "\<lambda>* ''x'' . S"
value "\<lambda>* ''x'' . I" 
value "\<lambda>* ''x'' .  ((V ''x'') \<leftarrow>  (V ''x''))" 
value "\<lambda>* ''x'' .  ((V ''y'') \<leftarrow>  (V ''x''))"
value "\<lambda>* ''x'' .  ((V ''y'') \<leftarrow>  (V ''y''))"
value "\<lambda>* ''x'' .  ((V ''x'') \<leftarrow>  (V ''y''))"
value "\<lambda>* ''x'' .  ((V ''y'') \<leftarrow>  ((V ''y'') \<leftarrow>  (V ''y'')))"
value "(\<lambda>* ''x'' . ((V ''u'') \<leftarrow> ((V ''v'') \<leftarrow>  (V ''x''))))"
value "\<lambda>* ''x'' . (\<lambda>* ''y'' . (\<lambda>* ''z'' . (((V ''x'') \<leftarrow> (V ''z'')) \<leftarrow> ((V ''y'') \<leftarrow> (V ''z'')))))"

(*Weak reduction one step - Contracting an occurrence of a weak redex in a CL-term*)
inductive weak_reduc_step :: "CLexp \<Rightarrow> CLexp \<Rightarrow> bool"  (infix "\<rightarrow>" 55)
where
I:"(I \<leftarrow> X) \<rightarrow> X"|
K: "((K \<leftarrow> X) \<leftarrow> Y) \<rightarrow> X"|
S: "(((S \<leftarrow> X) \<leftarrow> Y) \<leftarrow> Z) \<rightarrow> ((X \<leftarrow> Z) \<leftarrow> (Y \<leftarrow> Z ))"|
app1:"e\<^sub>1 \<rightarrow> e'\<^sub>1 \<Longrightarrow> (App e\<^sub>1 e\<^sub>2) \<rightarrow> (App e'\<^sub>1 e\<^sub>2) " |
app2:"e\<^sub>2 \<rightarrow> e'\<^sub>2 \<Longrightarrow> (App e\<^sub>1 e\<^sub>2) \<rightarrow> (App e\<^sub>1 e'\<^sub>2) " 

inductive_cases weak_reduc_stepE[elim!]: "weak_reduc_step X Y"

abbreviation
  weak_reduc_steps :: "CLexp \<Rightarrow> CLexp \<Rightarrow> bool" (infix "\<rightarrow>*" 55)
where "x \<rightarrow>* y == star weak_reduc_step x y"

code_pred weak_reduc_step .

lemma reduc_FV: "X \<rightarrow> Y \<Longrightarrow> FV (Y) \<subseteq> FV(X)"
by(induction rule: weak_reduc_step.induct)(auto simp add: subset_iff)+

lemma reduc_steps_FV:"X \<rightarrow>* Y \<Longrightarrow> FV (Y) \<subseteq> FV(X)"
apply(induction rule: star.induct)
apply simp
by (meson dual_order.trans reduc_FV)

lemma app1_steps:"X \<rightarrow> Y \<Longrightarrow> (X \<leftarrow> Z) \<rightarrow>* (Y \<leftarrow> Z)"
using app1 by auto

lemma app2_steps:" Z \<rightarrow> W \<Longrightarrow> (X \<leftarrow> Z) \<rightarrow>* (X \<leftarrow> W)"
using app2 by auto

lemma app3_steps: "X \<rightarrow> Y \<Longrightarrow> Z \<rightarrow> W \<Longrightarrow> (X \<leftarrow> Z) \<rightarrow>* (Y \<leftarrow> W)"
using app1_steps app2_steps by (meson star_trans)

lemma app4_steps:"Z \<rightarrow>* W \<Longrightarrow> (X \<leftarrow> Z) \<rightarrow>* (X \<leftarrow> W)"
apply(induction rule:star.induct)
apply simp
by (meson app2 star.simps)

lemma app_steps:"X \<rightarrow>* Y \<Longrightarrow> Z \<rightarrow>* W \<Longrightarrow> (X \<leftarrow> Z) \<rightarrow>* (Y \<leftarrow> W)"
apply(induction rule:star.induct)
using app4_steps apply blast
by (meson app1 star.simps)

lemma "(((S \<leftarrow> K) \<leftarrow> K) \<leftarrow> X) \<rightarrow>* X "
by (meson K S star.simps)

(*A property that abs has *)
theorem abs_reduction_steps:"((\<lambda>* x . M)  \<leftarrow> N) \<rightarrow>* (M[x:== N])"
proof(induction  M)
case I 
  have 1:"(\<lambda>* x . I) = (K \<leftarrow> I)" by auto
  have 2:"((K \<leftarrow> I) \<leftarrow> N) \<rightarrow> I" by (rule K) 
  thus ?case using 1 by simp
next
case K
  have 1:"(\<lambda>* x . K) = (K \<leftarrow> K)" by auto
  have 2:"((K \<leftarrow> K) \<leftarrow> N) \<rightarrow> K" by (rule K) 
  thus ?case using 1 by simp
next
case S
  have 1:"(\<lambda>* x . S) = (K \<leftarrow> S)" by auto
  have 2:"((K \<leftarrow> S) \<leftarrow> N) \<rightarrow> S" by (rule K) 
  thus ?case using 1 by simp 
next
case (V y) thus ?case
  proof(cases "x=y")
    case t:True
    from t have 1:"(\<lambda>* x . (V y)) = I" by auto
    have 2:"(I \<leftarrow> N) \<rightarrow> N" by rule 
    have 3:"(V y)[x:==N] = N" using t by(simp add:V_subst1)
    thus ?thesis using 1 2 3 by simp
  next
    case f:False
    hence 1:"(\<lambda>* x . (V y)) = (K \<leftarrow> (V y))" by auto
    have 2:"((K \<leftarrow> (V y)) \<leftarrow> N) \<rightarrow> (V y)" by (rule K) 
    from 1 2 f show ?thesis by simp
  qed
next
  case (App U W)
  thus ?case 
  proof(cases "W")
    case I 
    show ?thesis 
    proof(cases "x \<notin> FV(U)")
      case t:True
      from t  have 1:"(\<lambda>* x . (U \<leftarrow> I)) = (K \<leftarrow> ( U \<leftarrow> I)) " by auto
      have 2:"((K \<leftarrow> ( U \<leftarrow> I)) \<leftarrow> N)  \<rightarrow> ( U \<leftarrow> I) " by rule
      from UI_subst[OF t] have 3:"(U \<leftarrow> I)[x:== N] = (U \<leftarrow> I)" by auto 
      have "((K \<leftarrow> ( U \<leftarrow> I)) \<leftarrow> N) \<rightarrow> ((U \<leftarrow> I)[x:== N])" using 2 3 by simp
      thus ?thesis using 1 2 3 t I by simp
    next
      case f:False
      let ?V = "( \<lambda>* x . U)"
      let ?U = "(K \<leftarrow> I)"
      from f  have 1:"(\<lambda>* x . (U \<leftarrow> I)) = ((S \<leftarrow> ?V) \<leftarrow> ?U)  " by auto
      have 2: "(((S \<leftarrow> ?V) \<leftarrow> ?U) \<leftarrow> N)  \<rightarrow> ((?V \<leftarrow> N) \<leftarrow> (?U \<leftarrow> N))" by rule
      have 3: " (?V \<leftarrow> N) \<rightarrow>* (U [x :== N])" using App by auto
      from f I have 4: " (?U \<leftarrow> N) \<rightarrow>* (W [x :== N])" using App by auto
      from  3 4 app_steps have "((?V \<leftarrow> N) \<leftarrow> (?U \<leftarrow> N)) \<rightarrow>* ((U [x :== N]) \<leftarrow> (W [x :== N]))" by auto 
      thus ?thesis using 1 2 3 4 f I by (simp add: star.step)
    qed
    next
    case K 
    thus ?thesis
      proof(cases "x \<notin> FV(U)")
      case t:True
      from t  have 1:"(\<lambda>* x . (U \<leftarrow> K)) = (K \<leftarrow> ( U \<leftarrow> K)) " by auto
      have 2:"((K \<leftarrow> ( U \<leftarrow> K)) \<leftarrow> N)  \<rightarrow> ( U \<leftarrow> K) " by rule
      from UI_subst[OF t] have 3:"(U \<leftarrow> K)[x:== N] = (U \<leftarrow> K)" by auto 
      have "((K \<leftarrow> ( U \<leftarrow> K)) \<leftarrow> N) \<rightarrow> ((U \<leftarrow> K)[x:== N])" using 2 3 by simp
      thus ?thesis using 1 2 3 t K by simp
    next
      case f:False
      let ?V = "( \<lambda>* x . U)"
      let ?U = "( \<lambda>* x . K)"
      from f  have 1:"(\<lambda>* x . (U \<leftarrow> K)) = ((S \<leftarrow> ?V) \<leftarrow> ?U)  " by auto
      have 2: "(((S \<leftarrow> ?V) \<leftarrow> ?U) \<leftarrow> N)  \<rightarrow> ((?V \<leftarrow> N) \<leftarrow> (?U \<leftarrow> N))" by rule
      have 3: " (?V \<leftarrow> N) \<rightarrow>* (U [x :== N])" using App by auto
      from f K have 4: " (?U \<leftarrow> N) \<rightarrow>* (W [x :== N])" using App by auto
      from  3 4 app_steps have "((?V \<leftarrow> N) \<leftarrow> (?U \<leftarrow> N)) \<rightarrow>* ((U [x :== N]) \<leftarrow> (W [x :== N]))" by auto 
      thus ?thesis using 1 2 3 4 f K by (simp add: star.step)
    qed
    next
        case S 
    thus ?thesis
      proof(cases "x \<notin> FV(U)")
      case t:True
      from t  have 1:"(\<lambda>* x . (U \<leftarrow> S)) = (K \<leftarrow> ( U \<leftarrow> S)) " by auto
      have 2:"((K \<leftarrow> ( U \<leftarrow> S)) \<leftarrow> N)  \<rightarrow> ( U \<leftarrow> S) " by rule
      from UI_subst[OF t] have 3:"(U \<leftarrow> S)[x:== N] = (U \<leftarrow> S)" by auto 
      have "((K \<leftarrow> ( U \<leftarrow> S)) \<leftarrow> N) \<rightarrow> ((U \<leftarrow> S)[x:== N])" using 2 3 by simp
      thus ?thesis using 1 2 3 t S by simp
    next
      case f:False
      let ?V = "( \<lambda>* x . U)"
      let ?U = "( \<lambda>* x . S)"
      from f  have 1:"(\<lambda>* x . (U \<leftarrow> S)) = ((S \<leftarrow> ?V) \<leftarrow> ?U)  " by auto
      have 2: "(((S \<leftarrow> ?V) \<leftarrow> ?U) \<leftarrow> N)  \<rightarrow> ((?V \<leftarrow> N) \<leftarrow> (?U \<leftarrow> N))" by rule
      have 3: " (?V \<leftarrow> N) \<rightarrow>* (U [x :== N])" using App by auto
      from f S have 4: " (?U \<leftarrow> N) \<rightarrow>* (W [x :== N])" using App by auto
      from  3 4 app_steps have "((?V \<leftarrow> N) \<leftarrow> (?U \<leftarrow> N)) \<rightarrow>* ((U [x :== N]) \<leftarrow> (W [x :== N]))" by auto 
      thus ?thesis using 1 2 3 4 f S by (simp add: star.step)
    qed
    next
    case (V y)
    thus ?thesis
    proof(cases "x=y")
      case eq:True
      thus ?thesis 
      proof(cases "x \<notin> FV(U)")
        case t: True 
        let ?V = "( \<lambda>* x . U)"
        let ?U = "( \<lambda>* x . (V y))"
        from UI_subst[OF t] have 1:"U[x:== N] = U " by auto
        from t eq  have 2:"(\<lambda>* x . (U \<leftarrow> (V y))) =  U" by auto
        have 3: " (?V \<leftarrow> N) \<rightarrow>* (U [x :== N])" using App by auto
        from  V have 4: " (?U \<leftarrow> N) \<rightarrow>* (W [x :== N])" using App by auto
        from  3 4 app_steps have "((?V \<leftarrow> N) \<leftarrow> (?U \<leftarrow> N)) \<rightarrow>* ((U [x :== N]) \<leftarrow> (W [x :== N]))" by auto 
        thus ?thesis using 1 2 3 4 t eq V by (simp add: star.step)
      next
        case f: False 
        let ?V = "( \<lambda>* x . U)"
        let ?U = "( \<lambda>* x . (V y))"
        from f V have 1:"(\<lambda>* x . (U \<leftarrow> (V y))) = ((S \<leftarrow> ?V) \<leftarrow> ?U)  " by auto
        have 2: "(((S \<leftarrow> ?V) \<leftarrow> ?U) \<leftarrow> N)  \<rightarrow> ((?V \<leftarrow> N) \<leftarrow> (?U \<leftarrow> N))" by rule
        have 3: " (?V \<leftarrow> N) \<rightarrow>* (U [x :== N])" using App by auto
        from f V have 4: " (?U \<leftarrow> N) \<rightarrow>* (W [x :== N])" using App by auto
        from  3 4 app_steps have "((?V \<leftarrow> N) \<leftarrow> (?U \<leftarrow> N)) \<rightarrow>* ((U [x :== N]) \<leftarrow> (W [x :== N]))" by auto 
        thus ?thesis using 1 2 3 4 f V by (simp add: star.step)
       qed
      next
        case neq:False
        thus ?thesis 
        proof(cases "x \<notin> FV(U)")
        case t: True 
        let ?V = "( \<lambda>* x . U)"
        let ?U = "( \<lambda>* x . (V y))"
        from neq t have 1:"(\<lambda>* x . (U \<leftarrow> (V y))) = (K \<leftarrow> ( U \<leftarrow> (V y))) " by auto
        have 2:"((K \<leftarrow> ( U \<leftarrow> (V y))) \<leftarrow> N)  \<rightarrow> ( U \<leftarrow> (V y)) " by rule
        from UI_subst[OF t] neq have 3:"(U \<leftarrow> (V y))[x:== N] = (U \<leftarrow> (V y))" by auto 
        have "((K \<leftarrow> ( U \<leftarrow> (V y))) \<leftarrow> N) \<rightarrow> ((U \<leftarrow> (V y))[x:== N])" using 2 3 by simp
        thus ?thesis using 1 2 3 t V by simp
      next
        case f: False 
        let ?V = "( \<lambda>* x . U)"
        let ?U = "( \<lambda>* x . (V y))"
        from f V have 1:"(\<lambda>* x . (U \<leftarrow> (V y))) = ((S \<leftarrow> ?V) \<leftarrow> ?U)  " by auto
        have 2: "(((S \<leftarrow> ?V) \<leftarrow> ?U) \<leftarrow> N)  \<rightarrow> ((?V \<leftarrow> N) \<leftarrow> (?U \<leftarrow> N))" by rule
        have 3: " (?V \<leftarrow> N) \<rightarrow>* (U [x :== N])" using App by auto
        from f V have 4: " (?U \<leftarrow> N) \<rightarrow>* (W [x :== N])" using App by auto
        from  3 4 app_steps have "((?V \<leftarrow> N) \<leftarrow> (?U \<leftarrow> N)) \<rightarrow>* ((U [x :== N]) \<leftarrow> (W [x :== N]))" by auto 
        thus ?thesis using 1 2 3 4 f V by (simp add: star.step)
       qed
      qed
  next
  case (App L H)
    thus ?thesis
      proof(cases "x \<notin> FV(U \<leftarrow> W)")
      case t:True
      from t App have 1:"(\<lambda>* x . (U \<leftarrow> W)) = (K \<leftarrow> ( U \<leftarrow> W)) " by auto
      have 2:"((K \<leftarrow> ( U \<leftarrow> W)) \<leftarrow> N)  \<rightarrow> ( U \<leftarrow> W) " by rule
      from UI_subst[OF t] have 3:"(U \<leftarrow> W)[x:== N] = (U \<leftarrow> W)" by auto 
      have "((K \<leftarrow> ( U \<leftarrow> W)) \<leftarrow> N) \<rightarrow> ((U \<leftarrow> W)[x:== N])" using 2 3 by simp
      thus ?thesis using 1 2 3 t App by simp
    next
      case f:False
      let ?V = "( \<lambda>* x . U)"
      let ?U = "( \<lambda>* x . W)"
       from f App have 1:"(\<lambda>* x . (U \<leftarrow> W)) = ((S \<leftarrow> ?V) \<leftarrow> ?U)  " by auto
      have 2: "(((S \<leftarrow> ?V) \<leftarrow> ?U) \<leftarrow> N)  \<rightarrow> ((?V \<leftarrow> N) \<leftarrow> (?U \<leftarrow> N))" by rule
      from  `(?U \<leftarrow> N) \<rightarrow>* (W [x :== N])` `(?V \<leftarrow> N) \<rightarrow>* (U [x :== N])`  app_steps 
          have "((?V \<leftarrow> N) \<leftarrow> (?U \<leftarrow> N)) \<rightarrow>* ((U [x :== N]) \<leftarrow> (W [x :== N]))" by auto 
      from this `(?U \<leftarrow> N) \<rightarrow>* (W [x :== N])` `(?V \<leftarrow> N) \<rightarrow>* (U [x :== N])` 1 2 f App
          show  ?thesis  by (simp add: star.step)
    qed
qed
qed

abbreviation  I' ::  "CLexp" where
"I'  \<equiv> (\<lambda>* ''x'' . (V ''x'')) "
values "{(c') | c'.  I' \<rightarrow>* c' }"

abbreviation  K' ::  "CLexp" where
"K' \<equiv> \<lambda>* ''x''. (\<lambda>* ''y'' . (V ''x'')) "

values "{(c') | c'. K \<rightarrow>* c' }"

abbreviation  K'' ::  "CLexp" where
"K'' \<equiv> \<lambda>* ''x''. (\<lambda>* ''y'' . (V ''y'')) "
values "{(c') | c'. K'' \<rightarrow>* c' }"

abbreviation  S' ::  "CLexp" where
"S' \<equiv> \<lambda>* ''x''. \<lambda>* ''y'' . \<lambda>* ''z''. (((V ''x'')\<leftarrow> (V ''z'')) \<leftarrow> ((V ''y'') \<leftarrow> (V ''z'')) )"
values "{(c') |c'. S'  \<rightarrow>* c'  }"

values "{(c') | c'. (((S \<leftarrow> K) \<leftarrow> (I))  \<leftarrow> K') \<rightarrow>* c' }"
values "{(c') |c'.  (((((S \<leftarrow> (K \<leftarrow> S)) \<leftarrow> K) \<leftarrow> S) \<leftarrow> I) \<leftarrow> I)  \<rightarrow>* c'  }"
values "{(c') |c'.  ((((S \<leftarrow>K) \<leftarrow>S))\<leftarrow> K)    \<rightarrow>* c'  }"
values "{(c') |c'.  (((S \<leftarrow> I) \<leftarrow> K) \<leftarrow> (V ''x''))  \<rightarrow>* c'  }"
values "{(c') |c'.  ((((S \<leftarrow> S) \<leftarrow> K) \<leftarrow> (V ''x'')) \<leftarrow> (V ''y''))  \<rightarrow>* c'  }"
values "{ (c') | c'. ((\<lambda>* ''t''. ((((V ''y'')\<leftarrow> (V ''y'')) \<leftarrow> (((V ''y'')\<leftarrow> (V ''y'')) \<leftarrow> (V ''t'')))))\<leftarrow> (V ''w'')) \<rightarrow>* c'} "



(*Simple typing, Curry-style in CL*)
datatype ty = int | bool | Vt vname |Arrow ty ty  ("_ \<Rightarrow> _")

type_synonym tyenv = "vname \<Rightarrow> ty"

(*The type-assignment system *)
inductive typing :: "tyenv \<Rightarrow> CLexp \<Rightarrow> ty \<Rightarrow> bool"
  ("(1_/ \<turnstile>/ (_ :/ _))" [50,0,50] 50)
where
vt:"\<Gamma> \<turnstile> V x : (\<Gamma> x) "|
it:"\<Gamma> \<turnstile> I : (\<sigma> \<Rightarrow> \<sigma>)"|
kt:"\<Gamma> \<turnstile> K : (\<sigma> \<Rightarrow> (\<tau> \<Rightarrow> \<sigma>)) "| 
st:"\<Gamma> \<turnstile> S : (( \<rho> \<Rightarrow>( \<sigma> \<Rightarrow> \<tau>) ) \<Rightarrow> (( \<rho> \<Rightarrow> \<sigma> ) \<Rightarrow> (\<rho> \<Rightarrow> \<tau>)))  "| 
appte:"\<Gamma> \<turnstile> M: (\<sigma> \<Rightarrow> \<tau>) \<Longrightarrow> \<Gamma> \<turnstile> N: \<sigma> \<Longrightarrow>  \<Gamma> \<turnstile> M \<leftarrow> N : \<tau>" 

fun subst_ty :: "ty \<Rightarrow> ty \<Rightarrow> ty \<Rightarrow> ty" ("(_ [_ :=' _])" [0,0] 50) where
"int [a :=' b] = int" |
"bool [a :=' b] = bool" |
"(Vt x) [a :=' b] = ( if a = (Vt x)  then b else (Vt x))"|
"(\<sigma> \<Rightarrow> \<beta>) [a :=' b] = (case a of (Vt x) \<Rightarrow> ((\<sigma>[a :=' b]) \<Rightarrow> (\<beta>[a :=' b])) |
                       _\<Rightarrow> (\<sigma> \<Rightarrow> \<beta>) )" 

lemma VtyE:"\<Gamma> \<turnstile> (V x): \<sigma> \<Longrightarrow>  (\<Gamma> x) = \<sigma> "
by (metis CLexp.distinct(11) CLexp.distinct(15) CLexp.distinct(19) CLexp.distinct(5) CLexp.inject(1) typing.simps)

lemma ityE: "\<Gamma> \<turnstile> I : \<tau> \<Longrightarrow> EX \<sigma>. \<tau> = (\<sigma> \<Rightarrow> \<sigma>)"
using typing.simps by blast

lemma ktyE:"\<Gamma> \<turnstile> K: \<tau> \<Longrightarrow> EX \<sigma> \<beta>. \<tau> = (\<sigma> \<Rightarrow> (\<beta> \<Rightarrow> \<sigma>))"
using typing.simps by blast

lemma styE:" \<Gamma> \<turnstile> S : \<beta> \<Longrightarrow> EX \<rho> \<sigma> \<tau>. \<beta> = (( \<rho> \<Rightarrow>( \<sigma> \<Rightarrow> \<tau>) ) \<Rightarrow> (( \<rho> \<Rightarrow> \<sigma> ) \<Rightarrow> (\<rho> \<Rightarrow> \<tau>)))"
using typing.simps by blast

lemma apptyE: "\<Gamma> \<turnstile> M \<leftarrow> N :\<tau> \<Longrightarrow> EX \<sigma>. \<Gamma> \<turnstile> M: (\<sigma> \<Rightarrow> \<tau>) \<and> \<Gamma> \<turnstile> N: \<sigma>"
by (metis CLexp.distinct(13) CLexp.distinct(17) CLexp.distinct(19) CLexp.distinct(7) CLexp.inject(2) typing.simps)

lemma env_elim: "x \<notin> FV (M) \<Longrightarrow>  \<Gamma>(x := \<sigma>) \<turnstile> M : \<tau> \<Longrightarrow> \<Gamma> \<turnstile> M : \<tau>" 
apply(induction M arbitrary: \<tau>) 
using it ityE apply blast
using kt ktyE apply blast
using st styE apply blast
apply (metis FV.simps(4) VtyE fun_upd_other singleton_iff typing.simps)
by (metis FV.simps(5) UnI1 UnI2 apptyE typing.simps)

lemma env_add: "x \<notin> FV (M) \<Longrightarrow>  \<Gamma> \<turnstile> M : \<tau> \<Longrightarrow> \<Gamma>(x := \<sigma>) \<turnstile> M : \<tau>"
apply(induction M arbitrary: \<tau>) 
using it ityE apply blast
using kt ktyE apply blast
using st styE apply blast
apply (metis FV.simps(4) VtyE fun_upd_other singleton_iff typing.simps)
by (metis FV.simps(5) UnI1 UnI2 apptyE typing.simps)

(*Abstraction and types*)
lemma abstyI: 
 "\<Gamma>(x:=\<sigma>) \<turnstile> M: \<tau> \<Longrightarrow> \<Gamma> \<turnstile> \<lambda>* x. M: (\<sigma> \<Rightarrow> \<tau>)"
proof(induction M arbitrary: \<tau> \<sigma>)
  case I
  from ityE[OF I] obtain \<sigma> where 0:"\<tau> = (\<sigma> \<Rightarrow> \<sigma>)"  by blast
  show ?case using "0"  appte kt  it by fastforce
next 
  case K 
  from ktyE[OF K] obtain  \<sigma> \<beta> where "\<tau> = (\<sigma> \<Rightarrow> (\<beta> \<Rightarrow> \<sigma>))" by blast
  then show ?case using appte kt it  by fastforce
next
  case S 
  from styE[OF S] obtain \<rho> \<sigma> \<tau>' where 0:"\<tau> = ((\<rho> \<Rightarrow> (\<sigma> \<Rightarrow> \<tau>')) \<Rightarrow> ((\<rho> \<Rightarrow> \<sigma>) \<Rightarrow> (\<rho> \<Rightarrow> \<tau>')))" by blast
  thus ?case  using appte kt st it  by fastforce
next
  case (V a) thus ?case  
  apply (cases "x=a")
  using VtyE it apply fastforce
  by (metis VtyE abs.simps(1) fun_upd_apply typing.simps)
next
  case (App M1 M2)
  from apptyE[OF App(3)] obtain \<sigma>' where 0:"\<Gamma>(x := \<sigma>) \<turnstile> M1 : (\<sigma>' \<Rightarrow> \<tau>)" and 1:" \<Gamma>(x := \<sigma>) \<turnstile> M2 : \<sigma>'" by blast    
  from App(1)[OF 0] have 3:"\<Gamma> \<turnstile> \<lambda>* x . M1 : (\<sigma> \<Rightarrow> (\<sigma>' \<Rightarrow> \<tau>))" by blast
  from App(2)[OF 1] have 4: " \<Gamma> \<turnstile> \<lambda>* x . M2 : (\<sigma> \<Rightarrow> \<sigma>')" by blast
  show ?case
  proof(cases M2)
    case I thus ?thesis 
    proof(cases "x \<notin> FV(M1)")
      case t:True
      from apptyE[OF App(3)] obtain \<sigma>' where 0:"\<Gamma>(x := \<sigma>) \<turnstile> M1 : (\<sigma>' \<Rightarrow> \<tau>)" and 1:" \<Gamma>(x := \<sigma>) \<turnstile> M2 : \<sigma>'" by blast
      hence "\<Gamma>(x := \<sigma>) \<turnstile> I : \<sigma>'" by (simp add: local.I)
      have 1:"\<Gamma> \<turnstile> I : \<sigma>'"  using \<open>\<Gamma>(x := \<sigma>) \<turnstile> I : \<sigma>'\<close> it ityE by blast
      from t have 2:"(\<lambda>* x. (M1 \<leftarrow> I)) =  (K \<leftarrow> (M1 \<leftarrow> I))" by simp
      from t 0 have "\<Gamma> \<turnstile> M1 : (\<sigma>' \<Rightarrow> \<tau>)" using env_elim by blast
      from 1 this  have 3:" \<Gamma> \<turnstile> (M1 \<leftarrow> I): \<tau>" using appte by blast 
      from kt ktyE have 4:"\<Gamma> \<turnstile> K: (\<tau> \<Rightarrow>  (\<sigma> \<Rightarrow> \<tau>))" by simp 
      from 3 4 appte have 5:"\<Gamma> \<turnstile> (K \<leftarrow> (M1 \<leftarrow> I)) : (\<sigma> \<Rightarrow> \<tau>)" by blast  
      thus ?thesis using "2" local.I by auto 
    next
      case f:False
      from apptyE[OF App(3)] obtain \<sigma>' where 0:"\<Gamma>(x := \<sigma>) \<turnstile> M1 : (\<sigma>' \<Rightarrow> \<tau>)" and 1:" \<Gamma>(x := \<sigma>) \<turnstile> M2 : \<sigma>'" by blast
      hence "\<Gamma>(x := \<sigma>) \<turnstile> I : \<sigma>'" by (simp add: local.I)
      have I1:"\<Gamma> \<turnstile> I : \<sigma>'"  using \<open>\<Gamma>(x := \<sigma>) \<turnstile> I : \<sigma>'\<close> it ityE by blast
      from f have 2:"(\<lambda>* x. (M1 \<leftarrow> I)) =  ((S \<leftarrow> (\<lambda>* x. M1)) \<leftarrow> (\<lambda>* x .I)) " by simp
      from App(1)[OF 0] have 3:"\<Gamma> \<turnstile> \<lambda>* x . M1 : (\<sigma> \<Rightarrow> (\<sigma>' \<Rightarrow> \<tau>))" by blast
      from App(2)[OF 1] have 4: " \<Gamma> \<turnstile> \<lambda>* x . M2 : (\<sigma> \<Rightarrow> \<sigma>')" by blast
      hence  5:"\<Gamma> \<turnstile> \<lambda>* x . I : (\<sigma> \<Rightarrow> \<sigma>')" by (simp add: local.I)
      have 6:"\<Gamma> \<turnstile> S : ((\<sigma> \<Rightarrow> (\<sigma>' \<Rightarrow> \<tau>)) \<Rightarrow> ((\<sigma> \<Rightarrow> \<sigma>') \<Rightarrow> (\<sigma> \<Rightarrow> \<tau>)))" by (simp add: st) 
      have 7:"\<Gamma> \<turnstile> (S \<leftarrow> (\<lambda>* x. M1)):  ((\<sigma> \<Rightarrow> \<sigma>') \<Rightarrow> (\<sigma> \<Rightarrow> \<tau>))" using "3" "6" appte by auto 
      have "\<Gamma> \<turnstile> ((S \<leftarrow> (\<lambda>* x. M1)) \<leftarrow> (\<lambda>* x . M2)):(\<sigma> \<Rightarrow> \<tau>)"  using "4" "7" appte by auto
      hence "\<Gamma> \<turnstile> (\<lambda>* x. (M1 \<leftarrow> M2)):(\<sigma> \<Rightarrow> \<tau>)" using "2" local.I by auto 
      thus ?thesis by simp
    qed 
    next
    case K thus ?thesis
    proof(cases "x \<notin> FV(M1)")
      case t:True
      from apptyE[OF App(3)] obtain \<sigma>' where 0:"\<Gamma>(x := \<sigma>) \<turnstile> M1 : (\<sigma>' \<Rightarrow> \<tau>)" and 1:" \<Gamma>(x := \<sigma>) \<turnstile> M2 : \<sigma>'" by blast
      hence "\<Gamma>(x := \<sigma>) \<turnstile> K : \<sigma>'" by (simp add: local.K)
      have 1:"\<Gamma> \<turnstile> K : \<sigma>'"  using \<open>\<Gamma>(x := \<sigma>) \<turnstile> K : \<sigma>'\<close> kt ktyE by blast
      from t have 2:"(\<lambda>* x. (M1 \<leftarrow> K)) =  (K \<leftarrow> (M1 \<leftarrow> K))" by simp
      from t 0 have "\<Gamma> \<turnstile> M1 : (\<sigma>' \<Rightarrow> \<tau>)" using env_elim by blast
      from 1 this  have 3:" \<Gamma> \<turnstile> (M1 \<leftarrow> K): \<tau>" using appte by blast 
      from kt ktyE have 4:"\<Gamma> \<turnstile> K: (\<tau> \<Rightarrow>  (\<sigma> \<Rightarrow> \<tau>))" by simp 
      from 3 4 appte have 5:"\<Gamma> \<turnstile> (K \<leftarrow> (M1 \<leftarrow> K)) : (\<sigma> \<Rightarrow> \<tau>)" by blast  
      thus ?thesis using "2" local.K by auto 
    next
      case f:False
      from apptyE[OF App(3)] obtain \<sigma>' where 0:"\<Gamma>(x := \<sigma>) \<turnstile> M1 : (\<sigma>' \<Rightarrow> \<tau>)" and 1:" \<Gamma>(x := \<sigma>) \<turnstile> M2 : \<sigma>'" by blast
      hence "\<Gamma>(x := \<sigma>) \<turnstile> K : \<sigma>'" by (simp add: local.K)
      have I1:"\<Gamma> \<turnstile> K : \<sigma>'"  using \<open>\<Gamma>(x := \<sigma>) \<turnstile> K : \<sigma>'\<close> kt ktyE by blast
      from f have 2:"(\<lambda>* x. (M1 \<leftarrow> K)) =  ((S \<leftarrow> (\<lambda>* x. M1)) \<leftarrow> (\<lambda>* x .K)) " by simp
      from App(1)[OF 0] have 3:"\<Gamma> \<turnstile> \<lambda>* x . M1 : (\<sigma> \<Rightarrow> (\<sigma>' \<Rightarrow> \<tau>))" by blast
      from App(2)[OF 1] have 4: " \<Gamma> \<turnstile> \<lambda>* x . M2 : (\<sigma> \<Rightarrow> \<sigma>')" by blast
      hence  5:"\<Gamma> \<turnstile> \<lambda>* x . K : (\<sigma> \<Rightarrow> \<sigma>')" by (simp add: local.K)
      have 6:"\<Gamma> \<turnstile> S : ((\<sigma> \<Rightarrow> (\<sigma>' \<Rightarrow> \<tau>)) \<Rightarrow> ((\<sigma> \<Rightarrow> \<sigma>') \<Rightarrow> (\<sigma> \<Rightarrow> \<tau>)))" by (simp add: st) 
      have 7:"\<Gamma> \<turnstile> (S \<leftarrow> (\<lambda>* x. M1)):  ((\<sigma> \<Rightarrow> \<sigma>') \<Rightarrow> (\<sigma> \<Rightarrow> \<tau>))" using "3" "6" appte by auto 
      have "\<Gamma> \<turnstile> ((S \<leftarrow> (\<lambda>* x. M1)) \<leftarrow> (\<lambda>* x . M2)):(\<sigma> \<Rightarrow> \<tau>)"  using "4" "7" appte by auto
      hence "\<Gamma> \<turnstile> (\<lambda>* x. (M1 \<leftarrow> M2)):(\<sigma> \<Rightarrow> \<tau>)" using "2" local.K by auto 
      thus ?thesis by simp
    qed 
    next
    case S thus ?thesis
     proof(cases "x \<notin> FV(M1)")
      case t:True
      from apptyE[OF App(3)] obtain \<sigma>' where 0:"\<Gamma>(x := \<sigma>) \<turnstile> M1 : (\<sigma>' \<Rightarrow> \<tau>)" and 1:" \<Gamma>(x := \<sigma>) \<turnstile> M2 : \<sigma>'" by blast
      hence "\<Gamma>(x := \<sigma>) \<turnstile> S : \<sigma>'" by (simp add: local.S)
      have 1:"\<Gamma> \<turnstile> S : \<sigma>'"  using \<open>\<Gamma>(x := \<sigma>) \<turnstile> S : \<sigma>'\<close> st styE by blast
      from t have 2:"(\<lambda>* x. (M1 \<leftarrow> S)) =  (K \<leftarrow> (M1 \<leftarrow> S))" by simp
      from t 0 have "\<Gamma> \<turnstile> M1 : (\<sigma>' \<Rightarrow> \<tau>)" using env_elim by blast
      from 1 this  have 3:" \<Gamma> \<turnstile> (M1 \<leftarrow> S): \<tau>" using appte by blast 
      from kt ktyE have 4:"\<Gamma> \<turnstile> K: (\<tau> \<Rightarrow>  (\<sigma> \<Rightarrow> \<tau>))" by simp 
      from 3 4 appte have 5:"\<Gamma> \<turnstile> (K \<leftarrow> (M1 \<leftarrow> S)) : (\<sigma> \<Rightarrow> \<tau>)" by blast  
      thus ?thesis using "2" local.S by auto 
    next
      case f:False
      from apptyE[OF App(3)] obtain \<sigma>' where 0:"\<Gamma>(x := \<sigma>) \<turnstile> M1 : (\<sigma>' \<Rightarrow> \<tau>)" and 1:" \<Gamma>(x := \<sigma>) \<turnstile> M2 : \<sigma>'" by blast
      hence "\<Gamma>(x := \<sigma>) \<turnstile> S : \<sigma>'" by (simp add: local.S)
      have I1:"\<Gamma> \<turnstile> S : \<sigma>'"  using \<open>\<Gamma>(x := \<sigma>) \<turnstile> S : \<sigma>'\<close> st styE by blast
      from f have 2:"(\<lambda>* x. (M1 \<leftarrow> S)) =  ((S \<leftarrow> (\<lambda>* x. M1)) \<leftarrow> (\<lambda>* x .S)) " by simp
      from App(1)[OF 0] have 3:"\<Gamma> \<turnstile> \<lambda>* x . M1 : (\<sigma> \<Rightarrow> (\<sigma>' \<Rightarrow> \<tau>))" by blast
      from App(2)[OF 1] have 4: " \<Gamma> \<turnstile> \<lambda>* x . M2 : (\<sigma> \<Rightarrow> \<sigma>')" by blast
      hence  5:"\<Gamma> \<turnstile> \<lambda>* x . S : (\<sigma> \<Rightarrow> \<sigma>')" by (simp add: local.S)
      have 6:"\<Gamma> \<turnstile> S : ((\<sigma> \<Rightarrow> (\<sigma>' \<Rightarrow> \<tau>)) \<Rightarrow> ((\<sigma> \<Rightarrow> \<sigma>') \<Rightarrow> (\<sigma> \<Rightarrow> \<tau>)))" by (simp add: st) 
      have 7:"\<Gamma> \<turnstile> (S \<leftarrow> (\<lambda>* x. M1)):  ((\<sigma> \<Rightarrow> \<sigma>') \<Rightarrow> (\<sigma> \<Rightarrow> \<tau>))" using "3" "6" appte by auto 
      have "\<Gamma> \<turnstile> ((S \<leftarrow> (\<lambda>* x. M1)) \<leftarrow> (\<lambda>* x . M2)):(\<sigma> \<Rightarrow> \<tau>)"  using "4" "7" appte by auto
      hence "\<Gamma> \<turnstile> (\<lambda>* x. (M1 \<leftarrow> M2)):(\<sigma> \<Rightarrow> \<tau>)" using "2" local.S by auto 
      thus ?thesis by simp
    qed 
    next
    case (V y) thus ?thesis 
     proof(cases "x \<notin> FV(M1)")
      case t:True thus ?thesis
      proof(cases "x \<noteq> y")
        case t2: True 
        from apptyE[OF App(3)] obtain \<sigma>' where 0:"\<Gamma>(x := \<sigma>) \<turnstile> M1 : (\<sigma>' \<Rightarrow> \<tau>)" and 1:" \<Gamma>(x := \<sigma>) \<turnstile> M2 : \<sigma>'" by blast    
        hence "\<Gamma>(x := \<sigma>) \<turnstile> V y : \<sigma>'" by (simp add: local.V)
        from this t2 have 1:"\<Gamma> \<turnstile> V y : \<sigma>'" using FV.simps(4) env_elim singleton_iff by blast 
        from t t2  have 2:"(\<lambda>* x. (M1 \<leftarrow> (V y))) =  (K \<leftarrow> (M1 \<leftarrow> (V y)))" by simp
        from t 0 have "\<Gamma> \<turnstile> M1 : (\<sigma>' \<Rightarrow> \<tau>)" using env_elim by blast
        from 1 this  have 3:" \<Gamma> \<turnstile> (M1 \<leftarrow> (V y)): \<tau>" using appte by blast 
        from kt ktyE have 4:"\<Gamma> \<turnstile> K: (\<tau> \<Rightarrow>  (\<sigma> \<Rightarrow> \<tau>))" by simp 
        from 3 4 appte have 5:"\<Gamma> \<turnstile> (K \<leftarrow> (M1 \<leftarrow> (V y))) : (\<sigma> \<Rightarrow> \<tau>)" by blast  
        thus ?thesis using "2" V by auto 
      next
        case f2: False 
        from apptyE[OF App(3)] obtain \<sigma>' where 0:"\<Gamma>(x := \<sigma>) \<turnstile> M1 : (\<sigma>' \<Rightarrow> \<tau>)" and 1:" \<Gamma>(x := \<sigma>) \<turnstile> M2 : \<sigma>'" and "\<sigma>'= \<sigma>" using V VtyE f2 by fastforce 
        hence 3:"\<Gamma> \<turnstile> M1 : (\<sigma> \<Rightarrow> \<tau>)" using "0" env_elim t by blast
        from f2 t have 4:"(\<lambda>* x. (M1 \<leftarrow> (V y))) = M1  " by simp
        hence "\<Gamma> \<turnstile> (\<lambda>* x. (M1 \<leftarrow> M2)):(\<sigma> \<Rightarrow> \<tau>)" using "3" "4" local.V by auto 
        thus ?thesis by simp
      qed
    next
      case f:False 
      thus ?thesis 
       proof(cases "x \<noteq> y")
        case t2: True 
        from apptyE[OF App(3)] obtain \<sigma>' where 0:"\<Gamma>(x := \<sigma>) \<turnstile> M1 : (\<sigma>' \<Rightarrow> \<tau>)" and 1:" \<Gamma>(x := \<sigma>) \<turnstile> M2 : \<sigma>'" by blast
        hence "\<Gamma>(x := \<sigma>) \<turnstile> (V y) : \<sigma>'" by (simp add: local.V)
        from t2 this have I1:"\<Gamma> \<turnstile> (V y) : \<sigma>'" using FV.simps(4) env_elim singleton_iff by blast  
        from f have 2:"(\<lambda>* x. (M1 \<leftarrow> (V y))) =  ((S \<leftarrow> (\<lambda>* x. M1)) \<leftarrow> (\<lambda>* x .(V y))) " by simp
        from App(1)[OF 0] have 3:"\<Gamma> \<turnstile> \<lambda>* x . M1 : (\<sigma> \<Rightarrow> (\<sigma>' \<Rightarrow> \<tau>))" by blast
        from App(2)[OF 1] have 4: " \<Gamma> \<turnstile> \<lambda>* x . M2 : (\<sigma> \<Rightarrow> \<sigma>')" by blast
        hence  5:"\<Gamma> \<turnstile> \<lambda>* x . (V y) : (\<sigma> \<Rightarrow> \<sigma>')" by (simp add: local.V)
        have 6:"\<Gamma> \<turnstile> S : ((\<sigma> \<Rightarrow> (\<sigma>' \<Rightarrow> \<tau>)) \<Rightarrow> ((\<sigma> \<Rightarrow> \<sigma>') \<Rightarrow> (\<sigma> \<Rightarrow> \<tau>)))" by (simp add: st) 
        have 7:"\<Gamma> \<turnstile> (S \<leftarrow> (\<lambda>* x. M1)):  ((\<sigma> \<Rightarrow> \<sigma>') \<Rightarrow> (\<sigma> \<Rightarrow> \<tau>))" using "3" "6" appte by auto 
        have "\<Gamma> \<turnstile> ((S \<leftarrow> (\<lambda>* x. M1)) \<leftarrow> (\<lambda>* x . M2)):(\<sigma> \<Rightarrow> \<tau>)"  using "4" "7" appte by auto
        hence "\<Gamma> \<turnstile> (\<lambda>* x. (M1 \<leftarrow> M2)):(\<sigma> \<Rightarrow> \<tau>)" using "2" local.V by auto 
        thus ?thesis by simp
      next
        case f2: False
        from apptyE[OF App(3)] obtain \<sigma>' where 0:"\<Gamma>(x := \<sigma>) \<turnstile> M1 : (\<sigma>' \<Rightarrow> \<tau>)" and 1:" \<Gamma>(x := \<sigma>) \<turnstile> M2 : \<sigma>'" by blast
        hence "\<Gamma>(x := \<sigma>) \<turnstile> (V y) : \<sigma>'" by (simp add: local.V) 
        from f have 2:"(\<lambda>* x. (M1 \<leftarrow> (V y))) =  ((S \<leftarrow> (\<lambda>* x. M1)) \<leftarrow> (\<lambda>* x .(V y))) " by simp
        from App(1)[OF 0] have 3:"\<Gamma> \<turnstile> \<lambda>* x . M1 : (\<sigma> \<Rightarrow> (\<sigma>' \<Rightarrow> \<tau>))" by blast
        from App(2)[OF 1] have 4: " \<Gamma> \<turnstile> \<lambda>* x . M2 : (\<sigma> \<Rightarrow> \<sigma>')" by blast
        hence  5:"\<Gamma> \<turnstile> \<lambda>* x . (V y) : (\<sigma> \<Rightarrow> \<sigma>')" by (simp add: local.V)
        have 6:"\<Gamma> \<turnstile> S : ((\<sigma> \<Rightarrow> (\<sigma>' \<Rightarrow> \<tau>)) \<Rightarrow> ((\<sigma> \<Rightarrow> \<sigma>') \<Rightarrow> (\<sigma> \<Rightarrow> \<tau>)))" by (simp add: st) 
        have 7:"\<Gamma> \<turnstile> (S \<leftarrow> (\<lambda>* x. M1)):  ((\<sigma> \<Rightarrow> \<sigma>') \<Rightarrow> (\<sigma> \<Rightarrow> \<tau>))" using "3" "6" appte by auto 
        have "\<Gamma> \<turnstile> ((S \<leftarrow> (\<lambda>* x. M1)) \<leftarrow> (\<lambda>* x . M2)):(\<sigma> \<Rightarrow> \<tau>)"  using "4" "7" appte by auto
        hence "\<Gamma> \<turnstile> (\<lambda>* x. (M1 \<leftarrow> M2)):(\<sigma> \<Rightarrow> \<tau>)" using "2" local.V by auto 
        thus ?thesis by simp   
      qed
    qed 
    next
    case (App W Y)
    thus ?thesis
    proof(cases "x \<notin> FV(M1)")
      case t:True thus ?thesis 
       proof(cases "x \<notin> FV( W \<leftarrow> Y)")
        case t2: True 
        from 0 1 have "\<Gamma>(x := \<sigma>) \<turnstile> ( W \<leftarrow> Y): \<sigma>'" by (simp add: local.App)
        from this t2 have 1:"\<Gamma> \<turnstile> ( W \<leftarrow> Y) : \<sigma>'" using FV.simps(1) env_elim singleton_iff by blast
        from t t2  have 2:"(\<lambda>* x. (M1 \<leftarrow> ( W \<leftarrow> Y))) =  (K \<leftarrow> (M1 \<leftarrow> ( W \<leftarrow> Y)))" by simp
        from t 0 have "\<Gamma> \<turnstile> M1 : (\<sigma>' \<Rightarrow> \<tau>)" using env_elim by blast
        from 1 this  have 3:" \<Gamma> \<turnstile> (M1 \<leftarrow> ( W \<leftarrow> Y)): \<tau>" using appte by blast 
        from kt ktyE have 4:"\<Gamma> \<turnstile> K: (\<tau> \<Rightarrow>  (\<sigma> \<Rightarrow> \<tau>))" by simp 
        from 3 4 appte have 5:"\<Gamma> \<turnstile> (K \<leftarrow> (M1 \<leftarrow> ( W \<leftarrow> Y))) : (\<sigma> \<Rightarrow> \<tau>)" by blast  
        thus ?thesis using "2" App by auto 
      next
        case f2: False 
        from 0 1 have  "\<Gamma>(x := \<sigma>) \<turnstile> ( W \<leftarrow> Y) : \<sigma>'" by (simp add: local.App)
        from f2 have 2:"(\<lambda>* x. (M1 \<leftarrow> ( W \<leftarrow> Y))) =  ((S \<leftarrow> (\<lambda>* x. M1)) \<leftarrow> (\<lambda>* x .( W \<leftarrow> Y))) " by simp
        from 4 have  5:"\<Gamma> \<turnstile> \<lambda>* x . ( W \<leftarrow> Y) : (\<sigma> \<Rightarrow> \<sigma>')" by (simp add: local.App)
        have 6:"\<Gamma> \<turnstile> S : ((\<sigma> \<Rightarrow> (\<sigma>' \<Rightarrow> \<tau>)) \<Rightarrow> ((\<sigma> \<Rightarrow> \<sigma>') \<Rightarrow> (\<sigma> \<Rightarrow> \<tau>)))" by (simp add: st) 
        have 7:"\<Gamma> \<turnstile> (S \<leftarrow> (\<lambda>* x. M1)):  ((\<sigma> \<Rightarrow> \<sigma>') \<Rightarrow> (\<sigma> \<Rightarrow> \<tau>))" using "3" "6" appte by auto 
        have "\<Gamma> \<turnstile> ((S \<leftarrow> (\<lambda>* x. M1)) \<leftarrow> (\<lambda>* x . M2)):(\<sigma> \<Rightarrow> \<tau>)"  using "4" "7" appte by auto
        hence "\<Gamma> \<turnstile> (\<lambda>* x. (M1 \<leftarrow> M2)):(\<sigma> \<Rightarrow> \<tau>)" using "2" local.App by auto 
        thus ?thesis by simp
      qed
    next
      case f:False 
      thus ?thesis 
       proof(cases "x \<notin> FV( W \<leftarrow> Y)")
        case t2: True 
         from 0 1 have  "\<Gamma>(x := \<sigma>) \<turnstile> ( W \<leftarrow> Y) : \<sigma>'" by (simp add: local.App)
        from t2 f have 2:"(\<lambda>* x. (M1 \<leftarrow> ( W \<leftarrow> Y))) =  ((S \<leftarrow> (\<lambda>* x. M1)) \<leftarrow> (\<lambda>* x .( W \<leftarrow> Y))) " by simp
        from 4 have  5:"\<Gamma> \<turnstile> \<lambda>* x . ( W \<leftarrow> Y) : (\<sigma> \<Rightarrow> \<sigma>')" by (simp add: local.App)
        have 6:"\<Gamma> \<turnstile> S : ((\<sigma> \<Rightarrow> (\<sigma>' \<Rightarrow> \<tau>)) \<Rightarrow> ((\<sigma> \<Rightarrow> \<sigma>') \<Rightarrow> (\<sigma> \<Rightarrow> \<tau>)))" by (simp add: st) 
        have 7:"\<Gamma> \<turnstile> (S \<leftarrow> (\<lambda>* x. M1)):  ((\<sigma> \<Rightarrow> \<sigma>') \<Rightarrow> (\<sigma> \<Rightarrow> \<tau>))" using "3" "6" appte by auto 
        have "\<Gamma> \<turnstile> ((S \<leftarrow> (\<lambda>* x. M1)) \<leftarrow> (\<lambda>* x . M2)):(\<sigma> \<Rightarrow> \<tau>)"  using "4" "7" appte by auto
        hence "\<Gamma> \<turnstile> (\<lambda>* x. (M1 \<leftarrow> M2)):(\<sigma> \<Rightarrow> \<tau>)" using "2" local.App by auto 
        thus ?thesis by simp
      next
        case f2: False 
        from 0 1 have  "\<Gamma>(x := \<sigma>) \<turnstile> ( W \<leftarrow> Y) : \<sigma>'" by (simp add: local.App)
        from f2 have 2:"(\<lambda>* x. (M1 \<leftarrow> ( W \<leftarrow> Y))) =  ((S \<leftarrow> (\<lambda>* x. M1)) \<leftarrow> (\<lambda>* x .( W \<leftarrow> Y))) " by simp
        from 4 have  5:"\<Gamma> \<turnstile> \<lambda>* x . ( W \<leftarrow> Y) : (\<sigma> \<Rightarrow> \<sigma>')" by (simp add: local.App)
        have 6:"\<Gamma> \<turnstile> S : ((\<sigma> \<Rightarrow> (\<sigma>' \<Rightarrow> \<tau>)) \<Rightarrow> ((\<sigma> \<Rightarrow> \<sigma>') \<Rightarrow> (\<sigma> \<Rightarrow> \<tau>)))" by (simp add: st) 
        have 7:"\<Gamma> \<turnstile> (S \<leftarrow> (\<lambda>* x. M1)):  ((\<sigma> \<Rightarrow> \<sigma>') \<Rightarrow> (\<sigma> \<Rightarrow> \<tau>))" using "3" "6" appte by auto 
        have "\<Gamma> \<turnstile> ((S \<leftarrow> (\<lambda>* x. M1)) \<leftarrow> (\<lambda>* x . M2)):(\<sigma> \<Rightarrow> \<tau>)"  using "4" "7" appte by auto
        hence "\<Gamma> \<turnstile> (\<lambda>* x. (M1 \<leftarrow> M2)):(\<sigma> \<Rightarrow> \<tau>)" using "2" local.App by auto 
        thus ?thesis by simp
      qed
    qed
  qed
qed

(*Substitution Lemma*)
lemma subs_ii: " \<Gamma>(x:= \<rho>) \<turnstile> M : \<tau>  \<Longrightarrow> \<Gamma> \<turnstile> N :  \<rho> \<Longrightarrow> \<Gamma> \<turnstile>M[x :== N]:\<tau>  "
proof(induction M arbitrary: \<Gamma> \<tau>  \<rho> )
  case (V x) thus ?case using VtyE fun_upd_apply subst.simps(1) vt by fastforce
next
  case (App M1 M2)
  from apptyE[OF App(3)] obtain \<sigma>' where 1:"\<Gamma>(x :=  \<rho>) \<turnstile> M1 :  \<sigma>' \<Rightarrow> \<tau> "and 2:" \<Gamma>(x :=  \<rho>) \<turnstile> M2 :  \<sigma>'" by blast
  from App(1)[OF 1 App(4)] App(2)[OF 2 App(4)] have "\<Gamma> \<turnstile> (M1 [x :== N]) \<leftarrow> (M2 [x :== N]) :  \<tau>" by (simp add: appte) 
  thus ?case  using subst.simps(2) by presburger
next
 case I thus ?case using it ityE subst.simps(3) by fastforce
next
 case K thus ?case using kt ktyE subst.simps(4) by fastforce
next
 case S thus ?case using st styE subst.simps(5) by fastforce
qed

lemma SRT_aux:"M \<rightarrow> M' \<Longrightarrow> \<Gamma> \<turnstile> M : \<sigma> \<Longrightarrow>  \<Gamma> \<turnstile> M' : \<sigma> "
proof(induction arbitrary: \<sigma> \<Gamma>  rule:"weak_reduc_step.induct") 
 case I thus ?case using apptyE ityE ty.inject(2) by blast
next
 case K thus ?case by (smt apptyE ktyE ty.inject(2))
next
 case (S x y z) 
 from apptyE[OF this] obtain \<sigma>' where 0:"\<Gamma> \<turnstile> ((S \<leftarrow> x) \<leftarrow> y) : (\<sigma>' \<Rightarrow> \<sigma>)" and 1:"\<Gamma> \<turnstile> z : \<sigma>'" by blast
 from apptyE[OF 0] obtain \<sigma>'' where 2:"\<Gamma> \<turnstile> (S \<leftarrow> x) : (\<sigma>'' \<Rightarrow> (\<sigma>' \<Rightarrow> \<sigma>))" and 3:"\<Gamma> \<turnstile> y : \<sigma>''" by blast
 from apptyE[OF 2] obtain \<sigma>''' where 4:"\<Gamma> \<turnstile> S : (\<sigma>''' \<Rightarrow> (\<sigma>'' \<Rightarrow> (\<sigma>' \<Rightarrow> \<sigma>)))" and 5:"\<Gamma> \<turnstile> x : \<sigma>'''" by blast
 thus ?case by (smt "1" "3" appte styE ty.inject(2)) 
next
 case app1 thus ?case by (meson apptyE typing.simps) 
next
 case app2 thus ?case by (meson apptyE typing.simps)
qed

(*Subject Reduction Theorem*)
lemma SRT:"M \<rightarrow>* M' \<Longrightarrow> \<Gamma> \<turnstile> M : \<sigma> \<Longrightarrow>  \<Gamma> \<turnstile> M' : \<sigma> "
by(induction rule:star.induct)(auto simp add:SRT_aux) 

fun NF ::"CLexp \<Rightarrow> bool" where
"NF (V x) =True" |
"NF K = True" |
"NF I =True"|
"NF S=True"|
"NF (I \<leftarrow> X) = False" |
"NF ((K \<leftarrow> X) \<leftarrow> Y) = False"|
"NF (((S \<leftarrow> X) \<leftarrow> Y) \<leftarrow> Z) = False"|
"NF (M \<leftarrow> N) \<longleftrightarrow> (NF M) \<and> (NF N)" 

lemma "(NF (((S \<leftarrow> (K \<leftarrow> K)) \<leftarrow> Y) \<leftarrow> (K \<leftarrow> K))) = False"
by simp

lemma NF_WR:"NF M \<Longrightarrow> \<not> weak_redex M"
by(induction M)  auto

lemma app_reduc1:"M1 \<rightarrow>* M' \<Longrightarrow> (M1 \<leftarrow> M2) \<rightarrow>* (M' \<leftarrow> M2)  "
proof(induction rule: star.induct)
  case (refl x) thus ?case by(rule refl)
next
 case (step x y z) thus ?case by (metis app1 star.step)
qed

lemma app_reduc2:"M2 \<rightarrow>* M'' \<Longrightarrow> (M1 \<leftarrow> M2) \<rightarrow>* (M1 \<leftarrow> M'') "
proof(induction rule: star.induct)
  case (refl x) thus ?case by(rule refl)
next
 case (step x y z) thus ?case by (metis app2 star.step)
qed

lemma app_reduc:"M1 \<rightarrow>* M' \<Longrightarrow> M2 \<rightarrow>* M'' \<Longrightarrow> (M1 \<leftarrow> M2) \<rightarrow>* (M' \<leftarrow> M'') "
using app_reduc1 app_reduc2 by (meson star_trans)

end