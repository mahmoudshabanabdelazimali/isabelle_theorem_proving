theory Classic_Relational_Algebra
imports Relational_Data_Model "~~/src/HOL/Library/Monad_Syntax"
begin

section\<open>Database and relation instances\<close>

type_synonym rel_inst = "tuple set"
type_synonym db_inst = "relname \<rightharpoonup> rel_inst"

section\<open>Invariants\<close>

subsection\<open>Finiteness of Database\<close>

definition invar_fin :: "db_schema \<Rightarrow> db_inst \<Rightarrow> bool" where 
  "invar_fin dbs dbinst \<longleftrightarrow>  finite (dom dbs) \<and>
 (\<forall>rn \<in> (dom dbs). finite (dom (the (dbs rn)))) \<and>
 (\<forall>rn \<in> (dom dbinst). finite  (the (dbinst rn)))"

(*
A database schema can not be empty. 
This could be irrelevant for formalization purposes and will add more work 

definition invar_dbs_noempty::"db_schema \<Rightarrow> bool" where
"invar_dbs_noempty dbs \<longleftrightarrow> dom dbs \<noteq> {}" 

lemma inv_nonemp:"invar_dbs_noempty stud_dbs"
  by(auto simp:stud_dbs_def invar_dbs_noempty_def)
*)

definition invar_db_c ::"db_schema \<Rightarrow> db_inst \<Rightarrow> bool" where 
"invar_db_c dbs dbinst  \<longleftrightarrow>  invar_fin dbs dbinst"

subsection\<open>Well-typeness of database ans relation\<close> 

subsubsection\<open>well-typeness of relations\<close>

definition wt_rel :: "rel_sign \<Rightarrow> rel_inst \<Rightarrow> bool" where
 "wt_rel rs ri \<longleftrightarrow> (\<forall>t\<in>ri. wt_tuple rs t)"

lemma wt_rel_dom_tuple:" wt_rel rs1 ri1 \<Longrightarrow> wt_rel rs2 ri2 \<Longrightarrow>  rs1 = rs2 \<Longrightarrow> 
    \<forall>t1 t2. t1 \<in> ri1 \<and> t2 \<in> ri2 \<longrightarrow> dom t1 = dom t2 "
    unfolding wt_rel_def wt_tuple_def
    apply auto
    apply (auto simp add: domD domI)
    by blast
 
lemma wt_rel_inter_dom1:" wt_rel rs1 ri1 \<Longrightarrow> wt_rel rs2 ri2 \<Longrightarrow>  rs1 = rs2
     \<Longrightarrow> \<forall>t1 t2. t1 \<in> ri1 \<and> t2 \<in> ri2 \<longrightarrow> dom t1 \<inter> dom t2 = dom t1 "
  unfolding wt_rel_def 
  apply auto
  by (metis (full_types) domD domI wt_tuple_def)

lemma wt_rel_inter_dom2:" wt_rel rs1 ri1 \<Longrightarrow> wt_rel rs2 ri2 \<Longrightarrow>  rs1 = rs2 \<Longrightarrow> 
    \<forall>t1 t2. t1 \<in> ri1 \<and> t2 \<in> ri2 \<longrightarrow> dom t1 \<inter> dom t2 = dom t2 "
  using wt_rel_inter_dom1 
  by blast

lemma attr_dom_rs_t: "wt_rel rs ri \<Longrightarrow> A \<in> dom rs \<Longrightarrow> t \<in>  ri \<Longrightarrow> A \<in> dom t"
  by (auto simp: wt_rel_def wt_tuple_def)
  
lemma t_ri_none_empty:"\<forall>t\<in>ri. \<forall>x. t x = None \<Longrightarrow>  xa \<in> ri \<Longrightarrow> empty \<in> ri"
 proof - 
  assume 1: "\<forall>t\<in>ri. \<forall>x. t x = None" and
         2: "xa \<in> ri"
  from 1 2 have "\<forall>x. xa x = None" by blast 
  hence "xa = empty" by fast
  then show "empty \<in> ri" using 2  by auto
qed

text\<open>There are two only two possible relation instances
     over the empty set of attributes: {} and {\<langle>empty\<rangle>}.\<close>

lemma wt_rel_empty_or: "wt_rel rs ri \<Longrightarrow> rs = empty \<Longrightarrow> ri = {} \<or> ri = {empty}"
  unfolding wt_rel_def wt_tuple_def dom_def
  apply (auto simp: t_ri_none_empty)
  done

subsubsection\<open>well-typeness of database\<close>

definition wt_db :: "db_schema \<Rightarrow> db_inst \<Rightarrow> bool" where
  "wt_db dbschema dbinst \<longleftrightarrow> 
    (dom dbschema = dom dbinst) \<and>
    (\<forall>rname rs ri. dbschema rname = Some rs \<and> dbinst rname = Some ri 
      \<longrightarrow> wt_rel rs ri)"

lemma rels_inst_fin:"wt_db dbschema dbinst  \<Longrightarrow> finite (dom dbschema) \<Longrightarrow> finite (dom dbinst)"
  unfolding   wt_db_def
  apply auto
  done

lemma wt_db_alt:"wt_db dbschema dbinst \<longleftrightarrow> 
    (dom dbschema = dom dbinst) \<and>
    (\<forall>rname \<in>(dom dbschema). wt_rel (the (dbschema rname)) (the (dbinst rname)) )"
  unfolding wt_db_def  wt_rel_def wt_tuple_def
  using domIff 
  apply auto
  apply fastforce
  apply fastforce
  apply fastforce
  apply (metis (mono_tags, lifting)  domD domI option.sel )
  apply (metis (mono_tags, lifting)  domD domI option.sel )
  by (metis (mono_tags, lifting)   domI option.sel )

subsection\<open>Invariants of Database \<close>

definition invar_db ::"db_schema  \<Rightarrow> db_inst \<Rightarrow> bool" where
"invar_db dbs dbinst \<longleftrightarrow> wt_db dbs dbinst \<and> invar_fin dbs dbinst "

lemma db_tuples_fin:
"invar_db dbs dbinst \<Longrightarrow> dbinst rn = Some ri \<Longrightarrow> t \<in> ri \<Longrightarrow> finite (dom t)"
  unfolding invar_db_def  invar_fin_def wt_db_def wt_rel_def wt_tuple_def  
  apply (auto simp: tuple_fin)
  by (metis (full_types) domI domIff option.exhaust_sel)

lemma db_relations_fin:
  "invar_db dbs dbinst \<Longrightarrow> finite (dom dbinst) "
   by(auto simp:rels_inst_fin invar_fin_def invar_db_def) 
  
lemma  dbs_rn_None: "dbs rn = None \<Longrightarrow> wt_db dbs dbinst \<Longrightarrow> dbinst rn = None" 
unfolding  wt_db_def invar_db_def
  by blast

lemma  dbs_rn_Some: "dbs rn = Some rs \<Longrightarrow> wt_db dbs dbinst \<Longrightarrow> \<exists>ri. dbinst rn = Some ri" 
unfolding  wt_db_def invar_db_def
  by blast

section\<open>Relational Algebra Operators' Semantics\<close>

subsection\<open>Selection\<close>

text\<open>selection semantics\<close>

definition selection_inst :: " selection_formule \<Rightarrow> rel_inst \<Rightarrow> rel_inst" ("\<sigma>")
  where "\<sigma> F ri = { t \<in> ri. (eval_F F) t }"

fun sel_gen_to_primitive :: "selection_formule \<Rightarrow> rel_inst \<Rightarrow>  rel_inst" where
"sel_gen_to_primitive (A =\<^sub>c a) ri =  \<sigma> (A =\<^sub>c a) ri "|
"sel_gen_to_primitive (A =\<^sub>a B) ri =  \<sigma> (A =\<^sub>a B) ri" |
"sel_gen_to_primitive (F\<^sub>1 \<and>\<^sub>s F\<^sub>2) ri =  sel_gen_to_primitive F\<^sub>1 (sel_gen_to_primitive F\<^sub>2 ri)  "

lemma sel_conj_compos_sel:
        "\<sigma> (F\<^sub>1 \<and>\<^sub>s F\<^sub>2) ri = \<sigma> F\<^sub>2 (\<sigma> F\<^sub>1 ri)"
  by(auto simp:selection_inst_def)

text\<open>One selection with positive conjunction of formulas
    is equivalent to a composition of selection\<close>

lemma gen_sel_correct:"sel_gen_to_primitive F ri = \<sigma> F ri " 
  apply (induction F arbitrary: ri)
  by(auto simp: sel_conj_compos_sel selection_inst_def)

lemma unsat_sel_q:"a\<noteq>b \<Longrightarrow> (\<sigma> (A =\<^sub>c a) ( \<sigma> (A =\<^sub>c b) ri )) = {}"
  apply (auto simp:selection_inst_def)
  done

subsubsection\<open>Selection Properties\<close>

lemma sel_abso_elm[simp]:"\<sigma> F {} = {}"
  using selection_inst_def by auto

lemma sel_idem[simp]:"\<sigma> F (\<sigma> F ri) = (\<sigma> F ri)"
  using selection_inst_def by auto

lemma sel_comm:"\<sigma> F\<^sub>2 (\<sigma> F\<^sub>1 ri) = \<sigma> F\<^sub>1 (\<sigma> F\<^sub>2 ri)"
  using selection_inst_def by auto

lemma sel_conj_comm:"\<sigma> (F\<^sub>1 \<and>\<^sub>s F\<^sub>2) ri = \<sigma> (F\<^sub>2 \<and>\<^sub>s F\<^sub>1) ri "
  using selection_inst_def by auto

lemma sel_conj_assoc:"\<sigma> (F\<^sub>1 \<and>\<^sub>s (F\<^sub>2 \<and>\<^sub>s F\<^sub>3)) ri = \<sigma> ((F\<^sub>1 \<and>\<^sub>s F\<^sub>2) \<and>\<^sub>s F\<^sub>3) ri "
  using selection_inst_def by auto

subsection\<open>Projection\<close>

text\<open>Projection semantics\<close>

definition projection_inst :: "attr set \<Rightarrow> rel_inst \<Rightarrow> rel_inst" ("\<pi>")
  where "projection_inst aset ri = { t|`aset | t. t\<in>ri }"

subsubsection\<open>Projection Properties\<close>

lemma proj_emptyset[simp]:"\<pi> l {} = {}"
  by  (auto simp:projection_inst_def )

lemma proj_empty[simp]:"ri \<noteq> {} \<Longrightarrow> \<pi> {} ri = {empty}"
  by  (auto simp:projection_inst_def )

lemma proj_inter_compos:"ri \<noteq> {} \<Longrightarrow> j \<inter> k = {} \<Longrightarrow> \<pi> j ( \<pi> k ri) =  {empty} "
  apply (auto simp:projection_inst_def inf_sup_aci(1)   )
 proof -
  fix x :: "char list \<Rightarrow> val option"
  assume a1: "x \<in> ri"
  assume a2: "j \<inter> k = {}"
  have "x |` {} = Map.empty"
    by simp
  then show "\<exists>f. Map.empty = f |` j \<and> (\<exists>fa. f = fa |` k \<and> fa \<in> ri)"
    using a2 a1 by (metis (no_types) inf_commute restrict_restrict)
qed

lemma projection_idem[simp]:"\<pi> j ( \<pi> j ri)  =  \<pi> j ri"
  apply (auto simp:projection_inst_def )
proof-
  fix t
  assume 1:" t \<in> ri "
  show "\<exists>ta. t |` j = ta |` j \<and> (\<exists>t. ta = t |` j \<and> t \<in> ri) "
    apply(rule exI[of _ "(t |` j)"])
    apply auto
    apply (rule exI[of _ "t"])
    using  \<open>t \<in> ri\<close> by auto
qed

lemma proj_subseteq_compos1:"k \<subseteq> j \<Longrightarrow> \<pi> j ( \<pi> k ri) = \<pi> k ri "
  apply (auto simp:projection_inst_def )
  apply (metis inf.orderE )  
  by (metis inf.orderE restrict_restrict)  

lemma proj_subseteq_compos2:" j \<subseteq> k \<Longrightarrow> \<pi> j ( \<pi> k ri) = \<pi> j ri"
  by (auto simp:projection_inst_def  inf.absorb_iff2;metis restrict_restrict  )

lemma inter_restrict_restrict:"ta \<in> ri \<Longrightarrow> \<exists>t. ta |` (k \<inter> j) = t |` k \<and> (\<exists>ta. t = ta |` j \<and> ta \<in> ri)"
  by(auto simp:inf_commute intro:exI[of _ "ta |` j"])

lemma projection_commute: "\<pi> j ( \<pi> k ri) = \<pi> k ( \<pi> j ri)"
  by (auto simp:projection_inst_def inter_restrict_restrict)

lemma proj_merge_inter:
  shows "\<pi> j ( \<pi> k ri) = \<pi> (j \<inter> k) ri"
  apply (auto simp:projection_inst_def)
  using inf_commute inf_sup_aci(1) restrict_restrict projection_inst_def
  by metis+

subsection\<open>Natural Join\<close>

text\<open>Natural Join semantics\<close>

definition natjoin_inst :: "rel_inst \<Rightarrow> rel_inst \<Rightarrow> rel_inst" (infixr " \<Join>" 70)
  where "natjoin_inst ri1 ri2 = { t1 ++ t2  | t1 t2. 
     t1 |` (dom t1 \<inter> dom t2)  = t2 |`(dom t1 \<inter> dom t2)
     \<and> t1\<in>ri1 \<and> t2\<in>ri2 }"

lemma natjoin_inst_alt_def: "natjoin_inst ri1 ri2 = { t1 ++ t2 | t1
t2. 
    t1\<in>ri1 \<and> t2\<in>ri2 \<and> ( let common_attrs = dom t1 \<inter> dom t2 in
      \<forall>attr\<in>common_attrs. t1 attr = t2 attr
    ) }"
    apply (auto simp: natjoin_inst_def restrict_map_def)
    apply (metis (no_types, hide_lams) Int_iff )  
  by fastforce  


subsubsection\<open>Auxiliary lemmmas\<close>

lemma finite_set_map_add:
   "finite ri1 \<Longrightarrow> finite ri2 \<Longrightarrow> finite {t1++t2| t1 t2 . t1 \<in> ri1 \<and> t2\<in> ri2}"
  using  finite_image_set2[of "\<lambda>x. x \<in> ri1" "\<lambda>x. x \<in> ri2"]
  by auto

lemma subset_set_map_add:
  "{t1++t2| t1 t2.  P t1 t2 \<and> t1 \<in> ri1 \<and> t2 \<in> ri2} \<subseteq> {t1++t2| t1 t2 . t1 \<in> ri1 \<and> t2\<in> ri2}"
  by auto

lemma natjoin_subset_set_map_add:
  "(ri1 \<Join> ri2) \<subseteq> {t1++t2| t1 t2 . t1 \<in> ri1 \<and> t2\<in> ri2}"
  using subset_set_map_add natjoin_inst_def
  by auto

lemma finite_set_map_add_P:"finite ri1 \<Longrightarrow> finite ri2 \<Longrightarrow> finite{t1++t2| t1 t2.  P t1 t2 \<and> t1 \<in> ri1 \<and> t2 \<in> ri2}"
  using  subset_set_map_add[of P ri1 ri2] finite_subset finite_set_map_add[of ri1 ri2]
  by auto

lemma wt_rel_tuples_dom_eq_dom:"\<And> t1 t2.  wt_rel rs ri \<Longrightarrow>  t1 \<in> ri \<Longrightarrow>  t2 \<in> ri  \<Longrightarrow>  dom t1 = dom t2 "
  by (auto;metis (full_types) domD domI wt_rel_def  wt_tuple_def)+

subsubsection\<open>Natural Join Properties\<close>

text\<open>The absorbing element is {}\<close>

lemma natjoin_empty_r[simp]:" ri \<Join> {} = {}"
  by(auto simp: natjoin_inst_def)

lemma natjoin_empty_l[simp]:"{} \<Join> ri = {}"
  by(auto simp: natjoin_inst_def)

text\<open>{empty} is the identity element \<close>

lemma natjoin_id_r[simp]:"ri \<Join> {empty} = ri"
 by(auto simp: natjoin_inst_def)

lemma natjoin_id_l[simp]:"{empty} \<Join> ri = ri"
  by(auto simp: natjoin_inst_def)

text\<open>Natural Join is idempotent\<close>

lemma natjoin_join_self_l:" wt_rel rs ri \<Longrightarrow> ri \<Join> ri  \<subseteq> ri"
  apply(cases "ri = {}")
  apply simp
  unfolding  natjoin_inst_def 
  apply rule
  apply simp
proof-
  fix x
  assume a: "ri \<noteq> {}" and b: "\<exists>t1 t2.
             x = t1 ++ t2 \<and>
             t1 |` (dom t1 \<inter> dom t2) = t2 |` (dom t1 \<inter> dom t2) \<and> t1 \<in> ri \<and> t2 \<in> ri"
            and c:"wt_rel rs ri"
  show "x \<in> ri "
  proof-  
    from a have "\<exists>t1. t1 \<in> ri" by blast
    then obtain t1 where 0:"t1 \<in> ri" by blast
      have 1:"t1 = t1 ++ t1"  by (simp add: map_add_subsumed2)
    from b obtain t1 where "\<exists>t2. x = t1 ++ t2 \<and>
            t1 |` (dom t1 \<inter> dom t2) = t2 |` (dom t1 \<inter> dom t2) \<and> t1 \<in> ri \<and> t2 \<in> ri" by blast
    from this obtain t2 where 2:"x = t1 ++ t2 \<and>
            t1 |` (dom t1 \<inter> dom t2) = t2 |` (dom t1 \<inter> dom t2) \<and> t1 \<in> ri \<and> t2 \<in> ri" by blast
    from c this have 3:"dom t1 = dom t2" using wt_rel_tuples_dom_eq_dom  by presburger
    from 2 this have 4:"t1 = t2"  by (metis inf.idem map_add_subsumed1 map_add_subsumed2 map_le_def restrict_in)
    from 2 this  have 5:"x = t1 ++ t1" by simp
    have "t1 = t1 ++ t1"  by (simp add: map_add_subsumed2)
    from this 5  have "x = t1 "  by presburger
    then  show "x \<in> ri" using 2 by auto
  qed 
qed

lemma natjoin_join_self_r:" ri \<subseteq>  ri \<Join> ri"
 apply(cases "ri = {}")
  apply simp
  unfolding  natjoin_inst_def 
  apply rule
  apply simp
  proof-
  fix x
  assume   b:"ri \<noteq> {}" and c: " x \<in> ri"
  have 1:"x = x++x"  by (simp add: map_add_subsumed1)
  
  show "\<exists>t1 t2.
                x = t1 ++ t2 \<and>
                t1 |` (dom t1 \<inter> dom t2) = t2 |` (dom t1 \<inter> dom t2) \<and> t1 \<in> ri \<and> t2 \<in> ri "
    using 1 c by blast 
qed

theorem natjoin_join_self:" wt_rel rs ri \<Longrightarrow> ri \<Join> ri  = ri"
  using natjoin_join_self_l natjoin_join_self_r by fast


text\<open> \<Join> is commutative\<close>

lemma natjoin_le:"ri1 \<Join> ri2 \<subseteq> ri2 \<Join> ri1"
 unfolding natjoin_inst_def
  apply (auto)
proof-
  fix t1 t2
  assume 1:"t1 |` (dom t1 \<inter> dom t2) = t2 |` (dom t1 \<inter> dom t2)" and 
         2:" t1 \<in> ri1"and 3:" t2 \<in> ri2"
  from 1 have 4:"t1 \<subseteq>\<^sub>m t1 ++ t2" unfolding map_le_def restrict_map_def 
    by (metis Int_iff map_add_dom_app_simps(1) map_add_dom_app_simps(3))
  from 4 map_le_iff_map_add_commute have " t1 ++ t2 = t2 ++ t1" by blast
  then show "\<exists>t1a t2a.
              t1 ++ t2 = t1a ++ t2a \<and>
              t1a |` (dom t1a \<inter> dom t2a) = t2a |` (dom t1a \<inter> dom t2a) \<and>
              t1a \<in> ri2 \<and> t2a \<in> ri1" 
    by (metis "1" "2" "3" Int_commute)
 qed

theorem natjoin_comm:"ri1 \<Join> ri2 = ri2 \<Join> ri1"
  using natjoin_le map_le_iff_map_add_commute
  by blast

text\<open> \<Join> is associative\<close>

lemma natjoin_assoc_subseteq1:" ri1 \<Join> (ri2 \<Join> ri3) \<subseteq>  (ri1 \<Join> ri2) \<Join> ri3 "
  unfolding natjoin_inst_def
  apply simp
  apply rule
  apply simp
proof-
  fix x 
  assume a:
            "\<exists>t1 t2.
            x = t1 ++ t2 \<and>
            t1 |` (dom t1 \<inter> dom t2) = t2 |` (dom t1 \<inter> dom t2) \<and>
            t1 \<in> ri1 \<and>
            (\<exists>t1 t2a.
                t2 = t1 ++ t2a \<and>
                t1 |` (dom t1 \<inter> dom t2a) = t2a |` (dom t1 \<inter> dom t2a) \<and>
                t1 \<in> ri2 \<and> t2a \<in> ri3)"
  from this obtain t1 where" \<exists>t2.
            x = t1 ++ t2 \<and>
            t1 |` (dom t1 \<inter> dom t2) = t2 |` (dom t1 \<inter> dom t2) \<and>
            t1 \<in> ri1 \<and>
            (\<exists>t1 t2a.
                t2 = t1 ++ t2a \<and>
                t1 |` (dom t1 \<inter> dom t2a) = t2a |` (dom t1 \<inter> dom t2a) \<and>
                t1 \<in> ri2 \<and> t2a \<in> ri3)" by blast
  from this obtain t1' where 1: "
            x = t1 ++ t1' \<and>
            t1 |` (dom t1 \<inter> dom t1') = t1' |` (dom t1 \<inter> dom t1') \<and>
            t1 \<in> ri1 \<and>
            (\<exists>t1 t2a.
                t1' = t1 ++ t2a \<and>
                t1 |` (dom t1 \<inter> dom t2a) = t2a |` (dom t1 \<inter> dom t2a) \<and>
                t1 \<in> ri2 \<and> t2a \<in> ri3)" by blast
    from this obtain t2  where "\<exists>t2a.
                t1' = t2 ++ t2a \<and>
                t2 |` (dom t2 \<inter> dom t2a) = t2a |` (dom t2 \<inter> dom t2a) \<and>
                t2 \<in> ri2 \<and> t2a \<in> ri3"  by blast
    from this obtain t3 where 2:"
                t1' = t2 ++ t3 \<and>
                t2 |` (dom t2 \<inter> dom t3) = t3 |` (dom t2 \<inter> dom t3) \<and>
                t2 \<in> ri2 \<and> t3 \<in> ri3" by blast
    from 1 have 3:"x = t1 ++ t1'" by simp
    from 1 have 4:"t1 |` (dom t1 \<inter> dom t1') = t1' |` (dom t1 \<inter> dom t1')" by simp
    from 1 have 5:"t1 \<in> ri1" by simp
    from 3 2 have 6:" x = (t1 ++ t2) ++ t3" by simp
    from 2  4  have 7:"t1 |` (dom t1 \<inter> dom t2) = t2 |` (dom t1 \<inter> dom t2)" 
      unfolding restrict_map_def
      apply -
      apply(rule ext)
      apply (auto split:if_splits)
      by (metis (no_types, lifting) domI map_add_dom_app_simps(1) map_add_dom_app_simps(3) option.inject)
    from 2 4 have 8:"(t1 ++ t2) |` (dom (t1 ++ t2) \<inter> dom t3) = t3 |` (dom (t1 ++ t2) \<inter> dom t3)" 
       unfolding restrict_map_def
       apply -
       apply(rule ext)
       apply (auto split:if_splits)
       apply (metis domI option.inject)
       by (metis (no_types, lifting) domI domIff map_add_Some_iff) 
      then  show "\<exists>t1 t2.
            x = t1 ++ t2 \<and>
            t1 |` (dom t1 \<inter> dom t2) = t2 |` (dom t1 \<inter> dom t2) \<and>
            (\<exists>t1a t2.
                t1 = t1a ++ t2 \<and>
                t1a |` (dom t1a \<inter> dom t2) = t2 |` (dom t1a \<inter> dom t2) \<and>
                t1a \<in> ri1 \<and> t2 \<in> ri2) \<and>
            t2 \<in> ri3" using 2 5 6 7 by blast 
    qed

lemma  natjoin_assoc_subseteq2:"(ri1 \<Join> ri2) \<Join> ri3  \<subseteq>  ri1 \<Join> (ri2 \<Join> ri3)"
 unfolding natjoin_inst_def
  apply simp
  apply rule
  apply simp
proof-
  fix x 
  assume a:"\<exists>t1 t2.
            x = t1 ++ t2 \<and>
            t1 |` (dom t1 \<inter> dom t2) = t2 |` (dom t1 \<inter> dom t2) \<and>
            (\<exists>t1a t2.
                t1 = t1a ++ t2 \<and>
                t1a |` (dom t1a \<inter> dom t2) = t2 |` (dom t1a \<inter> dom t2) \<and>
                t1a \<in> ri1 \<and> t2 \<in> ri2) \<and>
            t2 \<in> ri3"

  from this obtain t1' t3 where 1:"
            x = t1' ++ t3 \<and>
            t1' |` (dom t1' \<inter> dom t3) = t3 |` (dom t1' \<inter> dom t3) \<and>
            (\<exists>t1a t2.
                t1' = t1a ++ t2 \<and>
                t1a |` (dom t1a \<inter> dom t2) = t2 |` (dom t1a \<inter> dom t2) \<and>
                t1a \<in> ri1 \<and> t2 \<in> ri2) \<and>
            t3 \<in> ri3" by blast
  from this obtain t1 t2 where 2:"(
                t1' = t1 ++ t2 \<and>
                t1 |` (dom t1 \<inter> dom t2) = t2 |` (dom t1 \<inter> dom t2) \<and>
                t1 \<in> ri1 \<and> t2 \<in> ri2)" by blast
  from 1 2 have 3:"t2 |`(dom t2 \<inter> dom t3) = t3 |` (dom t2 \<inter> dom t3)" 
    unfolding restrict_map_def
    apply-
    apply(rule ext)
    apply(auto split:if_splits)
    by (metis (no_types, lifting) domI map_add_dom_app_simps(1) option.inject)
  from 1 have 4:"t1' |` (dom t1' \<inter> dom t3) = t3 |` (dom t1' \<inter> dom t3)" by simp
  from 2 4 have 5:"t1 |` (dom t1  \<inter> dom (t2 ++ t3)) = (t2 ++ t3) |` (dom t1 \<inter> dom (t2 ++ t3) ) " 
       unfolding restrict_map_def
       apply -
       apply(rule ext)
       apply (auto split:if_splits)
       apply (metis (no_types, lifting) option.inject domI domIff map_add_Some_iff)  
       by (metis (no_types, lifting) map_add_dom_app_simps(3) map_add_find_right)
     from 1 2 have 6:"x = t1 ++ (t2 ++ t3)"  by auto
     from 1 2 have 7:"t1 \<in> ri1 \<and> t2 \<in> ri2 \<and> t3 \<in> ri3" by argo
     from 6 5 3 7 show "\<exists>t1 t2.
            x = t1 ++ t2 \<and>
            t1 |` (dom t1 \<inter> dom t2) = t2 |` (dom t1 \<inter> dom t2) \<and>
            t1 \<in> ri1 \<and>
            (\<exists>t1 t2a.
                t2 = t1 ++ t2a \<and>
                t1 |` (dom t1 \<inter> dom t2a) = t2a |` (dom t1 \<inter> dom t2a) \<and>
                t1 \<in> ri2 \<and> t2a \<in> ri3)" 
      by blast
  qed

theorem natjoin_assoc:"(ri1  \<Join> ri2)  \<Join> ri3 = ri1  \<Join> ri2  \<Join> ri3"
  using natjoin_assoc_subseteq1 natjoin_assoc_subseteq2
  by fast
  
text\<open>If sort of ri1 and ri2 is the same than (ri1 \<Join> ri2) = (ri1 \<inter> ri2) \<close>

lemma join_subseteq_inter:
assumes "wt_rel rs1 ri1" and "wt_rel rs2 ri2" and "rs1 = rs2"
shows "(ri1 \<Join> ri2) \<subseteq> (ri1 \<inter> ri2) "
 using assms  unfolding natjoin_inst_def
  apply auto
  apply (metis (no_types, lifting) inf.idem map_add_subsumed2 map_le_def restrict_in wt_rel_def wt_tuple_def)
  by (metis (no_types, lifting) restrict_map_def  inf.idem map_add_subsumed1 map_le_def  wt_rel_def wt_tuple_def)
 
lemma inter_subseteq_join:
shows "(ri1 \<inter> ri2) \<subseteq>  (ri1 \<Join> ri2)"
 by (auto simp: natjoin_inst_def;metis (no_types, lifting) map_add_subsumed2 map_le_def)
 
theorem join_inter:"wt_rel rs1 ri1 \<Longrightarrow> wt_rel rs2 ri2 \<Longrightarrow> rs1=rs2 
      \<Longrightarrow> (ri1 \<Join> ri2) = (ri1 \<inter> ri2)"
  using inter_subseteq_join join_subseteq_inter
  by (simp add: subset_antisym)

text\<open>If sort of ri1 and ri2 have no attributes in commune
      then (ri1 \<Join> ri2) = (ri1 \<times> ri2) \<close>

definition natjoin_inst_pairs :: "rel_inst \<Rightarrow> rel_inst \<Rightarrow>(tuple \<times> tuple) set" (infixl " \<Join>\<^sub>p" 70)
  where"ri1 \<Join>\<^sub>p ri2 = { (t1, t2)  | t1 t2. t1 |` (dom t1 \<inter> dom t2) = t2 |`(dom t1 \<inter> dom t2)
     \<and> t1\<in>ri1 \<and> t2\<in>ri2 }"

lemma natjoin_pairs_prod:"wt_rel rs1 ri1  \<Longrightarrow> wt_rel rs2 ri2  \<Longrightarrow> dom rs1 \<inter> dom rs2 = {}  \<Longrightarrow>       
   (ri1 \<Join>\<^sub>p ri2) = (ri1 \<times> ri2)"
  by ( auto simp add:natjoin_inst_pairs_def wt_rel_def wt_tuple_def)

lemma natjoin_pairs_prod_card:"wt_rel rs1 ri1  \<Longrightarrow> wt_rel rs2 ri2  \<Longrightarrow> dom rs1 \<inter> dom rs2 = {}  \<Longrightarrow> 
   finite  ri1 \<Longrightarrow> finite ri2 \<Longrightarrow>
   card (ri1 \<Join>\<^sub>p ri2) = card (ri1 \<times> ri2)"
  by (auto simp:natjoin_pairs_prod)

lemma natjoin_pairs_prod_card_mult:"wt_rel rs1 ri1  \<Longrightarrow> wt_rel rs2 ri2  \<Longrightarrow> dom rs1 \<inter> dom rs2 = {} \<Longrightarrow> card ri1 = n  \<Longrightarrow> card ri2 = m  \<Longrightarrow>
  finite  ri1 \<Longrightarrow> finite ri2 \<Longrightarrow>       
  card (ri1 \<Join>\<^sub>p ri2) = n * m"
  by (auto simp:natjoin_pairs_prod_card)

definition join_pairs_tuples :: "(tuple \<times> tuple) set \<Rightarrow> rel_inst" where
 "join_pairs_tuples ri\<^sub>p = { t1 ++ t2  | t1 t2. (t1,t2)\<in> ri\<^sub>p} "

text\<open>Functional Correctness  natjoin_inst_pairs \<close>

lemma natjoin_pairs_correct:"(ri1 \<Join> ri2) = join_pairs_tuples (ri1 \<Join>\<^sub>p ri2)"
  unfolding join_pairs_tuples_def natjoin_inst_pairs_def natjoin_inst_def
  by force
lemma natjoin_prod:"wt_rel rs1 ri1  \<Longrightarrow> wt_rel rs2 ri2  \<Longrightarrow> dom rs1 \<inter> dom rs2 = {}  \<Longrightarrow>       
   (ri1 \<Join> ri2) = join_pairs_tuples (ri1 \<times> ri2)"
  by ( fastforce simp add:natjoin_inst_pairs_def wt_rel_def join_pairs_tuples_def  natjoin_pairs_correct wt_tuple_def)

lemma natjoin_prod_card:"wt_rel rs1 ri1  \<Longrightarrow> wt_rel rs2 ri2  \<Longrightarrow> dom rs1 \<inter> dom rs2 = {}  \<Longrightarrow>
  finite  ri1 \<Longrightarrow> finite ri2
   \<Longrightarrow> card (ri1 \<Join> ri2) = card (join_pairs_tuples (ri1 \<times> ri2))"
  by ( auto simp add:natjoin_prod )
  
subsection\<open>Renaming\<close>

definition renaming_inst :: "(attr \<rightharpoonup> attr)\<Rightarrow>  rel_inst  \<Rightarrow> rel_inst" ("\<rho>") where
"\<rho> f ri= {  (rename t f) | t.  t \<in> ri }"

subsection\<open>Union\<close>  

definition union_inst :: "rel_inst \<Rightarrow> rel_inst \<Rightarrow>  rel_inst"  (infixl " \<union>\<^sub>r" 65) where
"union_inst ri1 ri2 = ri1 \<union> ri2 "

subsection\<open>Difference\<close>

definition diff_inst :: "rel_inst \<Rightarrow> rel_inst \<Rightarrow>  rel_inst"  (infixl "-\<^sub>r" 65) where 
" ri1 -\<^sub>r ri2 = ri1 - ri2 " 

section\<open>well-typness of Relational Algebra operators\<close>

text\<open>selection is well typed\<close>

lemma selection_wt:
  assumes "selection_pre F rs"
  assumes "wt_rel rs ri"
  shows "wt_rel (selection_schema F rs) (\<sigma> F ri)"
  using assms(2)  
  unfolding wt_rel_def selection_inst_def
  by auto  

text\<open>Projection is well typed\<close>

lemma projection_wt:
  assumes "projection_pre aset rs"
  assumes "wt_rel rs ri"
  shows   "wt_rel (projection_schema aset rs) (projection_inst aset ri)"  
  using assms
  unfolding projection_pre_def projection_schema_def projection_inst_def wt_rel_def wt_tuple_def
  apply (auto)
  apply (metis domD domI option.simps(3) restrict_map_def)  
  apply (meson domI domIff restrict_out)
  by (metis option.simps(3) restrict_map_def)

text\<open>Natural Join is well typed\<close>

lemma natjoin_wt:
  assumes "natjoin_pre rs1 rs2"
  assumes "wt_rel rs1 ri1"
  assumes "wt_rel rs2 ri2"
  shows "wt_rel (natjoin_schema rs1 rs2) (natjoin_inst ri1 ri2)"
  using assms
  unfolding wt_rel_def natjoin_schema_def natjoin_inst_def natjoin_pre_def 
 apply (auto simp add: wt_tuple_def domI map_le_def  )
 apply (metis domD domI map_add_Some_iff)
 apply (metis (full_types) domD domI map_add_dom_app_simps(1) map_add_dom_app_simps(3))
 apply blast
 apply blast
  by (metis domI domIff)+

text\<open>Renaming is well typed\<close>

lemma renaming_wt:
assumes "rename_pre rs f "
assumes "wt_rel rs ri" 
shows "wt_rel (renaming_schema rs f) (\<rho> f ri)"
unfolding  renaming_schema_def renaming_inst_def 
using assms apply(auto simp add:rename_def rename_pre_set_def wt_rel_def wt_tuple_def )
apply (metis domD domIff  option.distinct(1) )
apply (metis  domD domI domIff)
apply (metis domD option.simps(3))    
apply (auto simp add: domD domI)
apply (metis domI domIff)     
apply (metis domI domIff)
apply (meson option.distinct(1))
apply (metis domD)
  by (metis domIff)

text\<open>Union is well typed\<close>

lemma union_wt:
  assumes "union_pre rs1 rs2"
  assumes "wt_rel rs1 ri1"
  assumes "wt_rel rs2 ri2"
  shows "wt_rel (union_schema rs1 rs2) (ri1 \<union>\<^sub>r ri2)"
 using assms
 unfolding wt_rel_def union_schema_def union_inst_def union_pre_def
 by (auto)

(***************diff*****************)
text\<open>Difference is well typed\<close>

lemma diff_wt1a:
  assumes "diff_pre rs1 rs2"(*Not necessary*)
  assumes "wt_rel rs1 ri1"
  assumes "wt_rel rs2 ri2"(*Not necessary*)
  shows "wt_rel (diff_schema rs1 rs2) ( ri1 -\<^sub>r ri2)"
 using assms
 unfolding wt_rel_def diff_schema_def diff_inst_def diff_pre_def
 by (auto)

lemma diff_wt1b:
 assumes "wt_rel rs1 ri1"
 shows "wt_rel rs1 (ri1 -\<^sub>r ri2)"
 using assms
 unfolding wt_rel_def diff_inst_def 
 by (auto)

lemma diff_wt2:
  assumes "diff_pre rs1 rs2"(*Now it is necessary*)
  assumes "wt_rel rs1 ri1"
  assumes "wt_rel rs2 ri2"(*Now it is  necessary*)
  shows "wt_rel (diff_schema rs2 rs1) ( ri1 -\<^sub>r ri2)"
 using assms
 unfolding wt_rel_def diff_schema_def diff_inst_def diff_pre_def 
 by(auto)


section\<open>Relational algebra operators preserve finiteness\<close>

lemma sel_preserve_instance_fin': 
  "finite ri \<Longrightarrow>  finite (\<sigma> F ri)"
  by(auto simp:selection_inst_def)

lemma proj_preserve_instance_fin': 
  "finite ri \<Longrightarrow>  finite (\<pi> l ri)"
  by(auto simp:projection_inst_def)

lemma natjoin_preserve_instance_fin:
   "finite ri1 \<Longrightarrow> finite ri2 \<Longrightarrow> finite (ri1 \<Join> ri2)"
  by(auto simp:natjoin_inst_def finite_set_map_add_P)

lemma renaming_preserve_instance_fin:
   "finite ri \<Longrightarrow> finite (\<rho> f ri) "
  by(auto simp:renaming_inst_def)

lemma union_preserve_instance_fin:
   "finite ri1 \<Longrightarrow> finite ri2 \<Longrightarrow> finite ( ri1 \<union>\<^sub>r ri2)"
  by(auto simp:union_inst_def)

lemma diff_preserve_instance_fin:
   "finite ri1  \<Longrightarrow> finite ( ri1 -\<^sub>r ri2)"
  by(auto simp:diff_inst_def)

section\<open> The equivalence-preserving SPJR algebra rewrite rules\<close>

lemma merge_select:" \<sigma> F\<^sub>2 (\<sigma> F\<^sub>1 ri ) = \<sigma> (F\<^sub>1 \<and>\<^sub>s F\<^sub>2) ri "
  by  (induction F\<^sub>1 arbitrary: ri)(auto simp:selection_inst_def)

lemma merge_project[simp]: 
  assumes "wt_rel rs ri" and "j \<subseteq> k" and "projection_pre k rs"
  shows " \<pi> j ( \<pi> k ri) = \<pi> j ri"
  using assms
  apply (auto simp: projection_pre_def projection_inst_def wt_rel_def)
  apply (metis inf.absorb_iff2)
  by (metis inf.absorb_iff2 restrict_restrict)

lemma push_select_through_singleton: 
  assumes   "wt_rel rs ri"
        and "dom rs \<inter> {A} = {}"
        and "B \<in> dom rs"
        shows"\<sigma> (A =\<^sub>a B) ({[A \<mapsto> a]} \<Join> ri)  = {[A \<mapsto> a]} \<Join>  (\<sigma> (B =\<^sub>c a ) ri )"
proof
  show "\<sigma> (A =\<^sub>a B) ({[A \<mapsto> a]} \<Join> ri) \<subseteq> {[A \<mapsto> a]} \<Join>  (\<sigma> (B =\<^sub>c a ) ri )" 
   unfolding selection_inst_def  natjoin_inst_def   
   apply (auto)
   apply(rule exI[of _ "a"])
 prefer 2 
apply (metis assms(2) assms(3) disjoint_iff_not_equal dom_empty dom_fun_upd fun_upd_same map_add_dom_app_simps(2) option.distinct(1) restrict_map_insert)
proof -
  fix t2 :: "char list \<Rightarrow> val option"
  assume a1: "t2 \<in> ri"
  assume a2: "\<forall>t2a. A \<in> dom t2a \<and> (t2a \<in> ri \<longrightarrow> [A \<mapsto> a] = t2a |` {A} \<longrightarrow> t2a |` {A} ++ t2 = t2a |` {A} ++ t2a \<longrightarrow> t2a B \<noteq> Some a) \<or> A \<notin> dom t2a \<and> (t2a \<in> ri \<longrightarrow> [A \<mapsto> a] ++ t2 = [A \<mapsto> a] ++ t2a \<longrightarrow> t2a B \<noteq> Some a)"
  assume a3: "([A \<mapsto> a] ++ t2) A = ([A \<mapsto> a] ++ t2) B"
  have f4: "dom rs = dom t2 \<and> (\<forall>cs d v. rs cs \<noteq> Some d \<or> t2 cs \<noteq> Some v \<or> wt_val d v)"
    using a1 assms(1) wt_rel_def wt_tuple_def by auto
  then have "B \<in> dom t2"
    using assms(3) by blast
  then show "t2 A = Some a"
    using f4 a3 a2 a1 assms(2) by fastforce
next
    fix t2 :: "char list \<Rightarrow> val option"
  assume a1: "t2 \<in> ri"
  assume a2: "(t2 |` {A} ++ t2) A = (t2 |` {A} ++ t2) B"
  assume a3: "[A \<mapsto> a] = t2 |` {A}"
  have f4: "B \<notin> insert A (dom (Map.empty::char list \<Rightarrow> val option))"
    using assms(2) assms(3) by fastforce
  have "t2 A = Some a"
    using a3 by (metis (no_types) fun_upd_eqD restrict_map_insert)
  then have "t2 B = Some a"
    using f4 a2 by (simp add: map_add_dom_app_simps(2))
  then show "\<exists>f. (A \<in> dom f \<longrightarrow> t2 |` {A} ++ t2 = t2 |` {A} ++ f \<and> t2 |` {A} = f |` {A} \<and> f \<in> ri \<and> f B = Some a) \<and> (A \<notin> dom f \<longrightarrow> t2 |` {A} ++ t2 = t2 |` {A} ++ f \<and> f \<in> ri \<and> f B = Some a)"
      using a1 by blast
qed   
   next
     show " {[A \<mapsto> a]} \<Join>  (\<sigma> (B =\<^sub>c a ) ri ) \<subseteq> \<sigma> (A =\<^sub>a B) ({[A \<mapsto> a]} \<Join> ri)" 
       unfolding selection_inst_def  natjoin_inst_def   
       apply (auto)
       apply (simp add: map_add_Some_iff restrict_map_insert)+
       using fun_upd_eqD by fastforce 
qed

lemma push_join_through_select1:
  assumes "wt_rel rs1 ri1"
      and "wt_rel rs2 ri2"
      and "selection_pre F rs1"
      and "dom rs1 \<inter> dom rs2 = {}"
    shows "((\<sigma> F ri1) \<Join> ri2) = \<sigma> F (ri1 \<Join> ri2)"
proof
  show "\<sigma> F ri1  \<Join> ri2 \<subseteq> \<sigma> F (ri1  \<Join> ri2)" 
    using assms
  proof(induction F arbitrary: ri1 ri2)
    case (Eqc A c)
    then obtain d where 1:" rs1 A = Some d \<and> wt_val d c" using selection_pre_def by auto
    from assms this have 2:"rs2 A = None" by blast 
    show ?case
    proof(auto)
      fix t
      assume 3:"t \<in> \<sigma> (A  =\<^sub>c c) ri1  \<Join> ri2" 
      from this  have 4:"t \<in> ri1  \<Join> ri2" 
        by (auto simp:selection_inst_def natjoin_inst_def)
      from  2 3 Eqc(2) have "t A =Some c"  
        apply (auto simp:selection_inst_def natjoin_inst_def wt_rel_def wt_tuple_def)
        by (metis domIff map_add_dom_app_simps(3))
      from this 4 show  "t \<in> \<sigma> (A  =\<^sub>c c) (ri1  \<Join> ri2)" 
        by (auto simp:selection_inst_def natjoin_inst_def)     
    qed
  next
    case (Eqa A B)
     then obtain d1 d2 where 1:" rs1 A = Some d1 \<and> rs1 B = Some d2  \<and> d1 = d2" 
          using selection_pre_def by auto
    from assms this have 2:"rs2 A = None \<and> rs2 B = None" by blast 
    then show ?case
     proof(auto)
      fix t
      assume 3:"t \<in> \<sigma> (A  =\<^sub>a B) ri1  \<Join> ri2" 
      from this have 4:"t \<in> ri1  \<Join> ri2" 
        by (auto simp:selection_inst_def natjoin_inst_def)
      from  2 3 Eqa(2) have "t A =t B"  
        apply (auto simp:selection_inst_def natjoin_inst_def wt_rel_def wt_tuple_def)
        by (metis domIff map_add_dom_app_simps(3))
      from this 4 show  "t \<in> \<sigma> (A  =\<^sub>a B) (ri1  \<Join> ri2)" 
        by (auto simp:selection_inst_def natjoin_inst_def)     
    qed
  next
    case (And F1 F2)
    then show ?case 
 proof(auto)
      fix t
      assume 3:"t \<in> \<sigma> (F1 \<and>\<^sub>s F2)  ri1  \<Join> ri2" 
      from this have 4:"t \<in> ri1  \<Join> ri2" 
        by (auto simp:selection_inst_def natjoin_inst_def)
      from 3  And(1)[of ri1 ri2] And(3,4,5,6) have 5:"t \<in> \<sigma> F1 (ri1  \<Join> ri2)"  
        by(auto simp:selection_inst_def  natjoin_inst_def)
        from 3  And(2)[of ri1 ri2] And(3,4,5,6) have 6:"t \<in> \<sigma> F2 (ri1  \<Join> ri2)"  
          by(auto simp:selection_inst_def  natjoin_inst_def)
      from this 4 3 5 6 show  "t \<in> \<sigma> (F1 \<and>\<^sub>s F2) (ri1  \<Join> ri2)" 
        by (auto simp:selection_inst_def natjoin_inst_def)     
    qed
  qed
next
  show " \<sigma> F (ri1  \<Join> ri2) \<subseteq> \<sigma> F ri1  \<Join> ri2" 
  using assms
  proof(induction F arbitrary: ri1 ri2)
    case (Eqc A c)
    then obtain d where 1:" rs1 A = Some d \<and> wt_val d c" using selection_pre_def by auto
    from assms this have 2:"rs2 A = None" by blast 
    show ?case
    proof(auto)
      fix t
      assume 3:"t \<in> \<sigma> (A  =\<^sub>c c) (ri1  \<Join> ri2)" 
      from this Eqc  show "t \<in> \<sigma> (A  =\<^sub>c c) ri1  \<Join> ri2" 
        apply (auto simp:selection_inst_def natjoin_inst_def)
        by (metis "2" domI domIff wt_rel_def wt_tuple_def) 
    qed
next
 case (Eqa A B)
     then obtain d1 d2 where 1:" rs1 A = Some d1 \<and> rs1 B = Some d2  \<and> d1 = d2" 
          using selection_pre_def by auto
    from assms this have 2:"rs2 A = None \<and> rs2 B = None" by blast 
    then show ?case
     proof(auto)
      fix t
      assume 3:"t \<in> \<sigma> (A  =\<^sub>a B) (ri1  \<Join> ri2)" 
      from this show "t \<in> \<sigma> (A  =\<^sub>a B) ri1 \<Join> ri2" 
        apply (auto simp:selection_inst_def natjoin_inst_def)
        by (metis (no_types, lifting) "2" Eqa.prems(2) domIff map_add_dom_app_simps(3) wt_rel_def wt_tuple_def)
      qed

next
  case (And F1 F2)
  then show ?case 
 proof(auto)
      fix t
      assume 3:"t \<in> \<sigma> (F1 \<and>\<^sub>s F2)  (ri1  \<Join> ri2)" 
      from this have 4:"t \<in> ri1  \<Join> ri2" 
        by (auto simp:selection_inst_def natjoin_inst_def)
      from And have "selection_pre F1 rs1" by (auto)
      from And this have "\<sigma> F1 (ri1  \<Join> ri2) \<subseteq> \<sigma> F1 ri1  \<Join> ri2" by auto
      from  3  have 5:"t \<in> \<sigma> F1 (ri1  \<Join> ri2)" 
        by (auto simp:selection_inst_def natjoin_inst_def) 
     from And  have 6:"t \<in> \<sigma> F1 ri1  \<Join> ri2" 
       using \<open>t \<in> \<sigma> F1 (ri1 \<Join> ri2)\<close> by auto
     from And(3) have 7:"wt_rel rs1 (\<sigma> F1 ri1)" by (auto simp: selection_inst_def wt_rel_def)
     from And have 8:"selection_pre F2 rs1"  by (auto)
     from And(4,6) 7 8  And.IH(2)[of "\<sigma> F1 ri1 " ri2]
     have 9:"\<sigma> F2 (\<sigma> F1 ri1  \<Join> ri2) \<subseteq> \<sigma> F2 (\<sigma> F1 ri1)  \<Join> ri2"  by auto
     have "t \<in> \<sigma> F2 (\<sigma> F1 ri1  \<Join> ri2)" 
       using "3" "6" selection_inst_def by auto
     hence "t \<in> \<sigma> F2 (\<sigma> F1 ri1)  \<Join> ri2" using "9" by auto
     thus "t \<in> \<sigma> (F1 \<and>\<^sub>s F2) ri1  \<Join> ri2" using merge_select by auto
    qed
  qed
qed

lemma push_join_through_select2:
  assumes "wt_rel rs1 ri1"
   and    "wt_rel rs2 ri2"
          "selection_pre F rs2"
      and "dom rs1 \<inter> dom rs2 = {}"
    shows "( ri1 \<Join> (\<sigma> F ri2)) = \<sigma> F (ri1 \<Join> ri2)"
  by (metis inf_commute natjoin_comm push_join_through_select1 assms)

lemma push_join_through_project1:
  assumes "wt_rel rs1 ri1"
   and    "wt_rel rs2 ri2"
          "projection_pre l rs1"
      and "dom rs1 \<inter> dom rs2 = {}"
    shows "(\<pi> l ri1) \<Join>  ri2 = \<pi> (l\<union>dom rs2) (ri1 \<Join> ri2)"
proof
  show " \<pi> l ri1  \<Join> ri2 \<subseteq> \<pi> (l \<union> dom rs2) (ri1  \<Join> ri2)" 
  proof 
    fix t
    assume 1:"t \<in> \<pi> l ri1  \<Join> ri2"
    from 1 have "\<exists> t1 t2. t= t1++t2 \<and>
     t1 |` (dom t1 \<inter> dom t2)  = t2 |`(dom t1 \<inter> dom t2)
     \<and> t1\<in> (\<pi> l ri1) \<and> t2\<in>ri2" by(auto simp:  natjoin_inst_def)
    then obtain t1 t2 where 2:" t= t1++t2 \<and> t1 |` (dom t1 \<inter> dom t2)  = t2 |`(dom t1 \<inter> dom t2)
                        \<and> t1\<in> (\<pi> l ri1) \<and> t2\<in>ri2" by blast
    from this have 3:"t1\<in> (\<pi> l ri1)" by simp
    from this  have "\<exists>t1'.  t1 = t1' |` l \<and>  t1' \<in> ri1 " 
      by( auto simp:  projection_inst_def  ) 
    then obtain t1' where 4:"t1 = t1' |` l \<and>  t1' \<in> ri1" by auto
    then have 5:" t1' \<in> ri1" by blast
    from this assms(1) have 6:"dom t1' = dom rs1"
      by (auto simp:  wt_rel_def  wt_tuple_def)
    hence 7:"dom t1 = l " using 4  assms(3) projection_pre_def by auto
    from 2 have 8:"t2 \<in> ri2" by simp
    from assms 5 8 have 9:"dom t1' \<inter> dom t2 = {}" 
      using  wt_rel_def wt_tuple_def  
      by auto
    hence 10:"t1' |` (dom t1' \<inter> dom t2)  = t2 |`(dom t1' \<inter> dom t2)" by simp
    from 5 8 this have 11:"t1' ++ t2 \<in> ri1 \<Join> ri2" by( auto simp:natjoin_inst_def)
    have 12:"dom t1 \<subseteq> dom t1' "
      using "6" "7" assms(3) projection_pre_def by auto
    from 2 have 13:"t = t1 ++ t2 " by simp
    have 14:"t1 = t1' |` l  " 
      by (simp add: "4")
    hence 15:"t1 ++ t2 = t1' |` l  ++ t2 " by simp
    have 16:"l \<subseteq> dom t1'" using "12" "7" by auto
    have "dom t2 = dom rs2" 
      using "8" assms(2) wt_rel_def wt_tuple_def by auto
    hence "t1' |` l  ++ t2 = (t1' ++ t2) |` (l \<union> dom rs2)"
      apply -
      apply(auto simp: map_add_def restrict_map_def 9 16)
      apply(rule ext)
      by(auto split:option.splits)  
    hence 17:"t = (t1' ++ t2) |` (l \<union> dom rs2)" 
      by (simp add: "13" "15")
    from 11  have "(t1' ++ t2) |` (l \<union> dom rs2) \<in>  \<pi> (l \<union> dom rs2) (ri1  \<Join> ri2) "
      by(auto simp:projection_inst_def)
     thus  " t \<in> \<pi> (l \<union> dom rs2) (ri1  \<Join> ri2)"  
        by (auto simp: 17  projection_inst_def) 
  qed
next
  show "\<pi> (l \<union> dom rs2) (ri1  \<Join> ri2) \<subseteq> \<pi> l ri1  \<Join> ri2" 
    proof 
    fix t
    assume 1:" t \<in>\<pi> (l \<union> dom rs2) (ri1  \<Join> ri2)"
    from 1 have "\<exists> t' . t= t' |` ((l \<union> dom rs2)) \<and> t' \<in>  ri1 \<Join> ri2 " by(auto simp: projection_inst_def)
    then obtain t' where 2:"t = t' |` ((l \<union> dom rs2)) \<and> t' \<in>  ri1 \<Join> ri2 " by blast
    from this have 3:"t = t' |` (l \<union> dom rs2)" by simp
    from 2 have 4:"t' \<in>  ri1 \<Join> ri2" by simp
    from this  obtain t1 t2  where 5:"t' = t1 ++ t2 \<and>
     t1 |` (dom t1 \<inter> dom t2) = t2 |` (dom t1 \<inter> dom t2) \<and> t1 \<in> ri1 \<and> t2 \<in> ri2 " 
      by( auto simp:  natjoin_inst_def)
    have 6:"l \<subseteq> dom t1" 
      by (meson "5" assms(1) assms(3) attr_dom_rs_t projection_pre_def rev_subsetD subsetI)
    hence 7:"dom(t1 |` l) \<inter> dom t2 = {}" 
      apply -
      apply(auto simp: wt_rel_def wt_tuple_def assms  "5" projection_pre_def)
      by (metis "5" IntI assms(2) assms(3) assms(4) domI empty_iff projection_pre_def set_rev_mp wt_rel_def wt_tuple_def)
    from 5 have 8:"t1 |` l \<in> \<pi> l ri1" 
      by(auto simp:projection_inst_def)
    from 7  have 9:"(t1 |` l)  |` (dom (t1 |` l ) \<inter> dom t2) = t2 |` (dom (t1 |` l ) \<inter> dom t2)" by auto
    from 5 8 9 have 10:"t1 |` l ++ t2 \<in>  \<pi> l ri1 \<Join> ri2" 
      using natjoin_inst_def by fastforce
    have 11:"dom t2 = dom rs2 " 
      using "5" assms(2) wt_rel_def wt_tuple_def by force
    have 12:" l \<subseteq> dom rs1" using assms(3) projection_pre_def by auto
    have 13:"l \<inter>dom rs2 = {} "using "11" "6" "7" by auto
    have 14:"(t1 ++ t2) |` (l \<union> dom rs2) = (t1 |` l) ++ t2|` dom rs2" 
      unfolding    map_add_def  restrict_map_def
      apply(rule ext)
      apply(auto  split: option.splits) 
      using "11" by blast+
     have 15:"(t1 |` l) ++ t2|` dom rs2 = (t1 |` l) ++ t2"   
        unfolding    map_add_def  restrict_map_def
        apply(rule ext)
      apply(auto  split: option.splits) 
        using "11" by blast+
      from this 14 have 16:"(t1 ++ t2) |` (l \<union> dom rs2) = (t1 |` l) ++ t2" 
        by argo
      from 5 this have 17:"t' |` (l \<union> dom rs2) = (t1 |` l) ++ t2" by fastforce
      from this 2 have 18:"t = t1 |` l ++ t2" by argo
      then show "t \<in> \<pi> l ri1 \<Join> ri2" 
        using "10" by blast
    qed
  qed 


lemma push_join_through_project2:
  assumes "wt_rel rs1 ri1"
   and    "wt_rel rs2 ri2"
          "projection_pre l rs2"
      and "dom rs1 \<inter> dom rs2 = {}"
    shows "ri1 \<Join>  (\<pi> l ri2)  = \<pi> (l \<union> dom rs1) (ri1 \<Join> ri2) "
  by (metis inf.commute  assms push_join_through_project1  natjoin_comm )

lemma push_select_through_project1:
   assumes "wt_rel rs ri" and
          "projection_pre l rs" and
          "selection_pre F  (rs |` l)"
        shows " \<sigma> F (\<pi> l ri)  \<subseteq> \<pi> l (\<sigma> F ri )"
  using assms 
proof(induction F arbitrary: l rs ri )
  case (Eqc A c)
   show ?case
  proof
  fix t 
     assume 1:"t \<in> \<sigma> (A  =\<^sub>c c) (\<pi> l ri)" 
     hence  2:"t \<in> (\<pi> l ri) \<and> eval_F  (A  =\<^sub>c c) t"  by (simp add: selection_inst_def)
     hence  3:"\<exists> t'. t = t' |` l \<and> t' \<in> ri "by (simp add: projection_inst_def)
     then obtain t' where 4:"t = t' |` l \<and> t' \<in> ri "  by auto
     from Eqc have 9:"l \<subseteq> dom rs" by(simp add: projection_pre_def )
     from Eqc(1) 4 have 10:"dom t' = dom rs" 
       using wt_rel_def wt_tuple_def by force   
     from 9 10 have 11:"l \<subseteq> dom t'" by auto
     have 12:"eval_F (A  =\<^sub>c c) t'"  by (metis "10" "2" "4" "9" domI dom_restrict eval_F.simps(1) inf.absorb_iff2 restrict_in)
     from this 4 have "t' \<in> \<sigma> (A  =\<^sub>c c) ri" by(auto simp:selection_inst_def) 
     thus "t \<in> \<pi> l (\<sigma> (A  =\<^sub>c c) ri)" using 4 by(auto simp:projection_inst_def)
   qed
next
  case (Eqa A B)
   show ?case 
   proof
     fix t 
     assume 1:"t \<in> \<sigma> (A  =\<^sub>a B) (\<pi> l ri)" 
     hence  2:"t \<in> (\<pi> l ri) \<and> eval_F  (A  =\<^sub>a B) t"  by (simp add: selection_inst_def)
     hence  3:"\<exists> t'. t = t' |` l \<and> t' \<in> ri "by (simp add: projection_inst_def)
     then obtain t' where 4:"t = t' |` l \<and> t' \<in> ri "  by auto
     from Eqa have 9:"l \<subseteq> dom rs" by(simp add: projection_pre_def )
     from Eqa(1) 4 have 10:"dom t' = dom rs" 
       using wt_rel_def wt_tuple_def by force   
     from 9 10 have 11:"l \<subseteq> dom t'" by auto
     from Eqa(3) have "(\<exists> d1 d2. (rs |` l) A = Some d1 \<and> (rs |` l) B = Some d2  \<and> d1 = d2 )" 
       by(simp  )
     then obtain d1 d2 where " (rs |` l) A = Some d1 \<and> (rs |` l) B = Some d2  \<and> d1 = d2 " by blast
     hence "A \<in> l \<and> B \<in> l" using domIff restrict_map_def 
       by (fastforce)
     then have "eval_F (A  =\<^sub>a B) t'" using 2 4 by auto
     hence "t' \<in> (\<sigma> (A  =\<^sub>a B) ri)" using 4 selection_inst_def by auto
     thus "t \<in> \<pi> l (\<sigma> (A  =\<^sub>a B) ri)" using projection_inst_def 4 by auto
qed
next
  case (And F1 F2)
    then have 20:"\<sigma> F1 (\<pi> l ri) \<subseteq> \<pi> l (\<sigma> F1 ri)"  by auto
    have 21:"wt_rel rs (\<sigma> F1 ri)" 
      using And.prems(1) selection_inst_def wt_rel_def by auto
    from  And.prems(3) have " selection_pre F2 (rs |` l)" by fastforce
    from  And.IH(2)[of rs "(\<sigma> F1 ri)" l ]  And(4) this 21
      have 22:"\<sigma> F2 (\<pi> l (\<sigma> F1 ri)) \<subseteq> \<pi> l (\<sigma> F2 (\<sigma> F1 ri))"  by blast 
   from 20 have 23:"\<sigma> F2 (\<sigma> F1 (\<pi> l ri)) \<subseteq> \<sigma> F2 (\<pi> l (\<sigma> F1 ri))" 
    by (simp add: selection_inst_def subset_iff)
  from this 22 have "\<sigma> F2 (\<sigma> F1 (\<pi> l ri)) \<subseteq> \<pi> l (\<sigma> F2 (\<sigma> F1 ri))" by blast
  thus "\<sigma>  (F1 \<and>\<^sub>s F2) (\<pi> l ri) \<subseteq> \<pi> l (\<sigma> (F1 \<and>\<^sub>s F2) ri)" by (simp add: merge_select)
qed

lemma push_select_through_project2:
   assumes "wt_rel rs ri" and
          "projection_pre l rs" and
          "selection_pre F  (rs |` l)"
        shows " \<pi> l (\<sigma> F ri )  \<subseteq>  \<sigma> F (\<pi> l ri)"  
 using assms 
proof(induction F arbitrary: l rs ri )
  case (Eqc A c)
  show ?case
  proof
  fix t 
     assume 1:"t \<in>  \<pi> l (\<sigma> (A  =\<^sub>c c) ri)" 
     hence  2:"\<exists> t'. t = t' |` l \<and> t' \<in> (\<sigma> (A  =\<^sub>c c) ri) "
       by (simp add: projection_inst_def)
     then obtain t' where 3:"t = t' |` l \<and> t' \<in> (\<sigma> (A  =\<^sub>c c) ri)" 
        by auto
     then have 4:"t' \<in> ri" 
        by(auto simp: selection_inst_def )
     from 3 have 5:"t = t' |` l " 
       by argo
     from Eqc have 9:"l \<subseteq> dom rs" 
       by(simp add: projection_pre_def )
     from Eqc(1) 4  have 10:"dom t' = dom rs" 
       using wt_rel_def wt_tuple_def  by force   
     from 9 10 have 11:"l \<subseteq> dom t'" 
       by auto
     have 12: "t \<in> ( \<pi> l ri)" 
       using 5 4 projection_inst_def by auto 
     have "eval_F (A  =\<^sub>c c) ( t' |` l)" 
       by (metis (mono_tags, lifting) "10" "11" "3" CollectD Eqc.prems(3) domI dom_restrict eval_F.simps(1) inf.absorb_iff2 restrict_in selection_inst_def selection_pre_def selection_valid_F.simps(1))
     thus "t \<in>  \<sigma> (A  =\<^sub>c c) ( \<pi> l ri)"
       using 12 5 selection_inst_def  by blast
   qed
 next
  case (Eqa A B)
  show ?case 
  proof
  fix t 
     assume 1:"t \<in>  \<pi> l (\<sigma> (A  =\<^sub>a B) ri)" 
     hence  2:"\<exists> t'. t = t' |` l \<and> t' \<in> (\<sigma> (A  =\<^sub>a B) ri) "
       by (simp add: projection_inst_def)
     then obtain t' where 3:"t = t' |` l \<and> t' \<in> (\<sigma> (A  =\<^sub>a B) ri)" 
        by auto
     then have 4:"t' \<in> ri" 
        by(auto simp: selection_inst_def )
     from 3 have 5:"t = t' |` l " 
       by argo
     from Eqa have 9:"l \<subseteq> dom rs" 
       by(simp add: projection_pre_def )
     from Eqa(1) 4  have 10:"dom t' = dom rs" 
       using wt_rel_def wt_tuple_def  by force   
     from 9 10 have 11:"l \<subseteq> dom t'" 
       by auto
     have 12: "t \<in> ( \<pi> l ri)" 
       using 5 4 projection_inst_def by auto 
     have "eval_F (A  =\<^sub>a B) ( t' |` l)" 
       by (metis (mono_tags, lifting) "10" "11" "3" CollectD Eqa.prems(3) domI dom_restrict eval_F.simps(2) inf.absorb_iff2 restrict_in selection_inst_def selection_pre_def selection_valid_F.simps(2))
     thus "t \<in>  \<sigma> (A  =\<^sub>a B) ( \<pi> l ri)"
       using 12 5 selection_inst_def  by blast
   qed
next
  case (And F1 F2)
  then have 1:"\<pi> l (\<sigma> F1 ri) \<subseteq> \<sigma> F1 (\<pi> l ri)" 
    by auto
  have 2:"wt_rel rs (\<sigma> F1 ri)" 
    using And.prems(1) selection_inst_def wt_rel_def by auto
  have 3:"selection_pre  F2 (rs |` l)" using And(5) 
    by simp
  from And.IH(2) And.IH(2)[of rs "(\<sigma> F1 ri)" l ] 2 And(4) 3
  have 4:"\<pi> l (\<sigma> F2 (\<sigma> F1 ri)) \<subseteq> \<sigma> F2 (\<pi> l (\<sigma> F1 ri))" 
    by blast
  from 1  this have "\<pi> l (\<sigma> (F1 \<and>\<^sub>s F2) ri) \<subseteq>   \<sigma> (F1 \<and>\<^sub>s F2) (\<pi> l ri)"
    using selection_inst_def merge_select by auto
  then show ?case by auto
qed

theorem push_select_through_project:
" \<lbrakk>wt_rel rs ri; projection_pre l rs; selection_pre F (rs |` l)\<rbrakk>
 \<Longrightarrow>  \<sigma> F (\<pi> l ri) =  \<pi> l (\<sigma> F ri) "
  using push_select_through_project1 push_select_through_project2 
  by blast

theorem push_project_through_union:
"\<pi> l ( ri1 \<union>\<^sub>r ri2) = (\<pi> l ri1) \<union>\<^sub>r \<pi> l ri2"
  by( auto simp:projection_inst_def union_inst_def)  

(*End of Classical relational algebra *)
end