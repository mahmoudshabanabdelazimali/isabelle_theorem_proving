theory Relational_Model_Examples 
  imports Bag_Relational_Algebra
begin
definition stud_rels :: rel_sign where
  "stud_rels = [ ''Name'' \<mapsto> STRING, ''Age'' \<mapsto> INTEGER ]"

definition stud_rel3 :: rel_sign where
 "stud_rel3 = [''Id'' \<mapsto> INTEGER,''Name'' \<mapsto> STRING, ''Age'' \<mapsto> INTEGER]"

lemma  "arity stud_rels = 2"
  unfolding arity_def stud_rels_def
  by simp

lemma "dom stud_rels = {''Name'' , ''Age''}"
  unfolding  stud_rels_def 
  apply auto
  done

definition course_dom :: rel_sign where
  "course_dom = [ ''Id'' \<mapsto> INTEGER, ''Name'' \<mapsto> STRING ]"

lemma "dom course_dom = {''Id'' , ''Name''}"
  unfolding sort_def course_dom_def 
  apply auto
  done

definition stud_dbs :: db_schema where
  "stud_dbs = [ ''students'' \<mapsto> stud_rels, ''courses'' \<mapsto> course_dom ]"

lemma  "arity stud_dbs = 2"
  unfolding arity_def stud_dbs_def
  by simp

lemma "sort stud_dbs ''students'' = dom stud_rels"
  unfolding sort_def stud_rels_def stud_dbs_def 
  apply auto
  done

lemma "dom stud_dbs = {''students'', ''courses''}"
  unfolding   stud_dbs_def 
  apply auto
  done

lemma "arity  [''Name'' \<mapsto> Sv ''Mahmoud2'', ''Age'' \<mapsto> Iv 25] = 2"
  by(simp add:arity_def)

definition t1::"tuple" where 
  "t1 = [''Name'' \<mapsto> Sv ''Mahmoud'', ''Age'' \<mapsto> Iv 26]"
definition t2::"tuple" where 
  "t2 = [''Name'' \<mapsto> Sv ''Mahmoud'', ''Age'' \<mapsto> Iv 26, ''MTK'' \<mapsto> Iv 1]"
definition
  "V = {''Name'', ''Age''}"

value "the ((t2\<langle>V\<rangle>) ''Name'')"

lemma "t2\<langle>V\<rangle> = [''Name'' \<mapsto> Sv ''Mahmoud'', ''Age'' \<mapsto> Iv 26]"
  by(auto simp add: V_def t2_def restrict_map_def restrict_tuple_def)

value "the ((stud_rels\<langle>{''Name''}\<rangle>) ''Name'')"


lemma "wt_val STRING (the (t1 ''Name''))"
  by(auto simp: t1_def) 

lemma "wt_tuple stud_rels t1"
  by(simp add:stud_rels_def t1_def wt_tuple_def)

lemma "\<not>wt_tuple stud_rels t2"
proof
  assume a:"wt_tuple stud_rels t2 "
  from a have "  dom [''Name'' \<mapsto> STRING, ''Age'' \<mapsto> INTEGER] =
  dom [''Name'' \<mapsto> Sv ''Mahmoud'', ''Age'' \<mapsto> Iv 26, ''MTK'' \<mapsto> Iv 1]"
    unfolding wt_tuple_def stud_rels_def t2_def by blast
  hence "{''Name'' , ''Age''} = {''Name'' , ''Age'', ''MTK''}" 
    by (simp add: insert_commute)
  hence 1:"card {''Name'' , ''Age''} = card {''Name'' , ''Age'', ''MTK''}" 
    by auto
  have 2:"card {''Name'' , ''Age''} = (2::nat)" by simp 
  have 3:"card {''Name'' , ''Age'',''MTK''} = (3::nat)" by simp
  have "(2::nat) = 3" using 1 2 3 by linarith
  have "2\<noteq>3" using \<open>2 = 3\<close> by auto
  from \<open>(2::nat) = 3\<close>  \<open>2\<noteq>3\<close> show "False"  by simp
qed


lemma "t1 |` {''Name''} = [''Name'' \<mapsto> Sv ''Mahmoud'']"
  by(auto simp add:restrict_map_def t1_def)


lemma "selection_valid_F (''Name'' =\<^sub>c Sv ''Mahmoud'') [''Name'' \<mapsto> STRING,''age'' \<mapsto> INTEGER]"
  by auto
lemma "\<not>selection_valid_F (''Name'' =\<^sub>c Iv 1) [''Name'' \<mapsto> STRING,''age'' \<mapsto> INTEGER]"
  by auto
lemma "\<not>selection_valid_F (''surname'' =\<^sub>c Sv ''Mahmoud'') [''Name'' \<mapsto> STRING,''age'' \<mapsto> INTEGER]"
  by auto
lemma "selection_valid_F (''Name'' =\<^sub>c Sv ''Ali'') [''Name'' \<mapsto> STRING,''age'' \<mapsto> INTEGER]"
  by auto
lemma "selection_valid_F (''Name'' =\<^sub>a ''Surname'') [''Name'' \<mapsto> STRING,''Surname'' \<mapsto> STRING]"
  by auto
lemma "\<not>selection_valid_F (''Name'' =\<^sub>a ''age'') [''Name'' \<mapsto> STRING,''Surname'' \<mapsto> STRING]"
  by auto
lemma "\<not>selection_valid_F (''Name2'' =\<^sub>a ''Surname2'') [''Name'' \<mapsto> STRING ,''Surname'' \<mapsto> STRING]"
  by simp


lemma "eval_F (''Name'' =\<^sub>c Sv ''Mahmoud'') [''Name'' \<mapsto> Sv ''Mahmoud'',''age'' \<mapsto> Iv 27]"
  by auto

lemma "\<not>eval_F (''Name'' =\<^sub>c Iv 1) [''Name'' \<mapsto> Sv ''Mahmoud'',''age'' \<mapsto> Iv 27]"
  by auto
 
lemma "\<not> eval_F (''Name'' =\<^sub>c Sv ''Ali'') [''Name'' \<mapsto> Sv ''Mahmoud'',''age'' \<mapsto> Iv 27]"
  by auto

lemma "\<not>eval_F (''Name'' =\<^sub>a ''Surname'') [''Name'' \<mapsto> Sv ''Mahmoud'',''age'' \<mapsto> Iv 27]"
  by simp

lemma "\<not>eval_F (''Name'' =\<^sub>a ''Surname'') [''Name'' \<mapsto> Sv ''Mahmoud'',''Surname'' \<mapsto> Sv ''Ali'']"
  by simp

lemma "eval_F (''Name'' =\<^sub>a ''Surname'') [''Name'' \<mapsto> Sv ''Mahmoud'',''Surname'' \<mapsto> Sv ''Mahmoud'']"
  by simp
lemma "\<not>eval_F (''Name'' =\<^sub>a ''Surname2'') [''Name'' \<mapsto> Sv ''Mahmoud'',''Surname'' \<mapsto> Sv ''Mahmoud'']"
  by simp
lemma "\<not>eval_F (''Name2'' =\<^sub>a ''Surname'') [''Name'' \<mapsto> Sv ''Mahmoud'',''Surname'' \<mapsto> Sv ''Mahmoud'']"
  by simp
(*False Positive :selection precondition is needed to get ride of these cases*)
lemma "eval_F (''Name2'' =\<^sub>a ''Surname2'') [''Name'' \<mapsto> Sv ''Mahmoud'',''Surname'' \<mapsto> Sv ''Mahmoud'']"
  by simp

(*This could be a False Positive :so selection precondition is needed 
  to make sure that values are well typed*)
lemma "eval_F (''Name'' =\<^sub>a ''Surname'') [''Name'' \<mapsto> Iv 1,''Surname'' \<mapsto> Iv 1]"
  by simp

(*The selection precondition is violated*)
lemma "\<not>eval_F (''surname'' =\<^sub>c Sv ''Mahmoud'') [''Name'' \<mapsto> Sv ''Mahmoud'',''age'' \<mapsto> Iv 27]"
  oops

lemma "selection_schema F stud_rels =  stud_rels" 
  by simp

lemma "projection_pre {''Name''} stud_rel3" 
  unfolding projection_pre_def stud_rel3_def
  apply auto 
  done 

lemma "projection_schema {''Name''} stud_rel3 = [''Name'' \<mapsto> STRING]"
  by(auto simp:projection_schema_def stud_rel3_def)

lemma "natjoin_pre [''Name'' \<mapsto> STRING, ''Age'' \<mapsto> INTEGER] [''Name'' \<mapsto> STRING, ''ID'' \<mapsto> INTEGER ] "
  unfolding natjoin_pre_def 
  by auto

lemma "natjoin_pre [''Name'' \<mapsto> STRING, ''Age'' \<mapsto> INTEGER] empty"
  unfolding natjoin_pre_def
  by auto
  
lemma "natjoin_pre [''Surname'' \<mapsto> STRING,''ID'' \<mapsto> INTEGER] [''Name'' \<mapsto> STRING, ''Age'' \<mapsto> INTEGER]"
  unfolding natjoin_pre_def
  by auto

lemma "natjoin_pre [''Name'' \<mapsto> STRING,''ID'' \<mapsto> INTEGER] [''Name'' \<mapsto> STRING, ''Age'' \<mapsto> INTEGER]"
  unfolding natjoin_pre_def
  by auto

lemma "\<not>natjoin_pre [''Name'' \<mapsto> STRING,''ID'' \<mapsto> INTEGER] [''Name'' \<mapsto> STRING, ''ID'' \<mapsto> STRING]"
  unfolding natjoin_pre_def
  by fastforce


lemma "natjoin_schema [''Surname'' \<mapsto> STRING,''ID'' \<mapsto> INTEGER] 
                      [''Name'' \<mapsto> STRING, ''Age'' \<mapsto> INTEGER] = 
 [''ID'' \<mapsto> INTEGER, ''Name'' \<mapsto> STRING, ''Surname'' \<mapsto> STRING, ''Age'' \<mapsto> INTEGER]"   
  unfolding natjoin_schema_def
  by auto

lemma "natjoin_schema empty [''Name'' \<mapsto> STRING, ''Age'' \<mapsto> INTEGER] = 
                            [''Name'' \<mapsto> STRING, ''Age'' \<mapsto> INTEGER]"   
  unfolding natjoin_schema_def
  by auto

lemma "renaming_schema [''A'' \<mapsto> STRING,''C'' \<mapsto> STRING] [''A''\<mapsto>''B'']
                 = [''B'' \<mapsto> STRING,''C'' \<mapsto> STRING]"
  unfolding renaming_schema_def rename_def
  by auto

lemma "renaming_schema [''A'' \<mapsto> STRING,''C'' \<mapsto> STRING] empty
                 = [''A'' \<mapsto> STRING,''C'' \<mapsto> STRING]"
  unfolding renaming_schema_def rename_def
  by auto

lemma "renaming_schema [''A'' \<mapsto> STRING,''C'' \<mapsto> INTEGER] [''C'' \<mapsto> ''A'',''A'' \<mapsto> ''C''] k
                 = [''A'' \<mapsto> INTEGER,''C'' \<mapsto> STRING] k"
  unfolding renaming_schema_def rename_def
  by(auto)

(*Alert: A and B has different type*)
lemma " rename_pre [''A'' \<mapsto> STRING,''C'' \<mapsto> INTEGER] [''C'' \<mapsto> ''A'',''A'' \<mapsto> ''C'']"
  unfolding rename_pre_set_def
  by auto
lemma "(rename [''A'' \<mapsto> STRING,''C'' \<mapsto> INTEGER] [''C'' \<mapsto> ''A'',''A'' \<mapsto> ''C'']) ''C''  = Some STRING"
  unfolding rename_def
  by (auto)

lemma "(rename [''A'' \<mapsto> STRING,''C'' \<mapsto> INTEGER] [''C'' \<mapsto> ''A'',''A'' \<mapsto> ''C'']) ''A''  = Some INTEGER"
  unfolding rename_def
  by (auto)

lemma "\<not> rename_preserve_type [''A'' \<mapsto> STRING,''C'' \<mapsto> INTEGER] [''C'' \<mapsto> ''A'',''A'' \<mapsto> ''C'']"
  unfolding rename_def rename_preserve_type_def
  apply rule
  by (auto split:if_splits)
 
(*Alert: A has two values in the renaming function! *)
lemma " rename_pre [''A'' \<mapsto> STRING] [''A'' \<mapsto> ''A'',''A'' \<mapsto> ''C'']"
  unfolding rename_pre_set_def
  by auto

lemma " rename_pre [''A'' \<mapsto> STRING] [''A'' \<mapsto> ''A'']"
  unfolding rename_pre_set_def
  by auto

(*rename_pre2 does not allow  an attribute to have two values in the renaming function*)
lemma " \<not>rename_pre2 [''A'' \<mapsto> STRING,''C'' \<mapsto> INTEGER]  [''A'' \<mapsto> ''A'',''A'' \<mapsto> ''C'']"
  unfolding rename_pre_set2_def
  by auto

(*rename_pre2 does not allow swaping *)
lemma "\<not> rename_pre2 [''A'' \<mapsto> STRING,''C'' \<mapsto> INTEGER] [''C'' \<mapsto> ''A'',''A'' \<mapsto> ''C'']"
  unfolding rename_pre_set2_def
  by auto

lemma "\<not> rename_pre2 [''A'' \<mapsto> STRING] [''A'' \<mapsto> ''A'']"
  unfolding rename_pre_set2_def
  by auto

lemma "rename [''A'' \<mapsto> STRING] [''A'' \<mapsto> ''A''] k = rename  [''A'' \<mapsto> STRING] empty k "
  unfolding rename_def 
  by auto

value "(\<lambda>t. (the (t ''Name'')) = Sv ''Peter'') [''Name'' \<mapsto> Sv
''Mahmoud'', ''Age'' \<mapsto> Iv 15]"

(*Classical relational algebra*)
definition stud_inst2 :: rel_inst where
 "stud_inst2 = { 
                [''Name'' \<mapsto> Sv ''Mahmoud2'', ''Age'' \<mapsto> Iv 25],
                [''Name'' \<mapsto> Sv ''Peter'', ''Age'' \<mapsto> Iv 36] 
               }"
definition stud_inst :: rel_inst where
 "stud_inst = { 
                [''Name'' \<mapsto> Sv ''Mahmoud'', ''Age'' \<mapsto> Iv 25],
                [''Name'' \<mapsto> Sv ''Peter'', ''Age'' \<mapsto> Iv 36] 
               }"
 
lemma "\<not> stud_inst = stud_inst2"
  unfolding stud_inst_def stud_inst2_def  
  by (smt doubleton_eq_iff fun_upd_twist list.inject map_upd_eqD1 not_Cons_self2 val.inject(2))

definition course_inst :: rel_inst where
 "course_inst = { 
                  [ ''Id'' \<mapsto> Iv 761, ''Name'' \<mapsto> Sv ''Logics'' ],
                  [ ''Id'' \<mapsto> Iv 761, ''Name'' \<mapsto> Sv ''Semantics'' ] 
               }"

definition stud_db_inst :: db_inst where
"stud_db_inst = 
                [
                ''students'' \<mapsto> stud_inst, 
                ''courses'' \<mapsto> course_inst
                ]"

lemma "dom stud_db_inst = {''students'', ''courses''}"
  unfolding stud_db_inst_def
  apply auto
  done

definition db_inst :: db_inst where
  "db_inst = [ 
    ''students'' \<mapsto> { 
      [''Name'' \<mapsto> Sv ''Mahmoud'', ''Age'' \<mapsto> Iv 26],
      [''Name'' \<mapsto> Sv ''Peter'', ''Age'' \<mapsto> Iv 36]
    },
    ''courses'' \<mapsto> {
      [ ''Id'' \<mapsto> Iv 761, ''Name'' \<mapsto> Sv ''Logics'' ]
    }
  ]"

lemma "\<not>stud_db_inst = db_inst"
  unfolding stud_db_inst_def db_inst_def stud_inst_def course_inst_def 
  apply auto
proof -
  assume a1: "[''students'' \<mapsto> {[''Name'' \<mapsto> Sv ''Mahmoud'', ''Age'' \<mapsto> Iv 25],
                                [''Name'' \<mapsto> Sv ''Peter'', ''Age'' \<mapsto> Iv 36]}, 
               ''courses'' \<mapsto> {[''Id'' \<mapsto> Iv 761, ''Name'' \<mapsto> Sv ''Logics''],
                               [''Id'' \<mapsto> Iv 761, ''Name'' \<mapsto> Sv ''Semantics'']}] 
                             = 
             [''students'' \<mapsto> {[''Name'' \<mapsto> Sv ''Mahmoud'', ''Age'' \<mapsto> Iv 26], 
                               [''Name'' \<mapsto> Sv ''Peter'', ''Age'' \<mapsto> Iv 36]},
                ''courses'' \<mapsto> {[''Id'' \<mapsto> Iv 761, ''Name'' \<mapsto> Sv ''Logics'']}]"
have f2: "\<forall>f F. (f::char list \<Rightarrow> val option) \<notin> F \<or> insert f F = F"
  by blast
  have f3: "[''Id'' \<mapsto> Iv 761, ''Name'' \<mapsto> Sv ''Logics''] \<noteq> [''Id'' \<mapsto> Iv 761, ''Name'' \<mapsto> Sv ''Semantics'']"
    using map_upd_eqD1 by fastforce
  have f4: "\<forall>f cs F fa Fa. f(cs::char list \<mapsto> F::(char list \<Rightarrow> val option) set) \<noteq> fa(cs \<mapsto> Fa) \<or> F = Fa"
by (meson map_upd_eqD1)
  have "[''Id'' \<mapsto> Iv 761, ''Name'' \<mapsto> Sv ''Logics''] \<notin> {[''Id'' \<mapsto> Iv 761, ''Name'' \<mapsto> Sv ''Semantics'']}"
    using f3 by force
  then show False
    using f4 f2 a1 by (metis (no_types) Diff_insert_absorb insert_not_empty)
qed

lemma inv_fin:"invar_fin stud_dbs db_inst"
  by (auto simp:invar_fin_def stud_dbs_def db_inst_def course_dom_def stud_rels_def)

lemma "invar_db_c  stud_dbs db_inst" 
  by (simp add:invar_db_c_def inv_fin )

lemma "wt_rel stud_rels stud_inst"
  by(auto simp add:stud_rels_def stud_inst_def wt_rel_def wt_tuple_def)

(*Proved by using the wt_db_alt lemma*)
lemma wt_db_ex:"wt_db stud_dbs db_inst"
  by (auto simp:  wt_db_alt  stud_dbs_def db_inst_def  stud_rels_def  course_dom_def  wt_rel_def wt_tuple_def)

(*proved by using the definition of wt_db*)
lemma "wt_db stud_dbs stud_db_inst"
  by (auto simp:  wt_db_def  stud_dbs_def stud_db_inst_def  stud_rels_def  stud_inst_def course_inst_def course_dom_def  wt_rel_def wt_tuple_def)

lemma "invar_db stud_dbs db_inst"
  by (simp add:wt_db_ex inv_fin  invar_db_def )


lemma "sel_gen_to_primitive (((A =\<^sub>c a) \<and>\<^sub>s (B =\<^sub>c a)) \<and>\<^sub>s ((C =\<^sub>c a) \<and>\<^sub>s (D =\<^sub>c a))) ri 
=   (\<sigma> (A =\<^sub>c a) ( \<sigma> (B =\<^sub>c a) ( \<sigma>  (C =\<^sub>c a) (\<sigma>  (D =\<^sub>c a) ri))))"
  by auto

lemma "sel_gen_to_primitive (((A =\<^sub>c a) \<and>\<^sub>s (B =\<^sub>c a)) \<and>\<^sub>s ((C =\<^sub>c a) \<and>\<^sub>s (D =\<^sub>c a))) ri 
=  \<sigma> (((A =\<^sub>c a) \<and>\<^sub>s (B =\<^sub>c a)) \<and>\<^sub>s ((C =\<^sub>c a) \<and>\<^sub>s (D =\<^sub>c a))) ri"
 by (auto simp add: selection_inst_def)

lemma "selection_pre (''Name'' =\<^sub>c Sv ''Mahmoud'') stud_rels
     \<and> \<sigma> (''Name'' =\<^sub>c Sv ''Mahmoud'') stud_inst = 
                { 
                [''Name'' \<mapsto> Sv ''Mahmoud'', ''Age'' \<mapsto> Iv 25]
               }"
  by(auto simp: stud_rels_def selection_inst_def stud_inst_def split:)

lemma "selection_pre (''Id'' =\<^sub>c Iv 761) course_dom
         \<and> \<sigma> (''Id'' =\<^sub>c Iv 761) course_inst = course_inst"
  by(auto simp: course_dom_def course_inst_def  selection_inst_def split:)


lemma "(selection_pre (''Id'' =\<^sub>c Iv 769) course_dom
         \<and> \<sigma> (''Id'' =\<^sub>c Iv 769) course_inst = {})"
  by(auto simp add: course_dom_def course_inst_def  selection_inst_def split:)
  
definition stud_inst3 :: rel_inst where
 "stud_inst3 = { 
                [''Id'' \<mapsto> Iv 25,''Name'' \<mapsto> Sv ''Mahmoud'', ''Age'' \<mapsto> Iv 25],
                [''Id'' \<mapsto> Iv 27,''Name'' \<mapsto> Sv ''Ali'', ''Age'' \<mapsto> Iv 27],
                [''Id'' \<mapsto> Iv 1 , ''Name'' \<mapsto> Sv ''Peter'', ''Age'' \<mapsto> Iv 36] 
               }"

lemma "selection_pre (''Id'' =\<^sub>a ''Age'' ) stud_rel3 \<and> 
       \<sigma> (''Id'' =\<^sub>a ''Age'' ) stud_inst3  
         = {[''Id'' \<mapsto> Iv 25,''Name'' \<mapsto> Sv ''Mahmoud'', ''Age'' \<mapsto> Iv 25],
            [''Id'' \<mapsto> Iv 27,''Name'' \<mapsto> Sv ''Ali'', ''Age'' \<mapsto> Iv 27]
           }"
 by(auto simp: selection_inst_def stud_inst3_def stud_rel3_def)

lemma "selection_pre ((''Id'' =\<^sub>a ''Age'') \<and>\<^sub>s (''Name'' =\<^sub>c Sv ''Ali'')) stud_rel3 \<and> 
       \<sigma> ((''Id'' =\<^sub>a ''Age'') \<and>\<^sub>s (''Name'' =\<^sub>c Sv ''Ali'')) stud_inst3  
         = {
            [''Id'' \<mapsto> Iv 27,''Name'' \<mapsto> Sv ''Ali'', ''Age'' \<mapsto> Iv 27]
           }"
  by(auto simp: selection_inst_def stud_inst3_def  stud_rel3_def)


lemma "\<pi> {''a'',''c''} {[''a'' \<mapsto> Iv 1]} = {[''a'' \<mapsto> Iv 1]}"
  unfolding projection_inst_def 
  by auto


lemma "\<pi> {''a'', ''c''} ( \<pi> {''a'', ''b''} {[''a'' \<mapsto> Iv 1, ''b'' \<mapsto> Iv 1 ,''c'' \<mapsto> Iv 1]}) = \<pi> {''a''}
   {[''a'' \<mapsto> Iv 1, ''b'' \<mapsto> Iv 1 ,''c'' \<mapsto> Iv 1]}"
  unfolding projection_inst_def
  by auto

lemma "\<pi> {''a'', ''c''} ( \<pi> {''a'', ''b''} {[''a'' \<mapsto> Iv 1, ''b'' \<mapsto> Iv 1 ,''c'' \<mapsto> Iv 1]}) = \<pi> {''a''}
   {[''a'' \<mapsto> Iv 1, ''b'' \<mapsto> Iv 1 ,''c'' \<mapsto> Iv 1]}"
  unfolding projection_inst_def
  by auto

  
definition L:: "attr set" where "L = {''Name''}"

lemma "\<pi> L stud_inst3 =  {  [''Name'' \<mapsto> Sv ''Mahmoud''],
                            [''Name'' \<mapsto> Sv ''Ali''],  
                            [''Name'' \<mapsto> Sv ''Peter''] }"
  unfolding L_def projection_inst_def
  apply (auto simp:projection_inst_def stud_inst3_def )
  apply(rule exI[of _ "[''Id'' \<mapsto> Iv 25, ''Name'' \<mapsto> Sv ''Mahmoud'', ''Age'' \<mapsto> Iv 25]"])
  apply auto
  apply(rule exI[of _ "[''Id'' \<mapsto> Iv 27, ''Name'' \<mapsto> Sv ''Ali'', ''Age'' \<mapsto> Iv 27]"])
  apply auto
  apply(rule exI[of _ "[''Id'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''Peter'', ''Age'' \<mapsto> Iv 36]"])
  by auto
  

(*This is a False positive since ''F'' is not an attribute the belong to relation schema
 stud_rels3 that correspond to the relation instance stud_inst3
 despite the fact that it has no bad effect since it is mapped to None*)
lemma "\<pi> {''Name'',''F''} stud_inst3 =  { [''Name'' \<mapsto> Sv ''Mahmoud''],
                                          [''Name'' \<mapsto> Sv ''Ali''],
                                          [''Name'' \<mapsto> Sv ''Peter'']}"
  apply (auto simp:projection_inst_def stud_inst3_def  )
  apply(rule exI[of _ "[''Id'' \<mapsto> Iv 25, ''Name'' \<mapsto> Sv ''Mahmoud'', ''Age'' \<mapsto> Iv 25]"])
  apply auto
  apply(rule exI[of _ "[''Id'' \<mapsto> Iv 27, ''Name'' \<mapsto> Sv ''Ali'', ''Age'' \<mapsto> Iv 27]"])
  apply auto
  apply(rule exI[of _ "[''Id'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''Peter'', ''Age'' \<mapsto> Iv 36]"])
  apply auto
  done


lemma sel_ex1:"{}  \<subseteq>( {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
        [''ID'' \<mapsto> Iv 3, ''Name'' \<mapsto> Sv ''N'']} 
                    \<Join> 
       {[''ID'' \<mapsto> Iv 2, ''Course'' \<mapsto> Sv ''FDS''],[''ID'' \<mapsto> Iv 4, ''Course'' \<mapsto> Sv ''SE'']})
        "
  by (auto simp: natjoin_inst_def)
 
lemma sel_ex2:"{[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
        [''ID'' \<mapsto> Iv 3, ''Name'' \<mapsto> Sv ''N'']} 
                    \<Join> 
       {[''ID'' \<mapsto> Iv 2, ''Course'' \<mapsto> Sv ''FDS''],[''ID'' \<mapsto> Iv 4, ''Course'' \<mapsto> Sv ''SE'']}
       \<subseteq> {} "
   by (auto simp: natjoin_inst_def)

(*case where all values of the common attribute disagree 
we expect to get as result {}
*)
lemma " {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
        [''ID'' \<mapsto> Iv 3, ''Name'' \<mapsto> Sv ''N'']} 
                    \<Join> 
       {[''ID'' \<mapsto> Iv 2, ''Course'' \<mapsto> Sv ''FDS''],
        [''ID'' \<mapsto> Iv 4, ''Course'' \<mapsto> Sv ''SE'']}
       = {}"
  using sel_ex1 sel_ex2 by auto

lemma sel_ex3:"{[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'',''Course'' \<mapsto> Sv ''SE'']}
                  \<subseteq>(
             {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
              [''ID'' \<mapsto> Iv 3, ''Name'' \<mapsto> Sv ''N'']} 
                    \<Join> 
       {[''ID'' \<mapsto> Iv 2, ''Course'' \<mapsto> Sv ''FDS''],
        [''ID'' \<mapsto> Iv 4, ''Course'' \<mapsto> Sv ''SE''],
        [''ID'' \<mapsto> Iv 1, ''Course'' \<mapsto> Sv ''SE'']})
        "
  apply (auto simp: natjoin_inst_def)
  apply(rule exI[of _ " [''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'']"])
  apply(rule exI[of _ "[''ID'' \<mapsto> Iv 1, ''Course'' \<mapsto> Sv ''SE'']"])
  by (auto )

lemma sel_ex4:"(
             {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
              [''ID'' \<mapsto> Iv 3, ''Name'' \<mapsto> Sv ''N'']} 
                    \<Join> 
              {[''ID'' \<mapsto> Iv 2, ''Course'' \<mapsto> Sv ''FDS''],
              [''ID'' \<mapsto> Iv 4, ''Course'' \<mapsto> Sv ''SE''],
              [''ID'' \<mapsto> Iv 1, ''Course'' \<mapsto> Sv ''SE'']}) 
         \<subseteq> {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'',''Course'' \<mapsto> Sv ''SE'']}"
    by (auto simp: natjoin_inst_def)
            
(*Case where some tuples disagree on the common attribute  and some agree 
we expect to get as result {the join of every two tuple that agree}
*)
lemma "(      {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
              [''ID'' \<mapsto> Iv 3, ''Name'' \<mapsto> Sv ''N'']} 
                    \<Join> 
              {[''ID'' \<mapsto> Iv 2, ''Course'' \<mapsto> Sv ''FDS''],
               [''ID'' \<mapsto> Iv 4, ''Course'' \<mapsto> Sv ''SE''],
              [''ID'' \<mapsto> Iv 1, ''Course'' \<mapsto> Sv ''SE'']}) 
         = {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'',''Course'' \<mapsto> Sv ''SE'']}"
  using sel_ex3 sel_ex4 by auto

lemma sel_ex5:"{[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'',''Course'' \<mapsto> Sv ''SE''],
                [''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'',''Course'' \<mapsto> Sv ''FDS'']
                }
                  \<subseteq>(
             {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'']
              } 
                    \<Join> 
       {[''ID'' \<mapsto> Iv 1, ''Course'' \<mapsto> Sv ''FDS''],
        [''ID'' \<mapsto> Iv 1, ''Course'' \<mapsto> Sv ''SE'']})
        "
  unfolding  natjoin_inst_def 
  apply simp
  apply rule
  apply(rule exI[of _ "[''ID'' \<mapsto> Iv 1, ''Course'' \<mapsto> Sv ''SE'']"])  
  apply auto[1]
  apply(rule exI[of _ "[''ID'' \<mapsto> Iv 1, ''Course'' \<mapsto> Sv ''FDS'']"])  
  by auto
 
lemma sel_ex6:"(
             {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'']
              } 
                    \<Join> 
       {[''ID'' \<mapsto> Iv 1, ''Course'' \<mapsto> Sv ''FDS''],
        [''ID'' \<mapsto> Iv 1, ''Course'' \<mapsto> Sv ''SE'']})
        \<subseteq>
        {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'',''Course'' \<mapsto> Sv ''SE''],
         [''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'',''Course'' \<mapsto> Sv ''FDS'']
                }
        "
  by (force simp: natjoin_inst_def)


(*Case where all tuples agree on the common attribute 
we expect to get as result {the join of every two tuple that agree}
*)
lemma "({[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''] } 
                    \<Join> 
       {[''ID'' \<mapsto> Iv 1, ''Course'' \<mapsto> Sv ''FDS''],
        [''ID'' \<mapsto> Iv 1, ''Course'' \<mapsto> Sv ''SE'']})
        =
        {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'',''Course'' \<mapsto> Sv ''SE''],
         [''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'',''Course'' \<mapsto> Sv ''FDS'']
                }
        "
  using sel_ex5 sel_ex6 by auto

lemma sel_ex7:"{} \<subseteq> 
       {[''ID'' \<mapsto> Iv 4, ''Name'' \<mapsto> Sv ''A''],
        [''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''B'']} 
                    \<Join> 
        {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
         [''ID'' \<mapsto> Iv 3, ''Name'' \<mapsto> Sv ''C'']}
        "
   by (auto simp: natjoin_inst_def)
  

lemma sel_ex8:"  
       {[''ID'' \<mapsto> Iv 4, ''Name'' \<mapsto> Sv ''A''],
        [''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''B'']} 
                    \<Join> 
        {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
         [''ID'' \<mapsto> Iv 3, ''Name'' \<mapsto> Sv ''C'']}
       \<subseteq> {}
        "
unfolding  natjoin_inst_def 
proof(auto) 
  assume a1: "[''ID'' \<mapsto> Iv 4, ''Name'' \<mapsto> Sv ''A''] = [''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'']"
  have f2: "(1::int) \<noteq> 4"
    by auto
  have "[''Name'' \<mapsto> Sv ''A'', ''ID'' \<mapsto> Iv 1] = [''ID'' \<mapsto> Iv 4, ''Name'' \<mapsto> Sv ''A'']"
    using a1 by (simp add: fun_upd_twist)
  then have "[''Name'' \<mapsto> Sv ''A'', ''ID'' \<mapsto> Iv 1] = [''Name'' \<mapsto> Sv ''A'', ''ID'' \<mapsto> Iv 4]"
    by (simp add: fun_upd_twist)
  then show False
    using f2 by (meson fun_upd_eqD option.inject val.inject(1))
next
  assume "[''ID'' \<mapsto> Iv 4, ''Name'' \<mapsto> Sv ''A''] = [''ID'' \<mapsto> Iv 3, ''Name'' \<mapsto> Sv ''C'']"
  then show "False"
    using fun_upd_eqD by fastforce
next
  assume " [''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''B''] = [''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''] "
  then show "False"
    using fun_upd_eqD by fastforce
next 
  assume "[''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''B''] = [''ID'' \<mapsto> Iv 3, ''Name'' \<mapsto> Sv ''C'']"
  then show "False"
    using fun_upd_eqD by fastforce
qed

(*Case where the sort of both arguments agree, we get as result the intersection
in this particular case all tuples disagree then we expect {}
  *)
lemma "{[''ID'' \<mapsto> Iv 4, ''Name'' \<mapsto> Sv ''A''],
        [''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''B'']} 
                    \<Join> 
        {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
         [''ID'' \<mapsto> Iv 3, ''Name'' \<mapsto> Sv ''C'']}
       = {}"
 using sel_ex7 sel_ex8 by auto

lemma sel_ex9:"{[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'']} \<subseteq> 
       {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
        [''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''B'']} 
                    \<Join> 
        {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
         [''ID'' \<mapsto> Iv 3, ''Name'' \<mapsto> Sv ''C'']}
        "
 unfolding  natjoin_inst_def 
  apply auto
  apply(rule exI[of _ "[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'']"])
  apply(rule exI[of _ "[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'']"])
  by auto
 
lemma sel_ex10:" 
       {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
        [''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''B'']} 
                    \<Join> 
        {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
         [''ID'' \<mapsto> Iv 3, ''Name'' \<mapsto> Sv ''C'']}
         \<subseteq> 
        {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'']}
        "
  apply (auto simp: natjoin_inst_def)
  using fun_upd_eqD by fastforce

(*Case where the sort of both arguments agree, we get as result the \<inter>
in this particular {the join of every two tuple that agree}
 *)
lemma " 
       {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
        [''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''B'']} 
                    \<Join> 
        {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
         [''ID'' \<mapsto> Iv 3, ''Name'' \<mapsto> Sv ''C'']}
         =
        {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'']}
        "
  using sel_ex9 sel_ex10 by auto

(*This case all tuples agree, we will not have the empty tuple in the result of 
the intersection
 *)

lemma "{[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'']} =
       {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'']} 
                    \<Join> 
        {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'']}
        "
  by(auto simp: natjoin_inst_def)
  
lemma sel_ex11:"{[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
                 [''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''C''] }
         \<subseteq>
       {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
        [''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''C'']} 
                    \<Join> 
        {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
         [''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''C'']
        }
        "
  apply (auto simp: natjoin_inst_def)
   apply(rule exI[of _ "[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'']"])
   apply(rule exI[of _ "[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'']"])
   apply auto
   apply(rule exI[of _ "[''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''C'']"])
  apply(rule exI[of _ "[''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''C'']"])
  by auto
 

lemma sel_ex12:"{[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
         [''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''C'']} 
                    \<Join> 
        {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
         [''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''C'']
        }
      \<subseteq> 
      {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
       [''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''C'']
      }
      "
by(auto simp: natjoin_inst_def)

(*In this case both arguments are identicals and have the same sort*)
lemma  "{[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
         [''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''C'']} 
                    \<Join> 
        {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
         [''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''C'']
        }
      = 
      {[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
       [''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''C'']
      }
      "
  using sel_ex11 sel_ex12 by auto

lemma  sel_ex13:" {[ ''MTK'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'',''ID'' \<mapsto> Iv 25,''Course'' \<mapsto> Sv ''FDS'' ],
       [ ''MTK'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'',''ID'' \<mapsto> Iv 27,''Course'' \<mapsto> Sv ''SE'' ],
       [ ''MTK'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''C'',''ID'' \<mapsto> Iv 27,''Course'' \<mapsto> Sv ''SE'' ],
       [ ''MTK'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''C'',''ID'' \<mapsto> Iv 25,''Course'' \<mapsto> Sv ''FDS'' ]
      }
      \<subseteq> 
        {[''MTK'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
         [''MTK'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''C'']} 
                    \<Join> 
        {[''ID'' \<mapsto> Iv 25, ''Course'' \<mapsto> Sv ''FDS''],
         [''ID'' \<mapsto> Iv 27, ''Course'' \<mapsto> Sv ''SE'']
        }
      "
  unfolding natjoin_inst_def
  apply auto
     apply(rule exI[of _ "[''MTK'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'']"])
     apply(rule exI[of _ "[''ID'' \<mapsto> Iv 25, ''Course'' \<mapsto> Sv ''FDS'']"])
      apply auto[1]
     apply(rule exI[of _ "[''MTK'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'']"])
     apply(rule exI[of _ "[''ID'' \<mapsto> Iv 27, ''Course'' \<mapsto> Sv ''SE'']"])
    apply fastforce
       apply(rule exI[of _ "[''MTK'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''C'']"])
   apply(rule exI[of _ "[''ID'' \<mapsto> Iv 27, ''Course'' \<mapsto> Sv ''SE'']"])
   apply fastforce
    apply(rule exI[of _ "[''MTK'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''C'']"])
  apply(rule exI[of _ "[''ID'' \<mapsto> Iv 25, ''Course'' \<mapsto> Sv ''FDS'']"])
  by fastforce

lemma   sel_ex14:"{[''MTK'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
                  [''MTK'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''C'']} 
                    \<Join> 
        {[''ID'' \<mapsto> Iv 25, ''Course'' \<mapsto> Sv ''FDS''],
         [''ID'' \<mapsto> Iv 27, ''Course'' \<mapsto> Sv ''SE'']
        }
      \<subseteq> 
      {[ ''MTK'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'',''ID'' \<mapsto> Iv 25,''Course'' \<mapsto> Sv ''FDS'' ],
       [ ''MTK'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'',''ID'' \<mapsto> Iv 27,''Course'' \<mapsto> Sv ''SE'' ],
       [ ''MTK'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''C'',''ID'' \<mapsto> Iv 27,''Course'' \<mapsto> Sv ''SE'' ],
       [ ''MTK'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''C'',''ID'' \<mapsto> Iv 25,''Course'' \<mapsto> Sv ''FDS'' ]
      }
      "
by(fastforce simp: natjoin_inst_def) 


(*
 \<Join> is associative
*)
(*ri1 = ri2 = ri3*)
lemma "{[''A'' \<mapsto> Iv 1]} \<Join> {[''A'' \<mapsto> Iv 1]} \<Join> {[''A'' \<mapsto> Iv 1]}
      ={[''A'' \<mapsto> Iv 1]} \<Join> ({[''A'' \<mapsto> Iv 1]} \<Join> {[''A'' \<mapsto> Iv 1]})"
  unfolding natjoin_inst_def
  by auto
(*ri1 = ri2 = ri3*)
lemma "{[''A'' \<mapsto> Iv 1,''B'' \<mapsto> Iv 1]} \<Join> {[''B'' \<mapsto> Iv 1,''C'' \<mapsto> Iv 1]} \<Join> {[''C'' \<mapsto> Iv 1,''D'' \<mapsto> Iv 1]}
      ={[''A'' \<mapsto> Iv 1,''B'' \<mapsto> Iv 1]} \<Join> ({[''B'' \<mapsto> Iv 1,''C'' \<mapsto> Iv 1]} \<Join> {[''C'' \<mapsto> Iv 1,''D'' \<mapsto> Iv 1]})"
  unfolding natjoin_inst_def
  by auto

(*ri1 \<noteq> ri2 = ri3*)
lemma "{[''A'' \<mapsto> Iv 1]} \<Join> {[''A'' \<mapsto> Iv 2]} \<Join> {[''A'' \<mapsto> Iv 2]}
      ={[''A'' \<mapsto> Iv 1]} \<Join> ({[''A'' \<mapsto> Iv 2]} \<Join> {[''A'' \<mapsto> Iv 2]})"
  unfolding natjoin_inst_def
  by auto


(*ri1 = ri3 \<noteq> ri2*)
lemma "{[''A'' \<mapsto> Iv 2]} \<Join> {[''A'' \<mapsto> Iv 1]} \<Join> {[''A'' \<mapsto> Iv 2]}
      ={[''A'' \<mapsto> Iv 2]} \<Join> ({[''A'' \<mapsto> Iv 1]} \<Join> {[''A'' \<mapsto> Iv 2]})"
  unfolding natjoin_inst_def
  by auto

(*ri1 \<noteq> ri3 \<noteq> ri2*)
lemma "{[''A'' \<mapsto> Iv 3]} \<Join> {[''A'' \<mapsto> Iv 1]} \<Join> {[''A'' \<mapsto> Iv 2]}
      ={[''A'' \<mapsto> Iv 3]} \<Join> ({[''A'' \<mapsto> Iv 1]} \<Join> {[''A'' \<mapsto> Iv 2]})"
  unfolding natjoin_inst_def
  by auto


lemma a: "{[''a'' \<mapsto> Sv ''b''], [''a'' \<mapsto> Sv ''a'']} \<Join>\<^sub>p{ [''a'' \<mapsto> Sv ''a'']} =
                {([''a'' \<mapsto> Sv ''a''],[''a'' \<mapsto> Sv ''a''])}"
  unfolding natjoin_inst_pairs_def
  by auto

lemma  "{[''a'' \<mapsto> Sv ''a''],[''b'' \<mapsto> Sv ''a'', ''c'' \<mapsto> Sv ''a''] } \<Join>\<^sub>p{[''b'' \<mapsto> Sv ''a'', ''c'' \<mapsto> Sv ''a''], [''a'' \<mapsto> Sv ''a'']} =
                {([''b'' \<mapsto> Sv ''a'', ''c'' \<mapsto> Sv ''a''],[''a'' \<mapsto> Sv ''a'']),
                 ([''a'' \<mapsto> Sv ''a''],[''b'' \<mapsto> Sv ''a'', ''c'' \<mapsto> Sv ''a'']),
                ([''a'' \<mapsto> Sv ''a''],[''a'' \<mapsto> Sv ''a'']),
                ([''b'' \<mapsto> Sv ''a'', ''c'' \<mapsto> Sv ''a''],[''b'' \<mapsto> Sv ''a'', ''c'' \<mapsto> Sv ''a'']) 
                }"
  unfolding natjoin_inst_pairs_def
  by auto

lemma "\<forall>rs. \<not>wt_rel rs {[''a'' \<mapsto> Sv ''a''],[''b'' \<mapsto> Sv ''a'', ''c'' \<mapsto> Sv ''a''] } "
 unfolding wt_rel_def wt_tuple_def
 by auto

lemma "\<rho> [''A'' \<mapsto> ''B'']  { [''A'' \<mapsto> Iv 1] } = { [''B'' \<mapsto> Iv 1] }"
  unfolding renaming_inst_def rename_def
  by auto

lemma  "union_inst {[''x'' \<mapsto> Iv 1],[''z'' \<mapsto> Iv 1]} {[''y'' \<mapsto> Iv 1]} = {[''z'' \<mapsto> Iv 1], [''y'' \<mapsto> Iv 1],[''x'' \<mapsto> Iv 1]} "
unfolding union_inst_def
  by auto

lemma  "(diff_inst {[''x'' \<mapsto> Iv 1],[''z'' \<mapsto> Iv 1]} {[''y'' \<mapsto> Iv 1]}) = {[''z'' \<mapsto> Iv 1],[''x'' \<mapsto> Iv 1]} "
unfolding diff_inst_def 
  by auto

lemma " \<sigma> F\<^sub>4 (\<sigma> F\<^sub>3 (\<sigma> F\<^sub>2 (\<sigma> F\<^sub>1 ri))) =\<sigma> (F\<^sub>1 \<and>\<^sub>s F\<^sub>2 \<and>\<^sub>s F\<^sub>3 \<and>\<^sub>s  F\<^sub>4) ri " 
  by(auto simp:merge_select)

(*Relational Algebra on Bag*)
definition stud_inst2' :: rel_inst' where
 "stud_inst2' = {# 
                [''Name'' \<mapsto> Sv ''Mahmoud2'', ''Age'' \<mapsto> Iv 25],
                [''Name'' \<mapsto> Sv ''Peter'', ''Age'' \<mapsto> Iv 36] 
               #}"

definition stud_inst' :: rel_inst' where
 "stud_inst' = {# 
                [''Name'' \<mapsto> Sv ''Mahmoud'', ''Age'' \<mapsto> Iv 25],
                [''Name'' \<mapsto> Sv ''Mahmoud'', ''Age'' \<mapsto> Iv 25],
                [''Name'' \<mapsto> Sv ''Peter'', ''Age'' \<mapsto> Iv 36] 
               #}"

definition 
 "stud_inst4 = {# 
                [''Name'' \<mapsto> Sv ''M'', ''Age'' \<mapsto> Iv 25],
                [''Name'' \<mapsto> Sv ''M'', ''Age'' \<mapsto> Iv 25],
                [''Name'' \<mapsto> Sv ''P'', ''Age'' \<mapsto> Iv 36] 
               #}"
 
lemma "\<not> stud_inst' = stud_inst2'"
  unfolding stud_inst'_def stud_inst2'_def
  by auto
  
definition course_inst' :: rel_inst' where
 "course_inst' = {# 
                  [ ''Id'' \<mapsto> Iv 761, ''Name'' \<mapsto> Sv ''Logics'' ],
                  [ ''Id'' \<mapsto> Iv 761, ''Name'' \<mapsto> Sv ''Semantics'' ] 
               #}"


definition stud_db_inst' :: db_inst' where
"stud_db_inst' = 
                [
                ''students'' \<mapsto> stud_inst', 
                ''courses'' \<mapsto> course_inst'
                ]"

lemma "dom stud_db_inst' = {''students'', ''courses''}"
  unfolding stud_db_inst'_def
  apply auto
  done

definition db_inst' :: db_inst' where
  "db_inst' = [ 
    ''students'' \<mapsto> {# 
      [''Name'' \<mapsto> Sv ''Mahmoud'', ''Age'' \<mapsto> Iv 26],
      [''Name'' \<mapsto> Sv ''Peter'', ''Age'' \<mapsto> Iv 36]
    #},
    ''courses'' \<mapsto> {#
      [ ''Id'' \<mapsto> Iv 761, ''Name'' \<mapsto> Sv ''Logics'' ]
    #}
  ]"

lemma "\<not>stud_db_inst' = db_inst'"
  unfolding stud_db_inst'_def db_inst'_def stud_inst'_def course_inst'_def 
  apply auto
  by (meson add_mset_eq_singleton_iff add_mset_not_empty map_upd_eqD1)

lemma inv_fin:"invar_fin' stud_dbs db_inst'"
  by (auto simp:invar_fin'_def stud_dbs_def db_inst'_def course_dom_def stud_rels_def)

lemma "invar_db'_c'  stud_dbs db_inst'" 
  by (simp add:invar_db'_c'_def inv_fin )

lemma "wt_rel' stud_rels stud_inst'"
  by(auto simp add:stud_rels_def stud_inst'_def wt_rel'_def wt_tuple_def)

(*Proved by using the wt_db'_alt lemma*)
lemma wt_db'_ex:"wt_db' stud_dbs db_inst'"
  by (auto simp:  wt_db'_alt  stud_dbs_def db_inst'_def  stud_rels_def  course_dom_def  wt_rel'_def wt_tuple_def)

(*proved by using the definition of wt_db'*)
lemma "wt_db' stud_dbs stud_db_inst'"
  by (auto simp:  wt_db'_def  stud_dbs_def stud_db_inst'_def  stud_rels_def  stud_inst'_def course_inst'_def course_dom_def  wt_rel'_def wt_tuple_def)

lemma "invar_db' stud_dbs db_inst'"
  by (simp add:wt_db'_ex inv_fin  invar_db'_def )

lemma "selection_pre (''Name'' =\<^sub>c Sv ''Mahmoud'') stud_rels
     \<and> selection_inst' (''Name'' =\<^sub>c Sv ''Mahmoud'') stud_inst' = 
                {# 
                [''Name'' \<mapsto> Sv ''Mahmoud'', ''Age'' \<mapsto> Iv 25],
                [''Name'' \<mapsto> Sv ''Mahmoud'', ''Age'' \<mapsto> Iv 25]
               #}"
  by(auto simp: stud_rels_def selection_inst'_def stud_inst'_def split:)

lemma "selection_pre (''Id'' =\<^sub>c Iv 761) course_dom
         \<and> \<sigma>' (''Id'' =\<^sub>c Iv 761) course_inst' = course_inst'"
  by(auto simp: course_dom_def course_inst'_def  selection_inst'_def split:)


lemma "(selection_pre (''Id'' =\<^sub>c Iv 769) course_dom
         \<and> \<sigma>' (''Id'' =\<^sub>c Iv 769) course_inst' = {#})"
  by(auto simp add: course_dom_def course_inst'_def  selection_inst'_def split:)
  

definition stud_inst'3 :: rel_inst' where
 "stud_inst'3 = {#
                [''Id'' \<mapsto> Iv 25,''Name'' \<mapsto> Sv ''Mahmoud'', ''Age'' \<mapsto> Iv 25],
                [''Id'' \<mapsto> Iv 27,''Name'' \<mapsto> Sv ''Ali'', ''Age'' \<mapsto> Iv 27],
                  [''Id'' \<mapsto> Iv 27,''Name'' \<mapsto> Sv ''Ali'', ''Age'' \<mapsto> Iv 27],
                [''Id'' \<mapsto> Iv 1 , ''Name'' \<mapsto> Sv ''Peter'', ''Age'' \<mapsto> Iv 36] 
               #}"

lemma "selection_pre (''Id'' =\<^sub>a ''Age'' ) stud_rel3 \<and> 
       \<sigma>' (''Id'' =\<^sub>a ''Age'' ) stud_inst'3  
         = {#[''Id'' \<mapsto> Iv 25,''Name'' \<mapsto> Sv ''Mahmoud'', ''Age'' \<mapsto> Iv 25],
            [''Id'' \<mapsto> Iv 27,''Name'' \<mapsto> Sv ''Ali'', ''Age'' \<mapsto> Iv 27],
              [''Id'' \<mapsto> Iv 27,''Name'' \<mapsto> Sv ''Ali'', ''Age'' \<mapsto> Iv 27]
           #}"
 by(auto simp: selection_inst'_def stud_inst'3_def stud_rel3_def)

lemma "selection_pre ((''Id'' =\<^sub>a ''Age'') \<and>\<^sub>s (''Name'' =\<^sub>c Sv ''Ali'')) stud_rel3 \<and> 
       \<sigma>' ((''Id'' =\<^sub>a ''Age'') \<and>\<^sub>s (''Name'' =\<^sub>c Sv ''Ali'')) stud_inst'3  
         = {#
              [''Id'' \<mapsto> Iv 27,''Name'' \<mapsto> Sv ''Ali'', ''Age'' \<mapsto> Iv 27],
            [''Id'' \<mapsto> Iv 27,''Name'' \<mapsto> Sv ''Ali'', ''Age'' \<mapsto> Iv 27]
           #}"
  by(auto simp: selection_inst'_def stud_inst'3_def  stud_rel3_def)

                                   
value "let m={# (1,2), (1,3), (1,2), (2::int,1::int) #} in 
  {# fst x | x\<in>#m. snd x < 5 #}"    

lemma [simp]:"\<pi>' {''a''} {#[''a'' \<mapsto> Iv 1,''b'' \<mapsto> Iv 1],[''a'' \<mapsto> Iv 1,''b'' \<mapsto> Iv 1]#} = {#[''a'' \<mapsto> Iv 1],[''a'' \<mapsto> Iv 1]#}"
  unfolding projection_inst'_def 
  by auto

lemma "\<pi>' {''a''} (\<pi>' {''a''} {#[''a'' \<mapsto> Iv 1, ''b'' \<mapsto> Iv 1],[''a'' \<mapsto> Iv 1, ''b'' \<mapsto> Iv 1]#})
  =\<pi>' {''a''} {#[''a'' \<mapsto> Iv 1,''b'' \<mapsto> Iv 1],[''a'' \<mapsto> Iv 1,''b'' \<mapsto> Iv 1]#}"
  unfolding projection_inst'_def 
  by auto

lemma "\<pi>' {''a'', ''c''} ( \<pi>' {''a'', ''b''} {#[''a'' \<mapsto> Iv 1, ''b'' \<mapsto> Iv 1 ,''c'' \<mapsto> Iv 1]#}) = \<pi>' {''a''}
   {#[''a'' \<mapsto> Iv 1, ''b'' \<mapsto> Iv 1 ,''c'' \<mapsto> Iv 1]#}"
  unfolding projection_inst'_def
  by auto

lemma "\<pi>' {''a'', ''c''} ( \<pi>' {''a'', ''b''} {#[''a'' \<mapsto> Iv 1, ''b'' \<mapsto> Iv 1 ,''c'' \<mapsto> Iv 1]#}) = \<pi>' {''a''}
   {#[''a'' \<mapsto> Iv 1, ''b'' \<mapsto> Iv 1 ,''c'' \<mapsto> Iv 1]#}"
  unfolding projection_inst'_def
  by auto

definition attrL:: "attr set" where "attrL = {''Name''}"

lemma "\<pi>' attrL (stud_inst'3) =  {#  [''Name'' \<mapsto> Sv ''Mahmoud''], [''Name'' \<mapsto> Sv ''Ali''], [''Name'' \<mapsto> Sv ''Ali''], [''Name'' \<mapsto> Sv ''Peter''] 
               #}"
    by(auto simp: attrL_def stud_inst'3_def projection_inst'_def )
    
(*This is a False positive since ''F'' is not in the sort of stud_rels3 
despite the fact that it has no bad effect since it is mapped to None*)
lemma "\<pi>' {''Name'',''F''} stud_inst'3 =  {# 
                [''Name'' \<mapsto> Sv ''Mahmoud''],
                [''Name'' \<mapsto> Sv ''Ali''],
                [''Name'' \<mapsto> Sv ''Ali''],
                [''Name'' \<mapsto> Sv ''Peter''] 
               #}"
  by(auto simp:projection_inst'_def stud_inst'3_def  )

(*case where all values of the common attribute disagree 
we expect to get as result {}
*)
lemma " ({#[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'']#}
                    \<Join>\<^sub>b 
       {#[''ID'' \<mapsto> Iv 4, ''Course'' \<mapsto> Sv ''SE'']#}) ={#} 
        "
using map_upd_eqD1   by (fastforce simp add: natjoin_inst'_def fun_upd_twist)

lemma         "{#[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'']#} 
                    \<Join>\<^sub>b 
               {#[''ID'' \<mapsto> Iv 1, ''Course'' \<mapsto> Sv ''SE'']#} =
              {#[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'',''Course'' \<mapsto> Sv ''SE'']#}"
   unfolding  natjoin_inst'_def  map_add_def restrict_map_def 
   by auto

(*Case where some tuples disagree on the common attribute  and some agree
we expect to get as result {the join of every two tuple that agree}*)

lemma " {#[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'',''Course'' \<mapsto> Sv ''SE''] #} = 
       
         (     {#[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
                [''ID'' \<mapsto> Iv 3, ''Name'' \<mapsto> Sv ''N'']#} 
                    \<Join>\<^sub>b 
              {# [''ID'' \<mapsto> Iv 1, ''Course'' \<mapsto> Sv ''SE'']#})
      "
  using map_upd_eqD1  by (fastforce simp add: natjoin_inst'_def fun_upd_twist)

    
(*Case where all tuples agree on the common attribute 
we expect to get as result {the join of every two tuple that agree}
*)

lemma "({#[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'']#} 
                    \<Join>\<^sub>b 
       {#[''ID'' \<mapsto> Iv 1, ''Course'' \<mapsto> Sv ''FDS''],
        [''ID'' \<mapsto> Iv 1, ''Course'' \<mapsto> Sv ''SE'']#})
       =
        {#[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'',''Course'' \<mapsto> Sv ''SE''],
         [''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'',''Course'' \<mapsto> Sv ''FDS'']
                #}
        "
   using map_upd_eqD1   by (fastforce simp add: natjoin_inst'_def fun_upd_twist)

(*Case where the sort of both arguments agree, we get as result the intersection,
in this particular case all tuples disagree then we expect {#} as result
  *)

lemma "{#[''ID'' \<mapsto> Iv 4, ''Name'' \<mapsto> Sv ''A''],
        [''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''B'']#} 
                    \<Join>\<^sub>b 
        {#[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
         [''ID'' \<mapsto> Iv 3, ''Name'' \<mapsto> Sv ''C'']#}
       = {#} "
   proof-
  have 1:"[''ID'' \<mapsto> Iv 4, ''Name'' \<mapsto> Sv ''A''] ''ID'' \<noteq>
        [''ID'' \<mapsto> Iv 3, ''Name'' \<mapsto> Sv ''C''] ''ID''
      " by fastforce

  have 2:"[''ID'' \<mapsto> Iv 4, ''Name'' \<mapsto> Sv ''A''] ''ID'' \<noteq>
        [''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''] ''ID''
      " by fastforce

  have 3:"[''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''B''] ''ID'' \<noteq>
        [''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''] ''ID''
      " by fastforce

  have 4:"[''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''B''] ''ID'' \<noteq>
        [''ID'' \<mapsto> Iv 3, ''Name'' \<mapsto> Sv ''C''] ''ID''
      " by fastforce
  show "{#[''ID'' \<mapsto> Iv 4, ''Name'' \<mapsto> Sv ''A''],
        [''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''B'']#} 
                    \<Join>\<^sub>b 
        {#[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
         [''ID'' \<mapsto> Iv 3, ''Name'' \<mapsto> Sv ''C'']#}
       = {#}"
    apply (auto simp:natjoin_inst'_def)
    using  1 2 3 4 map_upd_eqD1 by fastforce+
qed


(*Case where the sort of both arguments agree, we get as result the \<inter>
in this particular {the join of every two tuple that agree}
 *)
lemma " 
       {#[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
        [''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''B'']#} 
                    \<Join>\<^sub>b 
        {#[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
         [''ID'' \<mapsto> Iv 3, ''Name'' \<mapsto> Sv ''C'']#}
         =
        {#[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A'']#}
        "
  using map_upd_eqD1  by (fastforce simp add: natjoin_inst'_def fun_upd_twist)

(*In this case both arguments are identicals and have the same sort*)
lemma  "{#[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
         [''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''C'']#} 
                    \<Join>\<^sub>b 
        {#[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
         [''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''C'']
        #}
      = 
      {#[''ID'' \<mapsto> Iv 1, ''Name'' \<mapsto> Sv ''A''],
        [''ID'' \<mapsto> Iv 2, ''Name'' \<mapsto> Sv ''C'']
      #}
      "
using map_upd_eqD1  by (fastforce simp add: natjoin_inst'_def fun_upd_twist)

lemma " {#t2 \<in># {#[''ID'' \<mapsto> Iv 1], [''ID'' \<mapsto> Iv 1]#}.
           [''ID'' \<mapsto> Iv 1] |` (dom [''ID'' \<mapsto> Iv 1] \<inter> dom t2) = t2 |` (dom [''ID'' \<mapsto> Iv 1] \<inter> dom t2)#}
      ={#[''ID'' \<mapsto> Iv 1], [''ID'' \<mapsto> Iv 1]#}
      "
  by auto

lemma "\<Union>#{#{#[''ID'' \<mapsto> Iv 1], [''ID'' \<mapsto> Iv 1]#},{#[''ID'' \<mapsto> Iv 1], [''ID'' \<mapsto> Iv 1]#}#}
    = {#[''ID'' \<mapsto> Iv 1], [''ID'' \<mapsto> Iv 1],[''ID'' \<mapsto> Iv 1], [''ID'' \<mapsto> Iv 1]#}  
  "
  by auto

(*ri1 = ri2 = ri3*)
lemma "{#[''A'' \<mapsto> Iv 1]#} \<Join>\<^sub>b {#[''A'' \<mapsto> Iv 1]#} \<Join>\<^sub>b {#[''A'' \<mapsto> Iv 1]#}
       = {#[''A'' \<mapsto> Iv 1]#} \<Join>\<^sub>b ({#[''A'' \<mapsto> Iv 1]#} \<Join>\<^sub>b {#[''A'' \<mapsto> Iv 1]#})"
  unfolding natjoin_inst'_def
  by auto

(*ri1 = ri2 \<noteq> ri3*)
lemma "{#[''A'' \<mapsto> Iv 1,''B'' \<mapsto> Iv 1]#} \<Join>\<^sub>b {#[''B'' \<mapsto> Iv 1,''C'' \<mapsto> Iv 1]#} \<Join>\<^sub>b {#[''C'' \<mapsto> Iv 1,''D'' \<mapsto> Iv 1]#}
      = ({#[''A'' \<mapsto> Iv 1,''B'' \<mapsto> Iv 1]#} \<Join>\<^sub>b {#[''B'' \<mapsto> Iv 1,''C'' \<mapsto> Iv 1]#}) \<Join>\<^sub>b {#[''C'' \<mapsto> Iv 1,''D'' \<mapsto> Iv 1]#}"
  unfolding natjoin_inst'_def
  by auto

(*ri1 = ri2 \<noteq> ri3*)
lemma "{#[''A'' \<mapsto> Iv 1]#} \<Join>\<^sub>b {#[''A'' \<mapsto> Iv 2]#} \<Join>\<^sub>b {#[''A'' \<mapsto> Iv 2]#}
      = ({#[''A'' \<mapsto> Iv 1]#} \<Join>\<^sub>b {#[''A'' \<mapsto> Iv 2]#}) \<Join>\<^sub>b {#[''A'' \<mapsto> Iv 2]#}"
  unfolding natjoin_inst'_def
  by auto

(*ri1 = ri3 \<noteq> ri2*)
lemma "{#[''A'' \<mapsto> Iv 2]#} \<Join>\<^sub>b {#[''A'' \<mapsto> Iv 1]#} \<Join>\<^sub>b {#[''A'' \<mapsto> Iv 2]#}
     =({#[''A'' \<mapsto> Iv 2]#} \<Join>\<^sub>b {#[''A'' \<mapsto> Iv 1]#}) \<Join>\<^sub>b {#[''A'' \<mapsto> Iv 2]#}"
  unfolding natjoin_inst'_def
  by auto

(*ri1 \<noteq> ri3 \<noteq> ri2*)
lemma "{#[''A'' \<mapsto> Iv 3]#} \<Join>\<^sub>b {#[''A'' \<mapsto> Iv 1]#} \<Join>\<^sub>b {#[''A'' \<mapsto> Iv 2]#}
      ={#[''A'' \<mapsto> Iv 3]#} \<Join>\<^sub>b ({#[''A'' \<mapsto> Iv 1]#} \<Join>\<^sub>b {#[''A'' \<mapsto> Iv 2]#})"
  unfolding natjoin_inst'_def
  by auto

 
lemma "{#[A \<mapsto> Iv 1]#} \<Join>\<^sub>b {#[A \<mapsto> Iv 1], [A \<mapsto> Iv 1] #} = {#[A \<mapsto> Iv 1], [A \<mapsto> Iv 1]#}"
  unfolding natjoin_inst'_def
  by simp

lemma "{#[A \<mapsto> Iv 1]#} \<inter># {#[A \<mapsto> Iv 1], [A \<mapsto> Iv 1] #} = {# [A \<mapsto> Iv 1]#}"
  unfolding natjoin_inst'_def
  by simp


lemma "\<rho>' [''A'' \<mapsto> ''B'']  {# [''A'' \<mapsto> Iv 1] #} = {# [''B'' \<mapsto> Iv 1] #}"
  unfolding renaming_inst'_def rename_def
  by auto

lemma  " {#[''x'' \<mapsto> Iv 1],[''x'' \<mapsto> Iv 1],[''z'' \<mapsto> Iv 1]#}  \<union>\<^sub>b
        {#[''y'' \<mapsto> Iv 1]#} = {#[''x'' \<mapsto> Iv 1],[''x'' \<mapsto> Iv 1],[''z'' \<mapsto> Iv 1], [''y'' \<mapsto> Iv 1]#} "
unfolding union_inst'_def
by auto


lemma  "( {#[''y'' \<mapsto> Iv 1] , [''y'' \<mapsto> Iv 1],[''z'' \<mapsto> Iv 1]#} -\<^sub>b {#[''y'' \<mapsto> Iv 1]#}) = {#[''y'' \<mapsto> Iv 1] ,[''z'' \<mapsto> Iv 1]#}"
unfolding diff_inst'_def 
  by auto

lemma  "({#[''y'' \<mapsto> Iv 1]#}) -\<^sub>b {#[''y'' \<mapsto> Iv 1] , [''y'' \<mapsto> Iv 1],[''z'' \<mapsto> Iv 1]#}  = {#}"
unfolding diff_inst'_def 
by auto

lemma "( {#[''y'' \<mapsto> Iv 1] , [''y'' \<mapsto> Iv 1],[''z'' \<mapsto> Iv 1]#} \<inter># {#[''y'' \<mapsto> Iv 1],[''y'' \<mapsto> Iv 1]#}) = {#[''y'' \<mapsto> Iv 1],[''y'' \<mapsto> Iv 1]#}"
  by auto


lemma "\<not> abs_rel(stud_inst') = abs_rel(stud_inst2')"
  unfolding stud_inst'_def  stud_inst2'_def
  apply auto
  by (smt doubleton_eq_iff fun_upd_twist list.inject map_upd_eqD1 not_Cons_self2 val.inject(2))

lemma ex_not_join_inter:
shows "\<exists>ri1 ri2. \<not>(ri1 \<Join> ri2) \<subseteq> (ri1 \<inter> ri2)"
 apply (rule  exI[of _ "{[''A'' \<mapsto> Iv 2]}" ]  )
 apply (rule  exI[of _ "{[''B'' \<mapsto> Iv 3]}" ]  )
 by(auto simp: natjoin_inst_def)

lemma ex_not_join_empty:
shows "\<exists>ri1 ri2. \<not>(ri1 \<Join> ri2) \<subseteq> {empty}"
  by(auto simp: natjoin_inst_def intro!:exI[of _ "{[''A'' \<mapsto> Iv 2]}"])

end