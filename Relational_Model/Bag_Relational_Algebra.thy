theory Bag_Relational_Algebra
  imports  Classic_Relational_Algebra   "~~/src/HOL/Library/Multiset"

begin

section\<open>Database and relation instances on bag\<close>

type_synonym rel_inst' = "tuple multiset"
type_synonym db_inst' = "relname \<rightharpoonup> rel_inst'"

section\<open>Invariants\<close>

subsection\<open>Finiteness of Database\<close>

definition invar_fin' :: "db_schema \<Rightarrow> db_inst' \<Rightarrow> bool" where 
  "invar_fin' dbs dbinst \<longleftrightarrow>  finite (dom dbs)  \<and> 
(\<forall>rn \<in> (dom dbs). finite (dom (the (dbs rn)))) \<and>
(\<forall>rn \<in> (dom dbinst). finite (set_mset (the (dbinst rn)))) \<and>
(\<forall>rn \<in> (dom dbinst).  \<forall> t\<in># (the (dbinst rn)). 
                               (\<exists>n. count (the (dbinst rn)) t = n) )"(*No infinite duplicates*)


definition invar_db'_c' ::"db_schema \<Rightarrow> db_inst' \<Rightarrow> bool" where 
"invar_db'_c' dbs dbinst  \<longleftrightarrow>  invar_fin' dbs dbinst"

subsection\<open>Well-typeness of database ans relation\<close> 

subsubsection\<open>well-typeness of relations\<close>

definition wt_rel' :: "rel_sign \<Rightarrow> rel_inst' \<Rightarrow> bool" where
 "wt_rel' rs ri \<longleftrightarrow> (\<forall>t\<in>#ri. wt_tuple rs t)"

lemma  wt_rel'_add_mset_sg:"wt_rel' rs (add_mset x ri) \<Longrightarrow> wt_rel' rs {#x#} " 
  by(auto simp: wt_rel'_def wt_tuple_def )

lemma  wt_rel'_add_mset:"wt_rel' rs (add_mset x ri) \<Longrightarrow> wt_rel' rs ri" 
      apply(auto simp: wt_rel'_def wt_tuple_def )
  by blast+

lemma attr_dom_rs_t: "wt_rel' rs ri \<Longrightarrow> A \<in> dom rs \<Longrightarrow> t \<in>#  ri \<Longrightarrow> A \<in> dom t"
  by (auto simp: wt_rel'_def wt_tuple_def)
  
lemma t_ri_none_empty:"\<forall>t\<in>#ri. \<forall>x. t x = None \<Longrightarrow>  xa \<in># ri \<Longrightarrow> empty \<in># ri"
 proof - 
  assume 1: "\<forall>t\<in>#ri. \<forall>x. t x = None" and
         2: "xa \<in># ri"
  from 1 2 have "\<forall>x. xa x = None" by blast 
  hence "xa = empty" by fast
  then show "empty \<in># ri" using 2  by auto
qed

text\<open>There are are many possible relation instances
     over the empty set of attributes: {} and {\<langle>empty\<rangle>,..,\<langle>empty\<rangle>}.\<close>

lemma wt_rel'_empty_ri:"wt_rel' empty ri \<longleftrightarrow> ri = {#} \<or> (\<forall>t\<in>#ri.  t = empty)"
       using t_ri_none_empty wt_rel'_def wt_tuple_def
      by (auto simp add: rs_empty_wt_tuple)

subsubsection\<open>well-typeness of database\<close>

definition wt_db' :: "db_schema \<Rightarrow> db_inst' \<Rightarrow> bool" where
  "wt_db' dbschema dbinst \<longleftrightarrow> 
    (dom dbschema = dom dbinst) \<and>
    (\<forall>rname rs ri. dbschema rname = Some rs \<and> dbinst rname = Some ri \<longrightarrow> wt_rel' rs ri)"

lemma rels_inst_fin':"wt_db' dbschema dbinst  \<Longrightarrow> finite (dom dbschema) \<Longrightarrow> finite (dom dbinst)"
  unfolding   wt_db'_def
  apply auto
  done

lemma wt_db'_alt:"wt_db' dbschema dbinst \<longleftrightarrow> 
    (dom dbschema = dom dbinst) \<and>
    (\<forall>rname \<in>(dom dbschema). wt_rel' (the (dbschema rname)) (the (dbinst rname)) )"
 unfolding wt_db'_def  wt_rel'_def wt_tuple_def
  using domIff 
  apply auto
  apply fastforce
  apply fastforce
  apply fastforce
  apply (metis (mono_tags, lifting)  domD domI option.sel )
  apply (metis (mono_tags, lifting)  domD domI option.sel )
  by (metis (mono_tags, lifting)   domI option.sel )

subsection\<open>Invariants of Database\<close>

definition invar_db' ::"db_schema  \<Rightarrow> db_inst' \<Rightarrow> bool" where
"invar_db' dbs dbinst \<longleftrightarrow> wt_db' dbs dbinst \<and> invar_fin' dbs dbinst "

lemma db_tuples_fin':
"invar_db' dbs dbinst \<Longrightarrow> dbinst rn = Some ri \<Longrightarrow> t \<in># ri \<Longrightarrow> finite (dom t)"
  unfolding invar_db'_def  invar_fin'_def wt_db'_def wt_rel'_def wt_tuple_def  
  apply (auto simp: tuple_fin)
  by (metis (full_types) domI domIff option.exhaust_sel)

lemma db_relations_fin':
  "invar_db' dbs dbinst \<Longrightarrow> finite (dom dbinst) "
  by(auto simp:rels_inst_fin' invar_fin'_def invar_db'_def) 

lemma  dbs_rn_None: "dbs rn = None \<Longrightarrow> wt_db' dbs dbinst \<Longrightarrow> dbinst rn = None" 
 unfolding  wt_db'_def invar_db'_def
  by blast


lemma  dbs_rn_Some: "dbs rn = Some rs \<Longrightarrow> wt_db' dbs dbinst \<Longrightarrow> \<exists>ri. dbinst rn = Some ri" 
 unfolding  wt_db'_def invar_db'_def
  by blast

section\<open>Relational Algebra on bag Operators' Semantics\<close>

subsection\<open>Selection\<close>

text\<open>selection semantics\<close>

definition selection_inst' :: " selection_formule \<Rightarrow> rel_inst' \<Rightarrow> rel_inst'" ("\<sigma>'")
  where "selection_inst' F ri = {# t \<in># ri. (eval_F F) t #}"

lemma unsat_selection_queries:"a\<noteq>b \<Longrightarrow> (\<sigma>' (A =\<^sub>c a) ( \<sigma>' (A =\<^sub>c b) ri )) = {#}"
  by (induction ri)(auto simp:selection_inst'_def)

 lemma sel_sg_add:"(\<sigma>' F {#t#}) + (\<sigma>' F ri) = \<sigma>' F (add_mset t ri)"
 by (auto simp add: selection_inst'_def)

lemma add_sel:"(\<sigma>' F ri1) + (\<sigma>' F ri2) = \<sigma>' F (ri1 + ri2)"
  by (auto simp add: selection_inst'_def)
lemma sel_add:"  \<sigma>' F (ri1 + ri2) =  (\<sigma>' F ri1) + (\<sigma>' F ri2)"
  by (auto simp add: selection_inst'_def)

lemma filter_filter_comm_f:"filter_mset (\<lambda>x. p x) (filter_mset (\<lambda>x. q x)  ri)
 = filter_mset (\<lambda>x. q x)  (filter_mset (\<lambda>x. p x) ri)"
using conj_commute filter_filter_mset[of "(\<lambda>x. p x)" "(\<lambda>x. q x)" ri]
            filter_filter_mset[of "(\<lambda>x. q x)" "(\<lambda>x. p x)" ri]
  by presburger

subsubsection\<open>Selection Properties\<close>

lemma sel_emptyset'[simp]:"\<sigma>' F {#} = {#}"
  using selection_inst'_def by auto

lemma sel_idem'[simp]:"\<sigma>' F (\<sigma>' F ri) = (\<sigma>' F ri)"
by(auto simp add: selection_inst'_def filter_mset_eq_conv )
 
lemma sel_comm:"\<sigma>' F\<^sub>2 (\<sigma>' F\<^sub>1 ri) = \<sigma>' F\<^sub>1 (\<sigma>' F\<^sub>2 ri)"
  by (simp add:filter_filter_comm_f selection_inst'_def )
  
lemma sel_conj_comm:"\<sigma>' (F\<^sub>1 \<and>\<^sub>s F\<^sub>2) ri = \<sigma>' (F\<^sub>2 \<and>\<^sub>s F\<^sub>1) ri "
  using conj_commute filter_filter_comm_f selection_inst'_def by simp

lemma sel_conj_assoc:"\<sigma>' (F\<^sub>1 \<and>\<^sub>s (F\<^sub>2 \<and>\<^sub>s F\<^sub>3)) ri = \<sigma>' ((F\<^sub>1 \<and>\<^sub>s F\<^sub>2) \<and>\<^sub>s F\<^sub>3) ri "
  using selection_inst'_def by auto

text\<open>The meaning of selection's conjunction formule\<close>

lemma sel_conj_compos_sel:"\<sigma>' (F\<^sub>1 \<and>\<^sub>s F\<^sub>2) ri = \<sigma>' F\<^sub>2 (\<sigma>' F\<^sub>1 ri)"
  apply(induction ri)
  by(auto simp:selection_inst'_def)

lemma merge_select:" \<sigma>' F\<^sub>2 (\<sigma>' F\<^sub>1 ri ) = \<sigma>' (F\<^sub>1 \<and>\<^sub>s F\<^sub>2) ri "     
  apply(induction ri)
by(auto simp:selection_inst'_def)

subsection\<open>Projection\<close>

text\<open>Projection semantics\<close>

definition projection_inst' :: "attr set \<Rightarrow> rel_inst' \<Rightarrow> rel_inst'" ("\<pi>'")
  where "projection_inst' aset ri = {# t|`aset .  t\<in>#ri #}"

subsubsection\<open>Projection Properties\<close>

lemma proj_emptyset'[simp]:"\<pi>' l {#} = {#}"
  by  (auto simp:projection_inst'_def )

lemma proj_empty'[simp]:"ri \<noteq> {#} \<Longrightarrow> set_mset ( \<pi>' {} ri) = {empty}"
  by  (auto simp:projection_inst'_def )
 
lemma projection_idem[simp]: "\<pi>' j ( \<pi>' j ri) = \<pi>' j ri "
  apply (auto simp:projection_inst'_def)
  proof -
  have "{#f |` j. f \<in># ri#} = image_mset ((\<lambda>f. f |` j) \<circ> (\<lambda>f. f |` j)) ri"
    by (metis (no_types) inf.orderE o_def restrict_restrict subsetI)
    then show "{#f |` j. f \<in># {#f |` j. f \<in># ri#}#} = {#f |` j. f \<in># ri#}"
      by (metis multiset.map_comp)
  qed

lemma proj_inter_compos':"ri \<noteq> {#} \<Longrightarrow> j \<inter> k = {} \<Longrightarrow> set_mset(\<pi>' j ( \<pi>' k ri)) =  {empty} "
  apply (auto simp:projection_inst'_def inf_sup_aci(1)   )
  proof -
  assume a1: "ri \<noteq> {#}"
  assume a2: "j \<inter> k = {}"
  obtain FF :: "(char list \<Rightarrow> val option) set \<Rightarrow> (char list \<Rightarrow> val option) \<Rightarrow> (char list \<Rightarrow> val option) set" where
    "\<forall>x0 x1. (\<exists>v2. x0 = insert x1 v2 \<and> x1 \<notin> v2) = (x0 = insert x1 (FF x0 x1) \<and> x1 \<notin> FF x0 x1)"
    by moura
  then obtain zz :: "(char list \<Rightarrow> val option) multiset \<Rightarrow> char list \<Rightarrow> val option" where
    f3: "set_mset ri = insert (zz ri) (FF (set_mset ri) (zz ri)) \<and> zz ri \<notin> FF (set_mset ri) (zz ri)"
    using a1 by (meson mk_disjoint_insert multiset_nonemptyE)
have "zz ri |` k |` j = Map.empty"
  using a2 by (simp add: Int_commute)
  then show "Map.empty \<in> (\<lambda>f. f |` j) ` (\<lambda>f. f |` k) ` set_mset ri"
    using f3 by force
qed

lemma proj_subseteq_compos1':" k \<subseteq> j \<Longrightarrow> \<pi>' j ( \<pi>' k ri) = \<pi>' k  ri "
  apply (auto simp:projection_inst'_def)
  by (metis (mono_tags, lifting) image_mset.compositionality image_mset_cong inf.orderE o_def restrict_restrict)

lemma proj_subseteq_compos2':" j \<subseteq> k \<Longrightarrow> \<pi>' j ( \<pi>' k ri) = \<pi>' j ri"
   apply (auto simp:projection_inst'_def  inf.absorb_iff2  )
   by (metis (mono_tags, lifting) image_mset.compositionality image_mset_cong o_def restrict_restrict)

lemma projection_commute': "\<pi>' j ( \<pi>' k ri) = \<pi>' k ( \<pi>' j ri)"
  apply (auto simp:projection_inst'_def inter_restrict_restrict)
  proof -
  have f1: "\<forall>m f fa. (\<exists>fb. (fb::char list \<Rightarrow> val option) \<in># m \<and> (f fb::char list \<Rightarrow> val option) \<noteq> fa fb) \<or> image_mset f m = image_mset fa m"
    using image_mset_cong by fastforce
  obtain zz :: "((char list \<Rightarrow> val option) \<Rightarrow> char list \<Rightarrow> val option) \<Rightarrow> ((char list \<Rightarrow> val option) \<Rightarrow> char list \<Rightarrow> val option) \<Rightarrow> (char list \<Rightarrow> val option) multiset \<Rightarrow> char list \<Rightarrow> val option" where
    "\<forall>x0 x1 x2. (\<exists>v3. v3 \<in># x2 \<and> x1 v3 \<noteq> x0 v3) = (zz x0 x1 x2 \<in># x2 \<and> x1 (zz x0 x1 x2) \<noteq> x0 (zz x0 x1 x2))"
    by moura
  then have f2: "\<forall>m f fa. zz fa f m \<in># m \<and> f (zz fa f m) \<noteq> fa (zz fa f m) \<or> image_mset f m = image_mset fa m"
    using f1 by presburger
  have "((\<lambda>f. f |` k) \<circ> (\<lambda>f. f |` j)) (zz ((\<lambda>f. f |` j) \<circ> (\<lambda>f. f |` k)) ((\<lambda>f. f |` k) \<circ> (\<lambda>f. f |` j)) ri) = ((\<lambda>f. f |` j) \<circ> (\<lambda>f. f |` k)) (zz ((\<lambda>f. f |` j) \<circ> (\<lambda>f. f |` k)) ((\<lambda>f. f |` k) \<circ> (\<lambda>f. f |` j)) ri)"
    by (simp add: Int_ac(3))
  then show "{#f |` j. f \<in># {#f |` k. f \<in># ri#}#} = {#f |` k. f \<in># {#f |` j. f \<in># ri#}#}"
    using f2 by (metis (no_types) multiset.map_comp)
qed


lemma proj_merge_inter':
   shows " \<pi>' j ( \<pi>' k ri) = \<pi>' (j \<inter> k) ri"
  apply (auto simp:projection_inst'_def)
  using  inf_commute inf_sup_aci(1) restrict_restrict projection_inst'_def
  proof -
  have f1: "\<forall>m f fa. (\<exists>fb. (fb::char list \<Rightarrow> val option) \<in># m \<and> (f fb::char list \<Rightarrow> val option) \<noteq> fa fb) \<or> image_mset f m = image_mset fa m"
  using image_mset_cong by fastforce
  obtain zz :: "((char list \<Rightarrow> val option) \<Rightarrow> char list \<Rightarrow> val option) \<Rightarrow> ((char list \<Rightarrow> val option) \<Rightarrow> char list \<Rightarrow> val option) \<Rightarrow> (char list \<Rightarrow> val option) multiset \<Rightarrow> char list \<Rightarrow> val option" where
    f2: "\<forall>x0 x1 x2. (\<exists>v3. v3 \<in># x2 \<and> x1 v3 \<noteq> x0 v3) = (zz x0 x1 x2 \<in># x2 \<and> x1 (zz x0 x1 x2) \<noteq> x0 (zz x0 x1 x2))"
    by moura
  have "zz ((\<lambda>f. f |` j) \<circ> (\<lambda>f. f |` k)) (\<lambda>f. f |` (j \<inter> k)) ri |` (j \<inter> k) = ((\<lambda>f. f |` j) \<circ> (\<lambda>f. f |` k)) (zz ((\<lambda>f. f |` j) \<circ> (\<lambda>f. f |` k)) (\<lambda>f. f |` (j \<inter> k)) ri)"
    by (simp add: inf_sup_aci(1))
  then have "{#f |` (j \<inter> k). f \<in># ri#} = image_mset ((\<lambda>f. f |` j) \<circ> (\<lambda>f. f |` k)) ri"
  using f2 f1 by meson
  then show "{#f |` j. f \<in># {#f |` k. f \<in># ri#}#} = {#f |` (j \<inter> k). f \<in># ri#}"
  by (simp add: multiset.map_comp)
qed

subsection\<open>Natural Join\<close>

text\<open>Natural Join semantics\<close>

definition natjoin_inst' :: "rel_inst' \<Rightarrow> rel_inst' \<Rightarrow> rel_inst'" (infixr "\<Join>\<^sub>b" 70)
  where "natjoin_inst' ri1 ri2 = \<Union># {# {# t1++t2 | t2\<in>#ri2.   t1|`(dom t1\<inter>dom t2) = t2|`(dom t1\<inter>dom t2) #}. t1\<in>#ri1 #}"

subsubsection\<open>Auxiliary lemmmas\<close>
lemma Union_mset_ri[simp]:"\<Union>#{#{#t1#}. t1 \<in># ri#} = ri  "
  apply(induct ri)
  by fastforce+

lemma image_mset_add_empty[simp]:"image_mset (op ++ Map.empty) ri = ri"
  apply(induct ri)
  by auto

lemma wt_rel'_tuples_dom_eq_dom:"\<And> t1 t2.  wt_rel' rs ri \<Longrightarrow>  t1 \<in># ri \<Longrightarrow>  t2 \<in># ri  \<Longrightarrow>  dom t1 = dom t2 "
  by (auto;metis (full_types) domD domI wt_rel'_def  wt_tuple_def)+

lemma map_add_self[simp]:"x ++ x = x"
  unfolding map_add_def by (rule ext) (auto split:option.splits)


lemma natjoin_self_sg[simp]:"{#x#} \<Join>\<^sub>b {#x#} = {#x#}"
  by (auto simp:natjoin_inst'_def)

lemma add_mset_natjoin_l:"(add_mset x ri1)  \<Join>\<^sub>b ri2 = ( ({#x#}  \<Join>\<^sub>b ri2 ) + (ri1  \<Join>\<^sub>b ri2 ))"
 by (auto simp:natjoin_inst'_def)

lemma add_mset_natjoin_r:" ri1  \<Join>\<^sub>b  (add_mset x ri2) =  (ri1 \<Join>\<^sub>b  {#x#})  + (ri1  \<Join>\<^sub>b ri2 )"
  apply(induct ri1)
  by (auto simp:natjoin_inst'_def)


lemma restrict_inter_le:"x |` (dom x \<inter>  dom y) = y |` (dom x \<inter>  dom y) \<Longrightarrow> x \<subseteq>\<^sub>m x ++ y"
  unfolding map_le_def   
     by (metis (no_types, lifting) map_add_dom_app_simps(1) map_add_dom_app_simps(3) restrict_in restrict_restrict)

lemma plus_natjoin:"(ri1 + ri2) \<Join>\<^sub>b ri3 = ((ri1 \<Join>\<^sub>b ri3) + (ri2 \<Join>\<^sub>b ri3))"
  unfolding natjoin_inst'_def
  by auto


lemma restrict_dom_map_add_r:
    assumes "y |` (dom y \<inter> dom z) = z |` (dom y \<inter> dom z)"  
      and "x |` (dom x \<inter> (dom z \<union> dom y)) = (y ++ z) |` (dom x \<inter> (dom z \<union> dom y))"
    shows "x |` (dom x \<inter> dom y) = y |` (dom x \<inter> dom y)"
  using assms
    unfolding map_add_def  restrict_map_def
    apply simp
    apply(rule ext)
    apply auto
    by (metis (no_types, lifting) domI domIff map_add_def map_add_self option.case_eq_if option.inject)

lemma restrict_dom_map_add_l:
    assumes "x |` (dom x \<inter> dom y) = y |` (dom x \<inter> dom y)"  
      and " (x ++ y) |` ( (dom x \<union> dom y) \<inter> dom z ) = z |` ( (dom x \<union> dom y) \<inter> dom z )"
    shows "y |` (dom y \<inter> dom z) = z |` (dom y \<inter> dom z)"
  using assms
    unfolding map_add_def  restrict_map_def
    apply simp
    apply(rule ext)
    apply auto
    by (metis (no_types, lifting) domI domIff map_add_def map_add_self option.case_eq_if option.inject)

lemma map_add_restrict_dom_inter_r:
  assumes "x |` (dom x \<inter> dom y) = y |` (dom x \<inter> dom y)"  
      and "z |` (dom z \<inter> (dom x \<union> dom y)) = (x ++ y) |` (dom z \<inter> (dom x \<union> dom y))"
    shows "x |` (dom x \<inter> (dom y \<union> dom z) ) = (y ++ z) |` (dom x \<inter> (dom y \<union> dom z) )"
  unfolding natjoin_inst'_def  map_add_def restrict_map_def
  apply(rule ext)
  apply auto
  using assms 
  apply (metis dom_map_add map_add_def map_add_find_right map_le_iff_map_add_commute restrict_inter_le)
  by (metis (no_types, lifting) assms(1) assms(2) domI dom_map_add map_le_def map_le_iff_map_add_commute option.inject restrict_inter_le)

lemma map_add_restrict_dom_inter_l:
   assumes "y |` (dom y \<inter> dom z) = z |` (dom y \<inter> dom z)"  
      and "x |` (dom x \<inter> (dom z \<union> dom y)) = (y ++ z) |` (dom x \<inter> (dom z \<union> dom y))"
    shows "(x ++ y) |` ((dom y \<union> dom x) \<inter> dom z) = z |` ((dom y \<union> dom x) \<inter> dom z)"
  unfolding natjoin_inst'_def  map_add_def restrict_map_def
  apply(rule ext)
  apply auto
  using assms
  apply (metis map_add_find_right map_le_iff_map_add_commute option.inject restrict_inter_le)
  by (metis (no_types, lifting) assms(1) assms(2) domI dom_map_add map_add_def map_le_def map_le_iff_map_add_commute restrict_inter_le)

lemma wt_rel'_inter_dom1:" wt_rel' rs1 ri1 \<Longrightarrow> wt_rel' rs2 ri2 \<Longrightarrow>  rs1 = rs2
     \<Longrightarrow> \<forall>t1 t2. t1 \<in># ri1 \<and> t2 \<in># ri2 \<longrightarrow> dom t1 \<inter> dom t2 = dom t1 "
  unfolding wt_rel'_def
  by (auto)(metis (full_types) domD domI wt_tuple_def)

lemma wt_rel'_inter_dom2:" wt_rel' rs1 ri1 \<Longrightarrow> wt_rel' rs2 ri2 \<Longrightarrow>  rs1 = rs2 \<Longrightarrow> 
    \<forall>t1 t2. t1 \<in># ri1 \<and> t2 \<in># ri2 \<longrightarrow> dom t1 \<inter> dom t2 = dom t2 "
  using wt_rel'_inter_dom1 
  by blast

subsubsection\<open>Natural Join Properties\<close>

text\<open>{#} is the absorbing element\<close>

lemma natjoin_mset_empty_r[simp]:" ri \<Join>\<^sub>b {#} = {#}"
  unfolding natjoin_inst'_def
  by auto

lemma natjoin_mset_empty_l[simp]:" {#} \<Join>\<^sub>b ri = {#}"
  unfolding natjoin_inst'_def
  by auto

text\<open>{#empty#} is the identity element for natjoin_inst'\<close>

lemma natjoin_empty_r[simp]:"ri \<Join>\<^sub>b {#empty#} = ri"
    by (auto simp: natjoin_inst'_def)

lemma join_empty_l[simp]:"{#empty#} \<Join>\<^sub>b ri  = ri"
  by (auto simp: natjoin_inst'_def)

text\<open>Natural join is not idempotent on multiset\<close>

lemma "wt_rel' rs ri \<Longrightarrow> \<exists>ri. ri \<Join>\<^sub>b ri  \<noteq> ri"
  unfolding natjoin_inst'_def
  apply(rule exI[of _ "{# empty, empty#}"])
  by auto

text\<open>Natural join is commutative\<close>

lemma natjoin_comm_sg_sg:"{#x#}  \<Join>\<^sub>b {#y#} = {#y#} \<Join>\<^sub>b  {#x#}"
  apply( cases "x = y")
  apply (auto simp add:natjoin_inst'_def inf.commute map_le_iff_map_add_commute)
  by (metis (full_types) map_le_iff_map_add_commute restrict_inter_le)

lemma natjoin_comm_sg_l:"{#x#} \<Join>\<^sub>b ri = ri \<Join>\<^sub>b {#x#}"
  apply(induction ri)
  apply simp
  by (metis add_mset_natjoin_l add_mset_natjoin_r natjoin_comm_sg_sg)

lemma natjoin_comm_sg_r:"ri \<Join>\<^sub>b {#x#}  = {#x#} \<Join>\<^sub>b ri"
  using natjoin_comm_sg_l by metis

theorem  natjoin_comm':"ri1 \<Join>\<^sub>b ri2 = ri2 \<Join>\<^sub>b ri1"
  apply(induction ri1)
  apply simp
   by (metis add_mset_natjoin_l add_mset_natjoin_r natjoin_comm_sg_r)

lemma natjoin_plus:"ri1  \<Join>\<^sub>b  (ri2 + ri3) = ((ri1 \<Join>\<^sub>b ri2) + (ri1 \<Join>\<^sub>b ri3))"
  using natjoin_comm' plus_natjoin by auto

text\<open> Natural Join is associative\<close>

lemma natjoin_assoc_sg_sg_sg:"{#x#} \<Join>\<^sub>b {#y#} \<Join>\<^sub>b {#z#} =  ({#x#} \<Join>\<^sub>b {#y#}) \<Join>\<^sub>b {#z#}"
  apply(cases "x=y")  
  apply(cases "y=z")
  apply(auto simp:natjoin_inst'_def)
  apply (metis (no_types, lifting) inf_commute inf_sup_absorb map_add_restrict_dom_inter_l map_add_self sup_inf_absorb)
  using restrict_dom_map_add_r apply blast
  using map_add_restrict_dom_inter_l apply blast
  apply (metis inf_sup_aci(5) restrict_dom_map_add_l)
  by (simp add: inf_sup_aci(1) map_add_restrict_dom_inter_r sup.commute)
   
lemma natjoin_assoc_sg_sg:"{#x#} \<Join>\<^sub>b ({#y#} \<Join>\<^sub>b ri) =  ({#x#} \<Join>\<^sub>b {#y#}) \<Join>\<^sub>b ri"
  apply(induction ri)
  by(simp)(metis natjoin_assoc_sg_sg_sg add_mset_natjoin_r natjoin_plus)
(*
an alternative Isar Proof
proof(induction ri)
  case empty
  then show ?case by simp
next
  case (add z ri)
  have "{#x#} \<Join>\<^sub>b {#y#} \<Join>\<^sub>b (add_mset z ri) = {#x#} \<Join>\<^sub>b (({#y#} \<Join>\<^sub>b {#z#}) + ({#y#} \<Join>\<^sub>b ri)) "
    by (metis add_mset_natjoin_r)
  also have "... = ( {#x#} \<Join>\<^sub>b  ({#y#} \<Join>\<^sub>b {#z#}) +  {#x#} \<Join>\<^sub>b  ({#y#} \<Join>\<^sub>b ri))"
    by (simp add: natjoin_plus)
  also have "... = {#x#} \<Join>\<^sub>b  ({#y#} \<Join>\<^sub>b {#z#}) +  (({#x#} \<Join>\<^sub>b  {#y#}) \<Join>\<^sub>b ri) "
    by (simp add: add.IH)
  also have "... = (({#x#} \<Join>\<^sub>b  {#y#}) \<Join>\<^sub>b {#z#}) +  (({#x#} \<Join>\<^sub>b  {#y#}) \<Join>\<^sub>b ri) "
    by (simp add: natjoin_assoc_sg_sg_sg)
  also have "... = ({#x#} \<Join>\<^sub>b  {#y#}) \<Join>\<^sub>b  (add_mset z ri)"
    by (metis add_mset_natjoin_r)
  finally show ?case 
    by simp
 qed*)
  
lemma natjoin_assoc_sg:" {#x#} \<Join>\<^sub>b (ri1 \<Join>\<^sub>b ri2) =  ({#x#} \<Join>\<^sub>b ri1) \<Join>\<^sub>b ri2 "
  apply(induction ri1)
  apply simp
proof -
fix xa  and ri1a 
assume a1: "{#x#} \<Join>\<^sub>b ri1a \<Join>\<^sub>b ri2 = ({#x#} \<Join>\<^sub>b ri1a) \<Join>\<^sub>b ri2"
  have "(({#} + {#x#}) \<Join>\<^sub>b {#xa#}) \<Join>\<^sub>b ri2 = {#x#} \<Join>\<^sub>b {#xa#} \<Join>\<^sub>b ri2"
    using natjoin_assoc_sg_sg by auto
  then show "{#x#} \<Join>\<^sub>b add_mset xa ri1a \<Join>\<^sub>b ri2 = ({#x#} \<Join>\<^sub>b add_mset xa ri1a) \<Join>\<^sub>b ri2"
  using a1 by (metis (no_types) add_mset_add_single natjoin_plus plus_natjoin)
qed


lemma natjoin_assoc:" ri1 \<Join>\<^sub>b (ri2 \<Join>\<^sub>b ri3) =  (ri1 \<Join>\<^sub>b ri2) \<Join>\<^sub>b ri3 "
apply(induction ri1)
by (simp)(metis  add_mset_natjoin_l plus_natjoin natjoin_assoc_sg)

lemma natjoin_noteq_inter:"\<exists> ri1 ri2 rs1 rs2. wt_rel' rs1 ri1 \<longrightarrow> wt_rel' rs2 ri2 \<longrightarrow> rs1=rs2 
      \<longrightarrow> (ri1 \<Join>\<^sub>b ri2) \<noteq> (ri1 \<inter># ri2)"
  apply(rule exI[of _ "{#[''A'' \<mapsto> Iv 1]#}"])
  apply(rule exI[of _ " {#[''A'' \<mapsto> Iv 1], [''A''  \<mapsto> Iv 1] #}"])
  apply(rule exI[of _ "[''A'' \<mapsto> INTEGER]"])
  apply(rule exI[of _ "[''A'' \<mapsto> INTEGER]"])
  apply rule+ 
  proof -
  assume "{#[''A'' \<mapsto> Iv 1]#} \<Join>\<^sub>b {#[''A'' \<mapsto> Iv 1], [''A'' \<mapsto> Iv 1]#} = {#[''A'' \<mapsto> Iv 1]#} \<inter># {#[''A'' \<mapsto> Iv 1], [''A'' \<mapsto> Iv 1]#}"
  then have "{#} + {#[''A'' \<mapsto> Iv 1]#} = {#} \<inter># {#[''A'' \<mapsto> Iv 1]#}"
    by (metis add_mset_add_mset_same_iff add_mset_add_single add_mset_inter_add_mset natjoin_plus natjoin_self_sg)
  then show False
    by force
qed

subsection\<open>Renaming\<close>

definition renaming_inst' :: "(attr \<rightharpoonup> attr)\<Rightarrow>  rel_inst'  \<Rightarrow> rel_inst'" ("\<rho>'") where
"\<rho>' f ri= {#  (rename t f) .  t \<in># ri #}"

subsection\<open>Union\<close>

definition union_inst' :: "rel_inst' \<Rightarrow> rel_inst' \<Rightarrow>  rel_inst'"  (infixl " \<union>\<^sub>b" 65) where
"ri1  \<union>\<^sub>b ri2 = ri1 + ri2 "

subsection\<open>Difference\<close>

definition diff_inst' :: "rel_inst' \<Rightarrow> rel_inst' \<Rightarrow>  rel_inst'"  (infixl "-\<^sub>b" 65) where 
" ri1 -\<^sub>b ri2 = ri1 - ri2 " 

subsection\<open>Derivative operators\<close>

subsubsection\<open>Intersection\<close>

definition inter_inst :: "rel_inst' \<Rightarrow> rel_inst' \<Rightarrow> rel_inst'" (infixl "\<inter>\<^sub>b" 70) where
 "  inter_inst A B = A -\<^sub>b (A -\<^sub>b B)"

lemma diff_multiset_inter[simp]:"A -\<^sub>b ( A -\<^sub>b B) = A \<inter># B"
  by (simp add: diff_inst'_def multiset_inter_def)

section\<open>well-typness of Relational Algebra operators\<close>

lemma selection_wt:
  assumes "selection_pre F rs"
  assumes "wt_rel' rs ri"
  shows "wt_rel' (selection_schema F rs) (\<sigma>' F ri)"
  using assms(2)  
  unfolding wt_rel'_def selection_inst'_def
  by auto  

(***************projection*****************)   
lemma projection_wt:
  assumes "projection_pre aset rs"
  assumes "wt_rel' rs ri"
  shows   "wt_rel' (projection_schema aset rs) (projection_inst' aset ri)"  
  using assms
  unfolding projection_pre_def projection_schema_def projection_inst'_def wt_rel'_def wt_tuple_def
  apply (auto)
  by (metis option.simps(3) restrict_map_def)  

(***************natural join*****************)  
lemma natjoin_wt:
  assumes "natjoin_pre rs1 rs2"
  assumes "wt_rel' rs1 ri1"
  assumes "wt_rel' rs2 ri2"
  shows "wt_rel' (natjoin_schema rs1 rs2) (natjoin_inst' ri1 ri2)"
  using assms
  unfolding wt_rel'_def natjoin_schema_def natjoin_inst'_def natjoin_pre_def  
 apply (auto simp add: wt_tuple_def domI map_le_def)
 apply (metis domD domI )
    apply (metis (full_types) domD domI )
   by (metis domI  domIff)+



(*******************Renaming***************)
lemma renaming_wt:
assumes "rename_pre rs f "
assumes "wt_rel' rs ri" 
shows "wt_rel' (renaming_schema rs f) (\<rho>' f ri)"
  unfolding  renaming_schema_def renaming_inst'_def 
using assms apply(auto simp add:rename_def rename_pre_set_def wt_rel'_def wt_tuple_def )
apply (metis domD domIff  option.distinct(1) )
apply (metis  domD domI domIff)
apply (metis domD option.simps(3))    
apply (auto simp add: domD domI)
apply (metis domI domIff)     
apply (metis domI domIff)
  by (meson option.distinct(1))



(***************union *****************) 
lemma union_wt:
  assumes "union_pre rs1 rs2"
  assumes "wt_rel' rs1 ri1"
  assumes "wt_rel' rs2 ri2"
  shows "wt_rel' (union_schema rs1 rs2) (ri1 \<union>\<^sub>b ri2)"
 using assms
 unfolding wt_rel'_def union_schema_def union_inst'_def union_pre_def
  by (auto)


(***************diff*****************) 
lemma diff_wt1a:
  assumes "diff_pre rs1 rs2"(*Not necessary*)
  assumes "wt_rel' rs1 ri1"
  assumes "wt_rel' rs2 ri2"(*Not necessary*)
  shows "wt_rel' (diff_schema rs1 rs2) ( ri1 -\<^sub>b ri2)"
 using assms
 unfolding wt_rel'_def diff_schema_def diff_inst'_def diff_pre_def
  by (meson in_diffD)

lemma diff_wt1b:
 assumes "wt_rel' rs1 ri1"
 shows "wt_rel' rs1 (ri1 -\<^sub>b ri2)"
 using assms
 unfolding wt_rel'_def diff_inst'_def 
 by (meson in_diffD)

lemma diff_wt2:
  assumes "diff_pre rs1 rs2"(*Now it is necessary*)
  assumes "wt_rel' rs1 ri1"
  assumes "wt_rel' rs2 ri2"(* still is not necessary*)
  shows "wt_rel' (diff_schema rs2 rs1) ( ri1 -\<^sub>b ri2)"
 using assms(1,2)
 unfolding wt_rel'_def diff_schema_def diff_inst'_def diff_pre_def 
 by (meson in_diffD)

section\<open>Relational algebra operators preserve finiteness\<close>

lemma sel_preserve_instance_fin:
  shows "finite (set_mset (\<sigma>' F ri))"
  by(auto)

lemma sel_preserve_instance_duplicate_fin:
  shows " \<forall>t \<in># (\<sigma>' F ri). \<exists>n. count (\<sigma>' F ri) t = n"  
  by simp

lemma proj_preserve_instance_fin:
  shows "finite (set_mset (\<pi>' l ri))"
  by(auto)

lemma proj_preserve_instance_duplicate_fin:
  shows " \<forall>t \<in># (\<pi>' l ri). \<exists>n. count (\<pi>' l ri) t = n"  
  by simp

lemma natjoin_preserve_instance_fin:
  shows "finite (set_mset ( ri1 \<Join>\<^sub>b ri2))"
  by(auto)

lemma natjoin_preserve_instance_duplicate_fin:
  shows " \<forall>t \<in># (ri1 \<Join>\<^sub>b ri2). \<exists>n. count (ri1 \<Join>\<^sub>b ri2) t = n"  
  by simp

lemma renaming_preserve_instance_fin:
  shows "finite (set_mset ( \<rho>' f ri))"
  by(auto)

lemma renaming_preserve_instance_duplicate_fin:
  shows " \<forall>t \<in># (\<rho>' f ri). \<exists>n. count (\<rho>' f ri) t = n"  
  by simp

lemma union_preserve_instance_fin:
  shows "finite (set_mset ( ri1 \<union>\<^sub>b ri2))"
  by(auto)

lemma union_preserve_instance_duplicate_fin:
  shows " \<forall>t \<in># (ri1 \<union>\<^sub>b ri2). \<exists>n. count (ri1 \<union>\<^sub>b ri2) t = n"  
  by simp

lemma diff_preserve_instance_fin:
  shows "finite (set_mset ( ri1 -\<^sub>b ri2))"
  by(auto)

lemma diff_preserve_instance_duplicate_fin:
  shows " \<forall>t \<in># (ri1 -\<^sub>b ri2). \<exists>n. count (ri1 -\<^sub>b ri2) t = n"  
  by simp
(*************Cross Product*************)

lemma size_prod_sg:"wt_rel' rs1 {#x#} \<Longrightarrow> wt_rel' rs2 ri \<Longrightarrow> 
    size ri = n \<Longrightarrow> dom rs1 \<inter> dom rs2 = {} \<Longrightarrow>
        size ({#x#} \<Join>\<^sub>b ri) = n"
  by(induction ri arbitrary: n)(auto simp: natjoin_inst'_def wt_rel'_def wt_tuple_def)

lemma size_prod_empty:"wt_rel' rs1 {#} \<Longrightarrow> wt_rel' rs2 ri \<Longrightarrow> 
    size ri = n \<Longrightarrow> dom rs1 \<inter> dom rs2 = {} \<Longrightarrow>
        size ({#} \<Join>\<^sub>b ri) = 0"
  by(induction ri arbitrary: n)(auto simp: natjoin_inst'_def wt_rel'_def wt_tuple_def)

(*If sort rs1 \<inter> sort rs1 = {} then ri1 \<Join>\<^sub>b ri2 = ri1 \<times> ri2,
                                  thus  size of ri1 \<Join>\<^sub>b ri2 equal size of ri1 \<times> ri2 as in set. 
 Note: i used \<times>  as i don't know the symbol for cross product in multiset 
 *)
lemma size_prod:"wt_rel' rs1 ri1 \<Longrightarrow> wt_rel' rs2 ri2 \<Longrightarrow> size ri1 = n  \<Longrightarrow>  size ri2 = m 
    \<Longrightarrow> dom rs1 \<inter> dom rs2 = {} \<Longrightarrow> size (ri1 \<Join>\<^sub>b ri2) = n * m"
  proof(induction ri1 arbitrary: n m)
    case empty
    thus  ?case  using   size_prod_empty[of rs1 rs2 ri2 n] by simp
  next
    case (add x ri1)
    hence 1:"wt_rel' rs1 {#x#}" 
      using wt_rel'_add_mset_sg by blast
    from add 1 size_prod_sg have 2:"size ({#x#} \<Join>\<^sub>b ri2) = m" by meson  
    have "size (add_mset x ri1  \<Join>\<^sub>b ri2) = size (({#x#} \<Join>\<^sub>b ri2) + (ri1  \<Join>\<^sub>b ri2))"
      by (metis add_mset_natjoin_l)
    also have "... = size ({#x#} \<Join>\<^sub>b ri2) + size (ri1  \<Join>\<^sub>b ri2)" by fastforce
    also have "... = m + ((n -1) * m)" using add.IH[of "n-1" "m"] 2 add
      by (metis One_nat_def diff_Suc_Suc minus_nat.diff_0 size_add_mset wt_rel'_add_mset)
    also have  "... = n * m" 
      using add.prems(3) by auto
    finally show ?case by simp
  qed

section\<open> The equivalence-preserving SPJR algebra rewrite rules\<close>

lemma union_inst'_commute:"A \<union>\<^sub>b B = B \<union>\<^sub>b A"
  by (auto simp:union_inst'_def)

lemma union_inst'_assoc:"A \<union>\<^sub>b B \<union>\<^sub>b C = A \<union>\<^sub>b (B \<union>\<^sub>b C)"
  by (auto simp:union_inst'_def)

lemma inter_inst_idem:"A \<inter>\<^sub>b A = A"
  by (auto simp:inter_inst_def subset_mset.inf_commute )

lemma inter_inst_commute:"A \<inter>\<^sub>b B = B \<inter>\<^sub>b A"
  by (auto simp:inter_inst_def subset_mset.inf_commute )

lemma inter_inst_assoc:"A \<inter>\<^sub>b  B \<inter>\<^sub>b  C = A \<inter>\<^sub>b (B \<inter>\<^sub>b C )"
  by (auto simp:inter_inst_def subset_mset.inf_assoc)

lemma union_inter_inst_distrib_r:"A \<union>\<^sub>b (B \<inter>\<^sub>b C) = (A \<union>\<^sub>b B) \<inter>\<^sub>b (A \<union>\<^sub>b C)"
  by (auto simp:inter_inst_def union_inst'_def inter_union_distrib_right )

lemma union_inter_inst_distrib_l:"(A \<inter>\<^sub>b B) \<union>\<^sub>b  C  = (A \<union>\<^sub>b C) \<inter>\<^sub>b (B \<union>\<^sub>b C)"
  by (metis union_inst'_commute union_inter_inst_distrib_r)
 
lemma push_proj_through_uni: "\<pi>' l (ri1 \<union>\<^sub>b ri2) = (\<pi>' l ri1) \<union>\<^sub>b (\<pi>' l ri2)"
  by (auto simp: union_inst'_def projection_inst'_def)

lemma comp_sel_inter:"\<sigma>' F\<^sub>2  (\<sigma>' F\<^sub>1 ri) = ((\<sigma>'  F\<^sub>1  ri) \<inter>\<^sub>b (\<sigma>'  F\<^sub>2 ri))"
  unfolding selection_inst'_def inter_inst_def
  apply(induction ri)
  by (auto simp add:inter_add_right1 inter_add_left1)

lemma push_sel_through_inter:"\<sigma>'  (F\<^sub>1 \<and>\<^sub>s F\<^sub>2) ri = ((\<sigma>'  F\<^sub>1  ri) \<inter>\<^sub>b (\<sigma>'  F\<^sub>2 ri))"
  unfolding selection_inst'_def inter_inst_def
  apply(induction ri)
  by (auto simp add:inter_add_right1 inter_add_left1)


lemma natjoin_sel_add:"(ri \<Join>\<^sub>b \<sigma>' F ri1) + (ri \<Join>\<^sub>b \<sigma>' F ri2) = (ri \<Join>\<^sub>b \<sigma>' F (ri1 + ri2)) "
  by (metis natjoin_plus sel_add)

lemma sel_sg_join_sg:
  assumes
       1:" A \<notin> dom t" and
       2:"B \<in> dom t"
     shows "\<sigma>' (A =\<^sub>a B) ({#[A \<mapsto> a]#} \<Join>\<^sub>b {#t#}) =  ({#[A \<mapsto> a]#} \<Join>\<^sub>b \<sigma>' (B =\<^sub>c a) {#t#})"
  using assms
  by(auto simp:natjoin_inst'_def selection_inst'_def map_add_def split:option.splits)

lemma push_select_through_singleton: 
  assumes   "wt_rel' rs ri"
        and "A \<notin> dom rs"
        and "B \<in> dom rs"
      shows"\<sigma>' (A =\<^sub>a B) ({#[A \<mapsto> a]#} \<Join>\<^sub>b ri)  = {#[A \<mapsto> a]#} \<Join>\<^sub>b  (\<sigma>' (B =\<^sub>c a ) ri )"
  using assms
proof(induction ri)
  case empty
  then show ?case   by (simp add: selection_inst'_def)
next
  case (add x ri)
from add(2) wt_rel'_add_mset have 1:"wt_rel' rs ri" 
    by blast
  have 2:"\<forall>t. t\<in># ri \<longrightarrow> A \<notin> dom t"
       using 1 add(3)  wt_rel'_def wt_tuple_def  by metis
   have 3:"\<forall>t. t\<in># ri \<longrightarrow> B \<in> dom t"
     using 1 add(4)  wt_rel'_def wt_tuple_def  by metis
       have "\<sigma>' (A =\<^sub>a B) ({#[A \<mapsto> a]#}  \<Join>\<^sub>b add_mset x ri) =
         \<sigma>' (A =\<^sub>a B) (({#[A \<mapsto> a]#}  \<Join>\<^sub>b {#x#}) + ({#[A \<mapsto> a]#}  \<Join>\<^sub>b ri))" 
      by (metis add_mset_natjoin_r)
    also have "... =  (\<sigma>' (A =\<^sub>a B) ({#[A \<mapsto> a]#}  \<Join>\<^sub>b {#x#})) + (\<sigma>' (A =\<^sub>a B) ({#[A \<mapsto> a]#}  \<Join>\<^sub>b ri))"
        by (simp add: selection_inst'_def)
      also have "... =  ({#[A \<mapsto> a]#}  \<Join>\<^sub>b \<sigma>' (B =\<^sub>c a) {#x#}) + (\<sigma>' (A =\<^sub>a B) ({#[A \<mapsto> a]#}  \<Join>\<^sub>b ri))"
        using add.prems(1)  sel_sg_join_sg  assms(2) assms(3) wt_rel'_def wt_tuple_def by auto
      also have "... =  ({#[A \<mapsto> a]#}  \<Join>\<^sub>b \<sigma>' (B =\<^sub>c a) {#x#}) + ( {#[A \<mapsto> a]#}  \<Join>\<^sub>b \<sigma>' (B =\<^sub>c a) ri) "
        using add.IH 1 add.prems(2,3) by presburger
      also have "... = {#[A \<mapsto> a]#}  \<Join>\<^sub>b  ((\<sigma>' (B =\<^sub>c a) {#x#}) + \<sigma>' (B =\<^sub>c a) ri) "
        by (simp add: natjoin_plus)
      also have "... = {#[A \<mapsto> a]#}  \<Join>\<^sub>b  (\<sigma>' (B =\<^sub>c a) ({#x#} +  ri))"
        using add_sel by force
      also have "... ={#[A \<mapsto> a]#}  \<Join>\<^sub>b \<sigma>' (B =\<^sub>c a) (add_mset x ri)" by simp
      finally show " \<sigma>' (A =\<^sub>a B) ({#[A \<mapsto> a]#}  \<Join>\<^sub>b add_mset x ri) =
                     {#[A \<mapsto> a]#}  \<Join>\<^sub>b \<sigma>' (B =\<^sub>c a) (add_mset x ri)" by simp
   qed


lemma push_join_through_select_sg_sg:
  assumes "wt_rel' rs1 {#x#}"
      and "wt_rel' rs2 {#y#}"
      and "selection_pre F rs1"
      and "dom rs1 \<inter> dom rs2 = {}"
    shows " \<sigma>' F {#x#} \<Join>\<^sub>b  {#y#} = \<sigma>' F ({#x#} \<Join>\<^sub>b  {#y#})"
  using assms
  apply(induction F)
  unfolding selection_inst'_def natjoin_inst'_def map_add_def
  apply auto
  apply (metis (no_types, lifting) disjoint_iff_not_equal domI domIff multi_member_last option.case_eq_if wt_rel'_def wt_tuple_def)
  apply (metis (no_types, lifting) Int_commute disjoint_insert(2) domIff insert_dom multi_member_last option.case_eq_if wt_rel'_def wt_tuple_def)
  apply (metis (no_types, lifting) attr_dom_rs_t domI map_add_def map_le_def multi_member_last restrict_inter_le)
  apply (metis (no_types, lifting) attr_dom_rs_t domIff map_add_def map_le_def multi_member_last option.simps(3) restrict_inter_le)
proof -
  fix F1 :: selection_formule and F2 :: selection_formule
  assume a1: "\<not> eval_F F2 (\<lambda>xa. case y xa of None \<Rightarrow> x xa | Some x \<Rightarrow> Some x)"
  assume a2: "{#\<lambda>xa. case y xa of None \<Rightarrow> x xa | Some x \<Rightarrow> Some x#} = (if eval_F F1 (\<lambda>xa. case y xa of None \<Rightarrow> x xa | Some x \<Rightarrow> Some x) then add_mset (\<lambda>xa. case y xa of None \<Rightarrow> x xa | Some x \<Rightarrow> Some x) (filter_mset (eval_F F1) {#}) else filter_mset (eval_F F1) {#})"
  assume a3: "(if eval_F F1 (\<lambda>xa. case y xa of None \<Rightarrow> x xa | Some x \<Rightarrow> Some x) then add_mset (\<lambda>xa. case y xa of None \<Rightarrow> x xa | Some x \<Rightarrow> Some x) (filter_mset (eval_F F1) {#}) else filter_mset (eval_F F1) {#}) = filter_mset (eval_F F2) (if eval_F F1 (\<lambda>xa. case y xa of None \<Rightarrow> x xa | Some x \<Rightarrow> Some x) then add_mset (\<lambda>xa. case y xa of None \<Rightarrow> x xa | Some x \<Rightarrow> Some x) (filter_mset (eval_F F1) {#}) else filter_mset (eval_F F1) {#})"
  have "filter_mset (eval_F F2) {#\<lambda>cs. case y cs of None \<Rightarrow> x cs | Some x \<Rightarrow> Some x#} = {#}"
    using a1 by simp
  then show False
    using a3 a2 by force
qed


lemma push_join_through_select_sg:
  assumes "wt_rel' rs1 {#x#}"
      and "wt_rel' rs2 ri"
      and "selection_pre F rs1"
      and "dom rs1 \<inter> dom rs2 = {}"
    shows "((\<sigma>' F {#x#}) \<Join>\<^sub>b ri) = \<sigma>' F ({#x#} \<Join>\<^sub>b ri)"
using assms 
proof(induction ri)
  case empty
  then show ?case by simp
next
  case (add y ri)
  have "\<sigma>' F {#x#}  \<Join>\<^sub>b (add_mset y ri) =  (\<sigma>' F   {#x#} \<Join>\<^sub>b  {#y#})+  ( \<sigma>' F  {#x#} \<Join>\<^sub>b ri ) "
    using add_mset_natjoin_r by blast
  also have " ... =   (\<sigma>' F   {#x#} \<Join>\<^sub>b  {#y#}) +  ( \<sigma>' F  ({#x#} \<Join>\<^sub>b ri ) ) " 
  using add wt_rel'_add_mset by presburger
   also have "... = ( \<sigma>' F   ({#x#} \<Join>\<^sub>b  {#y#}) ) +   ( \<sigma>' F  ({#x#} \<Join>\<^sub>b ri ) )"	
     using push_join_through_select_sg_sg add wt_rel'_add_mset_sg by metis
   also have "... =  \<sigma>' F   ( ({#x#} \<Join>\<^sub>b  {#y#}) +   ({#x#} \<Join>\<^sub>b ri ) )  "
    using add_sel by auto
   also have "... =  \<sigma>' F   ( {#x#} \<Join>\<^sub>b ({#y#} + ri )  )  " 
   using natjoin_plus by presburger
  finally show ?case by auto
qed


lemma push_join_through_select1:
  assumes "wt_rel' rs1 ri1"
      and "wt_rel' rs2 ri2"
      and "selection_pre F rs1"
      and "dom rs1 \<inter> dom rs2 = {}"
    shows "((\<sigma>' F ri1) \<Join>\<^sub>b ri2) = \<sigma>' F (ri1 \<Join>\<^sub>b ri2)"
  using assms 
proof(induction ri1)
  case empty
  then show ?case by simp
next
  case (add x ri1)
  have "\<sigma>' F (add_mset x ri1)  \<Join>\<^sub>b ri2 =  (\<sigma>' F   {#x#} +   \<sigma>' F  ri1 ) \<Join>\<^sub>b ri2"
    by (simp add: sel_sg_add)
  also have " ... = (\<sigma>' F   {#x#} \<Join>\<^sub>b ri2 ) +  ( \<sigma>' F  ri1 \<Join>\<^sub>b ri2 )  "
    using plus_natjoin by auto
  also have "... =  (\<sigma>' F   {#x#} \<Join>\<^sub>b ri2 ) +  ( \<sigma>' F ( ri1 \<Join>\<^sub>b ri2 ) )  "	
    using add  wt_rel'_add_mset by presburger
  also have "... = ( \<sigma>' F   ({#x#} \<Join>\<^sub>b ri2 ) ) +  ( \<sigma>' F ( ri1 \<Join>\<^sub>b ri2 ) )  "	
    using push_join_through_select_sg add wt_rel'_add_mset_sg by metis
   also have "... =  \<sigma>' F   (({#x#} \<Join>\<^sub>b ri2 )  +   ( ri1 \<Join>\<^sub>b ri2 ) )  "	 
     using add_sel by auto
   also have "... =  \<sigma>' F   ( ({#x#} + ri1) \<Join>\<^sub>b ri2  )  "	 
     using plus_natjoin by presburger
  finally show ?case by auto
qed
   

lemma push_join_through_select2:
  assumes "wt_rel' rs1 ri1"
      and "wt_rel' rs2 ri2"
      and "selection_pre F rs2"
      and "dom rs1 \<inter> dom rs2 = {}"
    shows "( ri1 \<Join>\<^sub>b (\<sigma>' F ri2)) = \<sigma>' F (ri1 \<Join>\<^sub>b ri2)"
  by (metis inf_commute natjoin_comm' push_join_through_select1 assms)


lemma wt_rel'_dom_inter_sg_sg:
  assumes "wt_rel' rs1 {#x#}"
      and "wt_rel' rs2 {#y#}"
      and "projection_pre l rs1"
      and "dom rs1 \<inter> dom rs2 = {}"
    shows  " (\<lambda>xa. if xa \<in> dom x \<and> xa \<in> dom y then x xa else None) =
     (\<lambda>xa. if xa \<in> dom x \<and> xa \<in> dom y then y xa else None)"
  apply rule                     
  using assms wt_rel'_def wt_tuple_def projection_pre_def by auto

lemma push_join_through_project_sg_sg:
  assumes "wt_rel' rs1 {#x#}"
      and "wt_rel' rs2 {#y#}"
      and "projection_pre l rs1"
      and "dom rs1 \<inter> dom rs2 = {}"
    shows "(\<pi>' l {#x#}) \<Join>\<^sub>b  {#y#} = \<pi>' (l\<union>dom rs2) ({#x#} \<Join>\<^sub>b {#y#})"
    using assms
    unfolding projection_inst'_def natjoin_inst'_def restrict_map_def map_add_def
    apply auto
    apply(rule ext)
    apply(auto split:option.splits)
    apply (meson attr_dom_rs_t domI domIff multi_member_last)
    apply (simp add: domD domI wt_rel'_def wt_tuple_def)
    apply(rule ext)
    apply(auto split:option.splits)
    apply (metis domI option.inject)
    apply (simp add: domD domI wt_rel'_def wt_tuple_def)
    using wt_rel'_dom_inter_sg_sg assms 
    by meson


lemma natjoin_projection_sg_sg:
  assumes "wt_rel' rs1 {#x#}"
      and "wt_rel' rs2 {#y#}"
      and "projection_pre l rs1"
      and "projection_pre l rs2"
      and "dom rs1 \<inter> dom rs2 = {}"
        shows "(\<pi>' l {#x#}) \<Join>\<^sub>b  (\<pi>' l {#y#})  = \<pi>' l ({#x#} \<Join>\<^sub>b {#y#})"
  using assms 
  by (fastforce  simp: wt_rel'_dom_inter_sg_sg[of rs1 x rs2 y l ] natjoin_inst'_def map_add_def restrict_map_def projection_inst'_def projection_pre_def   )

lemma  natjoin_projection_sg:
  assumes "wt_rel' rs1 {#x#}"
      and "wt_rel' rs2 ri2"
      and "projection_pre l rs1"
      and "projection_pre l rs2"
      and "dom rs1 \<inter> dom rs2 = {}"
      shows "(\<pi>' l {#x#}) \<Join>\<^sub>b  (\<pi>' l ri2)  = \<pi>' l ({#x#} \<Join>\<^sub>b ri2)"
  using assms 
  apply(induction ri2)
  apply auto 
  by (metis natjoin_projection_sg_sg add_mset_add_single image_mset_union natjoin_plus projection_inst'_def wt_rel'_add_mset wt_rel'_add_mset_sg)


lemma  push_join_through_project:
  assumes "wt_rel' rs1 ri1"
   and "wt_rel' rs2 ri2"
   and "projection_pre l rs1"
   and "projection_pre l rs2"
   and "dom rs1 \<inter> dom rs2 = {}"
   shows "(\<pi>' l ri1) \<Join>\<^sub>b  (\<pi>' l ri2)  = \<pi>' l (ri1 \<Join>\<^sub>b ri2)"
   using assms 
   apply(induction ri1) 
   apply auto
   by (metis natjoin_projection_sg add_mset_add_single image_mset_union plus_natjoin projection_inst'_def wt_rel'_add_mset wt_rel'_add_mset_sg)
  
lemma push_join_through_project_sg:
  assumes "wt_rel' rs1 {#x#}"
      and "wt_rel' rs2 ri2"
      and "projection_pre l rs1"
      and "dom rs1 \<inter> dom rs2 = {}"
    shows "(\<pi>' l {#x#}) \<Join>\<^sub>b  ri2 = \<pi>' (l\<union>dom rs2) ({#x#} \<Join>\<^sub>b ri2)"
    using assms
    apply(induction ri2)
    apply auto
    proof -
    fix xa :: "char list \<Rightarrow> val option" and ri2a :: "(char list \<Rightarrow> val option) multiset"
    assume a1: "wt_rel' rs1 {#x#}"
    assume a2: "projection_pre l rs1"
    assume a3: "dom rs1 \<inter> dom rs2 = {}"
    assume a4: "wt_rel' rs2 (add_mset xa ri2a)"
    assume a5: "wt_rel' rs2 ri2a \<Longrightarrow> \<pi>' l {#x#} \<Join>\<^sub>b ri2a = \<pi>' (l \<union> dom rs2) ({#x#} \<Join>\<^sub>b ri2a)"
    have f6: "\<pi>' l {#x#} \<Join>\<^sub>b add_mset xa ri2a = add_mset xa ri2a \<Join>\<^sub>b {#f |` l. f \<in># {#x#}#}"
      using natjoin_comm' projection_inst'_def by auto
    have f7: "{#xa#} \<Join>\<^sub>b {#f |` l. f \<in># {#x#}#} = \<pi>' (l \<union> dom rs2) ({#xa#} \<Join>\<^sub>b {#x#})"
      using a4 a3 a2 a1 natjoin_comm' projection_inst'_def push_join_through_project_sg_sg wt_rel'_add_mset_sg by force
    have f8: "ri2a \<Join>\<^sub>b {#f |` l. f \<in># {#x#}#} = \<pi>' l {#x#} \<Join>\<^sub>b ri2a"
      by (simp add: natjoin_comm' projection_inst'_def)
    have "{#xa#} \<Join>\<^sub>b {#x#} + {#x#} \<Join>\<^sub>b ri2a = {#x#} \<Join>\<^sub>b add_mset xa ri2a"
      by (metis (no_types) add_mset_natjoin_l natjoin_comm')
    then show "\<pi>' l {#x#} \<Join>\<^sub>b add_mset xa ri2a = \<pi>' (l \<union> dom rs2) ({#x#} \<Join>\<^sub>b add_mset xa ri2a)"
      using f8 f7 f6 a5 a4 by (metis (no_types) add_mset_natjoin_l image_mset_union projection_inst'_def wt_rel'_add_mset)
  qed
    
lemma push_join_through_project1:
  assumes "wt_rel' rs1 ri1"
      and "wt_rel' rs2 ri2"
      and "projection_pre l rs1"
      and "dom rs1 \<inter> dom rs2 = {}"
    shows "(\<pi>' l ri1) \<Join>\<^sub>b  ri2 = \<pi>' (l\<union>dom rs2) (ri1 \<Join>\<^sub>b ri2)"
    using assms
    apply(induction ri1)
    apply auto
  proof -
    fix x :: "char list \<Rightarrow> val option" and ri1a :: "(char list \<Rightarrow> val option) multiset"
    assume a1: "wt_rel' rs1 ri1a \<Longrightarrow> \<pi>' l ri1a \<Join>\<^sub>b ri2 = \<pi>' (l \<union> dom rs2) (ri1a \<Join>\<^sub>b ri2)"
    assume a2: "wt_rel' rs1 (add_mset x ri1a)"
    assume a3: "wt_rel' rs2 ri2"
    assume a4: "projection_pre l rs1"
    assume a5: "dom rs1 \<inter> dom rs2 = {}"
    have f6: "\<pi>' l (add_mset x ri1a) \<Join>\<^sub>b ri2 = ri2 \<Join>\<^sub>b ({#f |` l. f \<in># ri1a#} + {#f |` l. f \<in># {#x#}#})"
      using natjoin_comm' projection_inst'_def by auto
    have f7: "{#f |` (l \<union> dom rs2). f \<in># ri2 \<Join>\<^sub>b ri1a#} = ri2 \<Join>\<^sub>b \<pi>' l ri1a"
      using a2 a1 by (metis (no_types) natjoin_comm' projection_inst'_def wt_rel'_add_mset)
    have "wt_rel' rs1 {#x#}"
      using a2 wt_rel'_add_mset_sg by auto
    then have f8: "{#f |` (l \<union> dom rs2). f \<in># ri2 \<Join>\<^sub>b {#x#}#} = ri2 \<Join>\<^sub>b {#f |` l. f \<in># {#x#}#}"
    using a5 a4 a3 natjoin_comm' projection_inst'_def push_join_through_project_sg by force
     have "add_mset x ri1a \<Join>\<^sub>b ri2 = ri2 \<Join>\<^sub>b (ri1a + {#x#})"
    using natjoin_comm' by auto
    then show "\<pi>' l (add_mset x ri1a) \<Join>\<^sub>b ri2 = \<pi>' (l \<union> dom rs2) (add_mset x ri1a \<Join>\<^sub>b ri2)"
    using f8 f7 f6 by (metis image_mset_union natjoin_plus projection_inst'_def)
qed
    
lemma push_join_through_project2:
  assumes "wt_rel' rs1 ri1"
      and "wt_rel' rs2 ri2"
      and  "projection_pre l rs2"
      and "dom rs1 \<inter> dom rs2 = {}"
    shows " ri1 \<Join>\<^sub>b  (\<pi>' l ri2)  = \<pi>' (l \<union> dom rs1) (ri1 \<Join>\<^sub>b ri2)"
  by (metis inf.commute  assms push_join_through_project1  natjoin_comm' )

lemma push_project_through_select_sg:
" \<lbrakk>wt_rel' rs {#x#}; projection_pre l rs; selection_pre F (rs |` l)\<rbrakk>
 \<Longrightarrow> \<pi>' l (\<sigma>' F {#x#}) = \<sigma>' F (\<pi>' l {#x#})"
proof(induction F)
  case (And F1 F2)
  thus ?case 
    apply (auto simp:restrict_map_def projection_inst'_def selection_inst'_def projection_pre_def)
    apply (metis empty_not_add_mset filter_empty_mset)
    by (metis filter_empty_mset multi_self_add_other_not_self)
qed(auto simp:restrict_map_def projection_inst'_def selection_inst'_def projection_pre_def) 
   
theorem push_select_through_project:
" \<lbrakk>wt_rel' rs ri; projection_pre l rs; selection_pre F (rs |` l)\<rbrakk>
 \<Longrightarrow> \<pi>' l (\<sigma>' F ri) = \<sigma>' F (\<pi>' l ri)"
  apply(induction ri)
  apply auto
  by (metis push_project_through_select_sg add_mset_add_single image_mset_union projection_inst'_def sel_add selection_pre_def wt_rel'_add_mset wt_rel'_add_mset_sg)

(*End of Relational algerbra with Bag*)

(*Connecting classical relational algebra to the modified version with multiset*)


section\<open>Connecting the two algebras\<close>

abbreviation abs_rel :: "rel_inst' \<Rightarrow> rel_inst"  where "abs_rel \<equiv> set_mset"
definition abs_dbs :: "db_inst' \<Rightarrow> db_inst" where 
    "abs_dbs = (\<lambda>dbs rn. (if rn \<in> (dom dbs ) then Some (abs_rel (the (dbs rn))) else None))"

lemma selection_inst'_correct:"abs_rel (\<sigma>' F ri) =  \<sigma> F (abs_rel ri)"
  by (auto simp:   selection_inst'_def selection_inst_def)

lemma  projection_inst'_correct:"abs_rel (\<pi>' l ri) =  \<pi> l (abs_rel ri)"
  by (auto simp:  projection_inst'_def projection_inst_def)

lemma natjoin_inst'_correct:"abs_rel ( ri1 \<Join>\<^sub>b ri2) = (abs_rel ri1 \<Join> abs_rel ri2)"
  by (auto simp:  natjoin_inst'_def natjoin_inst_def)

lemma renaming_inst'_correct:"abs_rel (\<rho>' f ri) = \<rho> f  (abs_rel ri) "
  by (auto simp:  renaming_inst'_def renaming_inst_def)

lemma union_inst'_correct:"abs_rel ( ri1 \<union>\<^sub>b ri2) = (abs_rel ri1 \<union>\<^sub>r abs_rel ri2)"
  by (auto simp:  union_inst'_def union_inst_def)

lemma inter_inst'_correct:"abs_rel ( ri1 \<inter>\<^sub>b ri2) = (abs_rel ri1 \<inter> abs_rel ri2)"
  by (auto simp:  inter_inst_def )

text\<open>We can not always abstract with the difference operator \<close>

lemma ex_diff_notabs:"\<exists>ri1 ri2 . abs_rel ( ri1 -\<^sub>b ri2) \<noteq> abs_rel ri1 -\<^sub>r abs_rel ri1"
  apply (auto simp: diff_inst'_def diff_inst_def)
  apply(rule exI[of _ "{#[''A'' \<mapsto> Iv 1],[''A'' \<mapsto> Iv 1]#}"])
  apply(rule exI[of _ "{#[''A'' \<mapsto> Iv 1]#}"])
  by simp

lemma ex_diff_abs:"\<exists>ri1 ri2 . abs_rel ( ri1 -\<^sub>b ri2) = abs_rel ri1 -\<^sub>r abs_rel ri1"
  apply (auto simp: diff_inst'_def diff_inst_def)
  apply(rule exI[of _ "{#[''A'' \<mapsto> Iv 1]#}"])
  apply(rule exI[of _ "{#[''A'' \<mapsto> Iv 1],[''A'' \<mapsto> Iv 1]#}"])
  by simp

end