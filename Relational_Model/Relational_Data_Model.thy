theory Relational_Data_Model 
imports Main
begin

section\<open>Data definition\<close>

type_synonym name = string  
type_synonym attr = name 
type_synonym relname = name 

text\<open>Domains and Constants\<close>

datatype domain = INTEGER | STRING  
datatype val = Iv int | Sv string

text\<open>relation's signtures, database schema, tuples\<close>

type_synonym rel_sign = "attr \<rightharpoonup> domain"
type_synonym db_schema = "relname \<rightharpoonup> rel_sign"
type_synonym tuple = "attr \<rightharpoonup> val"

text\<open>function sort computes the set of attributes of
     a given relation name r definite in a database schema db\<close>

definition sort :: "db_schema  \<Rightarrow> relname \<Rightarrow> attr set " where
  "sort db r = dom (the (db r))"

text\<open>function arity computes the size of a relation schema or a data base\<close>

definition arity :: "('a \<rightharpoonup> 'b) \<Rightarrow> nat " where
  "arity r = card (dom r)"

text\<open>Restrict a tuple t to a set of attributes U \<close>
definition restrict_tuple ::"('a \<Rightarrow> 'b option) \<Rightarrow> 'a set \<Rightarrow> ('a \<Rightarrow> 'b option) " ("(_/\<langle>_\<rangle>)"  [0, 61] 61)  where
"t\<langle>U\<rangle> = restrict_map t U "

(*inductive wt_val :: "domain \<Rightarrow> val \<Rightarrow> bool" where
  "wt_val INTEGER (Iv _)"
| "wt_val STRING (Sv _)"  
*)  
subsection\<open>Well-typeness of data\<close>

text\<open>wt_val compute if a given value is well-typed with respect to a given domain\<close>

fun wt_val :: "domain \<Rightarrow> val \<Rightarrow> bool" where
  "wt_val INTEGER (Iv _) \<longleftrightarrow> True"
| "wt_val STRING (Sv _) \<longleftrightarrow> True"  
| "wt_val _ _ \<longleftrightarrow> False"

text\<open>wt_tuple compute if a given tuple t is well-typed with respect to a relation signature rs\<close>

definition wt_tuple :: "rel_sign \<Rightarrow> tuple \<Rightarrow> bool" where
  "wt_tuple rs t \<longleftrightarrow> 
    (dom rs = dom t) \<and> 
    (\<forall>attr d v. rs attr = Some d \<and> t attr = Some v \<longrightarrow> wt_val d v)"

lemma wt_tuple_alt:" wt_tuple rs t  \<longleftrightarrow> 
    (dom rs = dom t) \<and> 
    (\<forall>attr \<in> (dom rs). wt_val (the (rs attr)) (the (t attr)) ) "
  by(force simp:wt_tuple_def)


lemma tuple_fin: "wt_tuple rs t \<Longrightarrow> finite (dom rs) \<Longrightarrow> finite (dom t)"
  unfolding  wt_tuple_def
  apply auto
  done

lemma rs_empty_wt_tuple:"dom rs = {} \<Longrightarrow> wt_tuple rs t \<Longrightarrow> t = empty "
  by(auto simp add:wt_tuple_def)

(***************Selection*****************)
section\<open>Relational Algebra: Preconditions and Schemas\<close>

subsection\<open>selection\<close>

text\<open> The different forms that a selection's condition can have \<close>

datatype selection_formule =
              Eqc attr val  ( infix "=\<^sub>c" 60)|
              Eqa attr attr  (infix "=\<^sub>a" 60)|
              (*Positive conjunctive selection formulas*)
              And selection_formule selection_formule  (infixl "\<and>\<^sub>s"  70 )

text\<open>Verify the validity of a selection's condition for a given relation\<close>

fun selection_valid_F :: " selection_formule \<Rightarrow> rel_sign \<Rightarrow> bool"    
  where "selection_valid_F  (A =\<^sub>c a) rs = (\<exists> d. rs A = Some d  \<and> wt_val d a)" |
        "selection_valid_F  (A =\<^sub>a B) rs =  (\<exists> d1 d2. rs A = Some d1 \<and> rs B = Some d2  \<and> d1 = d2 )" |
        "selection_valid_F  (F\<^sub>1 \<and>\<^sub>s F\<^sub>2) rs = (selection_valid_F F\<^sub>1 rs \<and> selection_valid_F F\<^sub>2 rs)" 


lemma selection_valid_F_alt:"selection_valid_F F rs \<longleftrightarrow> (case F 
              of (A =\<^sub>c a) \<Rightarrow> ( A \<in> dom rs  \<and> wt_val (the (rs A)) a) |
                 (A =\<^sub>a B) \<Rightarrow> ( A \<in> dom rs \<and> B \<in> dom rs \<and> rs A = rs B) |
                 (F\<^sub>1 \<and>\<^sub>s F\<^sub>2) \<Rightarrow> (selection_valid_F F\<^sub>1 rs \<and> selection_valid_F F\<^sub>2 rs) 
                )"
  by(auto split:selection_formule.splits)

text\<open>Evalutes a selection's condition for a given tuple\<close>

fun eval_F :: "selection_formule \<Rightarrow> tuple  \<Rightarrow> bool" where
"eval_F (A =\<^sub>c a) t \<longleftrightarrow>  (t A) = Some a" | 
"eval_F (A =\<^sub>a B) t \<longleftrightarrow> t A = t B" |
"eval_F (F\<^sub>1 \<and>\<^sub>s F\<^sub>2) t \<longleftrightarrow> eval_F F\<^sub>1 t \<and> eval_F F\<^sub>2 t"

text\<open>Preconditions that need to be satisfied before selection application\<close>

definition selection_pre :: " selection_formule \<Rightarrow> rel_sign \<Rightarrow> bool"    
  where [simp]: "selection_pre F rs =  selection_valid_F F rs"

lemma sel_pre_tA_not_none: "selection_pre (A =\<^sub>c a) rs \<Longrightarrow>  wt_tuple rs t \<Longrightarrow> (t A) \<noteq> None"
  unfolding selection_pre_def   wt_tuple_def
  apply(auto)
  done

lemma sel_pre_tAB_not_none: "selection_pre (A =\<^sub>a B) rs \<Longrightarrow>  wt_tuple rs t \<Longrightarrow> (t A) \<noteq> None \<and> (t B) \<noteq> None"
  unfolding selection_pre_def wt_tuple_def
  apply(auto)
  done

text\<open>selection's result schema\<close>

definition selection_schema :: "selection_formule \<Rightarrow> rel_sign \<Rightarrow> rel_sign"
  where [simp]: "selection_schema _ rs = rs"

lemma sel_preserve_schema_fin:
  assumes "(finite (dom rs))"
  shows "finite (dom (selection_schema F rs))"
  using assms
  by(auto)

subsection\<open>Projection\<close>

text\<open>Preconditions that need to be satisfied before Projection application\<close>

definition projection_pre :: "attr set \<Rightarrow> rel_sign \<Rightarrow> bool"
  where "projection_pre aset rs \<longleftrightarrow> aset \<subseteq> dom rs"

text\<open>Projection's result schema\<close>

definition projection_schema :: "attr set \<Rightarrow> rel_sign \<Rightarrow> rel_sign"
  where "projection_schema aset rs = rs |` aset"
  
lemma proj_empty:"projection_schema {} rs = empty"
 by(auto simp:projection_schema_def)

lemma projection_pre_subset:
   assumes "projection_pre k rs"
   assumes "j \<subseteq> k"
   shows "projection_pre j (projection_schema k rs)"
   using assms
   unfolding projection_pre_def projection_schema_def  
   by auto

lemma proj_preserve_schema_fin:
  assumes "(finite (dom rs))"
  shows "finite (dom (projection_schema l rs))"
  using assms
  by(auto simp:projection_schema_def)

subsection\<open>Natural Join\<close>

text\<open>Preconditions that need to be satisfied before Natural join application\<close>

definition natjoin_pre :: "rel_sign \<Rightarrow> rel_sign \<Rightarrow> bool" 
  where "natjoin_pre rs1 rs2 = (\<forall>attr d1 d2. rs1 attr = Some d1 \<and> rs2 attr = Some d2  \<longrightarrow> d1=d2)"

lemma natjoin_pre_alt1:" natjoin_pre rs1 rs2 = (\<forall> attr. attr \<in> dom rs1 \<and> attr \<in> dom rs2 \<longrightarrow> rs1 attr =  rs2 attr ) "
   unfolding natjoin_pre_def 
   by auto

lemma natjoin_pre_alt2:" natjoin_pre rs1 rs2 =  (\<forall>attr \<in> dom rs1 \<inter> dom rs2. rs1 attr =  rs2 attr )"
     unfolding natjoin_pre_def 
     by (metis (mono_tags, lifting) IntD1 IntD2 IntI domD domI option.inject)

text\<open>Natural Join's result schema\<close>

definition natjoin_schema :: "rel_sign \<Rightarrow> rel_sign \<Rightarrow> rel_sign"
  where "natjoin_schema rs1 rs2 = rs1 ++ rs2"

lemma natjoin_assoc_rs:"natjoin_schema rs1 ( natjoin_schema rs2 rs3) =
                        natjoin_schema (natjoin_schema rs1  rs2) rs3"
  unfolding natjoin_schema_def
  by auto

lemma natjoin_preserve_schema_fin:
  assumes "(finite (dom rs1)) "
      and "(finite (dom rs2)) "
  shows "finite (dom (natjoin_schema rs1 rs2))"
  using assms
  by(auto simp:natjoin_schema_def)

definition sizeM :: "('a \<rightharpoonup> 'b) \<Rightarrow> nat " where
"sizeM m = (if m = empty then 0 else card (dom m))"

lemma join_prod_schema_card:
assumes "(dom rs1 \<inter> dom rs2) = {}"
assumes "finite (dom rs1)"
assumes "finite (dom rs2)"
shows "card (dom (natjoin_schema rs1 rs2)) = card (dom rs1) + card (dom rs2) "
unfolding natjoin_schema_def  
using assms
  by (simp add:  card_Un_disjoint inf_commute)

subsection\<open>Renaming\<close>

text\<open>Renaming a map \<close>

definition rename :: "('a \<rightharpoonup> 'b) \<Rightarrow> ('a \<rightharpoonup> 'a) \<Rightarrow> ('a \<rightharpoonup> 'b) " where
"rename m \<rho> k = (if k \<in> dom m - dom \<rho> then m k else if
                            k \<in> ran \<rho> then m (THE k'. \<rho> k' = Some k) else None )"


text\<open>Conditions that need to be satisfied by the renaming function\<close>

definition rename_pre_set :: "'a set \<Rightarrow> ('a \<rightharpoonup> 'a) \<Rightarrow> bool" where
  "rename_pre_set s \<rho> \<longleftrightarrow> dom \<rho> \<subseteq> s \<and> inj_on \<rho> (dom \<rho>) \<and> (s - dom \<rho>) \<inter> ran \<rho> = {}"

definition rename_pre_set2 :: "'a set \<Rightarrow> ('a \<rightharpoonup> 'a) \<Rightarrow> bool" where
  "rename_pre_set2 s \<rho> \<longleftrightarrow> dom \<rho> \<subseteq> s \<and> inj_on \<rho> (dom \<rho>) \<and> s \<inter> ran \<rho> = {}"

text\<open>Preconditions that need to be satisfied before renaming Application\<close>

abbreviation rename_pre ::  "('a \<rightharpoonup> 'b) \<Rightarrow> ('a \<rightharpoonup> 'a) \<Rightarrow> bool" where
  "rename_pre m \<rho> \<equiv> rename_pre_set (dom m) \<rho>"

text\<open>We use this if we do not want to allow swaping of attribute names or 
having an attribute renamed to itself\<close>

abbreviation rename_pre2 ::  "('a \<rightharpoonup> 'b) \<Rightarrow> ('a \<rightharpoonup> 'a) \<Rightarrow> bool" where
  "rename_pre2 m \<rho> \<equiv> rename_pre_set2 (dom m) \<rho>"

lemma uniq_key:
assumes " inj_on \<rho> (dom \<rho>)"
assumes "\<rho> a = Some x"
shows "(THE k'. \<rho> k' = Some x) = a"
using assms 
proof -
  obtain aa :: "'a \<Rightarrow> ('a \<Rightarrow> bool) \<Rightarrow> 'a" where
    "\<forall>x0 x1. (\<exists>v2. x1 v2 \<and> v2 \<noteq> x0) = (x1 (aa x0 x1) \<and> aa x0 x1 \<noteq> x0)"
    by moura
  then have "\<forall>p a. (\<not> p a \<or> p (aa a p) \<and> aa a p \<noteq> a) \<or> p (The p)"
    by (metis theI)
  then have "\<rho> (aa a (\<lambda>a. \<rho> a = Some x)) = Some x \<and> aa a (\<lambda>a. \<rho> a = Some x) \<noteq> a \<or> \<rho> (THE a. \<rho> a = Some x) = Some x"
    using assms(2) by fastforce
  then show ?thesis
    by (metis assms(1) assms(2) domI inj_onD)
qed

lemma dom_rename_eq:
assumes "rename_pre m \<rho>"
shows "dom (rename m \<rho>) =  (dom m - dom \<rho>) \<union>  ran \<rho> "
using assms
unfolding rename_pre_set_def rename_def[abs_def] 
by (auto 0 3 simp: uniq_key ran_def split: if_splits)


lemma dom_rename_eq2:
assumes "rename_pre2 m \<rho>"
shows "dom (rename m \<rho>) =  (dom m - dom \<rho>) \<union>  ran \<rho> "
using assms
unfolding rename_pre_set2_def rename_def[abs_def] 
  by (auto 0 3 simp: uniq_key ran_def split: if_splits)

lemma rename_eq_Some_iff:
  assumes "rename_pre m \<rho>"  
  shows "rename m \<rho> k = Some v \<longleftrightarrow> (\<exists>k'. m k' = Some v \<and> ( \<rho> k' = Some k  \<or>  k'=k \<and> k\<notin>dom \<rho> ))"
  using assms
  unfolding rename_pre_set_def rename_def  
  apply (auto simp: uniq_key ran_def) 
  apply (metis domI inj_on_contraD)  
  apply (metis domI inj_on_contraD)  
  done

lemma rename_eq_Some_iff2:
  assumes "rename_pre2 m \<rho>"  
  shows "rename m \<rho> k = Some v \<longleftrightarrow> (\<exists>k'. m k' = Some v \<and> ( \<rho> k' = Some k  \<or>  k'=k \<and> k\<notin>dom \<rho> ))"
  using assms
  unfolding rename_pre_set2_def rename_def  
  apply (auto simp: uniq_key ran_def) 
  apply (metis domI inj_on_contraD)  
  done 
  
lemma ren_pre_some_restrict:
shows "rename_pre m (Some |` dom m)"
unfolding rename_pre_set_def inj_on_def
by auto

lemma  ren_pre2_empty:
shows "rename_pre2 m empty"
unfolding rename_pre_set2_def 
  by auto

lemma ren_some_restrict_k:
shows "rename m (Some |` dom m) k = m k"
unfolding rename_def
apply(auto simp : ran_def)
apply(subst uniq_key;auto?)
apply(auto simp:inj_on_def restrict_map_def dom_def split:if_splits)
  by force

lemma ren_some_restrict_k_empty:
"rename m (Some |` dom m) k = rename m empty k"
  unfolding rename_def
  apply(auto simp : ran_def)
  apply(subst uniq_key;auto?)
  apply(auto simp:inj_on_def restrict_map_def dom_def split:if_splits)
  by force

(*TODO: Move to Map.thy*)
lemma singleton_eq_iff[simp]: "[k\<mapsto>v] = [k' \<mapsto> v'] \<longleftrightarrow> k=k' \<and> v=v'"
apply auto
apply (metis fun_upd_apply option.distinct(1))
  by (metis (mono_tags, lifting) map_upd_Some_unfold)

text\<open>Using this predicate will not allow rename to swap attributes names 
in case the new type is different\<close>

definition rename_preserve_type :: "('a \<Rightarrow> 'b option) \<Rightarrow> ('a \<Rightarrow> 'a option) \<Rightarrow> bool" where
    "rename_preserve_type rs f = (\<forall>a b.  rs a = Some b \<longrightarrow> ((rename rs f) a = Some b) \<or> (rename rs f) a =  None)"

text\<open>Renaming's result schema\<close>

definition renaming_schema :: "rel_sign \<Rightarrow> (attr \<rightharpoonup> attr) \<Rightarrow> rel_sign " where
"renaming_schema rs f = rename rs f"

lemma rename_rs_no_in_eq_out:"rename_pre2 rs \<rho> \<Longrightarrow> a \<in> dom \<rho> \<Longrightarrow> \<rho> a \<noteq> Some a"
  by (auto;metis  rename_pre_set2_def DiffD2 Diff_triv domI ranI subset_eq)

lemma renaming_preserve_schema_fin:
  assumes "(finite (dom rs))"
  and "rename_pre rs f"
  shows "finite (dom (renaming_schema rs f))"
  using assms
  by(auto simp:assms(2) dom_rename_eq finite_ran finite_subset renaming_schema_def rename_def rename_pre_set_def)

subsection\<open>Union\<close>

text\<open>Preconditions that need to be satisfied before Union application\<close>

definition union_pre :: "rel_sign \<Rightarrow> rel_sign \<Rightarrow> bool" where 
"union_pre rs1 rs2 \<longleftrightarrow> rs2 = rs1 "

text\<open>Union's result schema\<close>

definition union_schema :: "rel_sign \<Rightarrow> rel_sign \<Rightarrow> rel_sign" where 
"union_schema rs1 rs2 = rs1"

lemma union_preserve_schema_fin:
  assumes "(finite (dom rs1)) "
      and "(finite (dom rs2)) " (*Not necessary since union_schema rs1 rs2 = rs1*)
  shows "finite (dom (union_schema rs1 rs2))"
  using assms(1)
  by(auto simp:union_schema_def)

subsection\<open>Difference\<close>

text\<open>Preconditions that need to be satisfied before Difference application\<close>

definition diff_pre :: "rel_sign \<Rightarrow> rel_sign \<Rightarrow> bool" where 
"diff_pre rs1 rs2  \<longleftrightarrow> rs2 = rs1 "

text\<open>Union's result schema\<close>

definition diff_schema :: "rel_sign \<Rightarrow> rel_sign \<Rightarrow> rel_sign" where 
"diff_schema rs1 rs2 = rs1"

lemma diff_preserve_schema_fin:
  assumes "(finite (dom rs1)) "
      and "(finite (dom rs2)) " (*Not necessary since union_schema rs1 rs2 = rs1*)
  shows "finite (dom (diff_schema rs1 rs2))"
  using assms(1)
  by(auto simp:diff_schema_def)


subsection\<open>Derivative operators\<close>
subsubsection\<open>Intersection\<close>

text\<open>Preconditions that need to be satisfied before Intersection application\<close>

definition inter_pre :: "rel_sign \<Rightarrow> rel_sign \<Rightarrow> bool" where 
"inter_pre rs1 rs2  \<longleftrightarrow> diff_pre rs1 rs2 "

text\<open>Intersection's result schema\<close>

definition inter_schema :: "rel_sign \<Rightarrow> rel_sign \<Rightarrow> rel_sign" where 
"inter_schema rs1 rs2 = diff_schema rs1 rs2 "

end
