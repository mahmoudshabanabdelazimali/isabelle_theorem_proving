theory MahmoudShabanAbdelazimAli11_2
  imports Main
begin
(* Homework 11.2 Fixed point reasoning *)
theorem
  assumes "inj (f :: 'a \<Rightarrow> 'b)" and "inj (g :: 'b \<Rightarrow> 'a)"
  shows "\<exists>h :: 'a \<Rightarrow> 'b. inj h \<and> surj h"
proof
  def S \<equiv> "lfp (\<lambda>X. - (g ` (- (f ` X))))"
  let ?g' = "inv g"
  def h \<equiv> "\<lambda>z. if z \<in> S then f z else ?g' z"
  have mono_f:"mono (\<lambda>X. - (g ` (- (f ` X))))" using assms mono_def by blast
  then have 0:"S = - (g ` (- (f ` S)))" using S_def lfp_unfold by auto   
  have *: "?g' ` (- S) = - (f ` S)" by (metis "0" assms(2) double_complement image_inv_f_f)
  show "inj h \<and> surj h"
  proof
    from * show "surj h" using S_def h_def by auto
    moreover 
      have "inj_on ?g' (- S)"  using 0 inj_on_def by (smt double_complement f_inv_into_f range_eqI) 
    moreover
    { fix x y
      assume "x \<in> S" "y \<in> - S" and eq: "f x = ?g' y"
      hence False using * by blast
    }
    ultimately show "inj h"
     by (smt Compl_iff assms(1) h_def inj_onD inj_onI)
  qed
qed


end