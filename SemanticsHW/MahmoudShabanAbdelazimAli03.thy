(*<*)
theory MahmoudShabanAbdelazimAli03
imports Main "~~/src/HOL/IMP/BExp"
begin
(*>*)
(*Homework 3.1 Let expressions*)
datatype lexp = N' val | V' vname | Plus' lexp lexp | Let vname lexp lexp 

fun lval :: "lexp \<Rightarrow> state \<Rightarrow> val" where 
"lval (N' n) s = n"|
"lval (V' x) s = s x"|
"lval (Plus' a\<^sub>1 a\<^sub>2) s = lval a\<^sub>1 s + lval a\<^sub>2 s"|
"lval (Let x e1 e2) s = lval e2 (s(x := (lval e1 s))) "

value "lval (Let ''x'' (Plus' (N' 2) (N' 3)) (Plus' (V' ''x'') (N' 5))) <x:=10>"

fun subst :: "vname \<Rightarrow> aexp \<Rightarrow> aexp \<Rightarrow> aexp" where 
"subst a e (N n)  = (N n)" |
"subst a e (V v) = (if v=a then e else (V v))"|
"subst a e (Plus a1 a2) =  Plus (subst a e a1 ) (subst a e a2 )"

fun inline :: "lexp \<Rightarrow> aexp" where 
"inline (lexp.N' n) = aexp.N n" |
"inline (lexp.V' v) = aexp.V v" |
"inline (lexp.Plus' v va) = aexp.Plus (inline v) (inline va)" |
"inline (lexp.Let v va vb) =  subst v (inline va) (inline vb)" 

lemma subst_lemma[simp]: "aval (subst x a e) s = aval e (s(x := aval a s))"
  apply (induction e)
  apply auto
  done

lemma val_inline: "aval (inline e) st = lval e st"
apply (induction e arbitrary: st)
by auto

(*fun lvars :: "lexp \<Rightarrow> vname set" where 
"lvars (N' n)= {}" |
"lvars (V' v) = {v}" |
"lvars (Plus' v va) = (lvars v) Un (lvars va) "|
"lvars (Let v va vb) =  {v} Un (lvars va) Un (lvars vb)"*)
fun vars :: "aexp \<Rightarrow> vname set" where 
"vars (N n)= {}" |
"vars (V v) = {v}" |
"vars (Plus v va) = (vars v) Un (vars va) "


lemma ndep[simp]: "(x \<notin> (vars e))  \<Longrightarrow> aval e (s(x:=v)) = aval e s"
apply (induction e )
by auto

fun elim :: "lexp \<Rightarrow> lexp" where 
"elim (N' n) = N' n"|
"elim (V' v) = V' v" |
"elim (Plus' v va) =  Plus' (elim v) (elim va)"|
"elim (Let v va vb) = (if v \<in> (vars (inline(vb))) then Let v va vb else vb)"


lemma "lval (elim e) st = lval e st"
apply (induction e arbitrary: st)
apply simp
apply simp
apply simp
apply simp
using ndep val_inline by fastforce

(*Homework 3.2 Negation Normal Form*)
text \<open>Negation Normal Form\<close>

text \<open>
  In this assignment, you shall write a function that converts a boolean 
  expression over variables, conjunction, disjunction, and negation to
  negation normal form (NNF), and prove its correctness.
  A template for this homework is available on the lecture's homepage.

  We start by defining our version of boolean expressions:
\<close>

type_synonym vname = string 
type_synonym val = bool 
type_synonym state = "vname \<Rightarrow> val"

datatype bexp = Not bexp | And bexp bexp | Or bexp bexp | Var vname
fun bval :: "bexp \<Rightarrow> state \<Rightarrow> bool" -- "Definition in template"
where
  "bval (Not b) s = (\<not> bval b s)" 
| "bval (And b1 b2) s = (bval b1 s \<and> bval b2 s)" 
| "bval (Or b1 b2) s = (bval b1 s \<or> bval b2 s)" 
| "bval (Var n) s = s n"

text \<open>Next, we define a function that check whether a boolean expression is in NNF.\<close>

fun is_var where 
  "is_var (Var x) \<longleftrightarrow> True" 
| "is_var _ \<longleftrightarrow> False" -- "Note: This is a catch-all pattern for the remaining cases"

fun is_lit where "is_lit (Not b) \<longleftrightarrow> is_var b" | "is_lit b \<longleftrightarrow> is_var b"

fun is_nnf :: "bexp \<Rightarrow> bool" -- "Definition in template"
where
  "is_nnf (And a b) \<longleftrightarrow> is_nnf a \<and> is_nnf b"
| "is_nnf (Or a b) \<longleftrightarrow> is_nnf a \<and> is_nnf b"
| "is_nnf b \<longleftrightarrow> is_lit b"

text \<open>
  Define a function \<open>nnf\<close> which converts a boolean expression to NNF.
  This can be achieved by ``pushing in'' negations and eliminating double negations.
\<close>
(* Change this *)
fun nnf :: "bexp \<Rightarrow> bexp" where
  "nnf (Var x) = (Var x)" |
  "nnf (Not (Var x)) = (Not (Var x))" |
  "nnf (Not (Not e)) = nnf e" |
  "nnf (Not (And l r)) = Or (nnf (Not l)) (nnf (Not r))" |
  "nnf (Not (Or l r)) = And (nnf (Not l)) (nnf (Not r))" |
  "nnf (And l r) = And (nnf l) (nnf r)" |
  "nnf (Or l r) = Or (nnf l) (nnf r)"


text \<open>Prove that your function is correct.
  Hint: in case you are struggling to finish the proof with structural induction,
  it may be helpful to consider a different induction principle.
\<close>
(* Proof this *)
lemma [simp]: "bval (nnf b) s = bval b s"
   apply (induction b rule: nnf.induct)
   by auto

(* Proof this *)
lemma [simp]: "is_nnf (nnf b)"
  apply (induction b rule: nnf.induct)
  apply auto
  done
  
(*Homework 3.3 Conjunctive Normal Form*)
(*** BONUS ***)
text \<open>Conjunctive Normal Form\<close>
text \<open>{\em
  \textbf{Note}: This is a ``bonus'' assignment worth three additional points, making the
  maximum possible score for all homework on this sheet 13 out of 10 points.}

  \textbf{Warning}: This assignment is quite hard. Partial solutions will also be graded!
\<close>

text \<open>
  In this assignment your task is to extend the previous exercise to allow conversion
  to CNF.
  The approach we will take is to first convert expressions
  to NNF (negation normal form), and then apply the distributivity laws
  to get the CNF.
  We again start by defining a function to check whether a expression is in CNF:
\<close>

fun is_disj where
  "is_disj (Or b1 b2) \<longleftrightarrow> is_disj b1 \<and> is_disj b2" 
| "is_disj b \<longleftrightarrow> is_lit b"

fun is_cnf :: "bexp \<Rightarrow> bool" -- "Definition in template"
where
  "is_cnf (And b1 b2) \<longleftrightarrow> is_cnf b1 \<and> is_cnf b2" 
| "is_cnf b \<longleftrightarrow> is_disj b"

text \<open>
  The basic idea of converting an NNF to CNF is to first convert
  the operands of a disjunction, and then apply the distributivity law
  to get a conjunction of disjunctions. 
  The function \<open>merge (a\<^sub>1\<and>\<dots>\<and>a\<^sub>n) (b\<^sub>1\<and>\<dots>\<and>b\<^sub>m)\<close> shall return a 
  term of the form \<open>(a\<^sub>1\<or>b\<^sub>1) \<and> (a\<^sub>1\<or>b\<^sub>2) \<and> \<dots> (a\<^sub>n\<or>b\<^sub>m)\<close> that is equivalent to
  \<open>(a\<^sub>1\<and>\<dots>\<and>a\<^sub>n) \<or> (b\<^sub>1\<and>\<dots>\<and>b\<^sub>m)\<close>.
\<close>

(* Define this *)
fun merge :: "bexp \<Rightarrow> bexp \<Rightarrow> bexp" where
"merge (And a b) v = And (merge a v) (merge b v)"|
"merge v (And a b) = And (merge v a) (merge v b)"|
"merge a b = Or a b"

text \<open>Show that \<open>merge\<close> preserves the semantics and indeed yields 
  a CNF if its operands are already in CNF.
  Hint: For functions over multiple arguments, the syntax for induction is
  \<open>induction a b rule: merge.induct\<close>
\<close>

(* Proof this *)
lemma [simp]: "bval (merge a b) s \<longleftrightarrow> bval (Or a b) s"
  apply(induction a b rule: merge.induct)
  apply auto
  done

(* Proof this *)
lemma [simp]: "is_cnf a \<Longrightarrow> is_cnf b \<Longrightarrow> is_cnf (merge a b)"
  apply (induction a b rule: merge.induct)
  by auto
  
  
text \<open>Next, use \<open>merge\<close> to write a function that converts an NNF
  to a CNF. The idea is to first convert the operands of a compound expression,
  and then compute the overall CNF (using \<open>merge\<close> in 
  the \<open>Or\<close>-case)
\<close>

(* Define this *)
fun nnf_to_cnf :: "bexp \<Rightarrow> bexp" where
"nnf_to_cnf (Var v) =  (Var v)"|
"nnf_to_cnf (Not v) = (Not v)"|
"nnf_to_cnf (And v va) = And (nnf_to_cnf  v) (nnf_to_cnf va)"|
"nnf_to_cnf (Or v va) = merge (nnf_to_cnf v) (nnf_to_cnf va)"

text \<open>Prove the correctness of your function:\<close>
(* Proof this *)
lemma [simp]: "bval (nnf_to_cnf b) s = bval b s"
  apply (induction b)
  by auto

(* Proof this *)
lemma [simp]: "is_nnf b \<Longrightarrow> is_cnf (nnf_to_cnf b)"
  apply (induction b)
  by auto

text \<open>Finally, combine the two functions \<open>nnf\<close> 
  and \<open>nnf_to_cnf\<close>, to get a function that converts any boolean
  expression to CNF:\<close>
(* Proof this *)
definition cnf :: "bexp \<Rightarrow> bexp" 
  where "cnf b \<equiv> nnf_to_cnf(nnf b)"

(* Proof this *)
theorem [simp]: "bval (cnf b) s = bval b s"
apply (induction b)
by (auto simp add: cnf_def)
(* Proof this *)
theorem [simp]: "is_cnf (cnf b)"
apply (induction b)
by (auto simp add: cnf_def)

(*<*)
end
(*>*)