theory MahmoudShabanAbdelazimAli04
imports   "~~/src/HOL/IMP/AExp"
begin
(* Homework 4.1 Palindromes *)
inductive
  palindrome :: " 'a list => bool" 
    where
      "palindrome []" |
      "palindrome w \<Longrightarrow> palindrome ([a] @ w @ [a])"

lemma "palindrome xs \<Longrightarrow> rev xs = xs"
by(induction rule: palindrome.induct)(auto)
 
(*Homework 4.2 Compilation to Register Machine*)
type_synonym reg = nat
type_synonym rstate = "reg \<Rightarrow> int"

datatype instr = LDI val | LD vname | MV reg | ADD reg

fun exec :: "instr \<Rightarrow> state \<Rightarrow> rstate \<Rightarrow> rstate" where
"exec (ADD r1) s \<sigma> = \<sigma>(0 := \<sigma> r1 + \<sigma> 0)"| 
"exec (LDI v) s \<sigma> = \<sigma>(0 := v)"|
"exec (LD v) s \<sigma> = \<sigma>(0 := (s v))"|
"exec (MV v) s \<sigma> = \<sigma>(v := \<sigma> 0)"

fun execs :: "instr list \<Rightarrow> state \<Rightarrow> rstate \<Rightarrow> rstate" where 
"execs [] s \<sigma> = \<sigma>"|
"execs (v # l) s \<sigma> = execs l s (exec v s \<sigma>)"

fun cmp :: "aexp \<Rightarrow> reg \<Rightarrow> instr list" where 
"cmp (N n) r = [LDI n]"|
"cmp (V x) r =  [LD x]"|
"cmp (Plus a b) r = (cmp a (r+(1::nat))) @  
                       [MV (r+(1::nat))] @ 
                     (cmp b (r+(1::nat)))@ 
                       [ADD (r+(1::nat))]"

lemma exec_comm[simp]: "execs (a @ b) s \<sigma> = execs  b s (execs a s \<sigma>)" 
by (induction a  arbitrary: b s \<sigma>)(auto)

lemma cmp_non_alter[simp]:
assumes "i > 0" and  "i \<le> r"
shows "execs (cmp a r) s \<sigma> i =  (\<sigma> i)"
  using assms
  by(induction a arbitrary: r s \<sigma> i)(auto)

lemma cmpCorrect: "execs (cmp a r) s \<sigma> 0 = aval a s"
by (induction a arbitrary: r s \<sigma>)(auto)


end 