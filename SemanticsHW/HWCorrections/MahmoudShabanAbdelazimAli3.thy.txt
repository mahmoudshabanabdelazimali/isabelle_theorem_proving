(** Score: 2/4
- Definitions correct, proofs missing
*)
theory MahmoudShabanAbdelazimAli3
imports  "~~/src/HOL/IMP/BExp" "~~/src/HOL/IMP/Star"
begin
(*Homework 7.3 Algebra of Commands*)
datatype
  com = SKIP
      | Assign vname aexp       ("_ ::= _" [1000, 61] 61)
      | Seq    com  com         ("_;;/ _"  [60, 61] 60)
      | If     bexp com com     ("(IF _/ THEN _/ ELSE _)"  [0, 0, 61] 61)
      | While  bexp com         ("(WHILE _/ DO _)"  [0, 61] 61)
      | Par com com             (infix "\<parallel>" 59)

inductive
  small_step :: "com * state \<Rightarrow> com * state \<Rightarrow> bool" (infix "\<rightarrow>" 55)
where
Assign:  "(x ::= a, s) \<rightarrow> (SKIP, s(x := aval a s))" |

Seq1:    "(SKIP;;c\<^sub>2,s) \<rightarrow> (c\<^sub>2,s)" |
Seq2:    "(c\<^sub>1,s) \<rightarrow> (c\<^sub>1',s') \<Longrightarrow> (c\<^sub>1;;c\<^sub>2,s) \<rightarrow> (c\<^sub>1';;c\<^sub>2,s')" |

IfTrue:  "bval b s \<Longrightarrow> (IF b THEN c\<^sub>1 ELSE c\<^sub>2,s) \<rightarrow> (c\<^sub>1,s)" |
IfFalse: "\<not>bval b s \<Longrightarrow> (IF b THEN c\<^sub>1 ELSE c\<^sub>2,s) \<rightarrow> (c\<^sub>2,s)" |

While:   "(WHILE b DO c,s) \<rightarrow>
            (IF b THEN c;; WHILE b DO c ELSE SKIP,s)" |
ParL: "(c1,s) \<rightarrow> (c1',s') \<Longrightarrow> (c1 \<parallel> c2,s) \<rightarrow> (c1' \<parallel> c2,s')" |
ParLSkip: " (SKIP \<parallel> c,s) \<rightarrow> (c,s)" |
ParR: "(c2,s) \<rightarrow> (c2',s') \<Longrightarrow> (c1 \<parallel> c2,s) \<rightarrow> (c1 \<parallel> c2',s')" |
ParRSkip: "(c \<parallel> SKIP,s) \<rightarrow> (c,s)"

lemmas small_step_induct = small_step.induct[split_format(complete)]

inductive
  nsteps :: "com * state \<Rightarrow> nat \<Rightarrow> com * state \<Rightarrow> bool"
  ("_ \<rightarrow>^_ _" [60,1000,60]999)
where
  zero_steps[simp,intro]: "cs \<rightarrow>^0 cs" |
  one_step[intro]: "cs \<rightarrow> cs' \<Longrightarrow> cs' \<rightarrow>^n cs'' \<Longrightarrow> cs \<rightarrow>^(Suc n) cs''"

lemmas nsteps_induct = nsteps.induct[split_format(complete)]

inductive_cases zero_stepsE[elim!]: "cs \<rightarrow>^0 cs'"
thm zero_stepsE
inductive_cases one_stepE[elim!]: "cs \<rightarrow>^(Suc n) cs''"
thm one_stepE

abbreviation
  small_steps :: "com * state \<Rightarrow> com * state \<Rightarrow> bool" (infix "\<rightarrow>*" 55)
where "x \<rightarrow>* y == star small_step x y"


lemma small_steps_n: "cs \<rightarrow>* cs' \<Longrightarrow> (\<exists>n. cs \<rightarrow>^n cs')"
by(induction rule:star.induct)(auto intro: "nsteps.intros")

lemma n_small_steps: "cs \<rightarrow>^n cs' \<Longrightarrow> cs \<rightarrow>* cs'"
by(induction rule: "nsteps.induct")(auto intro:  Star.star.step)


lemma nsteps_trans: "cs \<rightarrow>^n1 cs' \<Longrightarrow> cs' \<rightarrow>^n2 cs'' \<Longrightarrow> cs \<rightarrow>^(n1+n2) cs''"
by(induction rule: "nsteps.induct")(auto intro: "nsteps.intros")

definition
  small_step_pre :: "com \<Rightarrow> com \<Rightarrow> bool" (infix "\<preceq>"  50) where
  "c \<preceq> c' \<equiv> (\<forall> s t n. (c,s) \<rightarrow>^n (SKIP,t) \<longrightarrow> (\<exists> n' \<ge> n. (c',s) \<rightarrow>^n' (SKIP,t)) )"

abbreviation  equiv_c :: "com \<Rightarrow> com \<Rightarrow> bool" (infix "\<approx>" 50) where
  "c \<approx> c' \<equiv> c \<preceq> c' \<and> c' \<preceq> c"


lemma small_eqv_implies_big_eqv:
  assumes "c \<preceq> c'" "c' \<preceq> c"
  shows "c \<approx> c'"
by (meson assms(1) assms(2) n_small_steps small_step_pre_def small_steps_n)

lemma par_commut': " d \<parallel> c \<preceq> c \<parallel> d"
by sorry

lemma par_commut'': " d \<parallel> c \<preceq> c \<parallel> d"
by sorry

lemma Par_commute: "c \<parallel> d \<approx> d \<parallel> c"
using par_commut' par_commut'' by auto

end