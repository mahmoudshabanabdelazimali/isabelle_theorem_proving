(** Score: 6/6
- 12.2 + 12.3
*)

theory MahmoudShabanAbdelazimAli2
imports Big_StepT
begin

text \<open>Next, we define a Hoare calculus that also accounts for execution times.\<close>

type_synonym assn = "state \<Rightarrow> bool"
type_synonym tbd = "state \<Rightarrow> nat" (* time bound *)

abbreviation state_subst :: "state \<Rightarrow> aexp \<Rightarrow> vname \<Rightarrow> state"
  (*<*)("_[_'/_]" [1000,0,0] 999)(*>*)
where "s[a/x] \<equiv> s(x := aval a s)"

definition hoare_Tvalid :: "assn \<Rightarrow> com \<Rightarrow> tbd \<Rightarrow> assn \<Rightarrow> bool"
  (*<*)("\<Turnstile>\<^sub>T {(1_)}/ (_)/ { _ \<Down>(1_)}" 50) where(*>*)
"\<Turnstile>\<^sub>T {P} c {q \<Down> Q}  \<longleftrightarrow>  (\<forall>s. P s \<longrightarrow> (\<exists>t p. ((c,s) \<Rightarrow> p \<Down> t) \<and> p \<le> (q s) \<and> Q t))"

inductive
  hoareT :: "assn \<Rightarrow> com \<Rightarrow> tbd \<Rightarrow> assn \<Rightarrow> bool" (*<*)("\<turnstile>\<^sub>T ({(1_)}/ (_)/ { _ \<Down> (1_)})" 50)(*>*)
where
Skip:  "\<turnstile>\<^sub>T {P} SKIP {\<lambda>s. Suc 0 \<Down> P}"  |
Assign:  "\<turnstile>\<^sub>T {\<lambda>s. P(s[a/x])} x::=a {\<lambda>s. Suc 0 \<Down> P}"  |
Seq: "\<lbrakk>\<And>u. \<turnstile>\<^sub>T {\<lambda> s. P\<^sub>1 s \<and> e2' s = u} c\<^sub>1 {e1 \<Down> \<lambda> s. P\<^sub>2 s \<and> e2 s \<le> u};
         \<turnstile>\<^sub>T {P\<^sub>2} c\<^sub>2 { e2 \<Down> P\<^sub>3};
         \<And>s. P\<^sub>1 s \<Longrightarrow> e1 s + e2' s \<le> e s\<rbrakk>
          \<Longrightarrow> \<turnstile>\<^sub>T {P\<^sub>1} c\<^sub>1;;c\<^sub>2 { e \<Down> P\<^sub>3}"  |
If: "\<lbrakk> \<turnstile>\<^sub>T {\<lambda>s. P s \<and> bval b s} c\<^sub>1 {e1 \<Down> Q}; \<turnstile>\<^sub>T {\<lambda>s. P s \<and> \<not> bval b s} c\<^sub>2 {e1 \<Down> Q} \<rbrakk>
  \<Longrightarrow> \<turnstile>\<^sub>T {P} IF b THEN c\<^sub>1 ELSE c\<^sub>2 {\<lambda>s. e1 s + Suc 0 \<Down> Q}"  |
While:
  "\<lbrakk>(\<And>u z::nat.
    \<turnstile>\<^sub>T {\<lambda>s. INV (z+1) s \<and> e' s = u} c {e'' \<Down> \<lambda>s. INV z s \<and> e s \<le> u});
       (\<And>s z. INV (z+1) s \<Longrightarrow> bval b s \<and> e s \<ge> 1 + e' s + e'' s);
       (\<And>s. INV 0 s \<Longrightarrow> \<not> bval b s \<and> 1 \<le> e s)
       \<rbrakk>
   \<Longrightarrow> \<turnstile>\<^sub>T {\<lambda>s. (\<exists>z. INV z s)} WHILE b DO c {e \<Down> INV 0}"  |
conseq: "\<lbrakk>\<forall>s. P' s \<longrightarrow> (P s \<and> e' s \<le> e s); \<turnstile>\<^sub>T {P} c {e' \<Down> Q}; \<forall>s. Q s \<longrightarrow> Q' s\<rbrakk> \<Longrightarrow>
           \<turnstile>\<^sub>T {P'} c {e \<Down> Q'}"

text{* Building in the consequence rule: *}

lemma strengthen_pre:
  "\<lbrakk> \<forall>s. P' s \<longrightarrow> P s;  \<turnstile>\<^sub>T {P} c { e \<Down> Q} \<rbrakk> \<Longrightarrow> \<turnstile>\<^sub>T {P'} c { e \<Down> Q}"
  by(rule conseq[where e'=e and Q=Q and P=P]; simp)

lemma weaken_post:
  "\<lbrakk> \<turnstile>\<^sub>T {P} c {e \<Down> Q};  \<forall>s. Q s \<longrightarrow> Q' s \<rbrakk> \<Longrightarrow> \<turnstile>\<^sub>T {P} c {e \<Down> Q'}"
  by(rule conseq[where e'=e and Q=Q and P=P]; simp)

lemma Assign': "\<forall>s. P s \<longrightarrow> Q(s[a/x]) \<Longrightarrow> (\<turnstile>\<^sub>T {P} x ::= a { (\<lambda> s. 1) \<Down> Q})"
  using strengthen_pre[OF _ Assign] by simp


section "soundness"

text \<open>Your task is to prove soundness of the calculus:\<close>

theorem hoareT_sound: "\<turnstile>\<^sub>T {P} c {e \<Down> Q} \<Longrightarrow> \<Turnstile>\<^sub>T {P} c {e \<Down> Q}"
proof(unfold hoare_Tvalid_def, induction rule: hoareT.induct)
  case (Skip P)
  show ?case by fastforce
next
  case (Assign P a x)
  show ?case by fastforce
next
  case (Seq P1 e2' c1 e1 P2 e2 c2 P3 e)
  show ?case
  proof clarify
    fix s
    assume a: "P1 s"
    with Seq(3) have 1: "e1 s + e2' s \<le> e s" by simp
    obtain u where 2: "e2' s = u" by simp
    with a Seq(4) obtain t p where
      c1: "(c1, s) \<Rightarrow> p \<Down> t" and cost1: " p \<le> (e1 s)" and P2: "P2 t" and 3: "e2 t \<le> u"
      by blast
    from Seq(5) P2 obtain t' p' where
      c2: "(c2, t) \<Rightarrow> p' \<Down> t'" and cost2: " p' \<le> (e2 t)" and P3: "P3 t'"
      by blast
    from c1 c2 have seq: "(c1;; c2, s) \<Rightarrow> p + p' \<Down> t'"
      apply (rule Big_StepT.Seq) by simp
    from cost1 cost2 2 3 have "p + p' \<le> e1 s + e2' s" by simp
    also have "\<dots> \<le> e s" using 1 by simp
    finally have cost: "p + p' \<le> e s" .
    from seq cost P3
    show "\<exists>t p. (c1;; c2, s) \<Rightarrow> p \<Down> t \<and>  p \<le> (e s) \<and> P3 t" by metis
  qed
next
  case (If P b c1 e Q c2)
  show ?case
  proof clarify
    fix s
    assume a: "P s"
    show "\<exists>t p. (IF b THEN c1 ELSE c2, s) \<Rightarrow> p \<Down> t \<and> p \<le> e s + Suc 0 \<and> Q t"
      proof(cases "bval b s")
        case b:True
         from b a If(3) obtain t1 p1 where c1:"(c1, s) \<Rightarrow> p1 \<Down> t1" and p1:"p1 \<le> e s " and t1:" Q t1" by blast
         from this IfTrue[OF b c1]  show ?thesis  by auto
        next
        case nb:False
         assume nb:"\<not>bval b s"
         from nb a If(4) obtain t2 p2 where c2:"(c2, s) \<Rightarrow> p2 \<Down> t2" and p2:"p2 \<le> e s " and t2:" Q t2" by blast
         from p2 t2 IfFalse[OF nb c2] show ?thesis by auto
      qed
    qed
next
  case (While INV e' c e'' e b)
  {
   fix s z
   have " INV z s \<Longrightarrow> (\<exists>t p. (WHILE b DO c, s) \<Rightarrow> p \<Down> t \<and> p \<le> e s \<and> INV 0 t)"
    proof(induction "z" arbitrary: s rule: less_induct)
    case (less n)
    then show ?case
    proof(cases "n=0")
      case 0:True
      hence 1:" INV 0 s" using less(2) by simp
      from While(3)[OF 1] have 2:"\<not> bval b s" and 3:"1 \<le> e s" by auto
      from WhileFalse[OF 2] 1 3 show ?thesis by (metis One_nat_def)
    next
      case suc:False
      from suc obtain m where 1:"n= (Suc m)" by (meson lessI less_Suc_eq_0_disj)
      from this have 2:"n=m+1" by simp
      hence 3:"m < n" by simp
      from 2 less have 4:"INV (m+1) s " by simp
      obtain u where 5:"e' s = u" by simp
      from While(4) 4 5 obtain p1 t1 where 6:"(c, s) \<Rightarrow> p1 \<Down> t1" and
                                           7:"p1 \<le> e'' s" and
                                           8:"INV m t1" and
                                           9:"e t1 \<le> u" by blast
       from less(1)[OF 3 8] obtain t2 p2 where 10:"(WHILE b DO c, t1) \<Rightarrow> p2 \<Down> t2 "and
                                               11:" p2 \<le> e t1 "and
                                               12:" INV 0 t2" by blast
       from While(2)[OF 4] have 13:"bval b s" and 14:"1 + e' s + e'' s \<le> e s" by auto
       obtain d where 15:"1 + p1 + p2 = d" by simp
       have  "d \<le> e s" using 11 5 9 14 7 15 by force
       thus ?thesis using WhileTrue[OF 13 6 10 15] using 12 by auto
    qed
   qed
  }
  then show ?case by auto
next
  case (conseq P' P e'  e c Q Q')
  then show ?case by fastforce
qed

end
