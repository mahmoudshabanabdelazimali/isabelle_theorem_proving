(*Homework 8.2 A Type System for Physical Units*)
section "A Typed Language"

theory MahmoudShabanAbdelazimAli08_2 imports "~~/src/HOL/IMP/Star" Complex_Main begin

datatype unit = N | M | Prod unit unit

type_synonym val = "real \<times> unit"
type_synonym vname = string
type_synonym state = "vname \<Rightarrow> val"


datatype aexp = Pc val | V vname | Plus aexp aexp | Mult aexp aexp

inductive unit_eq' :: "unit \<Rightarrow> unit \<Rightarrow> bool" where
u:"unit_eq' u u" |
prod2:"unit_eq' v k \<Longrightarrow> unit_eq' l h \<Longrightarrow> unit_eq' (Prod v l) (Prod k h)" |
prod3:"unit_eq' l k \<Longrightarrow> unit_eq' v h \<Longrightarrow> unit_eq' (Prod v l) (Prod k h)" 


inductive_cases unit_eqE[elim!]: "unit_eq' u v"
inductive_cases unit_eq_ProdE[elim!]:" unit_eq' (Prod v l) (Prod k h)"
thm unit_eq_ProdE
declare unit_eq'.intros [intro!]
 
lemma "unit_eq' (Prod (Prod N M) N) (Prod (Prod M N) N)"
apply (rule prod2)
apply (rule prod3)
apply auto
done
inductive taval :: "aexp \<Rightarrow> state \<Rightarrow> val \<Rightarrow> bool" where
"taval (Pc i) s  i" |
"taval (V x) s (s x)" |
plusa:"taval a1 s (i1,u1) \<Longrightarrow> taval a2 s (i2,u2) \<Longrightarrow> unit_eq' u1 u2
 \<Longrightarrow> taval (Plus a1 a2) s ((i1+i2),u1)" |
Multa:"taval a1 s (i1,u1) \<Longrightarrow> taval a2 s (i2,u2) 
 \<Longrightarrow> taval (Mult a1 a2) s ((i1*i2),Prod u1 u2)"

inductive_cases PlusE[elim!]:"taval (Plus a1 a2) s v"
inductive_cases MultE[elim!]: "taval (Mult a1 a2) s v"

inductive_cases [elim!]:
  "taval (Ic i) s v"  "taval (Rc i) s v"
  "taval (V x) s v"
  

subsection "Boolean Expressions"

datatype bexp = Bc bool | Not bexp | And bexp bexp | Less aexp aexp

inductive tbval :: "bexp \<Rightarrow> state \<Rightarrow> bool \<Rightarrow> bool" where
"tbval (Bc v) s v" |
"tbval b s bv \<Longrightarrow> tbval (Not b) s (\<not> bv)" |
"tbval b1 s bv1 \<Longrightarrow> tbval b2 s bv2 \<Longrightarrow> tbval (And b1 b2) s (bv1 & bv2)" |
lessb:"taval a1 s (i1,u1) \<Longrightarrow> taval a2 s (i2,u2) \<Longrightarrow> unit_eq' u1 u2 \<Longrightarrow> tbval (Less a1 a2) s (i1 < i2)" 

subsection "Syntax of Commands"
(* a copy of Com.thy - keep in sync! *)

datatype
  com = SKIP 
      | Assign vname aexp       ("_ ::= _" [1000, 61] 61)
      | Seq    com  com         ("_;; _"  [60, 61] 60)
      | If     bexp com com     ("IF _ THEN _ ELSE _"  [0, 0, 61] 61)
      | While  bexp com         ("WHILE _ DO _"  [0, 61] 61)


subsection "Small-Step Semantics of Commands"

inductive
  small_step :: "(com \<times> state) \<Rightarrow> (com \<times> state) \<Rightarrow> bool" (infix "\<rightarrow>" 55)
where
Assign:  "taval a s v \<Longrightarrow> (x ::= a, s) \<rightarrow> (SKIP, s(x := v))" |

Seq1:   "(SKIP;;c,s) \<rightarrow> (c,s)" |
Seq2:   "(c1,s) \<rightarrow> (c1',s') \<Longrightarrow> (c1;;c2,s) \<rightarrow> (c1';;c2,s')" |

IfTrue:  "tbval b s True \<Longrightarrow> (IF b THEN c1 ELSE c2,s) \<rightarrow> (c1,s)" |
IfFalse: "tbval b s False \<Longrightarrow> (IF b THEN c1 ELSE c2,s) \<rightarrow> (c2,s)" |

While:   "(WHILE b DO c,s) \<rightarrow> (IF b THEN c;; WHILE b DO c ELSE SKIP,s)"

lemmas small_step_induct = small_step.induct[split_format(complete)]

subsection "The Type System"

type_synonym ty = unit
type_synonym tyenv = "vname \<Rightarrow> ty"

inductive atyping :: "tyenv \<Rightarrow> aexp \<Rightarrow> ty \<Rightarrow> bool"
  ("(1_/ \<turnstile>/ (_ :/ _))" [50,0,50] 50)
where
Pc_ty: "\<Gamma> \<turnstile> Pc (i,u): u" |
V_ty: "\<Gamma> \<turnstile> V x : \<Gamma> x" |
Plus_ty: "\<Gamma> \<turnstile> a1 : \<tau> \<Longrightarrow> \<Gamma> \<turnstile> a2 : \<tau> \<Longrightarrow> \<Gamma> \<turnstile> Plus a1 a2 : \<tau>"|
Mult_ty: "\<Gamma> \<turnstile> a1 : r1 \<Longrightarrow> \<Gamma> \<turnstile> a2 : r2 \<Longrightarrow> \<Gamma> \<turnstile> Mult a1 a2 : Prod r1 r2"

declare atyping.intros [intro!]
inductive_cases [elim!]:
  "\<Gamma> \<turnstile> V x : \<tau>" "\<Gamma> \<turnstile> Pc i : \<tau>" "\<Gamma> \<turnstile> Plus a1 a2 : \<tau>" "\<Gamma> \<turnstile> Mult a1 a2 : \<tau>"

text{* Warning: the ``:'' notation leads to syntactic ambiguities,
i.e. multiple parse trees, because ``:'' also stands for set membership.
In most situations Isabelle's type system will reject all but one parse tree,
but will still inform you of the potential ambiguity. *}

inductive btyping :: "tyenv \<Rightarrow> bexp \<Rightarrow> bool" (infix "\<turnstile>" 50)
where
B_ty: "\<Gamma> \<turnstile> Bc v" |
Not_ty: "\<Gamma> \<turnstile> b \<Longrightarrow> \<Gamma> \<turnstile> Not b" |
And_ty: "\<Gamma> \<turnstile> b1 \<Longrightarrow> \<Gamma> \<turnstile> b2 \<Longrightarrow> \<Gamma> \<turnstile> And b1 b2" |
Less_ty: "\<Gamma> \<turnstile> a1 : \<tau> \<Longrightarrow> \<Gamma> \<turnstile> a2 : \<tau> \<Longrightarrow> \<Gamma> \<turnstile> Less a1 a2"

declare btyping.intros [intro!]
inductive_cases [elim!]: "\<Gamma> \<turnstile> Not b" "\<Gamma> \<turnstile> And b1 b2" "\<Gamma> \<turnstile> Less a1 a2"

inductive ctyping :: "tyenv \<Rightarrow> com \<Rightarrow> bool" (infix "\<turnstile>" 50) where
Skip_ty: "\<Gamma> \<turnstile> SKIP" |
Assign_ty: "\<Gamma> \<turnstile> a : \<Gamma>(x) \<Longrightarrow> \<Gamma> \<turnstile> x ::= a" |
Seq_ty: "\<Gamma> \<turnstile> c1 \<Longrightarrow> \<Gamma> \<turnstile> c2 \<Longrightarrow> \<Gamma> \<turnstile> c1;;c2" |
If_ty: "\<Gamma> \<turnstile> b \<Longrightarrow> \<Gamma> \<turnstile> c1 \<Longrightarrow> \<Gamma> \<turnstile> c2 \<Longrightarrow> \<Gamma> \<turnstile> IF b THEN c1 ELSE c2" |
While_ty: "\<Gamma> \<turnstile> b \<Longrightarrow> \<Gamma> \<turnstile> c \<Longrightarrow> \<Gamma> \<turnstile> WHILE b DO c"

declare ctyping.intros [intro!]
inductive_cases [elim!]:
  "\<Gamma> \<turnstile> x ::= a"  "\<Gamma> \<turnstile> c1;;c2"
  "\<Gamma> \<turnstile> IF b THEN c1 ELSE c2"
  "\<Gamma> \<turnstile> WHILE b DO c"

subsection "Well-typed Programs Do Not Get Stuck"

definition styping :: "tyenv \<Rightarrow> state \<Rightarrow> bool" (infix "\<turnstile>" 50)
where "\<Gamma> \<turnstile> s  \<longleftrightarrow>  (\<forall>x.  unit_eq' (snd(s x)) (\<Gamma> x))"

lemma eq_implies_eq[simp]:"u = v \<Longrightarrow> unit_eq' u v" by auto

lemma a: "unit_eq' u v \<Longrightarrow> unit_eq' v u" 
proof(induction rule: unit_eq'.induct)
  case u thus ?case by auto
next
  case (prod2 v k l h)
  from unit_eq'.prod2[OF prod2(3) prod2(4)] show ?case by auto  
next
  case (prod3 l k v h)
  from unit_eq'.prod3[OF prod3(4) prod3(3)] show ?case by auto
qed

lemma eq_unit_comm[simp]: " unit_eq' u v =  unit_eq' v u" 
proof
 assume "unit_eq' u v"
 from a[OF this] show "unit_eq' v u" by simp
next
assume "unit_eq' v u"
from a[OF this] show "unit_eq' u v" by simp
qed

lemma prod_ab:"unit_eq' (Prod k h) z \<Longrightarrow> (\<exists>a b. z = (Prod a b) )"
proof(cases z)
  assume "z = N" and "unit_eq' (Prod k h) z"
  hence 1: "\<not> unit_eq' (Prod k h) z" by auto
  then show ?thesis using \<open>unit_eq' (Prod k h) z\<close> by blast
next
 assume "z=M" and "unit_eq' (Prod k h) z"
 hence 1: "\<not> unit_eq' (Prod k h) z" by auto
 then show ?thesis using \<open>unit_eq' (Prod k h) z\<close> by blast
next
show "\<And>x y.
       \<lbrakk>unit_eq' (Prod k h) z; z = Prod x y\<rbrakk>
       \<Longrightarrow> \<exists>a b. z = Prod a b"
  proof-
    fix x y
    assume 1:" unit_eq' (Prod k h) z" and 2:"z = Prod x y"
    show "\<exists>a b. z = Prod a b" using 1 2 by auto
  qed
qed 

lemma eq_trans[simp]: "unit_eq' u v \<Longrightarrow> unit_eq' v z \<Longrightarrow> unit_eq' u z" 
proof(induction  arbitrary: z rule:unit_eq'.induct) 
  case u thus ?case by auto
next
  case (prod2 v k l h) 
  from prod_ab[OF prod2(5)] obtain a b where 0:" z = Prod a b" by blast
  from prod2(5) 0 have 1:"unit_eq' (Prod k h) (Prod a b)" by fast
  from 0 1 prod2 show ?case by (metis prod3 unit_eq'.prod2 unit_eq_ProdE)
 next 
  case (prod3  v k l h) 
  from prod_ab[OF prod3(5)] obtain a b where 0:" z = Prod a b" by blast
  from prod3(5) 0 have 1:"unit_eq' (Prod k h) (Prod a b)" by fast
  from 0 1 prod3 show ?case by (metis prod2 unit_eq'.prod3 unit_eq_ProdE)
qed

lemma taval_pair: "taval a1 s v1 \<Longrightarrow> (\<exists>a b. v1 = (a, b))" by auto

lemma apreservation:
  "\<Gamma> \<turnstile> a : \<tau> \<Longrightarrow> taval a s v \<Longrightarrow> \<Gamma> \<turnstile> s \<Longrightarrow> unit_eq' (snd v)  \<tau>"
proof(induction arbitrary: v rule: atyping.induct)
  case (Pc_ty \<Gamma> i u) thus ?case by auto 
next
  case (V_ty \<Gamma> x)
  then have "\<forall>x. unit_eq' (snd (s x)) (\<Gamma> x)" by (auto simp add: styping_def)
  from this have 1:"unit_eq' (snd (s x)) (\<Gamma> x)" by auto
  from V_ty have 2: "(snd (s x)) = snd v" by auto
  from eq_implies_eq[OF 2] have 3:"unit_eq' (snd (s x)) (snd v)" by blast
  from 3  have 4:"unit_eq' (snd v) (snd (s x)) " by auto
  from eq_trans[OF 4 1] show ?case by auto
next
case(Plus_ty \<Gamma> a1 \<tau> a2) 
from  PlusE[OF this(5)] obtain i1 u1 i2 u2 where
    1:"v = (i1 + i2, u1)" and 2:" taval a1 s (i1, u1)" and 3:" taval a2 s (i2, u2)" and 4:" unit_eq' u1 u2"
by metis
from Plus_ty(3)[OF 2 Plus_ty(6)] have 5:" unit_eq' (snd (i1, u1)) \<tau>" by blast
from Plus_ty(4)[OF 3 Plus_ty(6)] have 6:"unit_eq' (snd (i2, u2)) \<tau>" by blast
from 1 have "unit_eq' (snd v) (snd (i1, u1))" by auto
from eq_trans[OF this 5 ] show ?case by simp
next
case (Mult_ty \<Gamma> a1 r1 a2 r2)
from MultE[OF this(5)] obtain i1 u1 i2 u2 where
    1: "v = (i1 * i2, Prod u1 u2)" and 2: "taval a1 s (i1, u1)" and 3:"taval a2 s (i2, u2)"   by metis
    from Mult_ty(3)[OF 2 Mult_ty(6)] have 5:"unit_eq' (snd (i1, u1)) r1" by blast
    from Mult_ty(4)[OF 3 Mult_ty(6)] have 6:"unit_eq' (snd (i2, u2)) r2" by blast
    from 5 have 7:"unit_eq' u1 r1" by simp
    from 6 have 8:"unit_eq' u2 r2" by simp
    from prod2[OF 7 8] have 9:"unit_eq' (Prod u1 u2) (Prod r1 r2)" by auto
    from 1 have 10:"unit_eq' (snd v) (Prod u1 u2)" by auto
    from  eq_trans[OF 10 9] show ?case by auto
qed

lemma aprogress: "\<Gamma> \<turnstile> a : \<tau> \<Longrightarrow> \<Gamma> \<turnstile> s \<Longrightarrow> \<exists>v. taval a s v"
proof(induction rule: atyping.induct)
  case (Plus_ty \<Gamma> a1 t a2)
  then obtain v1 v2 where 1: "taval a1 s v1" and 2:"taval a2 s v2" by blast
  from taval_pair[OF 1] taval_pair[OF 2] obtain a' b' a'' b'' 
        where 3: "v1 = (a', b')" and 4: "v2 = (a'', b'')" by blast
  from apreservation 1 Plus_ty(1,5) have 5:"unit_eq' (snd v1)  t" by blast
  from apreservation 2 Plus_ty(2,5)  have 6:"unit_eq' (snd v2)  t" by blast
  from 3 1 have 7: "taval a1 s (a', b')" by auto
  from 4 2 have 8: "taval a2 s (a'', b'')" by auto
  from 6 have 9:"unit_eq' t (snd v2)" by auto
  from 3 4 eq_trans[OF 5 9] have 10:"unit_eq' b' b''" by auto
  from plusa[OF 7 8 10]  have 11:"taval (Plus a1 a2) s (a' + a'', b')" by blast 
  then show ?case by metis
next
case (Mult_ty \<Gamma> a1 r1 a2  r2 )
  then show ?case using apreservation taval.intros eq_trans taval_pair  by metis
next
  case (Pc_ty \<Gamma>) thus ?case using apreservation "taval.intros" by metis
next
  case (V_ty) thus ?case using apreservation "taval.intros" by metis
qed 

lemma bprogress: "\<Gamma> \<turnstile> b \<Longrightarrow> \<Gamma> \<turnstile> s \<Longrightarrow> \<exists>v. tbval b s v"
proof(induction rule: btyping.induct)
  case (Less_ty \<Gamma> a1 t a2)
  then obtain v1 v2 where 1: "taval a1 s v1" and 2:"taval a2 s v2"
    by (metis aprogress)
    from taval_pair[OF 1] taval_pair[OF 2] obtain a' b' a'' b'' 
        where 3: "v1 = (a', b')" and 4: "v2 = (a'', b'')" by blast
  from apreservation 1 Less_ty(1,3) have 5:"unit_eq' (snd v1)  t" by blast
  from apreservation 2 Less_ty(2,3)  have 6:"unit_eq' (snd v2)  t" by blast
  from 3 1 have 7: "taval a1 s (a', b')" by auto
  from 4 2 have 8: "taval a2 s (a'', b'')" by auto
  from 6 have 9:"unit_eq' t (snd v2)" by auto
  from 3 4 eq_trans[OF 5 9] have 10:"unit_eq' b' b''" by auto
  from lessb[OF 7 8 10] have "  tbval (Less a1 a2) s (a' < a'')" by blast
  then show ?case  by metis
qed (auto intro: tbval.intros)

theorem progress:
  "\<Gamma> \<turnstile> c \<Longrightarrow> \<Gamma> \<turnstile> s \<Longrightarrow> c \<noteq> SKIP \<Longrightarrow> \<exists>cs'. (c,s) \<rightarrow> cs'"
proof(induction rule: ctyping.induct)
  case Skip_ty thus ?case by simp
next
  case Assign_ty 
  thus ?case by (metis Assign aprogress)
next
  case Seq_ty thus ?case by simp (metis Seq1 Seq2)
next
  case (If_ty \<Gamma> b c1 c2)
  then obtain bv where "tbval b s bv" by (metis bprogress)
  show ?case
  proof(cases bv)
    assume "bv"
    with `tbval b s bv` show ?case by simp (metis IfTrue)
  next
    assume "\<not>bv"
    with `tbval b s bv` show ?case by simp (metis IfFalse)
  qed
next
  case While_ty show ?case by (metis While)
qed

theorem styping_preservation:
  "(c,s) \<rightarrow> (c',s') \<Longrightarrow> \<Gamma> \<turnstile> c \<Longrightarrow> \<Gamma> \<turnstile> s \<Longrightarrow> \<Gamma> \<turnstile> s'"
proof(induction rule: small_step_induct)
  case (Assign a s a' b' x)
  from Assign(2) have 1: "\<Gamma> \<turnstile> a : \<Gamma>(x)" by auto
  from apreservation[OF 1 Assign(1) Assign(3)] have 2: "unit_eq' b' (\<Gamma> x)" by simp
  from Assign styping_def show ?case by (metis \<open>unit_eq' (snd (a', b')) (\<Gamma> x)\<close> fun_upd_apply)
qed auto

theorem ctyping_preservation:
  "(c,s) \<rightarrow> (c',s') \<Longrightarrow> \<Gamma> \<turnstile> c \<Longrightarrow> \<Gamma> \<turnstile> c'"
by (induct rule: small_step_induct) (auto simp: ctyping.intros)

abbreviation small_steps :: "com * state \<Rightarrow> com * state \<Rightarrow> bool" (infix "\<rightarrow>*" 55)
where "x \<rightarrow>* y == star small_step x y"

theorem type_sound:
  "(c,s) \<rightarrow>* (c',s') \<Longrightarrow> \<Gamma> \<turnstile> c \<Longrightarrow> \<Gamma> \<turnstile> s \<Longrightarrow> c' \<noteq> SKIP
   \<Longrightarrow> \<exists>cs''. (c',s') \<rightarrow> cs''"
apply(induction rule:star_induct)
apply (metis progress)
by (metis styping_preservation ctyping_preservation)

end
