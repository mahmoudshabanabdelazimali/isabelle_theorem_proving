theory MahmoudShabanAbdelazimAli05
imports "~~/src/HOL/IMP/Big_Step" "~~/src/HOL/IMP/Small_Step"
begin
(*>*)

(*Homework 5.1 Paths in Graphs*)
definition "is_edge a b g \<equiv> (a,b) \<in> g"

inductive is_path :: "('v \<times>'v) set \<Rightarrow> 'v \<Rightarrow> 'v list \<Rightarrow> 'v \<Rightarrow> bool" where
empty_path: "is_path g u [] u" |
some_path: " is_edge (last (y#ys)) v g \<Longrightarrow> is_path g u (butlast (y#ys)) (last (y#ys)) \<Longrightarrow> is_path g u (y#ys) v" 

lemma "is_path {(i,i+1) | i::nat. True} 1 [] 1"
by(rule is_path.empty_path)

lemma "is_path {(i,i+1) | i::nat. True} 3 [3,4,5] 6"
by(auto simp add: is_edge_def is_path.some_path empty_path)
    
lemma[simp]: " is_path g v p w \<Longrightarrow> is_edge u v g \<Longrightarrow> is_path g u (u#p) w"
proof(induction  rule: "is_path.induct")
  case (empty_path ) 
  thus ?case by(auto simp add:is_edge_def is_path.some_path is_path.empty_path)
next
  case (some_path y ys v g s)
  from some_path.IH some_path.prems have "is_path g u (u # butlast (y # ys)) (last (y # ys))"  by (simp)
  from this  some_path.hyps(1) show "?case" by (simp add:  is_path.some_path last_ConsR ) 
qed

lemma path_appendI[simp]:
"\<lbrakk>is_path E u p1 v; is_path E v p2 w\<rbrakk> \<Longrightarrow> is_path E u (p1@p2) w"
 proof(induction arbitrary: p2 w  rule: is_path.induct)
    case empty_path thus ?case by auto
 next
    case (some_path y ys v g u  )
    from some_path.prems some_path.hyps(1) have "is_path g (last (y # ys)) (last (y # ys)#p2) w" by auto
    from this some_path.IH have "is_path g u (butlast (y # ys) @ (last (y # ys)#p2)) w" by blast
    thus ?case by (metis append_Cons append_Nil2  append_eq_append_conv2 append_eq_conv_conj  list.simps(3) snoc_eq_iff_butlast)
qed

lemma path_appendE:
"is_path E u (p1@p2) w \<Longrightarrow> \<exists>v. is_path E u p1 v \<and> is_path E v p2 w"
proof(induction p1 arbitrary: E p2 w u )
    case Nil
    thus ?case using append_Nil empty_path by fastforce
next
    case (Cons y ys)
    from this(2) show ?case 
    proof cases
        case empty_path thus ?thesis by simp
    next
        case (some_path x xs )
        print_cases
        from some_path have 1:"is_path E u (butlast ((y # ys) @ p2)) (last ((y # ys) @ p2))"and
                            2:"is_edge (last ((y # ys) @ p2)) w E" by auto
        from this have 3:"is_path E (last ((y # ys) @ p2)) [last ((y # ys) @ p2)] w" by (simp add: empty_path is_path.some_path last_ConsL) 
        from 1 2 3 show ?thesis using some_path sorry
    qed
qed
(*Homework 5.2 Grammars*)
datatype ab = a | b

inductive S :: "ab list \<Rightarrow> bool" where 
add:"S w \<Longrightarrow> S (a #w @[b])"
| nil: "S []"

lemma S_n:"S (replicate n a @ replicate n b)"
  proof(induction n)
      case 0 thus ?case by (simp add:S.nil)
  next
      case (Suc k) thus ?case by (auto simp add: S.nil) (metis add append_assoc replicate_append_same)
  qed

lemma S_correct:
"S w \<longleftrightarrow> (\<exists> n. w = replicate n a @ replicate n b)" (is "?L \<longleftrightarrow> ?R")
proof
  show "?L \<Longrightarrow> ?R" 
  proof (induction rule: S.induct)
      case nil 
      thus "?case" by auto
  next
      case (add w)
      from add(2) obtain n where 1 :"w = replicate n a @ replicate n b" by blast 
      from  1 have "a # w @ [b] = replicate (Suc n) a @ replicate (Suc n) b" by (simp add:  replicate_append_same)
      thus "?case" by blast
 qed

next
  show  "?R \<Longrightarrow> ?L" 
  
  proof (induction w)
      case Nil thus "?case" by (simp add: S.nil)
  next
      case Cons
      thus  "?case" by (auto simp add:S_n)
  qed 
qed    

end
