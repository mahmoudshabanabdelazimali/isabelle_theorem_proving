theory MahmoudShabanAbdelazimAli07_2
imports "~~/src/HOL/IMP/Big_Step"
begin


(*Homework 7.2*)
fun vars_a :: "aexp \<Rightarrow> vname set" where 
"vars_a (N n)= {}" |
"vars_a (V v) = {v}" |
"vars_a (Plus v va) = (vars_a v) Un (vars_a va) "

fun vars_b :: "bexp \<Rightarrow> vname set" where 
"vars_b (Bc v) = {}" |
"vars_b (bexp.Not v) = vars_b v"|
"vars_b (And v va) = (vars_b v) Un (vars_b va)"|
"vars_b (Less v va) = (vars_a v) Un (vars_a va) " 

fun vars :: "com \<Rightarrow> vname set" where 
"vars SKIP = {}" |
"vars (v ::= va) = {v} Un (vars_a va)"|
"vars (v;; va) = (vars v) Un (vars va)"|
"vars (IF v THEN va ELSE vb) = (vars_b v) Un (vars va) Un (vars vb)"|
"vars (WHILE v DO va) = (vars_b v) Un (vars va) "

lemma aval_equiv[simp]:
"(c, s) \<Rightarrow> t \<Longrightarrow> vars_a a \<inter> vars c = {} \<Longrightarrow> aval a t = aval a s"
proof(induction rule: big_step_induct)
  case (Assign x b s)  thus "aval a (s(x := aval b s)) = aval a s"  by(induction a)(fastforce +)
qed (force ; blast)+


lemma bval_equiv[simp]:
"(c, s) \<Rightarrow> t \<Longrightarrow> vars_b b \<inter> vars c = {} \<Longrightarrow> bval b t = bval b s"
proof(induction rule: big_step_induct)
case (Assign x a s)  thus ?case
  proof (induction b)
    case (Less a1 a2)
    from this have 1:"vars_a a1 \<inter> vars (x ::= a) = {}" by auto
    from Less have 2:"vars_a a2 \<inter> vars (x ::= a) = {}" by auto
    have 3:"(x::= a, s) \<Rightarrow> s(x:=aval a s) " by auto
    from 1 3 have 4:"aval a1 s = aval a1 (s(x := aval a s))" by auto
    from 2 3 have 5:"aval a2 s = aval a2 (s(x := aval a s))" by auto
    thus ?case using 1 2 3 4 5 using bval.simps(4) by presburger 
  qed auto
qed(force ; blast)+

lemma varsa_aval[simp]:" y \<notin> vars_a a \<Longrightarrow> aval a s = aval a (s(y := aval c s))"
by(induction a)(auto)

lemma s_equiv[simp]: "y \<noteq> x \<Longrightarrow> s(x := aval a s, y:= aval c s) = s(y := aval c s, x:= aval a s)"
by(induction a)(auto)

lemma a:" (c, s) \<Rightarrow> s'' \<Longrightarrow> x \<notin> (vars c) \<Longrightarrow> (c, s(x := aval a s)) \<Rightarrow> s''(x:= aval a s)"
sorry
lemma c: " (c, s(x := aval a s)) \<Rightarrow> t \<Longrightarrow>  x \<notin> (vars c) \<Longrightarrow>  (c, s) \<Rightarrow> s'"
sorry

lemma Seq_commute'[simp]:
"(c1, s) \<Rightarrow> s' \<Longrightarrow> (c2, s') \<Rightarrow> t \<Longrightarrow> vars c1 \<inter> vars c2 = {} \<Longrightarrow>(c2;;c1, s) \<Rightarrow> t"
proof(induction rule:big_step_induct)
  case Skip thus ?case by auto
next
  case (Assign x a s )  

  from Assign have 1:"vars_a a \<inter> vars c2 = {}" by auto
  from Assign have "x \<notin> (vars c2)" by auto
  from this c `(c2, s(x := aval a s)) \<Rightarrow> t` obtain s2 where b: "(c2, s) \<Rightarrow> s2" by blast
  from  b a `x \<notin> (vars c2)`have "(c2, s(x := aval a s)) \<Rightarrow> s2(x := aval a s)" by auto
  from big_step_determ `(c2, s(x := aval a s)) \<Rightarrow> t` this have 3:"t=s2(x := aval a s)" by auto
  have 2: "(x::=a,s2) \<Rightarrow> s2(x := aval a s2)" by auto
  from 1 b have "aval a s2 = aval a s" by auto
  from this 2 have "(x::=a,s2) \<Rightarrow> s2(x := aval a s)" by auto
  from this 3 have  "(x::=a,s2) \<Rightarrow> t" by auto 
  from b this show "(c2;;x::=a,s) \<Rightarrow> t" by auto
next
  case (WhileFalse b s c) 
  from WhileFalse have 1:"vars_b b \<inter> vars c2 = {}" by auto
  from 1 WhileFalse(2) have 2:"bval b s = bval b t" by auto
  from 2 WhileFalse(1) have 3:"\<not> bval b t" by auto
  from 3 have 4: "(WHILE b DO c,t) \<Rightarrow> t" by auto
  thus ?case using WhileFalse(2) by auto
next
  case (WhileTrue b s1 c s2 s3)
  thus ?case sorry
next
 case (IfFalse b s c'' s'' c')
 from IfFalse have 1:"(c2;; c'', s) \<Rightarrow> t" by auto
 from this obtain s''' where 2:"(c2,s) \<Rightarrow> s'''" and 3:"(c'',s''') \<Rightarrow> t" by blast
 from IfFalse(5) have 3:"vars_b b \<inter> vars c2 = {}" by auto
 from 2 3 have 4:"bval b s = bval b s'''" by auto
 from 4 IfFalse(1) have 5:"\<not> bval b s'''" by simp
 from 5 3 have 6:"(IF b THEN c' ELSE c'',s''') \<Rightarrow> t" by (metis "2" \<open>\<And>thesis. (\<And>s'''. \<lbrakk>(c2, s) \<Rightarrow> s'''; (c'', s''') \<Rightarrow> t\<rbrakk> \<Longrightarrow> thesis) \<Longrightarrow> thesis\<close> big_step.IfFalse big_step_determ)
 from 6 2 have "(c2;; IF b THEN c' ELSE c'', s) \<Rightarrow> t" by auto
 thus ?case by simp
next
 case (IfTrue b s c' s'' c'')
 from IfTrue  have 1:"(c2;; c', s) \<Rightarrow> t" by auto
 from this obtain s''' where 2:"(c2,s) \<Rightarrow> s'''" and 0:"(c',s''') \<Rightarrow> t" by blast
 from IfTrue(5) have 3:"vars_b b \<inter> vars c2 = {}" by auto
 from 2 3 have 4:"bval b s = bval b s'''" by auto
 from 4 IfTrue(1) have 5:"bval b s'''" by simp
 from 5 0 have 6:"(IF b THEN c' ELSE c'',s''') \<Rightarrow> t" by auto
 from 6 2 have "(c2;; IF b THEN c' ELSE c'', s) \<Rightarrow> t" by auto
 thus ?case by simp
next
case (Seq c' s1 s2 c'' s3 ) 
thus ?case  sorry
qed 

lemma Seq_commute:
"vars c1 \<inter> vars c2 = {} \<Longrightarrow> c1;;c2 \<sim> c2;;c1"
proof (induction c1 arbitrary: c2)
case While thus ?case using Seq_commute' sorry
next
case Assign thus ?case sorry
next
case If thus ?case sorry
next 
case Seq thus ?case sorry
qed auto 

end