theory MahmoudShabanAbdelazimAli02
imports 
  Main
begin
text \<open>\ExerciseSheet{2}{01.~11.~2016}\<close>

text \<open>\NumHomework{Tree traversal}{November 8}\<close>

datatype 'a tree = Tip | Node "'a tree" 'a "'a tree"

fun mirror :: "'a tree \<Rightarrow> 'a tree" where
"mirror Tip = Tip" |
"mirror (Node l x r) = Node (mirror r) x (mirror l)"

(* Example*)
value "mirror(Node (Node Tip a Tip) b t)"

(* Change this *)
fun in_order :: "'a tree \<Rightarrow> 'a list" where
  "in_order Tip = []" |
  "in_order (Node l v r) = (in_order l) @ [v] @ (in_order r)"
(* Proof this *)
theorem
  "rev (in_order t) = in_order (mirror t)"
apply (induction t)
apply auto
done
text \<open>\NumHomework{Tail-Recursive Form of Addition}{November 8}\<close>

(* The example function *)

fun itrev :: "'a list \<Rightarrow> 'a list \<Rightarrow> 'a list" where
  "itrev [] ys = ys" |
  "itrev (x#xs) ys = itrev xs (x#ys)"

(* Change this *)
fun add :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
  "add 0 x = x" |
  "add (Suc n) x = add n (Suc x)"
(* Proof this *)
lemma add_suc[simp]: "add x (Suc y) = Suc (add x y)"
apply (induction x arbitrary: y)
apply auto
done
lemma add2_0 [simp]: "add x 0 = x"
apply (induction x)
by (auto)

theorem "add (add x y) z = add x (add y z)"
apply (induction x)
by auto


(* Proof this *)
theorem "add x y = add y x"
apply (induction x)
by auto

end