(*First name: Mahmoud Shaban Abdelazim 
  Last name: Ali
  Matrikelnummer: 03685261
  Homework 1.1
*)
theory MahmoudShabanAbdelazimAli01
imports Main 
begin

(*A function count which counts the number of occurrences of a particular element in a list*)
fun count :: " 'a list \<Rightarrow> 'a \<Rightarrow> nat " where
"count [] _ = (0::nat)" |
"count (x # xs) e = (if x = e then Suc (count xs e) else count xs e)"    

(*A function snoc that appends an element at the right end of a list*)
fun snoc :: " 'a list \<Rightarrow> 'a \<Rightarrow> 'a list" where 
"snoc [] x = [x]" |
"snoc (x#xs) y = x # snoc xs y "

(*A function reverse that reverses the order of elements in a list*)
fun reverse :: " 'a list \<Rightarrow> 'a list" where 
"reverse [] = [] " | 
"reverse (x#xs) = snoc (reverse xs) x"

(*First requirement of the Homework 1.1:  
  A function spread that spreads an element among a list
*)
fun spread :: " 'a \<Rightarrow> 'a list \<Rightarrow> 'a list" where 
"spread _ [] = []" | 
"spread x (y # ys) = y # x # spread x ys"

(*
Second requirement of Homework 1.1
  Verifying the following evaluates to true, 
  for instance: value “spread 0 [1,2,3] = [1,0,2,0,3,0]”
*)
value "spread 0 [1,2,3] = [1,0,2,0,3,0]"

lemma "spread 0 [1,2,3] = [1,0,2,0,3,0]"
by simp

(*
Third requirement of Homework 1.1
  Prove that spreading an element among a list xs 
  adds exactly length xs copies of the element to the list.
*)
lemma "count (spread a xs) a = count xs a + length xs"
apply (induction xs)
by auto


(*
Final requirement of Homework 1.1
  Prove the following lemma connecting reverse and spread:
  
  snoc (reverse (spread a xs)) a = a # spread a (reverse xs)

  In order to prove that lemma, first i have to prove two auxiliary lemmas about spread and snoc.
  which are the following 
*)

(*First an auxiliary lemma*)
lemma app_snoc_snoc_spread: "snoc (snoc (spread a  xs) aa) a = (spread a xs) @ [aa,a] "
apply (induction xs)
by auto

(*Second  an auxiliary lemma*)
lemma app_spread_snoc_rev: " spread a (snoc xs aa) = (spread a xs) @ [aa,a]"
apply (induction xs)
by auto

(*The main lemma, intuition and description of the proof

 1. I have applied the induction method on xs since it is the only variable available
 2. Then I have applied the auto method which solved the basic case and was not able to solve 
the induction case which simplify to the following subgoal 

      snoc (snoc (spread a (reverse xs)) aa) a = spread a (snoc (reverse xs) aa)

In order to solve that last subgoal, my intuition was to prove the following 

 1.  snoc (snoc (spread a (reverse xs)) aa) a = z

 2.  spread a (snoc (reverse xs) aa) = z

And after i have proved point 1 and 2 by proving the corresponding auxiliary lemmas  
app_snoc_snoc_spread app_spread_snoc_rev, auto + those two auxiliary lemmas have
successfully proved the remaining subgoal of the main lemma 
*)
lemma "snoc (reverse (spread a xs)) a = a # spread a (reverse xs)"
apply (induction xs)
apply (auto simp: app_snoc_snoc_spread app_spread_snoc_rev)
done
 
end