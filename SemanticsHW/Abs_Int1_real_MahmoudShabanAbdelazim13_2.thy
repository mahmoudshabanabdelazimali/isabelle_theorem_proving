(* Author: Tobias Nipkow *)

theory Abs_Int1_real_MahmoudShabanAbdelazim13_2
imports Abs_Int1_MahmoudShabanAbdelazim13_2
begin

subsection "real Analysis"

datatype bound = NegInf (" \<infinity>\<^sup>- ") | PosInf (" \<infinity>\<^sup>+") | NaN | Real

datatype bounds = S "bound set"
text{* Instantiation of class @{class order} with type @{typ bound}: *}

instantiation bounds :: order
begin

text{* First the definition of the interface function @{text"\<le>"}. Note that
the header of the definition must refer to the ascii name @{const less_eq} of the
constants as @{text less_eq_bound} and the definition is named @{text
less_eq_bound_def}.  Inside the definition the symbolic names can be used. *}

definition less_eq_bounds where
"x \<le> y = (case (x,y) of (S x, S y) \<Rightarrow> x \<subseteq> y)"

definition less_bounds where
"x < y = (case (x,y) of (S x, S y) \<Rightarrow> x \<subset> y)"

text{*\noindent(The type annotation is necessary to fix the type of the polymorphic predicates.)

Now the instance proof, i.e.\ the proof that the definition fulfills
the axioms (assumptions) of the class. The initial proof-step generates the
necessary proof obligations. *}
instance
proof
  fix x::bounds show "x \<le> x" by(auto simp:less_eq_bounds_def split:bounds.split )
next
  fix x y z :: bounds assume "x \<le> y" "y \<le> z" thus "x \<le> z"
    by(auto simp: less_eq_bounds_def  split:bounds.splits)
next
  fix x y :: bounds assume "x \<le> y" "y \<le> x" thus "x = y"
    by(auto simp: less_eq_bounds_def  split:bounds.splits)
next
  fix x y :: bounds show "(x < y) = (x \<le> y \<and> \<not> y \<le> x)"
  by(auto simp: less_bounds_def less_eq_bounds_def split:bounds.splits)
qed

end

text{* Instantiation of class @{class semilattice_sup_top} with type @{typ bounds}: *}

instantiation bounds :: semilattice_sup_top
begin

definition sup_bounds where
"x \<squnion> y = (case (x,y) of (S w , S t ) \<Rightarrow> S (w \<union> t))"


lemma s_eq: "S x = S y \<Longrightarrow> x = y"
by simp

definition top_bounds where
"\<top> = S {Real,NaN,\<infinity>\<^sup>+,\<infinity>\<^sup>-}"

text{* Now the instance proof. This time we take a shortcut with the help of
proof method @{text goal_cases}: it creates cases 1 ... n for the subgoals
1 ... n; in case i, i is also the name of the assumptions of subgoal i and
@{text "case?"} refers to the conclusion of subgoal i.
The class axioms are presented in the same order as in the class definition. *}

instance
proof (standard, goal_cases)
  case 1 (*sup1*) show ?case by(auto simp:sup_bounds_def less_eq_bounds_def split:bounds.split)
next
  case 2 (*sup2*) show ?case by(auto simp: sup_bounds_def less_eq_bounds_def  split: bounds.split)
next
  case (3 x y z) (*sup least*)
  thus ?case  by(auto simp: sup_bounds_def less_eq_bounds_def  split: bounds.split)

next
  case 4 (*top*) show ?case using less_eq_bounds_def top_bounds_def by(auto  split: bounds.split  bounds.splits ) (metis (full_types) bound.exhaust insertI1 insertI2 s_eq)
qed

end


text{* Now we define the functions used for instantiating the abstract
interpretation locales. Note that the Isabelle terminology is
\emph{interpretation}, not \emph{instantiation} of locales, but we use
instantiation to avoid confusion with abstract interpretation.  *}

fun \<gamma>_bound ::"bound \<Rightarrow> val set" where
"\<gamma>_bound Real = { y . EX x::real. y = Some (ereal x) }"|
"\<gamma>_bound NaN = {None}"|
"\<gamma>_bound  \<infinity>\<^sup>- = {Some MInfty}"|
"\<gamma>_bound  \<infinity>\<^sup>+ = {Some PInfty}"

fun \<gamma>_bounds :: "bounds \<Rightarrow> val set" where
"\<gamma>_bounds (S x)  = (if x = {Real,NaN,\<infinity>\<^sup>+,\<infinity>\<^sup>-} then UNIV else \<Union> (\<gamma>_bound ` x))" 

lemma bounds_bound:"i \<in> \<gamma>_bounds(S x) \<Longrightarrow> x\<noteq> {Real,NaN,\<infinity>\<^sup>+,\<infinity>\<^sup>-} \<Longrightarrow>  (EX a. a\<in>x \<and> (i \<in> \<gamma>_bound a )) "
using bound.exhaust by auto

lemma er_r[simp]: "Some (ereal i) \<in> \<gamma>_bound Real"
by auto

lemma N_Na[simp]: "None \<in> \<gamma>_bound NaN"
by auto

lemma P_Pav[simp]: "Some PInfty \<in> \<gamma>_bound \<infinity>\<^sup>+"
by auto

lemma M_Mav[simp]: "Some MInfty  \<in> \<gamma>_bound \<infinity>\<^sup>-"
by auto

lemma ereal_some:
fixes i:: "ereal option"
assumes 1: "i = Some e" and 2:"\<not> (e = MInfty)" and 3:"\<not> (e = PInfty)" 
shows " EX r . e =ereal r"
using uminus_ereal.cases[where x=e] assms by auto

  
lemma bounds_UNIV:"\<And>i. i \<in> UNIV \<Longrightarrow>  (EX a::bound.  (i \<in> \<gamma>_bound a )) "
proof-
fix i::val
assume "i \<in> UNIV"
show "(EX a::bound.  (i \<in> \<gamma>_bound a ))" 
  proof(cases "i=Some  MInfty \<or> i=Some  PInfty \<or> i=None") 
    case t:True
    thus ?thesis using `i \<in> UNIV` using M_Mav P_Pav N_Na by blast
  next 
    case f:False
    from this have "EX e. i = Some e " using   `i \<in> UNIV` by simp
    then obtain e where 1:"i = Some e " by blast 
     obtain r where 2:" e =ereal r" using ereal_some f 1 by blast 
     from 1 2 have 3:"i = Some (ereal r) " by simp
     then have 3:"i\<in> \<gamma>_bound Real " by simp 
     thus ?thesis  by blast
  qed
qed

lemma nan_none:"NaN \<in> x \<Longrightarrow>  None \<in> \<gamma>_bounds (S x)"
by fastforce


lemma none_nan: "None \<in> \<gamma>_bound x \<Longrightarrow> x=NaN"
 by(induction x)  auto 

lemma[simp]: "x={Real,NaN,\<infinity>\<^sup>+,\<infinity>\<^sup>-} \<Longrightarrow> \<gamma>_bounds (S x) = UNIV "
by auto

lemma[simp]:"\<gamma>_bounds \<top>  = UNIV "
by(auto simp:top_bounds_def)

lemma[simp]: "x \<noteq> {Real,NaN,\<infinity>\<^sup>+,\<infinity>\<^sup>-} \<Longrightarrow> \<gamma>_bounds (S x) = \<Union> (\<gamma>_bound ` x)"
by auto
 
fun num_bounds :: "val \<Rightarrow> bounds" where
"num_bounds i = (case i of Some x \<Rightarrow> ( 
                                  case x of ereal r \<Rightarrow> S {Real} |
                                              PInfty \<Rightarrow> S {\<infinity>\<^sup>+} | 
                                              MInfty \<Rightarrow> S {\<infinity>\<^sup>-}
                                   )|
                            None \<Rightarrow> S {NaN})"

fun plus_bound :: "bound \<Rightarrow> bound \<Rightarrow> bound" where
"plus_bound Real Real = Real" |
"plus_bound  \<infinity>\<^sup>-  \<infinity>\<^sup>- = \<infinity>\<^sup>-  " |
"plus_bound  \<infinity>\<^sup>+  \<infinity>\<^sup>+ = \<infinity>\<^sup>+ " |
"plus_bound  \<infinity>\<^sup>-  \<infinity>\<^sup>+ = NaN  " |
"plus_bound  \<infinity>\<^sup>+  \<infinity>\<^sup>- = NaN  " |
"plus_bound  _  NaN = NaN" |
"plus_bound  NaN _ = NaN" |
"plus_bound \<infinity>\<^sup>+ Real = \<infinity>\<^sup>+"|
"plus_bound \<infinity>\<^sup>- Real = \<infinity>\<^sup>-"|
"plus_bound Real  \<infinity>\<^sup>-  = \<infinity>\<^sup>-"|
"plus_bound Real  \<infinity>\<^sup>+  = \<infinity>\<^sup>+"

fun plus_bounds :: "bounds \<Rightarrow> bounds \<Rightarrow> bounds" where
"plus_bounds (S x) (S y) =  (if x = {} then S y  
                            else (if y = {} then S x 
                            else S { s. EX t w. t \<in> x \<and> w \<in> y \<and> s= plus_bound t w}))"


lemma plus_tops:"plus_bounds \<top> \<top> = \<top>"
proof-
obtain x where 1:"x= {Real, NaN,  \<infinity>\<^sup>+,  \<infinity>\<^sup>- }" by auto
obtain y where 2:"y= {Real, NaN,  \<infinity>\<^sup>+,  \<infinity>\<^sup>- }" by auto
obtain a where 3:"a \<in> x"  using 1 by auto
obtain b where 4:"b \<in> y" using 2 by auto
 have 5:"EX z . plus_bound a b = z" by auto
from 1 2 obtain t where 6:"plus_bounds (S x) (S y) = S t"  using plus_bounds.elims by blast
have 7:"\<forall> a b.  a \<in> x & b \<in> y & (EX z. plus_bound a b = z)" using "1" "2" bound.exhaust by auto
have 8:"\<forall> r::bound . r \<in> { s. EX t w. t \<in> x \<and> w \<in> y \<and> s= plus_bound t w}" using 1 2 bound.exhaust by (smt "7" CollectI plus_bound.elims plus_bound.simps(2))
from this have 9:"{ s. EX t w. t \<in> x \<and> w \<in> y \<and> s= plus_bound t w} = {Real, NaN,  \<infinity>\<^sup>+,  \<infinity>\<^sup>- }" using  bound.exhaust by auto
from this 6 1 2 have "t={Real, NaN,  \<infinity>\<^sup>+,  \<infinity>\<^sup>- }"  by simp
hence 10:"S t = \<top>" using top_bounds_def by simp
show "plus_bounds \<top> \<top> = \<top>"  using 1 2 6 10 top_bounds_def by simp
qed

text{* First we instantiate the abstract value interface and prove that the
functions on type @{typ bound} have all the necessary properties: *}

lemma i_real:"\<And>i::ereal option . i \<noteq>Some PInfty & i\<noteq> Some MInfty  & i\<noteq>None \<Longrightarrow> EX x .  i = Some (ereal x)"
by (metis option.collapse uminus_ereal.elims) 

global_interpretation Val_semilattice
where \<gamma> = \<gamma>_bounds and num' = num_bounds and plus' = plus_bounds
proof (standard, goal_cases) txt{* subgoals are the locale axioms *}
  case (1 a b) thus ?case
                    apply(cases "a=S {Real,NaN,\<infinity>\<^sup>+,\<infinity>\<^sup>-}" ;cases "b=S {Real,NaN,\<infinity>\<^sup>+,\<infinity>\<^sup>-}" ;simp_all)
                     using antisym top_bounds_def apply fastforce
                     using less_eq_bounds_def by(auto split:"bounds.splits")
next
  case 2 show ?case by(auto simp: top_bounds_def split: bounds.splits)
next
  case (3 i ) show ?case  
  proof(cases "i=Some PInfty \<or> i= Some MInfty  \<or> i=None ")
    case t:True
    thus ?thesis by auto
    next
    case f:False
    hence "EX x . i = Some (ereal x)" using i_real by simp
    then obtain r where "i=Some (ereal r)" by blast
    thus ?thesis by auto
  qed
next
  case (4 a1 a2)
  then obtain x  where 1:"a1 = S x" using bounds.exhaust by auto
  then obtain y where 2:"a2 = S y" using bounds.exhaust by auto
  from 1 4 have 3:"x\<noteq>{}" using bounds.exhaust by force
  thus ?case 
  proof(cases "x={Real,NaN,\<infinity>\<^sup>+,\<infinity>\<^sup>-}")
    case t:True 
    thus ?thesis
    proof(cases "y={}")
        case True 
        hence "plus_bounds a1 a2= a1" using 1 2 by auto
        thus ?thesis using 4 by auto
    next
        case f:False
        then obtain z where 5:"z \<in> y" by auto
        have 6:"plus_bound  NaN z = NaN" using plus_bound.elims by blast
        have 7:"NaN \<in> x" using t by auto
        from f t 1 2 obtain t where 8:"plus_bounds a1 a2 = S t"  using plus_bounds.elims by blast
        from 6 7 5 have 9:"EX w s . w \<in> x \<and> s \<in> y \<and>  plus_bound w s=NaN"  by auto
        from 1 2 t f 8 have "t\<noteq>{}" by force
        from 9 have "NaN \<in> { s. EX t w. t \<in> x \<and> w \<in> y \<and> s= plus_bound t w}" using CollectI by fastforce
        from 1 2 t f 8 9 this have "NaN \<in> t" by simp
        thus ?thesis using 8 nan_none by simp
    qed
    next
    case f1:False
    thus ?thesis
        proof(cases "y={}")
        case True 
        hence "plus_bounds a1 a2= a1" using 1 2 by auto
        thus ?thesis using 4 by auto
    next
        case f2:False
        then obtain z where 5:"z \<in> y" by auto
        have 6:"plus_bound  NaN z = NaN" using plus_bound.elims by blast
        have bound_none:"\<gamma>_bound NaN = {None}" by auto
        then have ex_none:"\<exists>x\<in>x. None \<in> \<gamma>_bound x" using f1 4 1 by auto
        from this obtain v where v_NaN:" None \<in> \<gamma>_bound v" and "v\<in>x" by blast
        from this ex_none bound_none none_nan have 7:"NaN \<in> x "  by auto
        from f2 f1 1 2 obtain t where 8:"plus_bounds a1 a2 = S t"  using plus_bounds.elims by blast
        from 6 7 5 have 9:"EX w s . w \<in> x \<and> s \<in> y \<and>  plus_bound w s=NaN"  by auto
        from 1 2 f1 f2 8 3  have "t\<noteq>{}" by force
        from 9 have "NaN \<in> { s. EX t w. t \<in> x \<and> w \<in> y \<and> s= plus_bound t w}" using CollectI by fastforce
        from 1 2 f1 f2 3 8 9 this have "NaN \<in> t" by simp
        thus ?thesis using 8 nan_none by simp
    qed
  qed
  next
  case (5 a2 a1)  
  then obtain x  where 1:"a1 = S x" using bounds.exhaust by auto
  then obtain y where 2:"a2 = S y" using bounds.exhaust by auto
  from 2 5 have 3:"y\<noteq>{}" using bounds.exhaust by force
  thus ?case 
  proof(cases "y={Real,NaN,\<infinity>\<^sup>+,\<infinity>\<^sup>-}")
    case t:True 
    thus ?thesis
    proof(cases "x={}")
        case True 
        hence "plus_bounds a1 a2= a2" using 1 2 by auto
        thus ?thesis using 5 by auto
    next
        case f:False
        then obtain z where 4:"z \<in> x" by auto
        have 6:"plus_bound  z NaN= NaN" using plus_bound.elims by blast
        have 7:"NaN \<in> y" using t by auto
        from f t 1 2 obtain t where 8:"plus_bounds a1 a2 = S t"  using plus_bounds.elims by blast
        from 6 7 4 have 9:"EX w s . w \<in> x \<and> s \<in> y \<and>  plus_bound w s=NaN"  by force
        from 1 2 t f 8 have "t\<noteq>{}" by force
        from 9 have "NaN \<in> { s. EX t w. t \<in> x \<and> w \<in> y \<and> s= plus_bound t w}" using CollectI by fastforce
        from 1 2 t f 8 9 this have "NaN \<in> t" by simp
        thus ?thesis using 8 nan_none by simp
    qed
    next
    case f1:False
    thus ?thesis
        proof(cases "x={}")
        case True 
        hence "plus_bounds a1 a2= a2" using 1 2 by auto
        thus ?thesis using 5 by auto
    next
        case f2:False
        then obtain z where 4:"z \<in> x" by auto
        have 6:"plus_bound z NaN= NaN" using plus_bound.elims by blast
        have bound_none:"\<gamma>_bound NaN = {None}" by auto
        then have ex_none:"\<exists>x\<in>y. None \<in> \<gamma>_bound x" using f1 5 2 by force
        from this obtain v where v_NaN:" None \<in> \<gamma>_bound v" and "v\<in>y" by blast
        from this ex_none bound_none none_nan have 7:"NaN \<in> y "  by auto
        from f2 f1 1 2 obtain t where 8:"plus_bounds a1 a2 = S t"  using plus_bounds.elims by blast
        from 6 7 4 have 9:"EX w s . w \<in> x \<and> s \<in> y \<and>  plus_bound w s=NaN"  by force
        from 1 2 f1 f2 8 3  have "t\<noteq>{}" by force
        from 9 have "NaN \<in> { s. EX t w. t \<in> x \<and> w \<in> y \<and> s= plus_bound t w}" using CollectI by fastforce
        from 1 2 f1 f2 3 8 9 this have "NaN \<in> t" by simp
        thus ?thesis using 8 nan_none by simp
    qed
  qed
next
case (6 a1 a2) 
 then obtain x  where 1:"a1 = S x" using bounds.exhaust by auto
 then obtain y where 2:"a2 = S y" using bounds.exhaust by auto
 from 1 6 have 3:"x\<noteq>{}" using bounds.exhaust by force
 from 2 6 have 4:"y\<noteq>{}" using bounds.exhaust by force
 thus ?case 
 proof(cases "y={Real,NaN,\<infinity>\<^sup>+,\<infinity>\<^sup>-}")
  case t:True
  show ?thesis
  proof(cases "x={Real,NaN,\<infinity>\<^sup>+,\<infinity>\<^sup>-}")
    case xt:True
        hence 5:"\<infinity>\<^sup>+ \<in> x" by auto
        from t have 7:"\<infinity>\<^sup>- \<in> y" by auto
        have 8:"plus_bound \<infinity>\<^sup>+ \<infinity>\<^sup>-= NaN" using plus_bound.elims by blast
        from 1 2 obtain t where 9:"plus_bounds a1 a2 = S t"  using plus_bounds.elims by blast
        from 5 7 8 have 10:"EX w s . w \<in> x \<and> s \<in> y \<and>  plus_bound w s=NaN"  by force
        from 10 have "NaN \<in> { s. EX t w. t \<in> x \<and> w \<in> y \<and> s= plus_bound t w}" using CollectI by fastforce
        from 1 2 xt t  9 this have "NaN \<in> t" by simp
        thus ?thesis using 9 nan_none by simp
  next
    case xf:False
        have 5:"plus_bound \<infinity>\<^sup>+ \<infinity>\<^sup>-= NaN" using plus_bound.elims by blast
        have bound_pi:"\<gamma>_bound \<infinity>\<^sup>+ = {Some PInfty}" by auto
        have bound_mi:"\<gamma>_bound \<infinity>\<^sup>- = {Some MInfty}" by auto
        have ex_pi:"\<exists>x\<in>x. Some PInfty \<in> \<gamma>_bound x" using bound_pi xf 3 6(1) 1 by force 
        from ex_pi have 7:"\<infinity>\<^sup>+ \<in> x" using bound.exhaust by (smt \<gamma>_bound.simps(1) \<gamma>_bound.simps(2) bound_mi ereal.distinct(1) ereal.distinct(5) mem_Collect_eq option.distinct(1) option.inject singleton_iff)
        have 8:"\<infinity>\<^sup>- \<in> y" using t by auto
        from xf t 1 2 obtain t where 9:"plus_bounds a1 a2 = S t"  using plus_bounds.elims by blast
        from 7 8 5 have 10:"EX w s . w \<in> x \<and> s \<in> y \<and>  plus_bound w s=NaN"  by force
        from 1 2 t xf 9 3  have "t\<noteq>{}" by force
        from 10 have "NaN \<in> { s. EX t w. t \<in> x \<and> w \<in> y \<and> s= plus_bound t w}" using CollectI by fastforce
        from 1 2 t 3 9 this have "NaN \<in> t" by simp
        thus ?thesis using 9 nan_none  by simp
  qed
 next
  case f:False
  show ?thesis 
  proof(cases "x={Real,NaN,\<infinity>\<^sup>+,\<infinity>\<^sup>-}")
    case xt:True
     have 5:"plus_bound \<infinity>\<^sup>+ \<infinity>\<^sup>-= NaN" using plus_bound.elims by blast
        have bound_pi:"\<gamma>_bound \<infinity>\<^sup>+ = {Some PInfty}" by auto
        have bound_mi:"\<gamma>_bound \<infinity>\<^sup>- = {Some MInfty}" by auto
        have ex_Mi:"\<exists>x\<in>y. Some MInfty \<in> \<gamma>_bound x" using bound_mi f 3 6(2) 2 by force 
        from xt have 7:"\<infinity>\<^sup>+ \<in> x" by auto
        have 8:"\<infinity>\<^sup>- \<in> y" using ex_Mi using bound.exhaust by (smt \<gamma>_bound.simps(1) \<gamma>_bound.simps(2) bound_pi ereal.distinct(3) ereal.distinct(6) mem_Collect_eq option.distinct(1) option.inject singleton_iff)
        from f xt 1 2 obtain t where 9:"plus_bounds a1 a2 = S t"  using plus_bounds.elims by blast
        from 7 8 5 have 10:"EX w s . w \<in> x \<and> s \<in> y \<and>  plus_bound w s=NaN"  by force
        from 10 have "NaN \<in> { s. EX t w. t \<in> x \<and> w \<in> y \<and> s= plus_bound t w}" using CollectI by fastforce
        from 1 2 3 4 9 this have "NaN \<in> t" by simp
        thus ?thesis using 9 nan_none  by simp
  next
    case xf:False
     have 5:"plus_bound \<infinity>\<^sup>+ \<infinity>\<^sup>-= NaN" using plus_bound.elims by blast
        have bound_pi:"\<gamma>_bound \<infinity>\<^sup>+ = {Some PInfty}" by auto
        have bound_mi:"\<gamma>_bound \<infinity>\<^sup>- = {Some MInfty}" by auto
        have ex_pi:"\<exists>x\<in>x. Some PInfty \<in> \<gamma>_bound x" using  xf 3 6(1) 1 by force 
        have ex_mi:"\<exists>x\<in>y. Some MInfty \<in> \<gamma>_bound x" using  f 3 6(2) 2 by force 
        thm ereal.distinct
        from ex_pi have 7:"\<infinity>\<^sup>+ \<in> x" using bound.exhaust by (smt \<gamma>_bound.simps(1) \<gamma>_bound.simps(2) bound_mi ereal.distinct(1) ereal.distinct(5) mem_Collect_eq option.distinct(1) option.inject singleton_iff)
        have 8:"\<infinity>\<^sup>- \<in> y" using ex_mi using bound.exhaust by (smt \<gamma>_bound.simps(1) \<gamma>_bound.simps(2) bound_pi ereal.distinct(3) ereal.distinct(6) mem_Collect_eq option.distinct(1) option.inject singleton_iff)
        from xf f 1 2 3 4 obtain t where 9:"plus_bounds a1 a2 = S t"  using plus_bounds.elims by blast
        from 7 8 5 have 10:"EX w s . w \<in> x \<and> s \<in> y \<and>  plus_bound w s=NaN"  by force
        from 10 have "NaN \<in> { s. EX t w. t \<in> x \<and> w \<in> y \<and> s= plus_bound t w}" using CollectI by fastforce
        from 1 2 3 4 9 this have "NaN \<in> t" by simp
        thus ?thesis using 9 nan_none  by simp
  qed
 qed
next 
case (7 a1 a2) 
 then obtain x  where 1:"a1 = S x" using bounds.exhaust by auto
 then obtain y where 2:"a2 = S y" using bounds.exhaust by auto
 from 1 7 have 3:"x\<noteq>{}" using bounds.exhaust by force
 from 2 7 have 4:"y\<noteq>{}" using bounds.exhaust by force
 thus ?case 
 proof(cases "y={Real,NaN,\<infinity>\<^sup>+,\<infinity>\<^sup>-}")
  case t:True
  show ?thesis
  proof(cases "x={Real,NaN,\<infinity>\<^sup>+,\<infinity>\<^sup>-}")
    case xt:True
        hence 5:"\<infinity>\<^sup>- \<in> x" by auto
        from t have 7:"\<infinity>\<^sup>+ \<in> y" by auto
        have 8:"plus_bound \<infinity>\<^sup>- \<infinity>\<^sup>+= NaN" using plus_bound.elims by blast
        from 1 2 obtain t where 9:"plus_bounds a1 a2 = S t"  using plus_bounds.elims by blast
        from 5 7 8 have 10:"EX w s . w \<in> x \<and> s \<in> y \<and>  plus_bound w s=NaN"  by force
        from 10 have "NaN \<in> { s. EX t w. t \<in> x \<and> w \<in> y \<and> s= plus_bound t w}" using CollectI by fastforce
        from 1 2 xt t  9 this have "NaN \<in> t" by simp
        thus ?thesis using 9 nan_none by simp
  next
    case xf:False
        have 5:"plus_bound \<infinity>\<^sup>- \<infinity>\<^sup>+= NaN" using plus_bound.elims by blast
        have bound_pi:"\<gamma>_bound \<infinity>\<^sup>+ = {Some PInfty}" by auto
        have bound_mi:"\<gamma>_bound \<infinity>\<^sup>- = {Some MInfty}" by auto
        have ex_pi:"\<exists>x\<in>x. Some MInfty \<in> \<gamma>_bound x" using bound_mi xf 3 7(1) 1 by force 
        from ex_pi have 6:"\<infinity>\<^sup>- \<in> x" using bound.exhaust by (smt \<gamma>_bound.simps(1) \<gamma>_bound.simps(2) bound_pi ereal.distinct(3) ereal.distinct(5) mem_Collect_eq option.distinct(1) option.inject singleton_iff)
        have 8:"\<infinity>\<^sup>+ \<in> y" using t by auto
        from xf t 1 2 obtain t where 9:"plus_bounds a1 a2 = S t"  using plus_bounds.elims by blast
        from 6 8 5 have 10:"EX w s . w \<in> x \<and> s \<in> y \<and>  plus_bound w s=NaN"  by force
        from 10 have "NaN \<in> { s. EX t w. t \<in> x \<and> w \<in> y \<and> s= plus_bound t w}" using CollectI by fastforce
        from 1 2 t 3 9 this have "NaN \<in> t" by simp
        thus ?thesis using 9 nan_none  by simp
  qed
 next
  case f:False
  show ?thesis 
  proof(cases "x={Real,NaN,\<infinity>\<^sup>+,\<infinity>\<^sup>-}")
    case xt:True
     have 5:"plus_bound \<infinity>\<^sup>- \<infinity>\<^sup>+= NaN" using plus_bound.elims by blast
        have bound_pi:"\<gamma>_bound \<infinity>\<^sup>+ = {Some PInfty}" by auto
        have bound_mi:"\<gamma>_bound \<infinity>\<^sup>- = {Some MInfty}" by auto
        have ex_Mi:"\<exists>x\<in>y. Some PInfty \<in> \<gamma>_bound x" using bound_pi f 3 7(2) 2 by force 
        from xt have 6:"\<infinity>\<^sup>- \<in> x" by auto
        have 8:"\<infinity>\<^sup>+ \<in> y" using ex_Mi using bound.exhaust by (smt \<gamma>_bound.simps(1) \<gamma>_bound.simps(2) bound_mi ereal.distinct(1) ereal.distinct(6) mem_Collect_eq option.distinct(1) option.inject singleton_iff)
        from f xt 1 2 obtain t where 9:"plus_bounds a1 a2 = S t"  using plus_bounds.elims by blast
        from 7 8 6 have 10:"EX w s . w \<in> x \<and> s \<in> y \<and>  plus_bound w s=NaN"  by force
        from 10 have "NaN \<in> { s. EX t w. t \<in> x \<and> w \<in> y \<and> s= plus_bound t w}" using CollectI by fastforce
        from 1 2 3 4 9 this have "NaN \<in> t" by simp
        thus ?thesis using 9 nan_none  by simp
  next
    case xf:False
     have 5:"plus_bound \<infinity>\<^sup>- \<infinity>\<^sup>+= NaN" using plus_bound.elims by blast
        have bound_pi:"\<gamma>_bound \<infinity>\<^sup>+ = {Some PInfty}" by auto
        have bound_mi:"\<gamma>_bound \<infinity>\<^sup>- = {Some MInfty}" by auto
        have ex_pi:"\<exists>x\<in>x. Some MInfty \<in> \<gamma>_bound x" using  xf 3 7(1) 1 by force 
        have ex_mi:"\<exists>x\<in>y. Some PInfty \<in> \<gamma>_bound x" using  f 3 7(2) 2 by force 
        from ex_pi have 6:"\<infinity>\<^sup>- \<in> x" using bound.exhaust by (smt \<gamma>_bound.simps(1) \<gamma>_bound.simps(2) bound_pi ereal.distinct(3) ereal.distinct(5) mem_Collect_eq option.distinct(1) option.inject singleton_iff)
        from ex_mi have 8:"\<infinity>\<^sup>+ \<in> y" using  bound.exhaust by (smt \<gamma>_bound.simps(1) \<gamma>_bound.simps(2) bound_mi ereal.distinct(1) ereal.distinct(6) mem_Collect_eq option.distinct(1) option.inject singleton_iff)
        from xf f 1 2 3 4 obtain t where 9:"plus_bounds a1 a2 = S t"  using plus_bounds.elims by blast
        from 7 8 6 have 10:"EX w s . w \<in> x \<and> s \<in> y \<and>  plus_bound w s=NaN"  by force
        from 10 have "NaN \<in> { s. EX t w. t \<in> x \<and> w \<in> y \<and> s= plus_bound t w}" using CollectI by fastforce
        from 1 2 3 4 9 this have "NaN \<in> t" by simp
        thus ?thesis using 9 nan_none  by simp
  qed
 qed
next
case (8 i1 i2 a1 a2 )
 then obtain x  where 1:"a1 = S x" using bounds.exhaust by auto
 then obtain y where 2:"a2 = S y" using bounds.exhaust by auto
 from 1 8 have 3:"x\<noteq>{}" using bounds.exhaust by force
 from 2 8 have 4:"y\<noteq>{}" using bounds.exhaust by force
 obtain a where 5:"a \<in> x"  using 3 by auto
 obtain b where 6:"b \<in> y" using 4 by auto
 obtain z where 7:"plus_bound a b = z" by auto
  from 1 2 obtain t where 9:"plus_bounds a1 a2 = S t"  using plus_bounds.elims by blast
 from `a \<in> x` `b \<in> y` 7 have 10:"EX w s . w \<in> x \<and> s \<in> y \<and>  plus_bound w s=z"  by force
 hence 11:"z \<in> { s. EX t w. t \<in> x \<and> w \<in> y \<and> s= plus_bound t w}" using CollectI by fastforce
 from 9 10 1 2  3 4 5 6  have 12:"z \<in> t"  by force
 thus ?case sorry
qed

text{* In case 4 we needed to refer to particular variables.
Writing (i x y z) fixes the names of the variables in case i to be x, y and z
in the left-to-right order in which the variables occur in the subgoal.
Underscores are anonymous placeholders for variable names we don't care to fix. *}

text{* Instantiating the abstract interpretation locale requires no more
proofs (they happened in the instatiation above) but delivers the
instantiated abstract interpreter which we call @{text AI_bound}: *}

global_interpretation Abs_Int
where \<gamma> = \<gamma>_bounds and num' = num_bounds and plus' = plus_bounds
defines aval_bound = aval' and step_bound = step' and AI_bound = AI
..


subsubsection "Tests"

definition "test1_bounds =
  ''x'' ::= N 1;;
  WHILE Less (V ''x'') (N 100) DO ''x'' ::= Plus (V ''x'') (N 2)"


definition "test2_bounds =
  ''x'' ::= N 1;;
  WHILE Less (V ''x'') (N 100) DO ''x'' ::= Plus (V ''x'') (N 3)"

definition "steps c i = ((step_bound \<top>) ^^ i) (bot c)"


subsubsection "Termination"

global_interpretation Abs_Int_mono
where \<gamma> = \<gamma>_bounds and num' = num_bounds and plus' = plus_bounds
proof (standard, goal_cases)
  case (1 _ a1 _ a2) thus ?case
    apply(induction a1 a2 rule: plus_bounds.induct)
    apply(auto simp add:less_eq_bounds_def split: bounds.splits)
     sorry
qed

definition m_bounds :: "bounds \<Rightarrow> nat" where
"m_bounds x = (if x = S {Real,NaN,\<infinity>\<^sup>+,\<infinity>\<^sup>-} then 0 else 1)"

global_interpretation Abs_Int_measure
where \<gamma> = \<gamma>_bounds and num' = num_bounds and plus' = plus_bounds
and m = m_bounds and h = "1"
proof (standard, goal_cases)
  case 1 thus ?case by (simp add: m_bounds_def)
next
  case 2 thus ?case sorry
qed

thm AI_Some_measure

end
