theory MahmoudShabanAbdelazimAli06
imports "~~/src/HOL/IMP/BExp"
begin
(*Homework 6.1 Continue*)
  text \<open>
    Your task is to add a continue command to the IMP language.
    The continue command should skip all remaining commands in
    the innermost while loop.

    The new command datatype is:
    \<close>
  datatype
  com = SKIP 
      | Assign vname aexp       ("_ ::= _" [1000, 61] 61)
      | Seq    com  com         ("_;;/ _"  [60, 61] 60)
      | If     bexp com com     ("(IF _/ THEN _/ ELSE _)"  [0, 0, 61] 61)
      | While  bexp com         ("(WHILE _/ DO _)"  [0, 61] 61)
      | CONTINUE

  text \<open>
    The idea of the big-step semantics is to return not only a state, 
    but also a continue flag, which indicates that a continue has been triggered. 
    Modify/augment the big-step rules accordingly:
    \<close>
  inductive
    big_step :: "com \<times> state \<Rightarrow> bool \<times> state \<Rightarrow> bool" (infix "\<Rightarrow>" 55)
  where
   (* Modify and complete *)
  Skip: "(SKIP,s) \<Rightarrow> (False,s)" |
  Assign: "(x ::= a,s) \<Rightarrow> (False, s(x := aval a s))" |
  Seq1: "(c\<^sub>1,s\<^sub>1) \<Rightarrow> (True,s\<^sub>2) \<Longrightarrow> (c\<^sub>1;;c\<^sub>2, s\<^sub>1) \<Rightarrow>(True,s\<^sub>2)" |
  Seq2: "\<lbrakk> (c\<^sub>1,s\<^sub>1) \<Rightarrow> (False,s\<^sub>2);  (c\<^sub>2,s\<^sub>2) \<Rightarrow> (b',s\<^sub>3)  \<rbrakk> \<Longrightarrow> (c\<^sub>1;;c\<^sub>2, s\<^sub>1) \<Rightarrow> (b',s\<^sub>3)" |
  IfTrue: "\<lbrakk> bval b s;  (c\<^sub>1,s) \<Rightarrow> t \<rbrakk> \<Longrightarrow> (IF b THEN c\<^sub>1 ELSE c\<^sub>2, s) \<Rightarrow> t" |
  IfFalse:"\<lbrakk> \<not> bval b s;  (c\<^sub>2,s) \<Rightarrow> t \<rbrakk> \<Longrightarrow> (IF b THEN c\<^sub>1 ELSE c\<^sub>2, s) \<Rightarrow> t" |
  WhileFalse: "\<not>bval b s \<Longrightarrow> (WHILE b DO c,s) \<Rightarrow> (False,s)" |
  WhileTrue:  
  "\<lbrakk> bval b s\<^sub>1;  (c,s\<^sub>1) \<Rightarrow> (b', s\<^sub>2);  (WHILE b DO c, s\<^sub>2) \<Rightarrow>t \<rbrakk> 
  \<Longrightarrow> (WHILE b DO c, s\<^sub>1) \<Rightarrow> t" |
  Continue:"(CONTINUE,s) \<Rightarrow>(True,s)" 


text\<open>Proof automation:\<close>

text \<open>The introduction rules are good for automatically
constructing small program executions. The recursive cases
may require backtracking, so we declare the set as unsafe
intro rules.\<close>
declare big_step.intros [intro]

text\<open>The standard induction rule 
@{thm [display] big_step.induct [no_vars]}\<close>

thm big_step.induct

text\<open>
This induction schema is almost perfect for our purposes, but
our trick for reusing the tuple syntax means that the induction
schema has two parameters instead of the \<open>c\<close>, \<open>s\<close>,
and \<open>s'\<close> that we are likely to encounter. Splitting
the tuple parameter fixes this:
\<close>
lemmas big_step_induct = big_step.induct[split_format(complete)]
thm big_step_induct
text \<open>
@{thm [display] big_step_induct [no_vars]}
\<close>


subsection "Rule inversion"

text\<open>What can we deduce from @{prop "(SKIP,s) \<Rightarrow> (b, t)"} ?
That @{prop "s = t"}. This is how we can automatically prove it:\<close>

inductive_cases SkipE[elim!]: "(SKIP,s) \<Rightarrow> t"
thm SkipE

inductive_cases ContinueE[elim!]: "(CONTINUE,s) \<Rightarrow> t"
thm ContinueE
text\<open>This is an \emph{elimination rule}. The [elim] attribute tells auto,
blast and friends (but not simp!) to use it automatically; [elim!] means that
it is applied eagerly.

Similarly for the other commands:\<close>

inductive_cases AssignE[elim!]: "(x ::= a,s) \<Rightarrow> t"
thm AssignE
inductive_cases SeqE[elim!]: "(c1;;c2,s1) \<Rightarrow> s3"
thm SeqE
inductive_cases IfE[elim!]: "(IF b THEN c1 ELSE c2,s) \<Rightarrow> t"
thm IfE

inductive_cases WhileE[elim]: "(WHILE b DO c,s) \<Rightarrow> t"
thm WhileE
text\<open>Only [elim]: [elim!] would not terminate.\<close>

declare big_step.intros[simp]

(*>*)

text \<open>Now, write a function that checks that continues only occur in while-loops
\<close>
  fun continue_ok :: "com \<Rightarrow> bool" 
  where
    (* Complete this *)
    "continue_ok SKIP \<longleftrightarrow> True"|
    "continue_ok (v ::= va) \<longleftrightarrow> True"|
    "continue_ok (v;; va) \<longleftrightarrow> (continue_ok v)  & (continue_ok va)"|
    "continue_ok (IF v THEN va ELSE vb) \<longleftrightarrow>  (continue_ok va) & (continue_ok vb)"|
    "continue_ok (WHILE v DO va) \<longleftrightarrow> True"|
    "continue_ok CONTINUE \<longleftrightarrow> False"


text \<open>Show that the continue triggered-flag is not set after executing a well-formed command\<close>
  lemma
    "\<lbrakk>(c,s) \<Rightarrow> (continue,t); continue_ok c\<rbrakk> \<Longrightarrow> \<not>continue"
    by(induction rule: big_step_induct) (auto)
 
text \<open>
  In the presence of \<open>CONTINUE\<close>, some additional sources of dead code arise.
  We want to eliminate those which can be identified syntactically
  (that is we do not want to analyze boolean expressions).
  For instance, the following holds:
\<close>
abbreviation
  equiv_c :: "com \<Rightarrow> com \<Rightarrow> bool" (infix "\<sim>" 50) where
  "c \<sim> c' \<equiv> (\<forall>s t. (c,s) \<Rightarrow> t  =  (c',s) \<Rightarrow> t)"


lemma if_cont_cont: assumes "(IF b THEN CONTINUE ELSE CONTINUE, s) \<Rightarrow> t"
shows "t = (True,s)"
proof-
  from assms show ?thesis
  proof cases  --"inverting assms"
    case IfTrue thm IfTrue
    thus ?thesis by blast
  next
    case IfFalse thus ?thesis by blast
  qed
qed

lemma if_cont_cont_eq_cont: "(IF b THEN CONTINUE ELSE CONTINUE,s) \<Rightarrow> t =  (CONTINUE,s) \<Rightarrow> t"
by auto

lemma " (CONTINUE;;c,s) \<Rightarrow> t = (CONTINUE,s) \<Rightarrow> t"
by auto
 
lemma
  "(IF b THEN CONTINUE ELSE CONTINUE;; c) \<sim> (CONTINUE)" (is "?L \<sim> ?R ")
  proof-
   {fix s t
     {
       assume "(?R,s) \<Rightarrow> t"
       hence " (CONTINUE;;c,s) \<Rightarrow> t" by auto
       hence " (IF b THEN CONTINUE ELSE CONTINUE;;c,s) \<Rightarrow> t" by blast
       hence " (?L,s) \<Rightarrow> t" by auto
     }
     moreover
     {
        assume "(?L,s) \<Rightarrow> t" 
        hence " (CONTINUE;;c,s) \<Rightarrow> t" by auto
       hence " (?R,s) \<Rightarrow> t" by auto
     }
     ultimately 
      show "?thesis" by blast
   }
qed

 
text \<open>
  Write a function @{term[show_types] elim} that eliminates dead code caused by use of \<open>CONTINUE\<close>.
  You only need to contract commands because of \<open>CONTINUE\<close>, you do not need to eliminate \<open>SKIPs\<close>.
\<close>

(* Complete *)
fun elim where
  "elim SKIP = SKIP" |
  "elim (v ::= va) = (v ::= va)" |
  "elim (c1;; c2) =
    (if (elim c1) = CONTINUE then 
           CONTINUE
     else 
          (elim c1);; (elim c2)
         
     )"|
   "elim (IF v THEN c1 ELSE c2) = 
    (if ( (elim c1) = CONTINUE) \<and>  ((elim c2) = CONTINUE) then
            CONTINUE
      else
          (IF v THEN (elim c1) ELSE (elim c2) )
      )" |
    "elim (WHILE v DO va) = (WHILE v DO (elim va) )" |
    "elim CONTINUE = CONTINUE "

text \<open>
  The following should hold for \<open>elim\<close>:
\<close>
value "elim (IF b THEN CONTINUE ELSE CONTINUE;; c) = CONTINUE" 
lemma
  "elim c \<sim> c"
(*<*)
oops
(*>*)

text \<open>Prove this direction:\<close>

lemma assign_elim[simp]:" (x ::= a, s) \<Rightarrow> (False, s(x:=aval a s)) \<Longrightarrow> (elim (x ::= a), s) \<Rightarrow> (False, s(x:=aval a s))"
by auto
lemma elim_assign[simp]:"(elim (x ::= a), s) \<Rightarrow> (False, s(x:=aval a s))  \<Longrightarrow>  (x ::= a, s) \<Rightarrow> (False, s(x:=aval a s))"
by auto



(* Prove this *)
lemma elim_complete:
  "(c, s) \<Rightarrow> (b, s') \<Longrightarrow> (elim c, s) \<Rightarrow> (b, s')"
proof(induction rule: big_step_induct)
  case Skip thus ?case by auto
next
  case (Assign x a s) thus ?case using big_step.Assign elim.simps(2) by presburger
next
  case Seq1 thus ?case by auto
next
  case Seq2 thus ?case by auto
next 
  case IfFalse  thus ?case by auto
next 
  case IfTrue  thus ?case by auto
next  
  case WhileTrue  thus ?case by auto
next
  case WhileFalse thus ?case by auto
next
  case Continue  thus ?case by auto
qed

text \<open>\<open>BONUS\<close>: Also prove the converse direction:\<close>

lemma "(c, s) \<Rightarrow> (False, s) \<Longrightarrow> (SKIP, s) \<Rightarrow> (False, s)"
by auto
lemma "(elim SKIP, s) \<Rightarrow> (b, s) \<Longrightarrow> (SKIP, s) \<Rightarrow> (b, s)"
by auto

(* Prove this *)
lemma elim_sound:
  "(elim c, s) \<Rightarrow> (b, s') \<Longrightarrow> (c, s) \<Rightarrow> (b, s')"
proof(induction c arbitrary: s b s' rule: elim.induct[case_names SKIP ASS SEMI IF WHILE CONTINUE])
case SKIP thus ?case by simp
next
case (ASS x a) thus ?case by simp
next
case(IF bc c1 c2 )
thus ?case
proof (cases "(elim c1 = CONTINUE \<and> elim c2 = CONTINUE)")
  assume elim_c1c2:"(elim c1 = CONTINUE \<and> elim c2 = CONTINUE)"
  from this IF have c1:"(c1, s) \<Rightarrow> (True, s)" by simp
  from IF  elim_c1c2 have c2:"(c2, s) \<Rightarrow> (True, s)" by simp
  from c1 c2 have g:"(IF bc THEN c1 ELSE c1,s) \<Rightarrow> (True,s)" by auto
  from IF elim_c1c2 have elim_s:"(elim (IF bc THEN c1 ELSE c1),s) \<Rightarrow> (True,s)" by simp
  from IF elim_c1c2 this have b_s'_s:"(b,s') = (True,s)" by auto
  from this g have "(IF bc THEN c1 ELSE c1, s) \<Rightarrow> (b, s')" by auto
  from this IF elim_c1c2 show ?thesis by auto
next
  assume ass1: "\<not>(elim c1 = CONTINUE \<and> elim c2 = CONTINUE)"
  from this have "elim c1 \<noteq> CONTINUE \<or> elim c2 \<noteq> CONTINUE " by blast
  then show ?thesis 
  proof
    assume elim_c1:"elim c1 \<noteq> CONTINUE "
    show ?thesis 
    proof (cases "elim c2 = CONTINUE")
      assume elim_c2:"elim c2 = CONTINUE"
      show ?thesis 
      proof (cases "bval bc s = True")
        assume bc:"bval bc s=True"
        from IF(5) bc elim_c1  have "(elim(c1),s) \<Rightarrow>(b, s')" by auto
        from IF(1) this have "(c1,s) \<Rightarrow>(b, s')" by simp
        from this bc show ?thesis by auto
      next
        assume nbc: "\<not> (bval bc s = True)"
        from elim_c2 IF(2) have 0:"(c2, s) \<Rightarrow> (True, s)" by auto
        from  nbc this have g:"(IF bc THEN c1 ELSE c2 , s) \<Rightarrow> (True,s) " by auto
        from nbc elim_c2 have 1:"(elim c2 ,s ) \<Rightarrow> (True,s)" by simp
        from IF nbc elim_c2 have "(elim (IF bc THEN c1 ELSE c2), s) \<Rightarrow>(True,s)" by auto
        from IF elim_c1 elim_c2  1 nbc this have b_s'_s:"(b,s') = (True,s)" by auto
        from this nbc g have "(IF bc THEN c1 ELSE c2, s) \<Rightarrow> (b, s')" by auto
        then show ?thesis by auto
      qed
    next
      assume elim_c2:"elim c2 \<noteq> CONTINUE"
      show ?thesis
      proof (cases "bval bc s = True")
        assume bc:"bval bc s=True"
        from IF(5) bc elim_c1  have "(elim(c1),s) \<Rightarrow>(b, s')" by auto
        from IF(1) this have "(c1,s) \<Rightarrow>(b, s')" by simp
        from this bc show ?thesis by auto
      next
        assume nbc:"bval bc s \<noteq> True"
        from IF(5) nbc elim_c2  have "(elim(c2),s) \<Rightarrow>(b, s')" by auto
        from IF(2) this have "(c2,s) \<Rightarrow>(b, s')" by simp
        from this nbc show ?thesis by auto
        qed
    qed
  next
    assume elim_c2:"elim c2 \<noteq> CONTINUE "
    show ?thesis 
      proof (cases "elim c1 = CONTINUE")
      assume elim_c1:"elim c1 = CONTINUE"
      show ?thesis 
      proof (cases "bval bc s = True")
        assume nbc:"bval bc s = True"
        from elim_c1 IF(1) have 0:"(c1, s) \<Rightarrow> (True, s)" by auto
        from  nbc this have g:"(IF bc THEN c1 ELSE c2 , s) \<Rightarrow> (True,s) " by auto
        from nbc elim_c1 have 1:"(elim c1 ,s ) \<Rightarrow> (True,s)" by simp
        from IF nbc elim_c1 have "(elim (IF bc THEN c1 ELSE c2), s) \<Rightarrow>(True,s)" by auto
        from IF elim_c1 elim_c2  1 nbc this have b_s'_s:"(b,s') = (True,s)" by auto
        from this nbc g have "(IF bc THEN c1 ELSE c2, s) \<Rightarrow> (b, s')" by auto
        then show ?thesis by auto
      next 
        assume nbc:"bval bc s \<noteq> True"
        from IF(5) nbc elim_c2  have "(elim(c2),s) \<Rightarrow>(b, s')" by auto
        from IF(2) this have "(c2,s) \<Rightarrow>(b, s')" by simp
        from this nbc show ?thesis by auto 
      qed
    next
      assume elim_c1:"elim c1 \<noteq> CONTINUE"
      show ?thesis 
      proof (cases "bval bc s = True")
        assume bc:"bval bc s = True"
        from IF(5) bc elim_c1  have "(elim(c1),s) \<Rightarrow>(b, s')" by auto
        from IF(1) this have "(c1,s) \<Rightarrow>(b, s')" by simp
        from this bc show ?thesis by auto 
      next
         assume nbc:"bval bc s \<noteq> True"
        from IF(5) nbc elim_c2  have "(elim(c2),s) \<Rightarrow>(b, s')" by auto
        from IF(2) this have "(c2,s) \<Rightarrow>(b, s')" by simp
        from this nbc show ?thesis by auto 
    qed
  qed
qed
qed
next
case (CONTINUE)
then show ?case by simp
next
case (WHILE bc c)
show ?case
proof (cases "bval bc s = True")
  assume bc:"bval bc s = True"
 from bc WHILE obtain s2 b' where elim_c:"(elim c, s) \<Rightarrow> (b', s2)" 
  and elim_w:"(elim (WHILE bc DO c), s2) \<Rightarrow> (b, s')" by auto
  have 0:"\<forall> si sj bi. (elim c, si) \<Rightarrow> (bi, sj ) \<longrightarrow> (c, si) \<Rightarrow> (bi, sj)" using WHILE by blast
 
  from WHILE elim_c have 1:"(c, s) \<Rightarrow> (b', s2)" by auto
  from WHILE elim_w have 2: "( (WHILE bc DO elim c), s2) \<Rightarrow> (b, s')" by auto 
  show ?thesis sorry

next
  assume nbc:"bval bc s \<noteq> True"
  from this have w:"(WHILE bc DO c ,s) \<Rightarrow> (False,s)" by auto
  from WHILE nbc have "b=False" and "s=s'" by auto
  from w this show ?thesis by auto
qed
next
case (SEMI c1 c2)
show ?case
  proof (cases "elim c1 = CONTINUE")
    assume a:"elim c1 = CONTINUE"
    from this SEMI a show ?thesis by auto
  next
    assume a:"elim c1 \<noteq> CONTINUE"
    from this SEMI have  "((elim c1) ;; (elim c2),s) \<Rightarrow> (b,s')" by auto
    from this obtain s2  where "((elim c1,s) \<Rightarrow> (False, s2) \<and> (elim c2,s2) \<Rightarrow> (b, s')) \<or> ((elim c1,s) \<Rightarrow> (b, s') \<and> b=True )" by auto           
    then show ?thesis 
    proof 
      assume a2:"(elim c1, s) \<Rightarrow> (False, s2) \<and> (elim c2, s2) \<Rightarrow> (b, s')"
      from SEMI a2 have 1:"(c1, s) \<Rightarrow> (False, s2)" by auto
      from SEMI a2 a have 2:"(c2, s2) \<Rightarrow> (b, s')" by auto
     from 1 2  show ?thesis by auto
    next
      assume a2:"(elim c1, s) \<Rightarrow> (b, s') \<and> b=True"
      from  SEMI a2 have "(c1,s) \<Rightarrow> (True,s')" by auto
      from a2 this  show ?thesis by auto
    qed
  qed
qed



lemma
  "elim c \<sim> c"
using elim_sound elim_complete by fast

(*Homework 6.2 Fuel your executions*)

text \<open>
  \NumHomework{Fuel your executions}{December 6}
  Note: We provide a template for this homework on the lecture's homepage.

  If you try to define a function to execute a program,
  you will run into trouble with the termination proof (The program might 
  not terminate).

  In this exercise, you will define an execution function that tries to execute
  the program for a bounded number of loop iterations. It gets an additional 
  @{typ nat} argument, called fuel, which decreases for every loop iteration. If 
  the execution runs out of fuel, it stops returning @{term None}.
  We will work on the variant of IMP from the first exercise.
\<close>

fun exec :: "com \<Rightarrow> state \<Rightarrow> nat \<Rightarrow> (bool \<times> state) option" where
  "exec _ s 0 = None" 
| "exec SKIP s f = Some (False, s)"
| "exec (x::=v) s f = Some (False, s(x:=aval v s))"
| "exec (c1;;c2) s f = (
    case exec c1 s f of
      None \<Rightarrow> None
    | Some (True, s') \<Rightarrow> Some (True, s')
    | Some (False, s') \<Rightarrow> exec c2 s' f)"
| "exec (IF b THEN c1 ELSE c2) s f =
    (if bval b s then exec c1 s f else exec c2 s f)" 
| "exec (WHILE b DO c) s (Suc f) = (
    if bval b s then 
      (case (exec c s f) of
        None \<Rightarrow> None |
        Some (cont, s') \<Rightarrow> exec (WHILE b DO c) s' f) 
    else Some (False, s))"
| "exec CONTINUE s f = Some (True, s)"


text \<open>
  Prove that the execution function is correct wrt.\ the big-step semantics:
\<close>

theorem exec_equiv_bigstep: "(\<exists>i. exec c s f = Some s') \<longleftrightarrow> (c,s) \<Rightarrow> s'"

  (*<*)oops(*>*)

text \<open>In the following, we give you some guidance for this proof.
  The two directions are proved separately. The proof of the first 
  direction should be rather straightforward, and is left to you.
  Recall that is usually best to prove a statement for a (complex) recursive function
  using its specific induction rule (c.f. sect. 2.3.4 in the book),
  and that auto can automatically split ``case''-expressions
  using the \<open>split\<close> attribute (c.f. sect. 2.5.6).
\<close>


lemma exec_imp_bigstep: "exec c s f = Some s' \<Longrightarrow> (c,s) \<Rightarrow> s'"
proof (induction c s f arbitrary: s'
    rule: exec.induct[case_names None SKIP ASS SEMI IF WHILE CONTINUE])
case None
thus ?case by auto
next
case SKIP
thus ?case by auto
next
case ASS
thus ?case by auto
next
case (WHILE b c s i) 
thus ?case
proof(cases "bval b s = True")
  assume b:"bval b s = True"
   from this WHILE(3) obtain cont s'' where  c:"exec c s i = Some(cont,s'')" and w:"exec (WHILE b DO c) s'' i = Some s'" by  (auto split: option.split option.split_asm prod.split_asm bool.split)
   from b c WHILE(1) have 1: "(c, s) \<Rightarrow> (cont,s'') " by blast
   from  b c w WHILE(2) have 2: "(WHILE b DO c, s'') \<Rightarrow> s'" by blast
   from 1 2 b show ?thesis by simp  
next
  assume b:"bval b s \<noteq> True"
  from b have 1: "(WHILE b DO c, s) \<Rightarrow> (False,s)" by simp
  from b WHILE(3) have "s' = (False,s)" by simp
  thus ?thesis using 1 by auto
qed 
next
case (CONTINUE s i)
from CONTINUE have "s' = (True,s)" by simp
then show ?case by simp
next
case (IF b c1 c2 s i) 
show ?case
  proof(cases "bval b s = True")
    assume b:"bval b s = True"
    from b IF(3) have c1: "exec c1 s (Suc i) = Some s'" by simp
    from b IF(1) this show ?thesis by simp
  next
    assume nb:"bval b s \<noteq> True"
    from nb IF(3) have c2: "exec c2 s (Suc i) = Some s'" by simp
     from nb IF(2) this show ?thesis by simp
  qed
next
case (SEMI c1 c2 s i)
then show ?case by (auto split: option.split option.split_asm prod.split_asm bool.split  bool.splits)(smt Seq2 case_prodI2 case_prod_curry)
qed 

text \<open>
  For the other direction, prove a monotonicity lemma first:
  If the execution terminates with fuel \<open>f\<close>, it terminates
  with the same result using a larger amount of fuel \<open>f' \<ge> f\<close>.
  For this, first prove the following lemma:
\<close>

lemma exec_add: "exec c s f = Some s' \<Longrightarrow> exec c s (f + k) = Some s'"
proof (induction c s f arbitrary: s'
    rule: exec.induct[case_names None SKIP ASS SEMI IF WHILE])
  -- \<open>Note: The \<open>case_names\<close> attribute assigns (new) names to the cases
      generated by the induction rule, that can then be used with the 
      \<open>case\<close> - command, as done below. \<close>
     txt \<open>Hint: case distinction on the value of the condition \<open>b\<close>.\<close>
    (* Prove this *)
  case (WHILE b c s i s') thus ?case
  txt \<open>Hint: case distinction on the value of the condition \<open>b\<close>.\<close>
  (* Prove this *)
  proof(cases "bval b s = True")
    assume b:"bval b s = True"
      from this WHILE(3) obtain cont s'' where  c:"exec c s i = Some(cont,s'')" and w:"exec (WHILE b DO c) s'' i = Some s'" by  (auto split: option.split option.split_asm prod.split_asm bool.split)
      from b c WHILE(1) have c_ik: "exec c s (i+k) = Some(cont,s'') " by auto
      from b c w WHILE(2) have w_ik:"exec (WHILE b DO c) s'' (i + k) = Some s'" by auto
      from b c_ik w_ik have "exec (WHILE b DO c) s (Suc (i + k)) = Some s'" by auto 
    then show ?thesis by simp
  next
    assume nb: "bval b s \<noteq> True"
      from this WHILE(3) have w_ik:"exec (WHILE b DO c) s (Suc i+k) = Some (False, s)" by auto
      from nb this WHILE(3) have s':"s' = (False,s)" by auto
      from s' w_ik have "exec (WHILE b DO c) s (Suc i+k) = Some s'" by auto
    then show ?thesis by auto
  qed
   
qed (auto split: option.split option.split_asm prod.split_asm bool.split)


text \<open>Now the monotonicity lemma that we want follows easily:\<close>
lemma exec_mono: "exec c s f = Some (cont, s') \<Longrightarrow> f' \<ge> f \<Longrightarrow> exec c s f' = Some (cont, s')"
by (auto simp: exec_add dest: le_Suc_ex)

text \<open>
  The main lemma is proved by induction over the big-step semantics. Recall
  the adapted induction rule \<open>big_step_induct\<close> that nicely handles the
  pattern \<open>big_step (c,s) (cont, s')\<close>.
  You can find the skip, while-true and if-true cases in the template. The other
  cases are left to you.
\<close>
lemma bigstep_imp_si:
  "(c,s) \<Rightarrow> (cont, s') \<Longrightarrow> \<exists>k. exec c s k = Some (cont, s')"
(*<*)
proof (induct rule: big_step_induct)
  txt \<open>We demonstrate the skip, while-true and if-true case 
    here. The other cases are left to you!\<close>
  case (Skip s) have "exec SKIP s 1 = Some (False, s)" by auto
  thus ?case by blast
next
  case (WhileTrue b s1 c cont2 s2 cont3 s3)
  then obtain f1 f2 where "exec c s1 f1 = Some (cont2, s2)"
    and "exec (WHILE b DO c) s2 f2 = Some (cont3, s3)" by auto
  with exec_mono[of c s1 f1 cont2 s2 "max f1 f2"]
    exec_mono[of "WHILE b DO c" s2 f2 cont3 s3 "max f1 f2"] have 
    "exec c s1 (max f1 f2) = Some (cont2, s2)" 
    and "exec (WHILE b DO c) s2 (max f1 f2) = Some (cont3, s3)"
    by auto
  hence "exec (WHILE b DO c) s1 (Suc (max f1 f2)) = Some (cont3, s3)" 
    using \<open>bval b s1\<close> by (auto simp add: add_ac)
  thus ?case by blast
next
  case (WhileFalse b s c)
  hence "exec (WHILE b DO c) s 1 = Some (False,s)" by simp
  thus ?case by blast
next
  case (IfTrue b s c1 cont' t c2)
  then obtain k where "exec c1 s k = Some (cont', t)" by blast
  hence "exec (IF b THEN c1 ELSE c2) s k = Some (cont', t)"
  using \<open>bval b s\<close> by (cases k) auto 
  thus ?case by blast
next
  case (IfFalse b s c2 cont' t c1) 
  then obtain k where "exec c2 s k = Some (cont', t)" by blast
  hence "exec (IF b THEN c1 ELSE c2) s k = Some (cont', t)"
  using \<open>\<not> bval b s\<close> by (cases k) auto 
  thus ?case  by blast
next
  case (Continue s)
  hence "exec CONTINUE s 1 = Some (True, s)" by simp
  thus ?case  by blast
next
  case (Assign x a s )
  hence "exec (x ::= a) s 1 = Some (False, s(x := aval a s))" by simp
  thus ?case by blast
next
  case (Seq1 c1 s1 s2 c2)
  then obtain k where exec_c1:"exec c1 s1 k = Some (True, s2)" by blast
  from this have "exec (c1;; c2) s1 k = Some (True, s2)" by (cases k) auto 
  thus ?case by blast
next
  case (Seq2 c1 s1 s2 c2 b' s3)
    then obtain f1 f2 where exec_c1:"exec c1 s1 f1 = Some (False, s2)"
    and  exec_c2:"exec c2 s2 f2 = Some (b', s3)" by auto
  with exec_mono[of c1 s1 f1 False s2 "max f1 f2"] 
       exec_mono[of c2 s2 f2 b' s3 "max f1 f2"]
       have exec_c1_max: "exec c1 s1 (max f1 f2) = Some (False, s2)" and
           exec_c2_max: "exec c2 s2 (max f1 f2) = Some (b', s3)" by auto

  from this have exec_seq:"exec (c1;;c2) s1 (max f1 f2) = exec c2 s2 (max f1 f2)" 
  proof (cases "f1 = 0")
      assume "f1 = 0"
      then show ?thesis 
      proof (cases "f2 = 0")
          assume "f2=0"
          from this `f1=0` have "max f1 f2 = 0" by simp
          from this have 1:"exec (c1;; c2) s1 (max f1 f2) = exec (c1;; c2) s1 0" 
          and 2:"exec c2 s2 (max f1 f2) = exec c2 s2 0" by auto
          from this have "exec (c1;; c2) s1 0 = exec c2 s2 0" by auto
          from this 1 2 show ?thesis by auto
      next
          assume "f2\<noteq>0"
          from this obtain k where "f2 = Suc k" using gr0_implies_Suc neq0_conv by blast
          from this `f1=0` have max_suc:"max f1 f2 = Suc k" by simp
          from this have 3:"exec (c1;; c2) s1 (max f1 f2) = exec (c1;; c2) s1 (Suc k)" 
          and 4:"exec c2 s2 (max f1 f2) = exec c2 s2 (Suc k)" by auto 
          from exec_c1_max max_suc have 5: "exec c1 s1 (Suc k) =  Some(False,s2)" by auto
          from exec_c2_max max_suc have 6: "exec c2 s2 (Suc k) = Some (b', s3)" by auto
          from  5 6 have "exec (c1;;c2) s1 (Suc k) = exec c2 s2 (Suc k)" by simp
         from this max_suc show ?thesis by auto
      qed
  next
        assume f1:"f1 \<noteq> 0"
      then show ?thesis 
      proof (cases "f2=0")
        assume f2:"f2=0"
         from  f1 obtain k where "f1 = Suc k" using gr0_implies_Suc neq0_conv by blast
         from this `f2=0` have max_suc:"max f1 f2 = Suc k" by simp
         from this have 3:"exec (c1;; c2) s1 (max f1 f2) = exec (c1;; c2) s1 (Suc k)" 
         and 4:"exec c2 s2 (max f1 f2) = exec c2 s2 (Suc k)" by auto 
         from exec_c1_max max_suc have 5: "exec c1 s1 (Suc k) =  Some(False,s2)" by auto
         from exec_c2_max max_suc have 6: "exec c2 s2 (Suc k) = Some (b', s3)" by auto
         from  5 6 have "exec (c1;;c2) s1 (Suc k) = exec c2 s2 (Suc k)" by simp
         from this max_suc show ?thesis by auto
      next
        assume "f2\<noteq>0"
        from `f1\<noteq>0` obtain k1 where f1_suc:"f1 = Suc k1" using gr0_implies_Suc neq0_conv by blast
        from `f2\<noteq>0` obtain k2 where f2_suc:"f2 = Suc k2" using gr0_implies_Suc neq0_conv by blast
        show ?thesis 
        proof (cases "k1 > k2")
            assume "k1 > k2"
            from f1_suc f2_suc this have max_suc: "max f1 f2 = Suc k1" by simp
            from this have 3:"exec (c1;; c2) s1 (max f1 f2) = exec (c1;; c2) s1 (Suc k1)" 
            and 4:"exec c2 s2 (max f1 f2) = exec c2 s2 (Suc k1)" by auto 
            from exec_c1_max max_suc have 5: "exec c1 s1 (Suc k1) =  Some(False,s2)" by auto
            from exec_c2_max max_suc have 6: "exec c2 s2 (Suc k1) = Some (b', s3)" by auto
            from  5 6 have "exec (c1;;c2) s1 (Suc k1) = exec c2 s2 (Suc k1)" by simp
            from this max_suc show ?thesis by auto
        next 
            assume "\<not>(k1 > k2)"
            from f1_suc f2_suc this have max_suc: "max f1 f2 = Suc k2" by simp
            from this have 3:"exec (c1;; c2) s1 (max f1 f2) = exec (c1;; c2) s1 (Suc k2)" 
            and 4:"exec c2 s2 (max f1 f2) = exec c2 s2 (Suc k2)" by auto 
            from exec_c1_max max_suc have 5: "exec c1 s1 (Suc k2) =  Some(False,s2)" by auto
            from exec_c2_max max_suc have 6: "exec c2 s2 (Suc k2) = Some (b', s3)" by auto
            from  5 6 have "exec (c1;;c2) s1 (Suc k2) = exec c2 s2 (Suc k2)" by simp
            from this max_suc show ?thesis by auto
        qed
      qed  

    qed
  from exec_c2_max exec_seq have "exec (c1;;c2) s1 (max f1 f2) = Some( b',s3)" by simp
  thus ?case by blast
  qed
 
         
text \<open>Finally, prove the main theorem of the homework:\<close>
theorem exec_equiv_bigstep: "(\<exists>k. exec c s k = Some (cont, s')) \<longleftrightarrow> (c,s) \<Rightarrow> (cont, s')"
using exec_imp_bigstep bigstep_imp_si by fast

(*<*)
end
(*>*)