theory MahmoudshabanabdelazimAli10 imports   "~~/src/HOL/IMP/Sec_Type_Expr" "~~/src/HOL/IMP/Small_Step"
begin

subsection "Syntax Directed Typing"

inductive sec_type :: "nat \<Rightarrow> com \<Rightarrow> bool" ("(_/ \<turnstile> _)" [0,0] 50) where
Skip:
  "l \<turnstile> SKIP" |
Assign:
  "\<lbrakk> sec x \<ge> sec a;  sec x \<ge> l \<rbrakk> \<Longrightarrow> l \<turnstile> x ::= a" |
Seq:
  "\<lbrakk> l \<turnstile> c\<^sub>1;  l \<turnstile> c\<^sub>2 \<rbrakk> \<Longrightarrow> l \<turnstile> c\<^sub>1;;c\<^sub>2" |
If:
  "\<lbrakk> max (sec b) l \<turnstile> c\<^sub>1;  max (sec b) l \<turnstile> c\<^sub>2 \<rbrakk> \<Longrightarrow> l \<turnstile> IF b THEN c\<^sub>1 ELSE c\<^sub>2" |
While:
  "max (sec b) l \<turnstile> c \<Longrightarrow> l \<turnstile> WHILE b DO c"

code_pred (expected_modes: i => i => bool) sec_type .

value "0 \<turnstile> IF Less (V ''x1'') (V ''x'') THEN ''x1'' ::= N 0 ELSE SKIP"
value "1 \<turnstile> IF Less (V ''x1'') (V ''x'') THEN ''x''  ::= N 0 ELSE SKIP"
value "2 \<turnstile> IF Less (V ''x1'') (V ''x'') THEN ''x1'' ::= N 0 ELSE SKIP"

inductive_cases if_sec_tyE[elim!]:"l \<turnstile> IF b THEN c\<^sub>1 ELSE c\<^sub>2" 
inductive_cases w_sec_tyE[elim!]:"l \<turnstile> WHILE b DO c"
inductive_cases assign_sec_tyE[elim!]: "l \<turnstile> x ::= a"  
inductive_cases seq_sec_tyE[elim!]: "l \<turnstile> c\<^sub>1;;c\<^sub>2"   


text{* An important property: anti-monotonicity. *}

lemma anti_mono: "\<lbrakk> l \<turnstile> c;  l' \<le> l \<rbrakk> \<Longrightarrow> l' \<turnstile> c"
apply(induction arbitrary: l' rule: sec_type.induct)
apply (metis sec_type.intros(1))
apply (metis le_trans sec_type.intros(2))
apply (metis sec_type.intros(3))
apply (metis If le_refl sup_mono sup_nat_def)
apply (metis While le_refl sup_mono sup_nat_def)
done

lemma w_sec_type:" l \<turnstile> WHILE b DO c \<Longrightarrow> l \<turnstile> IF b THEN c;; WHILE b DO c ELSE SKIP"
by(induction "WHILE b DO c" rule:sec_type.induct)(simp add: If sec_type.Seq sec_type.Skip sec_type.While)

lemma preservation:"(c,s) \<rightarrow> (c',s') \<Longrightarrow> l \<turnstile> c \<Longrightarrow> l \<turnstile> c'"
proof(induction rule: small_step_induct)
  case Assign thus ?case by (simp add: sec_type.Skip)
next
  case Seq1 thus ?case by blast
next
  case Seq2 thus ?case using sec_type.Seq by auto
next
  case (While b c) 
  have "IF b THEN c;; WHILE b DO c ELSE SKIP \<sim> While b c" by auto
  thus ?case using  w_sec_type by (simp add: While.prems) 
next
  case IfFalse thus ?case using anti_mono if_sec_tyE max.cobounded2 by blast
next
  case IfTrue thus ?case using anti_mono if_sec_tyE max.cobounded2 by blast

qed
lemma state_trans:"s = s' (< l) \<Longrightarrow>  s' = s'' (< l) \<Longrightarrow> s = s'' (< l)"
by simp

lemma confinement_one_step: "\<lbrakk> (c,s) \<rightarrow> (c',s');  l \<turnstile> c \<rbrakk> \<Longrightarrow> s = s' (< l)"
by(induction rule: small_step_induct) auto

lemma confinement_steps: "\<lbrakk> (c,s) \<rightarrow>* (c',s');  l \<turnstile> c \<rbrakk> \<Longrightarrow> s = s' (< l)"
proof(induction rule: star_induct)
  case refl thus ?case by simp
next
  case (step c s c'' s'' c' s') thus ?case by (simp add: confinement_one_step preservation)
qed

theorem noninterference:
  "\<lbrakk> (c,s) \<rightarrow> (c',s'); (c,t) \<rightarrow> (c',t');  0 \<turnstile> c;  s = t (\<le> l) \<rbrakk>
   \<Longrightarrow> s' = t' (\<le> l)"
proof(induction arbitrary: t t' rule: small_step_induct)
  case (Assign x a s)
  have [simp]: "t' = t(x := aval a t)" using Assign by auto
  have "sec x >= sec a" using `0 \<turnstile> x ::= a` by auto
  show ?case
  proof auto
    assume "sec x \<le> l"
    with `sec x >= sec a` have "sec a \<le> l" by arith
    thus "aval a s = aval a t"
      by (rule aval_eq_if_eq_le[OF `s = t (\<le> l)`])
  next
    fix y assume "y \<noteq> x" "sec y \<le> l"
    thus "s y = t y" using `s = t (\<le> l)` by simp
  qed
next
  case Seq1 thus ?case by blast
next
  case Seq2 thus ?case by blast
next
  case (IfTrue b s c\<^sub>1 c\<^sub>2 t t')
  have "sec b \<turnstile> c\<^sub>1" "sec b \<turnstile> c\<^sub>2" using `0 \<turnstile> IF b THEN c\<^sub>1 ELSE c\<^sub>2` by auto
  show ?case
  proof cases
    assume "sec b \<le> l"
    hence "s = t (\<le> sec b)" using `s = t (\<le> l)` by auto
    hence "bval b t" using `bval b s` by(simp add: bval_eq_if_eq_le)
    with IfTrue IfTrue.prems(1,3) `sec b \<turnstile> c\<^sub>1`  anti_mono
    show ?thesis by auto
  next
    assume "\<not> sec b \<le> l"
    have 1: "sec b \<turnstile> IF b THEN c\<^sub>1 ELSE c\<^sub>2"
      by(rule sec_type.intros)(simp_all add: `sec b \<turnstile> c\<^sub>1` `sec b \<turnstile> c\<^sub>2`)
    thus ?case using IfTrue.prems(1) IfTrue.prems(3) by blast
  qed
next
  case (IfFalse b s c\<^sub>1 c\<^sub>2 t t')
  have "sec b \<turnstile> c\<^sub>1" "sec b \<turnstile> c\<^sub>2" using `0 \<turnstile> IF b THEN c\<^sub>1 ELSE c\<^sub>2` by auto
  show ?case
  proof cases
    assume "sec b \<le> l"
    hence "s = t (\<le> sec b)" using `s = t (\<le> l)` by auto
    hence "\<not> bval b t" using `\<not> bval b s` by(simp add: bval_eq_if_eq_le)
    with IfFalse IfFalse.prems(1,3) `sec b \<turnstile> c\<^sub>1` anti_mono
    show ?thesis by auto
  next
    assume "\<not> sec b \<le> l"
    have 1: "sec b \<turnstile> IF b THEN c\<^sub>1 ELSE c\<^sub>2"
      by(rule sec_type.intros)(simp_all add: `sec b \<turnstile> c\<^sub>1` `sec b \<turnstile> c\<^sub>2`)
    thus ?case using IfFalse.prems(1) IfFalse.prems(3) by blast
  qed
next
  case (While b c s t t') thus ?case by auto
qed

(*noninterference' it doesn't hold since it is not guaranted that for the intermediate steps 
the noninterference property hold
 *)
theorem noninterference':
  "\<lbrakk> (c,s) \<rightarrow>* (c',s'); (c,t) \<rightarrow>* (c',t');  0 \<turnstile> c;  s = t (\<le> l) \<rbrakk>
   \<Longrightarrow> s' = t' (\<le> l)"
apply(induction arbitrary: s s' t t' l)
apply (metis Small_Step.SkipE prod.inject star.simps)
apply (smt Small_Step.SkipE deterministic noninterference prod.inject small_step.intros(1) star.simps)
sorry


subsection "The Standard Typing System"

text{* The predicate @{prop"l \<turnstile> c"} is nicely intuitive and executable. The
standard formulation, however, is slightly different, replacing the maximum
computation by an antimonotonicity rule. We introduce the standard system now
and show the equivalence with our formulation. *}

inductive sec_type' :: "nat \<Rightarrow> com \<Rightarrow> bool" ("(_/ \<turnstile>'' _)" [0,0] 50) where
Skip':
  "l \<turnstile>' SKIP" |
Assign':
  "\<lbrakk> sec x \<ge> sec a; sec x \<ge> l \<rbrakk> \<Longrightarrow> l \<turnstile>' x ::= a" |
Seq':
  "\<lbrakk> l \<turnstile>' c\<^sub>1;  l \<turnstile>' c\<^sub>2 \<rbrakk> \<Longrightarrow> l \<turnstile>' c\<^sub>1;;c\<^sub>2" |
If':
  "\<lbrakk> sec b \<le> l;  l \<turnstile>' c\<^sub>1;  l \<turnstile>' c\<^sub>2 \<rbrakk> \<Longrightarrow> l \<turnstile>' IF b THEN c\<^sub>1 ELSE c\<^sub>2" |
While':
  "\<lbrakk> sec b \<le> l;  l \<turnstile>' c \<rbrakk> \<Longrightarrow> l \<turnstile>' WHILE b DO c" |
anti_mono':
  "\<lbrakk> l \<turnstile>' c;  l' \<le> l \<rbrakk> \<Longrightarrow> l' \<turnstile>' c"

lemma sec_type_sec_type': "l \<turnstile> c \<Longrightarrow> l \<turnstile>' c"
apply(induction rule: sec_type.induct)
apply (metis Skip')
apply (metis Assign')
apply (metis Seq')
apply (metis max.commute max.absorb_iff2 nat_le_linear If' anti_mono')
by (metis less_or_eq_imp_le max.absorb1 max.absorb2 nat_le_linear While' anti_mono')


lemma sec_type'_sec_type: "l \<turnstile>' c \<Longrightarrow> l \<turnstile> c"
apply(induction rule: sec_type'.induct)
apply (metis Skip)
apply (metis Assign)
apply (metis Seq)
apply (metis max.absorb2 If)
apply (metis max.absorb2 While)
by (metis anti_mono)

subsection "A Bottom-Up Typing System"

inductive sec_type2 :: "com \<Rightarrow> level \<Rightarrow> bool" ("(\<turnstile> _ : _)" [0,0] 50) where
Skip2:
  "\<turnstile> SKIP : l" |
Assign2:
  "sec x \<ge> sec a \<Longrightarrow> \<turnstile> x ::= a : sec x" |
Seq2:
  "\<lbrakk> \<turnstile> c\<^sub>1 : l\<^sub>1;  \<turnstile> c\<^sub>2 : l\<^sub>2 \<rbrakk> \<Longrightarrow> \<turnstile> c\<^sub>1;;c\<^sub>2 : min l\<^sub>1 l\<^sub>2 " |
If2:
  "\<lbrakk> sec b \<le> min l\<^sub>1 l\<^sub>2;  \<turnstile> c\<^sub>1 : l\<^sub>1;  \<turnstile> c\<^sub>2 : l\<^sub>2 \<rbrakk>
  \<Longrightarrow> \<turnstile> IF b THEN c\<^sub>1 ELSE c\<^sub>2 : min l\<^sub>1 l\<^sub>2" |
While2:
  "\<lbrakk> sec b \<le> l;  \<turnstile> c : l \<rbrakk> \<Longrightarrow> \<turnstile> WHILE b DO c : l"


lemma sec_type2_sec_type': "\<turnstile> c : l \<Longrightarrow> l \<turnstile>' c"
apply(induction rule: sec_type2.induct)
apply (metis Skip')
apply (metis Assign' eq_imp_le)
apply (metis Seq' anti_mono' min.cobounded1 min.cobounded2)
apply (metis If' anti_mono' min.absorb2 min.absorb_iff1 nat_le_linear)
by (metis While')

lemma sec_type'_sec_type2: "l \<turnstile>' c \<Longrightarrow> \<exists> l' \<ge> l. \<turnstile> c : l'"
apply(induction rule: sec_type'.induct)
apply (metis Skip2 le_refl)
apply (metis Assign2)
apply (metis Seq2 min.boundedI)
apply (metis If2 inf_greatest inf_nat_def le_trans)
apply (metis While2 le_trans)
by (metis le_trans)

end
