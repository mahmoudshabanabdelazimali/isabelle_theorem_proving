theory LC1
imports Main "~~/src/HOL/IMP/Star" 
begin

(*Lambda Calculus*)
type_synonym vname = string

(*Lambda expressions*)
datatype   Lexp = V vname |
           App Lexp Lexp ("_ \<leftarrow> _") | 
           Abs vname Lexp ("\<lambda>_._")

(*Computing the set of free variable in a lambda expression*)
fun FV :: "Lexp \<Rightarrow> vname set" where
"FV (V y)  = {y}"|
"FV (App M N) =  (FV M) Un (FV N)" |
"FV (Abs x M) = (FV M) - {x}"

(*substituting N for the free occurences of x in M,
 notation M[x :== N], is defined as follows.*)
fun subst :: " Lexp \<Rightarrow> vname \<Rightarrow> Lexp \<Rightarrow> Lexp" ("(_ [_ :== _])" [0,0] 50) where 
"(V y)[x:== N]  = (if x = y then N else (V y))" |
"(App M\<^sub>1 M\<^sub>2)[x:== N] = App (M\<^sub>1[x:== N])(M\<^sub>2[x:==N])"|
"(Abs y M)[x:==N] = (if x \<noteq> y \<and> y \<notin> (FV N) then
                      Abs y (M [x:== N])    else 
                       (Abs y M)
                      )"
lemma [simp]:"x = y \<Longrightarrow> (Abs y M)[x:==N] = (Abs y M)"
by simp

lemma [simp]:" y \<in> (FV N) \<Longrightarrow> (Abs y M)[x:==N] = (Abs y M)"
by simp

(*Lambda calculus evaluation - full \<beta> reduction*)
inductive small_step :: "Lexp \<Rightarrow> Lexp \<Rightarrow> bool"  (infix "\<rightarrow>" 55)
where
app1:"e\<^sub>1 \<rightarrow> e'\<^sub>1 \<Longrightarrow> (App e\<^sub>1 e\<^sub>2) \<rightarrow> (App e'\<^sub>1 e\<^sub>2) " |
app2:"e\<^sub>2 \<rightarrow> e'\<^sub>2 \<Longrightarrow> (App e\<^sub>1 e\<^sub>2) \<rightarrow> (App e\<^sub>1 e'\<^sub>2) " |
Abs:"e \<rightarrow> e' \<Longrightarrow> (Abs x e)  \<rightarrow> (Abs x e')" |
beta: "(App (Abs x e) e') \<rightarrow> (e[x:== e'])"

abbreviation
  small_steps :: "Lexp \<Rightarrow> Lexp \<Rightarrow> bool" (infix "\<rightarrow>*" 55)
where "x \<rightarrow>* y == star small_step x y"

code_pred small_step .

values "{(c') |c'. (((\<lambda> ''x''.(V ''x'')) \<leftarrow> (\<lambda> ''z''. (V ''z''))) \<leftarrow> (V ''y'')) \<rightarrow>* c'  }"
values "{(c') |c'. ( (\<lambda> ''x''. (\<lambda> ''y''. (V ''x'')) )  \<leftarrow> (V ''y'')) \<rightarrow>* c'  }"

(*‘logical’ axioms and rules. for lambda terms*)
(*Equality *)
lemma[simp]: "\<And> M::Lexp. M = M " by simp

lemma[simp]:
fixes N::Lexp and  M::Lexp
assumes "M=N"
shows "N=M" 
using assms by simp

lemma[simp]:
fixes N::Lexp and  M::Lexp and L::Lexp
assumes "M=N" and "N=L"
shows "N=L" 
using assms by simp

lemma[simp]: 
fixes M::Lexp and  M'::Lexp
assumes "M = M'"
shows "(App' M M') = (App' M' M )" using assms by force

(*Compatibility rules*)
lemma[simp]: 
fixes M::Lexp and  M'::Lexp and Z::Lexp
assumes "M = M'"
shows "App Z M = App Z M" using assms by force

lemma[simp]: 
fixes M::Lexp and  M'::Lexp and Z::Lexp
assumes "M = M'"
shows "App M Z = App M' Z" using assms by force 

lemma[simp]: 
fixes M::Lexp and  M'::Lexp
assumes "M = M'"
shows "Abs x M = Abs x M'" using assms by force

(*Standard Combinators*)
abbreviation  I ::  "Lexp" where
"I \<equiv> (\<lambda> ''x'' . (V ''x'')) "

values "{(c') | c'. ( I \<leftarrow> I) \<rightarrow>* c' }"

abbreviation  K ::  "Lexp" where
"K \<equiv> \<lambda> ''x''. (\<lambda> ''y'' . (V ''x'')) "

values "{(c') | c'. ((K \<leftarrow>(V ''1'')) \<leftarrow> (V ''2'')) \<rightarrow>* c' }"

abbreviation  K' ::  "Lexp" where
"K' \<equiv> \<lambda> ''x''. (\<lambda> ''y'' . (V ''y'')) "
values "{(c') | c'. ((K' \<leftarrow>(V ''1'')) \<leftarrow> (V ''2'')) \<rightarrow>* c' }"

abbreviation  S ::  "Lexp" where
"S \<equiv> \<lambda> ''x''. \<lambda> ''y'' . \<lambda> ''z''. ((V ''x''\<leftarrow> V ''z'') \<leftarrow> (V ''y'' \<leftarrow> V ''z'') )"
values "{(c') | c'. (((S \<leftarrow> K) \<leftarrow> (I))  \<leftarrow> K') \<rightarrow>* c' }"


declare [[coercion_enabled]] 
declare [[coercion "int :: nat \<Rightarrow> int"]]

(* We will want to access the ith element of a list,  where i is an int.*)
fun inth :: "'a list \<Rightarrow> int \<Rightarrow> 'a" (infixl "!!" 100) where
"(x # xs) !! i = (if i = 0 then x else xs !! (i - 1))"

(*The only additional lemma we need about this function is indexing over append: *)
lemma inth_append [simp]:
  "0 \<le> i \<Longrightarrow>
  (xs @ ys) !! i = (if i < size xs then xs !! i else ys !! (i - size xs))"
by (induction xs arbitrary: i) (auto simp: algebra_simps)

(* We hide coercion int applied to length  *)
abbreviation (output)
  "isize xs == int (length xs)"

notation isize ("size")

(*The system \<lambda>\<rightarrow>-Curry: a typed lambda calculus *)
datatype ty = int | bool | Vt vname |Arrow ty ty  ("_ \<Rightarrow> _")

type_synonym tyenv = "vname \<Rightarrow> ty"

inductive typing :: "tyenv \<Rightarrow> Lexp \<Rightarrow> ty \<Rightarrow> bool"
  ("(1_/ \<turnstile>/ (_ :/ _))" [50,0,50] 50)
where
vt:"\<Gamma> \<turnstile> V x : (\<Gamma> x) "|
appt:"\<Gamma> \<turnstile> M: (\<sigma> \<Rightarrow> \<tau>) \<Longrightarrow> \<Gamma> \<turnstile> N: \<sigma> \<Longrightarrow>  \<Gamma> \<turnstile> M \<leftarrow> N : \<tau>" |
abst:"\<Gamma>(x:=\<sigma>) \<turnstile> M :\<tau> \<Longrightarrow> \<Gamma> \<turnstile> \<lambda>x. M : \<sigma> \<Rightarrow> \<tau>"


lemma VtyE:"\<Gamma> \<turnstile> (V x): \<sigma> \<Longrightarrow>  (\<Gamma> x) = \<sigma> "
by (metis Lexp.distinct(1) Lexp.distinct(3) Lexp.inject(1) typing.simps)

lemma apptyE: "\<Gamma> \<turnstile> M \<leftarrow> N :\<tau> \<Longrightarrow> EX \<sigma>. \<Gamma> \<turnstile> M: (\<sigma> \<Rightarrow> \<tau>) \<and> \<Gamma> \<turnstile> N: \<sigma>"
by (metis Lexp.distinct(1) Lexp.distinct(5) Lexp.inject(2) typing.simps)

lemma abstyE': "(\<Gamma> \<turnstile> (\<lambda> x. M) : \<rho>) \<Longrightarrow> (EX \<sigma> \<tau>. (\<Gamma>(x:=\<sigma>) \<turnstile> M :\<tau> ) \<and> (\<rho> = \<sigma> \<Rightarrow> \<tau>))"
proof(induction M)
  case V thus ?case by (metis Lexp.distinct(3) Lexp.distinct(5) Lexp.inject(3) typing.cases)
next
  case App thus ?case by (metis Lexp.distinct(3) Lexp.distinct(5) Lexp.inject(3) typing.cases)
next
 case Abs thus ?case by (metis Lexp.distinct(3) Lexp.distinct(5) Lexp.inject(3) typing.cases)
qed

fun subst_ty :: "ty \<Rightarrow> ty \<Rightarrow> ty \<Rightarrow> ty" ("(_ [_ :=' _])" [0,0] 50) where
"int [a :=' b] = int" |
"bool [a :=' b] = bool" |
"(Vt x) [a :=' b] = ( if a = (Vt x)  then b else (Vt x))"|
"(\<sigma> \<Rightarrow> \<beta>) [a :=' b] = (case a of (Vt x) \<Rightarrow> ((\<sigma>[a :=' b]) \<Rightarrow> (\<beta>[a :=' b])) |
                       _\<Rightarrow> (\<sigma> \<Rightarrow> \<beta>) )" 

value "((Vt x) \<Rightarrow> (Vt x)) [(Vt x) :=' int]"
value "(int \<Rightarrow> bool) [(Vt x) :=' int]"
value "( ((Vt x) \<Rightarrow> (Vt x) )  \<Rightarrow> bool) [(Vt x) :=' int]"

lemma a'[simp]: "\<Gamma>(x := \<rho>, y := \<sigma>') = (\<Gamma>(x := \<rho>) )(y := \<sigma>')" by simp

lemma b'[simp]: "\<Gamma>(x := \<rho>, x := \<sigma>') = \<Gamma>(x := \<sigma>')" by simp

schematic_goal k_ty:" \<Gamma>  \<turnstile> \<lambda> ''x''. (\<lambda> ''y'' . (V ''x'')): ?t"
apply rule 
apply rule
apply rule
done 

schematic_goal k'_ty:"\<Gamma> \<turnstile> K': ?t"
apply rule 
apply rule
apply rule
done

lemma " \<Gamma> \<turnstile> I :  ty.int \<Rightarrow> ty.int"
using VtyE by (metis fun_upd_same typing.intros(1) typing.intros(3))

lemma " (\<Gamma> \<turnstile> I :  ty.bool) \<Longrightarrow> False"
using typing.simps by blast


lemma " \<Gamma> \<turnstile> I :  \<sigma> \<Rightarrow> \<sigma>"
using VtyE by (metis fun_upd_same typing.intros(1) typing.intros(3))

lemma "\<Gamma>(''y'':=\<sigma>) \<turnstile> (I \<leftarrow> (V ''y'')) : \<sigma> "
by (metis fun_upd_same typing.simps)

values "{(c') | c'. (( (\<lambda> ''f''. (\<lambda> ''x''. ((V ''f'') \<leftarrow> ((V ''f'') \<leftarrow> (V ''x'')) ) )) \<leftarrow> I) \<leftarrow> (V ''z''))  \<rightarrow>* c' }"

lemma app_reduc1:"M1 \<rightarrow>* M' \<Longrightarrow> (M1 \<leftarrow> M2) \<rightarrow>* (M' \<leftarrow> M2)  "
proof(induction rule: star.induct)
  case (refl x) thus ?case by(rule refl)
next
 case (step x y z) thus ?case by (metis app1 star.step)
qed

lemma app_reduc2:"M2 \<rightarrow>* M'' \<Longrightarrow> (M1 \<leftarrow> M2) \<rightarrow>* (M1 \<leftarrow> M'') "
proof(induction rule: star.induct)
  case (refl x) thus ?case by(rule refl)
next
 case (step x y z) thus ?case by (metis app2 star.step)
qed

lemma app_reduc:"M1 \<rightarrow>* M' \<Longrightarrow> M2 \<rightarrow>* M'' \<Longrightarrow> (M1 \<leftarrow> M2) \<rightarrow>* (M' \<leftarrow> M'') "
using app_reduc1 app_reduc2 by (meson star_trans)

lemma abs_reduc:"M \<rightarrow>* M' \<Longrightarrow> (Abs x M) \<rightarrow>* (Abs x M')"
proof(induction rule: star.induct)
  case (refl x) thus ?case by(rule refl)
next
 case (step x y z) thus ?case by (metis  Abs star.step)
qed


end