theory LC2
imports Main "~~/src/HOL/IMP/Star"
begin

(*Lambda Calculus with  de Bruijn Notation*)
type_synonym vname = string


(* de Bruijn Notation*)
type_synonym n = int
datatype Lexp' = V' n | Abs' Lexp' ("\<lambda>. _")|App' Lexp' Lexp' ("_ \<leftarrow> _")

(*We define FI(M), the set of free indices of a lambda term M *)
fun FI ::"Lexp' \<Rightarrow> n set" where
"FI (V' n) = {n}" |
"FI (\<lambda>. M) = {n-1 | n. n \<in> (FI M) \<and> n > 1}"|
"FI (M \<leftarrow> N) = (FI M) \<union> (FI N)"


(*an auxiliary function that shifts
 the indices of free variables above a cutoff c up by i:*)
fun shift :: "n \<Rightarrow> n \<Rightarrow> Lexp' \<Rightarrow> Lexp'" where 
"shift c i (V' n) = (if n < c then (V' n) else V' (n+i) )"|
"shift c i (Abs' e) = Abs' (shift (c+1) i e)"|
"shift c i (App' M N) = App' (shift c i M) (shift c i N)"

fun subst' :: " Lexp' \<Rightarrow> n \<Rightarrow> Lexp' \<Rightarrow> Lexp'" ("(_ [_ :==' _])" [0,0] 50) where 
" (V' n)[ m :==' e] = (if n = m then e else (V' n))" |
" (Abs' e\<^sub>1) [m :==' e] =  Abs' (e\<^sub>1[m+1 :==' (shift 0 1 e)])"|
" (App' e\<^sub>1 e\<^sub>2) [m :==' e] = App' (e\<^sub>1 [m :==' e]) (e\<^sub>2 [m :==' e])"

(*Lambda calculus evaluation in de Bruijn Notation  - full \<beta> reduction*)
inductive small_step' :: "Lexp' \<Rightarrow> Lexp' \<Rightarrow> bool"  (infix "\<rightarrow>:" 55)
where
"e\<^sub>1 \<rightarrow>: e'\<^sub>1 \<Longrightarrow> (App' e\<^sub>1 e\<^sub>2) \<rightarrow>: (App' e'\<^sub>1 e\<^sub>2) " |
"e\<^sub>2 \<rightarrow>: e'\<^sub>2 \<Longrightarrow> (App' e\<^sub>1 e\<^sub>2) \<rightarrow>: (App' e\<^sub>1 e'\<^sub>2) " |
"e \<rightarrow>: e' \<Longrightarrow> (Abs' e)  \<rightarrow>: (Abs' e')" |
beta': "(App' (Abs' e\<^sub>1) e\<^sub>2 ) \<rightarrow>: (shift 0 (-1) (e\<^sub>1[0:==' (shift 0 1 e\<^sub>2)]) )"



abbreviation
  small_steps' :: "Lexp' \<Rightarrow> Lexp' \<Rightarrow> bool" (infix "\<rightarrow>:*" 55)
where "x \<rightarrow>:* y == star small_step' x y"

code_pred small_step' .

values "{(c') |c'. (App' (Abs' (Abs' (V' 1)) ) (V' 0)) \<rightarrow>:* c' }"
values "{(c') |c'. (App'  (Abs' (V' 2) ) (V' 3)) \<rightarrow>:* c'}"
values "{(c') |c'. App' (Abs' (Abs' (V' 1))) (App' (V' 0)(V' 1)) \<rightarrow>:* c'}"
values "{(c') |c'. App' (Abs' (Abs' (V' 1))) (Abs' (V' 0)) \<rightarrow>:* c'}"
values "{(c') |c'. ((\<lambda>. \<lambda>. (V' 1)) \<leftarrow> (V' 0))   \<rightarrow>:* c'}"
(*‘logical’ axioms and rules. for lambda terms*)
(*Equality *)
lemma[simp]: "\<And> M::Lexp'. M = M " by simp

lemma[simp]:
fixes N::Lexp' and  M::Lexp' 
assumes "M=N"
shows "N=M" 
using assms by simp

lemma[simp]:
fixes N::Lexp' and  M::Lexp' and L::Lexp'
assumes "M=N" and "N=L"
shows "N=L" 
using assms by simp

lemma[simp]: 
fixes M::Lexp' and  M'::Lexp' 
assumes "M = M'"
shows "(App' M M') = (App' M' M )" using assms by force

(*Compatibility rules*)
lemma[simp]: 
fixes M::Lexp' and  M'::Lexp' and Z::Lexp'
assumes "M = M'"
shows "App' Z M = App' Z M'" using assms by force

lemma[simp]: 
fixes M::Lexp' and  M'::Lexp' and Z::Lexp'
assumes "M = M'"
shows "App' M Z = App' M' Z" using assms by force 

lemma[simp]: 
fixes M::Lexp' and  M'::Lexp' 
assumes "M = M'"
shows "Abs' M = Abs' M'" using assms by force

(*Standard Combinators*)
abbreviation  I ::  "Lexp'" where
"I \<equiv> Abs' (V' 0) "
values "{(c') | c'. (App' I (V' 3)) \<rightarrow>:* c' }"

abbreviation  K ::  "Lexp'" where
"K \<equiv> Abs' (Abs' (V' 1)) "
values "{(c') | c'. (App' K (V' 2)) \<rightarrow>:* c' }"

abbreviation  K' ::  "Lexp'" where
"K' \<equiv> Abs' (Abs' (V' 0))"
values "{(c') | c'. (App' K' (V' 3)) \<rightarrow>:* c' }"

abbreviation  S ::  "Lexp'" where
"S \<equiv> Abs' (Abs' (Abs' (App' (App'(V' 2)(V' 0 )) (App' (V' 1)(V' 0))) ) ) "


definition "NF e \<longleftrightarrow> \<not>(EX e'. e \<rightarrow>: e')"

declare [[coercion_enabled]] 
declare [[coercion "int :: nat \<Rightarrow> int"]]

(* We will want to access the ith element of a list,  where i is an int.*)
fun inth :: "'a list \<Rightarrow> int \<Rightarrow> 'a" (infixl "!!" 100) where
"(x # xs) !! i = (if i = 0 then x else xs !! (i - 1))"

(*The only additional lemma we need about this function is indexing over append: *)
lemma inth_append [simp]:
  "0 \<le> i \<Longrightarrow>
  (xs @ ys) !! i = (if i < size xs then xs !! i else ys !! (i - size xs))"
by (induction xs arbitrary: i) (auto simp: algebra_simps)

(* We hide coercion int applied to length  *)
abbreviation (output)
  "isize xs == int (length xs)"

notation isize ("size")

(*The system \<lambda>\<rightarrow>-Curry: a typed lambda calculus *)
datatype ty = int | bool |Vt vname|
           Arrow ty ty  ("_ \<Rightarrow> _")

type_synonym tyenv = "ty list"

inductive typing :: "tyenv \<Rightarrow> Lexp' \<Rightarrow> ty \<Rightarrow> bool"
  ("(1_/ \<turnstile>/ (_ :/ _))" [50,0,50] 50)
where
"\<Gamma> \<turnstile> (V' n) : (\<Gamma> !! n) "|
"\<Gamma> \<turnstile> M: (\<sigma> \<Rightarrow> \<tau>) \<Longrightarrow> \<Gamma> \<turnstile> N: \<sigma> \<Longrightarrow>  \<Gamma> \<turnstile> M \<leftarrow> N : \<tau>" |
"\<sigma>#\<Gamma> \<turnstile> M :\<tau> \<Longrightarrow> \<Gamma> \<turnstile> \<lambda>. M : (\<sigma> \<Rightarrow> \<tau>) "

lemma VtyE:"\<Gamma> \<turnstile> (V' n): \<sigma> \<Longrightarrow>  (\<Gamma> !! n) = \<sigma> "
proof (induction \<sigma>)
case int
  hence " ty.int = \<Gamma> !! n " using Lexp'.distinct(3) Lexp'.inject(1) ty.distinct(3) typing.cases by blast
  thus ?case by simp
next
case bool
  hence " ty.bool = \<Gamma> !! n " using Lexp'.distinct(3) Lexp'.inject(1) ty.distinct(3) typing.cases by blast
  thus ?case by simp
next
case (Arrow \<sigma>1 \<sigma>2)
thus ?case 
by (metis Lexp'.distinct(1) Lexp'.distinct(3) Lexp'.inject(1) typing.cases)
next
case Vt thus ?case
by (metis Lexp'.distinct(1) Lexp'.distinct(3) Lexp'.inject(1) typing.cases)
qed

lemma apptyE: "\<Gamma> \<turnstile> M \<leftarrow> N :\<tau> \<Longrightarrow> EX \<sigma>. \<Gamma> \<turnstile> M: (\<sigma> \<Rightarrow> \<tau>) \<and> \<Gamma> \<turnstile> N: \<sigma>"
proof(induction \<tau>)
  case Vt thus ?case by (metis Lexp'.distinct(3) Lexp'.distinct(5) Lexp'.inject(3) typing.cases)
next
  case int thus ?case
  by (metis Lexp'.distinct(3) Lexp'.inject(3) ty.distinct(5) typing.cases)
next
  case bool thus ?case 
  by (metis (no_types, lifting) Lexp'.distinct(3) Lexp'.inject(3) ty.distinct(9) typing.cases)
next
  case Arrow thus ?case 
  by (metis Lexp'.distinct(3) Lexp'.distinct(5) Lexp'.inject(3) typing.cases)
qed


lemma abstyE: "\<Gamma> \<turnstile> \<lambda>. M : (\<sigma> \<Rightarrow> \<tau>) \<Longrightarrow> \<sigma>#\<Gamma> \<turnstile> M :\<tau> "
proof(induction \<sigma>)
  case int thus ?case 
  by (metis Lexp'.distinct(1) Lexp'.distinct(5) Lexp'.inject(2) ty.inject typing.cases)
next
  case bool thus ?case 
  by (metis Lexp'.distinct(1) Lexp'.distinct(5) Lexp'.inject(2) ty.inject typing.cases)
next
  case Arrow thus ?case 
  by (metis Lexp'.distinct(1) Lexp'.distinct(5) Lexp'.inject(2) ty.inject typing.cases)
next
case Vt thus ?case 
by (metis Lexp'.distinct(1) Lexp'.distinct(5) Lexp'.inject(2) ty.inject(2) typing.cases)
qed


lemma abstyE': "\<Gamma> \<turnstile> \<lambda>. M : \<rho> \<Longrightarrow> (EX \<sigma> \<tau>. (\<sigma>#\<Gamma> \<turnstile> M :\<tau> ) \<and> (\<rho> = \<sigma> \<Rightarrow> \<tau>))"
proof(induction \<rho>)
  case int thus ?case 
  using Lexp'.distinct(1) Lexp'.distinct(5) ty.distinct(5) typing.cases by blast
next
  case bool thus ?case 
  using Lexp'.distinct(1) Lexp'.distinct(5) ty.distinct(9) typing.cases by blast
next
  case Vt thus ?case 
  using Lexp'.distinct(1) Lexp'.distinct(5) ty.distinct(11) typing.cases by blast
next
  case Arrow thus ?case 
  using abstyE by blast 
qed

fun subst_ty :: "ty \<Rightarrow> ty \<Rightarrow> ty \<Rightarrow> ty" ("(_ [_ :==: _])" [0,0] 50) where
"int [\<alpha> :==: \<tau>] = int" |
"bool [\<alpha> :==: \<tau>] = bool" |
"(Vt v) [\<alpha> :==: \<tau>] = (if (Vt v) = \<alpha> then \<tau> else  Vt v)"|
" (\<sigma> \<Rightarrow> \<beta>) [\<alpha> :==: \<tau>] =  ((\<sigma>[\<alpha> :==: \<tau>]) \<Rightarrow> (\<beta>[\<alpha> :==: \<tau>]))" 

fun subst_list :: " ty list \<Rightarrow> ty \<Rightarrow> ty \<Rightarrow> ty list" ("(_ [_ :=' _])" [0,0] 50)where
"subst_list [] _ _ = []"|
" (x # xs) [y :=' z]   =  (x[y:==:z]) #  ( xs [y :=' z])"


schematic_goal k_ty:"[] \<turnstile> \<lambda>. \<lambda>. (V' 1): ?t"
apply rule 
apply rule
apply rule
done 

schematic_goal k'_ty:"[] \<turnstile> K': ?t"
apply rule 
apply rule
apply rule
done

lemma " [] \<turnstile> \<lambda>. (V' 0) :  ty.int \<Rightarrow> ty.int"
using VtyE by (metis inth.simps typing.simps)

lemma " [] \<turnstile> \<lambda>. (V' 0) :  ty.bool \<Longrightarrow> False"
using typing.simps by blast


lemma " [] \<turnstile> \<lambda>. (V' 0) :  \<sigma> \<Rightarrow> \<sigma>"
using VtyE by (metis inth.simps typing.simps)

lemma "[\<sigma>] \<turnstile> (I \<leftarrow> (V' 0)) : \<sigma> "
by (metis inth.simps typing.simps)

values "{(c') | c'. (((\<lambda>. (\<lambda>. ((V' 1) \<leftarrow> ((V' 1) \<leftarrow> (V' 0)) ) )) \<leftarrow> I) \<leftarrow> (V' 2))  \<rightarrow>:* c' }"

end